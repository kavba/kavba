<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class A1AuthController extends Controller
{
	public function __construct()
    {
        $this->middleware('kavba');
    }

    public function test(Request $request) {
        return response()->json($request);
    }
}
