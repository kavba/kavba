<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\KavbaClass;
use App\Models\ClassRelation;
use App\Models\Post;
use App\Models\Comment;
use App\Http\Helpers\dbSetHelpers;

class BoardController extends Controller
{
    protected $myDB = "kavba";
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function createPost(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];

		$title = $request->title;
        $classId = $request->classId;
        $type = $request->type;
        $content = $request->content;
		if(!is_null($title) && !is_null($type)) {
            $today = date("Y-m-d H:i:s");
			$returnData['inserted'] = Post::create([
				'type'		=>	$type,
				'class_id'	=>	$classId,
                'title'		=>	$title,
                'content'   =>  $content,
                'rgst_date' =>  $today,
                'user_id'   =>  $request->user->id
            ]);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function editPost(Request $request, $postId = null) {
	    $returnData = [
            'result' => 'false'
        ];

		$title = $request->title;
        $classId = $request->classId;
        $type = $request->type;
        $content = $request->content;
		if(!is_null($postId) && !is_null($title) && !is_null($type)) {
            $temp = Post::where('id', $postId);
            $post = $temp->first();
            if(!is_null($post) && $request->user->id != $post->user_id) {
                $returnData['error'] = '권한 없음';
                return response()->json($returnData);
            }
			$temp->update([
				'type'		=>	$type,
				'class_id'	=>	$classId,
                'title'		=>	$title,
                'content'   =>  $content,
            ]);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function deletePost(Request $request, $postId = null) {
	    $returnData = [
            'result' => 'false'
        ];
		if(!is_null($postId)) {
            $temp = Post::where('id', $postId);
            $post = $temp->first();
            if(!is_null($post) && $request->user->id != $post->user_id) {
                $returnData['error'] = '권한 없음';
                return response()->json($returnData);
            }
            $temp->delete();
            Comment::where('post_id', $postId)->delete();
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function getAll(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $user = $request->user;
        $qry = $request->qry;
        $sort = ($request->sort=="true" ? "desc" : "asc");
        $classId = $request->classId;
        $type = $request->type;
        $perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
        if(!is_null($user)) {
            $list = Post::leftJoin('users', 'users.id', 'posts.user_id')
                ->leftJoin('comments', 'comments.post_id', 'posts.id')
                ->leftJoin('kavba_classes', 'kavba_classes.id', 'posts.class_id')
                ->when((!is_null($qry)), function($q) use($qry) {
                    return $q->where('posts.title', 'like', '%'.$qry.'%')
                            ->orWhere('users.name', 'like', '%'.$qry.'%');
                })
                ->when((!is_null($classId)), function($q) use($classId) {
                    return $q->where('posts.class_id', intval($classId));
                })
                ->when(!is_null($type), function($q) use($type) {
                    return $q->where('posts.type', intval($type));
                })
                ->whereNull('posts.deleted_at')
                ->whereNull('comments.deleted_at')
                ->selectRaw('posts.*, users.name as writer, count(comments.id) as comments_count, kavba_classes.name as class_name,
                        if(posts.user_id = ?, 1, 0) as auth', [ $user->id ])
                ->groupBy('posts.id')
                ->orderBy('created_at', $sort);

            $tmp = $list->get();
            $returnData['total'] = count($tmp);
            $posts = $list->skip($page * $perPage)
                ->take($perPage)
                ->get();
            foreach($posts as $post) {
                $post['comments'] = Comment::leftJoin('users', 'users.id', 'comments.user_id')
                    ->selectRaw('comments.*, users.name as writer, if(comments.user_id = ?, 1, 0) as auth', [ $user->id ])
                    ->where('post_id', $post->id)
                    ->whereNull('comments.deleted_at')
                    ->orderBy('comments.order', 'asc')
                    ->get();
            }
            $returnData['posts'] = $posts;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function createComment(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];

        $postId = $request->postId;
        $text = $request->text;
        $user = $request->user;
		if(!is_null($user)) {
            $cnt = Comment::where('post_id', $postId)->whereNull('deleted_at')->count();
            $today = date("Y-m-d H:i:s");
			$returnData['comment'] = Comment::create([
				'post_id'	=>	$postId,
				'text'	    =>	$text,
                'rgst_date' =>  $today,
                'user_id'   =>  $user->id,
                'depth'     =>  0,
                'order'     =>  $cnt
            ]);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function editComment(Request $request, $commentId = null) {
	    $returnData = [
            'result' => 'false'
        ];

		$text = $request->text;
		if(!is_null($commentId)) {
            $temp = Comment::where('id', $commentId);
            $comment = $temp->first();
            if(!is_null($comment) && $request->user->id != $comment->user_id) {
                $returnData['error'] = '권한 없음';
                return response()->json($returnData);
            }
			$temp->update([
				'text'		=>	$text,
            ]);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function deleteComment(Request $request, $commentId = null) {
	    $returnData = [
            'result' => 'false'
        ];
		if(!is_null($commentId)) {
            $temp = Comment::where('id', $commentId);
            $comment = $temp->first();
            if(!is_null($comment) && $request->user->id != $comment->user_id) {
                $returnData['error'] = '권한 없음';
                return response()->json($returnData);
            }
            $temp->delete();
            DB::connection($this->myDB)->select('
                update comments set `order` = `order` - 1 where post_id = '.$comment->post_id.' and `order` > '.$comment->order);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }
}
