<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\KavbaClass;
use App\Models\OverClass;
use App\Models\ClassRelation;
use App\Models\LongTermGoal;

class CommonController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getFilters(Request $request) {
        $returnData['classList'] = KavbaClass::get();
        $returnData['overList'] = DB::connection($this->myDB)->table('over_classes')->get();
        $returnData['parentList'] = DB::connection($this->myDB)->table('parents')->get();
        //$returnData['pyramidList'] = DB::table('pyramids')->get();
        $circleParents = DB::connection($this->myDB)->table('circle_parents')->get();
        foreach($circleParents as $parent) {
            $parent->circles = DB::connection($this->myDB)->table('circles')->where('parent_id', $parent->id)->get();
        }
        $returnData['circleList'] = $circleParents;
        return response()->json($returnData);
    }

    public function getUserFilters(Request $request) {
        $returnData['classList'] = KavbaClass::orderBy('name', 'desc')->where('visible', 1)->get();
        $returnData['list'] = KavbaClass::where('id', '!=', 0)->orderBy('name', 'desc')->get();
        $returnData['studentList'] = Student::whereNull('deleted_at')->select('id', 'name', 'birth_date')->orderBy('name', 'asc')->get();
        return response()->json($returnData);
    }

    public function getClassesUsers(Request $request) {
        $returnData['classes'] = KavbaClass::whereNull('deleted_at')
            ->where('visible', 1)
            ->orderBy('id', 'asc')
            ->get();
        $returnData['users'] = User::get();
        $returnData['centers'] = OverClass::whereNull('deleted_at')->get();
        $returnData['domains'] = LongTermGoal::get();
        return response()->json($returnData);
    }

    public function getStudentsUsers(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $selectedStandard = $request->selectedStandard;
        $selectedClass = $request->selectedClass;
        $list = [];
        if(!is_null($selectedStandard) && !is_null($selectedClass)) {
            if($selectedStandard == 1) {
                // 지시자
                $list = ClassRelation::leftJoin('users', 'users.id', 'class_relations.user_id')
                    ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                    ->where('kavba_classes.visible', 1)
                    ->where('class_relations.class_id', $selectedClass)
                    ->where('class_relations.flag', 1)
                    ->selectRaw('users.*, class_relations.id as class_relation_id')
                    ->orderBy('users.name', 'asc')
                    ->get();
            } else {
                // 학생
                if($selectedClass == 0) {
                    $list = ClassRelation::leftJoin('students', 'students.id', 'class_relations.student_id')
                        ->leftJoin('users', 'users.id', 'class_relations.user_id')
                        ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                        ->where('kavba_classes.visible', 1)
                        ->where('class_relations.class_id', $selectedClass)
                        ->where('class_relations.flag', 2)
                        ->whereNull('students.deleted_at')
                        ->selectRaw("students.birth_date, students.phone, students.id,
                            concat(students.name, ' / ', users.name) as name,
                            concat(students.id, '_', users.id) as re_id,
                            class_relations.user_id as relation_user_id,
                            class_relations.id as class_relation_id")
                        ->orderBy('students.name', 'asc')
                        ->get();
                } else {
                    $list = ClassRelation::leftJoin('students', 'students.id', 'class_relations.student_id')
                        ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                        ->where('kavba_classes.visible', 1)
                        ->where('class_relations.class_id', $selectedClass)
                        ->where('class_relations.flag', 2)
                        ->whereNull('students.deleted_at')
                        ->selectRaw('students.*, class_relations.id as class_relation_id')
                        ->orderBy('students.name', 'asc')
                        ->get();
                }
            }
            $returnData['result'] = 'true';
        }
        $returnData['list'] = $list;
        return response()->json($returnData);
    }

    public function getAuthOnClass(Request $request, $classId = null) {
        $returnData = [
            'result' => 'false'
        ];
        $user = $request->user;
        if($user->level > 10) {
            $returnData['result'] = "true";
        } else {
            if(!is_null($classId)) {
                $check = ClassRelation::where('user_id', $request->user->id)
                    ->where('class_id', $classId)
                    ->where('flag', 1)
                    ->count();
                if($check > 0) $returnData['result'] = "true";
            }
        }
        return $returnData;
    }
}
