<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserError;

class ErrorController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getErrors(Request $request, $userId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $sort = ($request->sort=="true" ? "desc" : "asc");
        $perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
        if(!is_null($userId)) {
            $list = UserError::leftJoin('students', 'students.id', 'user_errors.student_id')
                ->where('user_errors.user_id', $userId)
                ->whereNull('user_errors.deleted_at')
                ->selectRaw('user_errors.*, students.name as student_name')
                ->orderBy('user_errors.rgst_date', $sort)
                ->orderBy('user_errors.created_at', $sort);

            $returnData['total'] = $list->count();
            $returnData['errors'] = $list->skip($page * $perPage)
                ->take($perPage)
                ->get();
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function createError(Request $request, $userId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $rgstDate = $request->rgstDate;
        $count = $request->count;
        $count2 = $request->count2;
        $studentId = $request->studentId;
        if(!is_null($rgstDate) && !is_null($count)) {
            $err = UserError::where('user_id', $userId)
                ->where('rgst_date', $rgstDate)
                ->where('student_id', $studentId)
                ->whereNull('deleted_at')
                ->count();
            if($err == 0) {
                $returnData['inserted'] = UserError::create([
                    'user_id'       =>  $userId,
                    'rgst_date'     =>  $rgstDate,
                    'count'         =>  $count,
                    'count2'        =>  $count2,
                    'student_id'    =>  $studentId
                ]);
                $returnData['result'] = 'true';
            } else {
                $returnData['duplicate'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function editError(Request $request, $id = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $errId = $request->id;
        $rgstDate = $request->rgstDate;
        $count = $request->count;
        $count2 = $request->count2;
        $studentId = $request->studentId;
        if(!is_null($errId) && !is_null($rgstDate) && !is_null($count) && !is_null($count2)) {
            $myErr = UserError::where('id', $errId)->first();
            if(!is_null($myErr)) {
                if($rgstDate == $myErr->rgst_date) {
                    // 같은 날짜의 데이터라면 수정
                    $myErr->update([
                        'rgst_date'     =>  $rgstDate,
                        'count'         =>  $count,
                        'count2'        =>  $count2,
                        'student_id'    =>  $studentId
                    ]);
                    $returnData['errorData'] = UserError::where('id', $errId)->first();
                    $returnData['result'] = 'true';
                } else {
                    $userId = $myErr->user_id;
                    $err = UserError::where('user_id', $userId)
                        ->where('rgst_date', $rgstDate)
                        ->whereNull('deleted_at')
                        ->count();
                    if($err == 0) {
                        // 날짜를 수정하면 해당 데이터를 삭제하고 수정된 날짜에 있는지 체크 후 새로 생성
                        $myErr->delete();
                        $returnData['errorData'] = UserError::create([
                            'user_id'       =>  $userId,
                            'rgst_date'     =>  $rgstDate,
                            'count'         =>  $count,
                            'count2'        =>  $count2,
                            'student_id'    =>  $studentId
                        ]);
                        $returnData['result'] = 'true';
                    } else {
                        $returnData['duplicate'] = 'true';
                    }
                }
            }
        }
        return response()->json($returnData);
    }

    public function removeError(Request $request, $id = 0) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            UserError::where('id', $id)->delete();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }
}
