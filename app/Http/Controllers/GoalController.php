<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\LongTermGoal;
use App\Models\MidTermGoal;
use App\Models\ShortTermGoal;
use App\Models\Program;
use App\Models\Student;
use App\Models\StoItem;
use App\Models\StoGroup;
use App\Models\Point;
use App\Models\Stick;
use App\Models\Decision;
use App\Models\ClassRelation;

class GoalController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getLtos(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        $returnData['list'] = LongTermGoal::get();
        $returnData['result'] = 'true';
        // $userId = $request->userId;
        // if(is_null($userId)) {
        //     $returnData['list'] = LongTermGoal::get();
        //     $returnData['result'] = 'true';
        // } else {
        //     $returnData['list'] = ClassRelation::where('flag', 2)
        //         ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
        //         ->where('class_relations.user_id', $userId)
        //         ->groupBy('class_relations.class_id')
        //         ->orderBy('kavba_classes.name', 'asc')
        //         ->get();
        //     $returnData['result'] = 'true';
        // }

        return response()->json($returnData);
    }

    public function getLto(Request $request, $ltoId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($ltoId)) {
            $returnData['lto'] = LongTermGoal::where('id', intval($ltoId))->first();
            $returnData['stos'] = ShortTermGoal::where('lto_id', $ltoId)->where('using', 1)->get();
            if(!is_null($returnData['lto'])) {
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function createLto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $name = $request->name;
        $desc = $request->desc;
        $using = $request->using;
        if(!is_null($name) && !is_null($using)) {
            $returnData['lto_id'] = LongTermGoal::insertGetId([
                "name" => $name, "desc" => $desc, "using" => $using
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function editLto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $ltoId = $request->ltoId;
        $name = $request->name;
        $desc = $request->desc;
        if(!is_null($ltoId) && !is_null($name)) {
            LongTermGoal::where('id', $ltoId)->update([
                "name" => $name, "desc" => $desc
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function removeLto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $ltoId = $request->ltoId;
        if(!is_null($ltoId)) {
            LongTermGoal::where('id', $ltoId)->delete();
            ShortTermGoal::where('lto_id', $ltoId)->update([ 'lto_id' => null ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getSto(Request $request, $stoId = null) {
        $returnData = [
            'result' => 'false',
            'lto'	 => null,
            'sto'	 => null,
            'mid'	 => null,
        ];
        if(!is_null($stoId)) {
            $returnData['sto'] = ShortTermGoal::where('id', $stoId)->first();
            $returnData['sto']->target = json_decode($returnData['sto']->target);
            if(!is_null($returnData['sto'])) {
                $returnData['lto'] = LongTermGoal::where('id', $returnData['sto']->lto_id)->first();
                $returnData['mid'] = MidTermGoal::where('id', $returnData['sto']->mid_id)->first();
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function getStos(Request $request) {
        $returnData = [
            'list' => null,
            'result'    =>  'false'
        ];
        $ltoId = $request->ltoId;
        $list = ShortTermGoal::when(!is_null($ltoId), function($q) use ($ltoId) {
            return $q->where('lto_id', $ltoId); // 상급 조건 > 대분류 LTO 조건에 따른 조회
            })->get();
        foreach($list as $sto) {
            if($sto->target) $sto->target = json_decode($sto->target);
        }
        $returnData['list'] = $list;
        $returnData['result'] = 'true';

        return response()->json($returnData);
    }

/*
    public function getStosByStudent(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $ltoId = $request->ltoId;
        $studentId = $request->studentId;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        if(!is_null($ltoId) && !is_null($studentId)) {
            $returnData['list'] = ShortTermGoal::join('programs', 'programs.sto_id', 'short_term_goals.id')
                ->leftJoin('long_term_goals', 'long_term_goals.id', 'short_term_goals.lto_id')
                ->where('short_term_goals.user_id', $request->user->id)
                ->where('short_term_goals.lto_id', $ltoId)
                ->where('programs.student_id', $studentId)
                ->selectRaw('short_term_goals.*, long_term_goals.name as lto_name')
                ->orderBy('short_term_goals.id', 'desc')
                ->groupBy('short_term_goals.id')
                ->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }
*/

    public function createSto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $ltoId = $request->ltoId;
        $midId = $request->midId;
        $name = $request->name;
        $desc = $request->desc;
        $target = $request->target;
        if(!is_null($name)) {
            $t = (!is_null($target) ? json_encode($target) : null);
            $returnData['sto_id'] = DB::connection($this->myDB)->table('short_term_goals')->insertGetId([
                "lto_id" => $ltoId, "mid_id" => $midId, "name" => $name, "desc" => $desc, "target" => $t
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function editSto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $stoId = $request->stoId;
        $name = $request->name;
        $desc = $request->desc;
        $target = (!is_null($request->target) ? json_encode($request->target) : null);
        $ltoId = $request->ltoId;
        $midId = $request->midId;

        if(!is_null($stoId)) {
            DB::connection($this->myDB)->table('short_term_goals')->where('id', $stoId)->update([
                'name'          =>  $name,
                'desc'          =>  $desc,
                'target'        =>  $target,
                'lto_id'        =>  $ltoId,
                'mid_id'        =>  $midId
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function removeSto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $stoId = $request->stoId;
        if(!is_null($stoId)) {
            DB::connection($this->myDB)->table('short_term_goals')->where('id', $stoId)->delete();
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function toggleLto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $using = $request->using;
        $ltoId = $request->ltoId;
        if(!is_null($ltoId)) {
            LongTermGoal::where('id', $ltoId)->update([ "using" => $using ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function toggleSto(Request $request) {
        $returnData = [
            'result' => 'false'
        ];

        $using = (!is_null($request->using) ? is_null($request->using) : 0);
        $stoId = $request->stoId;
        if(!is_null($stoId)) {
            DB::connection($this->myDB)->table('short_term_goals')->where('id', $stoId)->update([ "using" => $using ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function createStoItem(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $programId = $request->programId;
        $stoGroupId = $request->groupId;
        $stoId = $request->stoId;
        $standardPerc = $request->standardPerc;
        $standardCnt = $request->standardCnt;
        $rows = $request->rows;
        $target = $request->target;
        $promptCode = $request->promptCode;
        $promptMemo = $request->promptMemo;
        $scheduleMemo = $request->scheduleMemo;
        $programMemo = $request->programMemo;

        if(!is_null($programId) && !is_null($stoGroupId)) {
            $program = Program::where('id', intval($programId))->first();
            if(!Student::checkManager($request->user->id, $program->student_id)) {
                $returnData['error'] = '정보 수정 권한 없음';
                $returnData['error_code'] = 'A01';
                return response()->json($returnData);
            }

            $order = StoItem::where('program_id', $programId)
                ->where('sto_group_id', $stoGroupId)
                ->whereNull('deleted_at')
                ->count();

            $returnData['stoItem'] = StoItem::create([
                'program_id'    =>  $programId,
                'sto_group_id'  =>  $stoGroupId,
                'sto_id'        =>  $stoId,
                'user_id'       =>  $request->user->id,
                'rows'          =>  (!is_null($rows) ? json_encode($rows) : null),
                'target'        =>  (!is_null($target) ? json_encode($target) : null),
                'order'         =>  $order,
                'standard_perc' =>  $standardPerc,
                'standard_cnt'  =>  $standardCnt,
                'total_reach'   =>  0,
                'finish'        =>  0,
                'prompt_code'   =>  $promptCode,
                'prompt_memo'   =>  $promptMemo,
                'schedule_memo' =>  $scheduleMemo,
                'program_memo'  =>  $programMemo,
            ]);

            $returnData['stoItem']['rows'] = $rows;
            $returnData['stoItem']['target'] = $target;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function editStoItem(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];

        $stoGroupId = $request->groupId;
        $rows = $request->rows;
        $target = $request->target;
        $standardPerc = $request->standardPerc;
        $standardCnt = $request->standardCnt;
        $promptCode = $request->promptCode;
        $promptMemo = $request->promptMemo;
        $scheduleMemo = $request->scheduleMemo;
        $programMemo = $request->programMemo;

        if(!is_null($id) && !is_null($stoGroupId)) {
            $stoItem = StoItem::where('id', $id)->first();
            $program = Program::where('id', $stoItem->program_id)->first();
            if(!Student::checkManager($request->user->id, $program->studentId)) {
                $returnData['error'] = '정보 수정 권한 없음';
                $returnData['error_code'] = 'A01';
                return response()->json($returnData);
            }
            $prevStandardPerc = $stoItem->standard_perc;
            $prevStandardCnt = $stoItem->standard_cnt;
            $stoItem->update([
                'rows'                  =>  (!is_null($rows) ? json_encode($rows) : null),
                'target'                =>  (!is_null($target) ? json_encode($target) : null),
                'standard_perc'         =>  $standardPerc,
                'standard_cnt'          =>  $standardCnt,
                'prompt_code'           =>  $promptCode,
                'prompt_memo'           =>  $promptMemo,
                'schedule_memo'         =>  $scheduleMemo,
                'program_memo'          =>  $programMemo,
                'last_update_user_id'   =>  $request->user->id
            ]);

            if(($prevStandardPerc != $standardPerc)) {
                // 준거도달 포인트 재계산
                $points = Point::where('sto_item_id', $id)->where('reach', '!=', 2)->whereNull('deleted_at')->get();
                foreach($points as $point) {
                    StoItem::calcReachOnPoint($point->id);
                }
            }
            if(($prevStandardPerc != $standardPerc || $prevStandardCnt != $standardCnt)) {
                $isReach = StoItem::isReach($stoItem);
                if($isReach != "") {
                    StoItem::where('id', $id)->update([ 'total_reach' => 1, 'finish' => 1, 'reach_date' => $isReach->real_date, 'reach_user_id' => $isReach->target_user_id ]);
                } else {
                    StoItem::where('id', $id)->update([ 'total_reach' => 0, 'finish' => 0, 'reach_date' => null, 'reach_user_id' => null ]);
                }
            }
            $stoItem->rows = $rows;
            $stoItem->target = $target;
            $returnData['stoItem'] = $stoItem;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function copyStoItem(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];

        if(!is_null($id)) {
            $sto = StoItem::where('id', $id)->first();
            if(!is_null($sto)) {
                $program = Program::where('id', $sto->program_id)->first();
                if(!Student::checkManager($request->user->id, $program->student_id)) {
                    $returnData['error'] = '정보 수정 권한 없음';
                    $returnData['error_code'] = 'A01';
                    return response()->json($returnData);
                }

                $order = StoItem::where('sto_group_id', $sto->sto_group_id)
                    ->count();

                $returnData['stoItem'] = StoItem::create([
                    'program_id'    =>  $program->id,
                    'sto_group_id'  =>  $sto->sto_group_id,
                    'sto_id'        =>  $sto->sto_id,
                    'user_id'       =>  $request->user->id,
                    'rows'          =>  $sto->rows,
                    'target'        =>  $sto->target,
                    'order'         =>  $order,
                    'standard_perc' =>  $sto->standard_perc,
                    'standard_cnt'  =>  $sto->standard_cnt,
                    'total_reach'   =>  0,
                    'finish'        =>  0
                ]);
                if(!is_null($sto->rows)) $returnData['stoItem']['rows'] = json_decode($sto->rows);
                if(!is_null($sto->target)) $returnData['stoItem']['target'] = json_decode($sto->target);
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function removeStoItem(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            // 하위 포인트와 스틱 조회시 sto & program 이 삭제되었는지 판별필수
            // **하위다 제거하기
            $stoItem = StoItem::where('id', $id)->first();
            $stoGroupId = $stoItem->sto_group_id;
            $points = Point::where('sto_item_id', $id)->get();
            foreach($points as $point) {
                $sticks = Stick::where('point_id', $point->id)->delete();
                $point->delete();
            }
            StoItem::where('id', $id)->delete();
            StoItem::resetOrder($stoGroupId);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getStoItem(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $s = StoItem::leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('users', 'users.id', 'sto_items.user_id')
                ->where('sto_items.id', $id)
                ->selectRaw('
                    users.name as user_name,
                    sto_items.*, sto_groups.name as sto_group_name, sto_groups.type,
                    case
                        when sto_items.prompt_code = 1 then "독립(independent)"
                        when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                        when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                        when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                        when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                        when sto_items.prompt_code = 6 then "몸짓(gestual)"
                        when sto_items.prompt_code = 7 then "글자(textual)"
                        when sto_items.prompt_code = 8 then "그림(picture)"
                    end as prompt,
                    case
                        when sto_groups.type = 0 then "일반"
                        when sto_groups.type = 1 then "누적"
                    end as reach_type
                ')
                ->first();
            if(!is_null($s->rows)) $s->rows = json_decode($s->rows);
            if(!is_null($s->target)) $s->target = json_decode($s->target);
            // 최초 포인트 시작 일자
            $tmp = Point::where('sto_item_id', $s->id)
            ->whereNull('deleted_at')
            ->selectRaw('if(postponed is null, rgst_date, postponed) as real_date')
            ->orderBy('real_date', 'asc')
            ->first();
            $returnData['startDate'] = $tmp->real_date;
            $returnData['stoItem'] = $s;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getStoItems(Request $request, $programId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($programId)) {
            $program = Program::where('id', $programId)->first();
            $stoItems = StoItem::where('program_id', $programId)
                ->whereNull('parent_id')
                ->orderBy('order', 'desc')
                ->get();
            $childs = [];
            foreach($stoItems as $sto) {
                $child = StoItem::where('parent_id', $sto->id)
                    ->orderBy('order', 'desc')
                    ->get();

                if($sto->target)
                    $sto->target = json_decode($sto->target);
                if($sto->rows)
                    $sto->rows = json_decode($sto->rows);
                foreach($child as $c) {
                    $t = StoItem::where('id', $c->parent_id)->first();
                    $c['parent_name'] = (!is_null($t) ? $t->name : "");
                    if($c->rows)
                        $c->rows = json_decode($c->rows);
                }
                $sto->total_reach = Point::where('sto_item_id', $sto->id)->where('reach', 1)->count();
                $childs[$sto->id] = $child;
            }
            // $stoItems = StoItem::where('program_id', $programId)
            //     ->orderBy('order', 'desc')
            //     ->get();
            // foreach($stoItems as $stoItem) {
            //     if(!is_null($stoItem->parent_id)) {
            //         $t = StoItem::where('id', $stoItem->parent_id)->first();
            //         $stoItem['parent_name'] = (!is_null($t) ? $t->name : "");
            //     }
            //     if($stoItem->rows)
            //         $stoItem->rows = json_decode($stoItem->rows);
            // }
            $returnData['stoItems'] = $stoItems;
            $returnData['stoChilds'] = $childs;
            $stg = ShortTermGoal::where('lto_id', $program->lto_id)
                ->orderBy('id', 'desc')
                ->get();
            foreach($stg as $s) {
                if($s->target) $s->target = json_decode($s->target);
            }
            $returnData['stos'] = $stg;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getReached(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $sort = ($request->sort=="true" ? "desc" : "asc");
        $classId = $request->classId;
        $studentId = $request->studentId;
        $perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
        //$status = $request->status;
        $ltoId = $request->ltoId;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;

        if(!is_null($classId) && !is_null($studentId)) {
            $user = $request->user;
            //$sub = Point::where('reach', '>', 0)
            // $sub = Point::where('status', '>', 0)
            //     ->selectRaw('
            //         points.*, if(points.postponed is null, points.rgst_date, points.postponed) as real_date
            //     ');
            $list = StoItem::leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->leftJoin('students', 'students.id', 'programs.student_id')
                ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('users', 'users.id', 'sto_items.user_id')
                ->when($classId == 0, function($q) use($studentId, $user) { // 개별학생의 프로그램 필터
                    return $q->join('class_relations', 'class_relations.student_id', 'programs.student_id')
                        ->where('class_relations.student_id', $studentId)
                        ->where('class_relations.user_id', $user->id)
                        ->where('class_relations.class_id', 0)
                        ->where('programs.personal', 1);
                })
                // ->when(!is_null($status), function($q) use($status) {
                //     return $q->where('sto_items.finish', $status);
                // })
                ->when(!is_null($ltoId), function($q) use($ltoId) {
                    return $q->where('programs.lto_id', $ltoId);
                })
                ->where('programs.student_id', $studentId)
                //->where('sto_items.total_reach', '>', 0)
                ->where('sto_items.finish', 1)
                ->whereRaw("sto_items.reach_date >= '".$fromDate."' and sto_items.reach_date <= '".$toDate."'")
                ->whereNull('sto_items.deleted_at')
                ->whereNull('sto_groups.deleted_at')
                ->whereNull('programs.deleted_at')
                ->selectRaw("sto_items.*,
                    date_format(sto_items.created_at, '%Y-%m-%d') as created,
                    long_term_goals.name as lto_name,
                    sto_groups.name as sto_group_name,
                    sto_groups.finish as lto_finish,
                    students.name as student_name,
                    sto_groups.type, users.name as user_name
                ")
                ->groupBy('sto_items.id')
                ->orderBy('sto_items.reach_date', $sort)
                ->orderBy('lto_name', 'asc')
                ->orderBy('sto_group_name', 'asc')
                ->orderBy('sto_items.updated_at', $sort);
            // $list = StoItem::leftJoin('programs', 'programs.id', 'sto_items.program_id')
            //     ->leftJoin('students', 'students.id', 'programs.student_id')
            //     ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
            //     ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
            //     ->leftJoin('users', 'users.id', 'sto_items.user_id')
            //     ->leftJoinSub($sub, 'p', function($join) {
            //         $join->on('p.sto_item_id', 'sto_items.id');
            //     })
            //     ->when($classId == 0, function($q) use($studentId, $user) { // 개별학생의 프로그램 필터
            //         return $q->join('class_relations', 'class_relations.student_id', 'programs.student_id')
            //             ->where('class_relations.student_id', $studentId)
            //             ->where('class_relations.user_id', $user->id)
            //             ->where('class_relations.class_id', 0)
            //             ->where('programs.personal', 1);
            //     })
            //     ->when(!is_null($status), function($q) use($status) {
            //         return $q->where('sto_items.finish', $status);
            //     })
            //     ->when(!is_null($ltoId), function($q) use($ltoId) {
            //         return $q->where('programs.lto_id', $ltoId);
            //     })
            //     ->where('programs.student_id', $studentId)
            //     ->where('sto_items.total_reach', '>', 0)
            //     ->whereRaw("p.real_date >= '".$fromDate."' and p.real_date <= '".$toDate."'")
            //     ->whereNull('sto_items.deleted_at')
            //     ->selectRaw("sto_items.*,
            //         date_format(sto_items.created_at, '%Y-%m-%d') as created,
            //         long_term_goals.name as lto_name,
            //         sto_groups.name as sto_group_name,
            //         sto_groups.finish as lto_finish,
            //         students.name as student_name,
            //         sto_groups.type, users.name as user_name,
            //         max(p.real_date) as real_date
            //     ")
            //     ->groupBy('sto_items.id')
            //     //->orderBy('users.name', 'asc')
            //     ->orderBy('real_date', $sort)
            //     ->orderBy('lto_name', 'asc')
            //     ->orderBy('sto_group_name', 'asc')
            //     ->orderBy('sto_items.updated_at', $sort);
            $tmp = $list->get();
            $returnData['total'] = count($tmp);

            $realList = $list->skip($page * $perPage)
                ->take($perPage)
                ->get();
            foreach($realList as $sto) {
                $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
                $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
            }
            $returnData['stoItems'] = $realList;

            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function setStatus(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $status = $request->status;
        if(!is_null($id) && !is_null($status)) {
            if(StoItem::setFinish($id, $status)) {
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function switchOrder(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $stoGroupId = $request->groupId;
        $myId = $request->myId;
        $targetId = $request->targetId;
        if(!is_null($myId) && !is_null($targetId)) {
            $m = StoItem::where('sto_group_id', $stoGroupId)->where('id', intval($myId))->first();
            $t = StoItem::where('sto_group_id', $stoGroupId)->where('id', intval($targetId))->first();
            if(!is_null($m) && !is_null($t)) {
                $temp = $m->order;
                $m->update([ 'order' => $t->order ]);
                $t->update([ 'order' => $temp ]);
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function checkLastDecision(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $returnData['result'] = StoItem::getLastDecisionPoint($id);
        }

        return $returnData;
    }

    public function getDecision(Request $request, $id = null) {
        $res = 0;
        $dec = Decision::where('sto_item_id', $id)->count();
        if($dec > 0) {
            $res = 1;
        }
        return $res;
    }

    public function getStoGroupsByProgram(Request $request, $programId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($programId)) {
            $list = StoGroup::where('program_id', $programId)
                ->whereNull('deleted_at')
                ->orderBy('order', 'desc')
                ->get();
            foreach($list as $sg) {
                $tmp = StoItem::where('sto_group_id', $sg->id)
                    ->whereNull('deleted_at')
                    ->orderBy('order', 'desc')
                    ->get();
                foreach($tmp as $sto) {
                    if($sto->target)
                        $sto->target = json_decode($sto->target);
                    if($sto->rows)
                        $sto->rows = json_decode($sto->rows);
                }
                $sg['stoItems'] = $tmp;
            }
            $returnData['stoGroups'] = $list;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function createStoGroup(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $programId = $request->programId;
        $name = $request->name;
        $type = $request->type;

        if(!is_null($programId)) {
            $programCheck = Program::where('id', intval($programId))->count();
            if($programCheck > 0) {
                $order = StoGroup::where('program_id', $programId)
                    ->whereNull('deleted_at')
                    ->count();

                $returnData['stoGroup'] = StoGroup::create([
                    'program_id'    =>  $programId,
                    'name'          =>  $name,
                    'type'          =>  $type,
                    'order'         =>  $order,
                    'finish'        =>  0
                ]);
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function setStoGroupStatus(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $status = $request->status;
        if(!is_null($id) && !is_null($status)) {
            if(StoGroup::setFinish($id, $status)) {
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function switchGroupOrder(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $myId = $request->myId;
        $targetId = $request->targetId;
        if(!is_null($myId) && !is_null($targetId)) {
            $m = StoGroup::where('id', intval($myId))->first();
            $t = StoGroup::where('id', intval($targetId))->first();
            if(!is_null($m) && !is_null($t)) {
                $temp = $m->order;
                $m->update([ 'order' => $t->order ]);
                $t->update([ 'order' => $temp ]);
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function editStoGroup(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $name = $request->name;
        $type = $request->type;
        if(!is_null($id)) {
            StoGroup::where('id', $id)->update([
                'name'          =>  $name,
                'type'          =>  $type,
            ]);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function removeStoGroup(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            // **하위다 제거하기
            $stoGroup = StoGroup::where('id', $id)->first();
            $programId = $stoGroup->program_id;
            $stos = StoItem::where('sto_group_id', $id)->get();
            foreach($stos as $sto) {
                $points = Point::where('sto_item_id', $sto->id)->get();
                foreach($points as $point) {
                    $sticks = Stick::where('point_id', $point->id)->delete();
                    $point->delete();
                }
                $sto->delete();
            }
            StoGroup::where('id', $id)->delete();
            StoGroup::resetOrder($programId);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function copyStoGroup(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];

        if(!is_null($id)) {
            $stoGroup = StoGroup::where('id', $id)->first();
            if(!is_null($stoGroup)) {
                $program = Program::where('id', $stoGroup->program_id)->first();
                if(!Student::checkManager($request->user->id, $program->student_id)) {
                    $returnData['error'] = '정보 수정 권한 없음';
                    $returnData['error_code'] = 'A01';
                    return response()->json($returnData);
                }

                $order = StoGroup::where('program_id', $stoGroup->program_id)
                    ->count();

                $returnData['stoGroup'] = StoGroup::create([
                    'name'          =>  $stoGroup->name,
                    'program_id'    =>  $stoGroup->program_id,
                    'type'          =>  $stoGroup->type,
                    'order'         =>  $order,
                    'finish'        =>  0
                ]);
                $returnData['stoGroup']['stoItems'] = [];
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function getStoStatus(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $sto = StoItem::where('id', $id)->whereNull('deleted_at')->first();
            $stoGroup = StoGroup::where('id',$sto->sto_group_id)->whereNull('deleted_at')->first();
            $returnData['stoItem'] = $sto;
            $returnData['stoGroup'] = $stoGroup;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function setDecisionsAll(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $res = StoItem::setAllDecisions($request->user, $id);
            $returnData['result_calc'] = $res;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }
}
