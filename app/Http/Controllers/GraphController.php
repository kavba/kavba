<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Point;
use App\Models\Program;
use App\Models\StoItem;
use App\Models\Stick;
use App\Models\Decision;
use App\Models\UserError;
use App\Models\LongTermGoal;
use App\Models\AssignProgram;

class GraphController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getGraphList(Request $request) {
        $returnData = [];
        $returnData['list'] = DB::connection($this->myDB)->table('graphs')->orderBy('id', 'asc')->get();
        $returnData['user'] = $request->user->id;
        return response()->json($returnData);
    }

    public function getData(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $standard = $request->standard;
        $target = $request->target;
        $graph = $request->graph;
        $domain = $request->domain;
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $personal = is_null($request->personal) ? null : $request->personal;
        if(!is_null($standard) && !is_null($target)) {
            $returnData['data'] = self::dataForGraph($graph, $standard, $target, $personal, $domain, $fromDate, $toDate);
            if($graph == 6) {
                $returnData['user'] = self::userDataForGraph($standard, $target, $fromDate, $toDate);
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    private function dataForGraph($graph = 0, $standard = 0, $target, $personal = null, $domain = null, $from, $to) {
        $res = [];
        if(!$graph || is_null($graph) || $graph == 0) {
            //$data = [];
        } else if($graph == 1) {
            $ltos = LongTermGoal::get();
            $ltoSql = "";
            foreach($ltos as $lto) {
                //if($i > 0) $ltoSql .= ", ";
                $ltoSql .= ", sum(if(programs.lto_id = ".$lto->id." and sticks.value = 1, 1, 0)) as 'p_".$lto->id."'";
                $ltoSql .= ", sum(if(programs.lto_id = ".$lto->id." and sticks.value is not null, 1, 0)) as 't_".$lto->id."'";
            }
            $res = Stick::leftJoin('points', 'points.id', 'sticks.point_id')
                ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->when($standard == 1, function($q) use($target) {
                    return $q->where('sticks.user_id', $target)
                        ->where('programs.personal', 0);
                })
                ->when($standard == 2, function($q) use($target, $personal) {
                    return $q->where('programs.student_id', $target)
                        ->when(is_null($personal), function($q2){
                            return $q2->where('programs.personal', 0);
                        })
                        ->when(!is_null($personal), function($q2){
                            return $q2->where('programs.personal', 1)
                                ->leftJoin('class_relations', function($join) use($personal) {
                                    $join->on('class_relations.student_id', 'programs.student_id')
                                        ->where('class_relations.user_id', $personal)
                                        ->where('class_relations.flag', 2)
                                        ->where('class_relations.class_id', 0);
                                });
                        });
                })
                ->when($standard == 3, function($q) use($target) {
                    return $q->leftJoin('class_relations', 'class_relations.student_id', 'programs.student_id')
                        ->where('class_relations.flag', 2)
                        ->whereNull('class_relations.user_id')
                        ->where('class_relations.class_id', $target)
                        ->where('programs.personal', 0);
                })
                ->when($standard == 4, function($q) use($target) {
                    return $q->leftJoin('class_relations', 'class_relations.student_id', 'programs.student_id')
                        ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                        ->where('class_relations.flag', 2)
                        ->whereNull('class_relations.user_id')
                        ->where('kavba_classes.over_id', $target);
                })
                ->whereRaw("
                    sticks.stick_date >= '".$from."'
                    and sticks.stick_date <= '".$to."'
                    and DAYOFWEEK(sticks.stick_date) not in (1,4,7)
                ")
                ->whereNotNull('points.id')
                ->whereNull('points.deleted_at')
                ->whereNull('sticks.deleted_at')
                ->whereNull('sto_items.deleted_at')
                ->whereNull('sto_groups.deleted_at')
                ->whereNull('programs.deleted_at')
                ->selectRaw("
                    count(DISTINCT sticks.id) as total,
                    count(DISTINCT if(sticks.value = 1, sticks.id, null)) as positive,
                    count(DISTINCT points.id) as reaches,
                    sticks.stick_date as end_date".$ltoSql)
                ->groupBy('end_date')
                ->orderBy('end_date', 'asc')
                ->get();
        } else if($graph == 2) {
            $query = "select
                    cal.*,
                    if(s.total is null, 0, s.total) as total,
                    if(s.reaches is null, 0, s.reaches) as reaches,
                    if(s.reaches is null OR s.reaches = 0, 0 ,round(if(s.total is null, 0, s.total) / if(s.reaches is null, 0, s.reaches), 1)) as criteria
                from (
                    select
                        d.end_date
                    from (
                        SELECT
                            DATE_FORMAT(DATE_SUB(v.c_date, INTERVAL (DAYOFWEEK(DATE_ADD(v.c_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
                        FROM
                        (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                        (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                        (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                        (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                        (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                        (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                        WHERE c_date >= '".$from."' AND c_date <= '".$to."'
                    ) d
                    group by d.end_date
                    order by d.end_date asc
                ) cal
                left join (
                    select
                        DATE_FORMAT(DATE_SUB(s.stick_date,
                            INTERVAL (DAYOFWEEK(DATE_ADD(s.stick_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date,
                        count(DISTINCT s.id) as total,
                        count(distinct si2.id) as reaches
                    from sticks s
                    left join (
                        select points.*, if(points.postponed is null, points.rgst_date, points.postponed) as real_date
                        from points
                    ) p on p.id = s.point_id
                    left join sto_items si on si.id = p.sto_item_id
                    left join sto_groups sg on sg.id = si.sto_group_id
                    left join programs pr on pr.id = si.program_id
                    left join (
                        select * from sto_items
                        where sto_items.finish = 1
                        and DAYOFWEEK(sto_items.reach_date) not in (1,4,7)
                    ) si2 on si2.id = p.sto_item_id and si2.reach_date = s.stick_date and si2.reach_user_id = s.user_id ";
            if($standard == 2) {
                if(is_null($personal)) {
                    $query .= "
                        where pr.student_id = ".$target."
                        and pr.personal = 0
                        ";
                    if(!is_null($domain)) {
                        $query .= "
                            and pr.lto_id = ".$domain;
                    }
                } else {
                    $query .= "
                        left join class_relations cr on cr.student_id = pr.student_id
                        where cr.flag = 2
                        and pr.student_id = ".$target."
                        and pr.personal = 1
                        and cr.class_id = 0
                        and cr.user_id = ".$personal;
                    if(!is_null($domain)) {
                        $query .= "
                            and pr.lto_id = ".$domain;
                    }
                }
            } else if($standard == 3) {
                $query .= "
                    left join class_relations cr on cr.student_id = pr.student_id
                    where cr.flag = 2
                    and pr.personal = 0
	                and cr.user_id is null
                    and cr.class_id = ".$target;
            } else if($standard == 4) {
                $query .= "
                    left join class_relations cr on cr.student_id = pr.student_id
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 2
                    and cr.user_id is null
                    and pr.personal = 0
                    and kc.over_id = ".$target;
            } else { // $standard == 1
                $query .= " where pr.personal = 0
                    and s.user_id = ".$target;
            }
            $query .= " and si.deleted_at is null
                        and sg.deleted_at is null
                        and p.deleted_at is null
                        and s.deleted_at is null
                        and pr.deleted_at is null
                        and s.stick_date >= '".$from."'
                        and s.stick_date <= '".$to."'
                        and DAYOFWEEK(s.stick_date) not in (1,4,7)
                        group by end_date
                    ) s on s.end_date = cal.end_date
                    order by cal.end_date asc";

            $res = DB::connection($this->myDB)->select(DB::raw($query));
        } else if($graph == 3) {
            $query = "select
                    cal.*,
                    if(s.cnt is null, 0, s.cnt) as cnt,
                    sum(if(s.cnt is null, 0, s.cnt)) over(order by cal.end_date) as stack
                from (
                    select
                        d.end_date
                    from (
                        SELECT
                            DATE_FORMAT(DATE_SUB(v.c_date, INTERVAL (DAYOFWEEK(DATE_ADD(v.c_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
                        FROM
                        (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                        (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                        (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                        (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                        (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                        (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                        WHERE c_date >= '".$from."' AND c_date <= '".$to."'
                    ) d
                    group by d.end_date
                    order by d.end_date asc
                ) cal
                left join (
                    select
                        count(DISTINCT si.id) as cnt,
                        DATE_FORMAT(DATE_SUB(si.reach_date,
                            INTERVAL (DAYOFWEEK(DATE_ADD(si.reach_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
                    from
                        sto_items si
                        left join sto_groups sg on sg.id = si.sto_group_id
                        left join programs pr on pr.id = si.program_id ";
            if($standard == 2) {
                if(is_null($personal)) {
                    $query .= "
                        where pr.student_id = ".$target."
                        and pr.personal = 0
                        ";
                } else {
                    $query .= "
                        left join class_relations cr on cr.student_id = pr.student_id
                        where cr.flag = 2
                        and pr.student_id = ".$target."
                        and pr.personal = 1
                        and cr.class_id = 0
                        and cr.user_id = ".$personal;
                }
            } else if($standard == 3) {
                $query .= "
                    join class_relations cr on cr.student_id = pr.student_id
                    where cr.flag = 2
                    and pr.personal = 0
                    and cr.user_id is null
                    and cr.class_id = ".$target;
            } else if($standard == 4) {
                $query .= "
                    left join class_relations cr on cr.student_id = pr.student_id
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 2
                    and pr.personal = 0
                    and cr.user_id is null
                    and kc.over_id = ".$target;
            } else { // $standard == 1
                $query .= " where si.reach_user_id = ".$target." and pr.personal = 0";
            }
            $query .= " and si.finish = 1
                        and si.reach_date >= '".$from."'
                        and si.reach_date <= '".$to."'
                        and si.deleted_at is null
                        and sg.deleted_at is null
                        and pr.deleted_at is null
                        and DAYOFWEEK(si.reach_date) not in (1,4,7)
                        group by end_date
                    ) s on s.end_date = cal.end_date
                    order by cal.end_date asc";
            $res = DB::connection($this->myDB)->select(DB::raw($query));
            // $query = "select
            //         cal.*,
            //         if(s.cnt is null, 0, s.cnt) as cnt,
            //         sum(if(s.cnt is null, 0, s.cnt)) over(order by cal.end_date) as stack
            //     from (
            //         select
            //             d.end_date
            //         from (
            //             SELECT
            //                 DATE_FORMAT(DATE_SUB(v.c_date, INTERVAL (DAYOFWEEK(DATE_ADD(v.c_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
            //             FROM
            //             (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
            //             (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
            //             (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
            //             (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
            //             (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
            //             (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
            //             WHERE c_date >= '".$from."' AND c_date <= '".$to."'
            //         ) d
            //         group by d.end_date
            //         order by d.end_date asc
            //     ) cal
            //     left join (
            //         select
            //             count(DISTINCT si.id) as cnt, max(sp.end_date) as end_date
            //         from
            //             sto_items si
            //             left join sto_groups sg on sg.id = si.sto_group_id
            //             join (
            //                 select
            //                     points.*,
            //                     DATE_FORMAT(DATE_SUB(if(points.postponed is null, points.rgst_date, points.postponed),
            //                     INTERVAL (DAYOFWEEK(DATE_ADD(if(points.postponed is null, points.rgst_date, points.postponed), INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
            //                 from points
            //             ) sp on sp.sto_item_id = si.id";
            // if($standard == 2) {
            //     if(is_null($personal)) {
            //         $query .= "
            //             left join programs pr on pr.id = si.program_id
            //             where pr.student_id = ".$target."
            //             and pr.personal = 0
            //             ";
            //     } else {
            //         $query .= "
            //             left join programs pr on pr.id = si.program_id
            //             left join class_relations cr on cr.student_id = pr.student_id
            //             where cr.flag = 2
            //             and pr.student_id = ".$target."
            //             and pr.personal = 1
            //             and cr.class_id = 0
            //             and cr.user_id = ".$personal;
            //     }
            // } else if($standard == 3) {
            //     $query .= "
            //         left join programs pr on pr.id = si.program_id
            //         left join class_relations cr on cr.student_id = pr.student_id
            //         where cr.flag = 2
            //         and pr.personal = 0
            //         and cr.user_id is null
            //         and cr.class_id = ".$target;
            // } else if($standard == 4) {
            //     $query .= "
            //         left join programs pr on pr.id = si.program_id
            //         left join class_relations cr on cr.student_id = pr.student_id
            //         left join kavba_classes kc on kc.id = cr.class_id
            //         where cr.flag = 2
            //         and cr.user_id is null
            //         and kc.over_id = ".$target;
            // } else { // $standard == 1
            //     $query .= " where sp.target_user_id = ".$target;
            // }
            // $query .= " and si.finish = 1
            //             and sp.reach > 0
            //             and sp.rgst_date >= '".$from."'
            //             and sp.rgst_date <= '".$to."'
            //             group by end_date
            //         ) s on s.end_date = cal.end_date
            //         order by cal.end_date asc";
        } else if($graph == 4) {
            $condition = "user_id = sub1.user_id";
            $query = "select
                ss.end_date,
                (ss.reaches + ss.no_reaches + ss.decs) as decisions,
                sum(ss.reaches + ss.no_reaches + ss.decs) over(order by ss.end_date) as stack,
                ss.reaches, ss.no_reaches, ss.decs
            from (
                select
                    DATE_FORMAT(DATE_SUB(sub1.real_date, INTERVAL (DAYOFWEEK(DATE_ADD(sub1.real_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date,
                    count(DISTINCT if(sub2.finish = 1, sub2.sto_item_id, null)) as reaches,
                    count(DISTINCT if(sub2.finish = 2, sub2.sto_item_id, null)) as no_reaches,
                    count(DISTINCT if(sub3.decision_id is not null, sub3.decision_id, null)) as decs
                from (
                    select
                        cal.*,
                        uu.user_id, uu.user_name,
                        uu.student_id, uu.student_name
                    from (
                        select
                            d.real_date
                        from (
                            SELECT
                                v.c_date as real_date
                            FROM
                            (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                            (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                            (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                            (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                            (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                            (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                            WHERE c_date >= '".$from."' AND c_date <= '".$to."'
                            and DAYOFWEEK(c_date) not in (1,4,7)
                        ) d
                        group by d.real_date
                        order by d.real_date asc
                    ) cal, (
                        select
                        distinct u.id as user_id, u.name as user_name,
                        st.id as student_id, st.name as student_name
                        from class_relations cr
                        left join users u on u.id = cr.user_id
                        left join students st on st.id = cr.student_id
                        left join kavba_classes kc on kc.id = cr.class_id ";
            if($standard == 2) {
                $query .= "
                    where cr.flag = 2
                    and cr.user_id is null
                    and cr.student_id = ".$target."
                    and kc.visible = 1
                    order by st.name asc
                ";
                $condition = "student_id = sub1.student_id";
            } else {
                if($standard == 3) {
                    $query .= "
                        where cr.flag = 1
                        and cr.class_id = ".$target."
                        and kc.visible = 1
                        order by u.name asc
                    ";
                } else if($standard == 4) {
                    $query .= "
                        where cr.flag = 1
                        and kc.over_id = ".$target."
                        and kc.visible = 1
                        group by u.id, st.id
                        order by u.name asc ";
                } else { // $standard == 1
                    $query .= "
                        where cr.flag = 1
                        and kc.visible = 1
                        and cr.user_id = ".$target."
                        order by u.name asc ";
                }
            }
            $query .= " ) uu
                    ) sub1
                    left join (
                        select
                            pr.student_id,
                            si.reach_user_id as user_id,
                            si.id as sto_item_id,
                            si.finish,
                            si.reach_date
                        from sto_items si
                        left join programs pr on pr.id = si.program_id
                        where si.finish in (1,2)
                        and si.reach_date >= '".$from."'
                        and si.reach_date <= '".$to."'
                        and si.deleted_at is null
                        and pr.deleted_at is null
                    ) sub2 on sub2.".$condition." and sub2.reach_date = sub1.real_date
                    left join (
                        select
                            pr.student_id,
                            d.master_id as user_id,
                            d.id as decision_id,
                            if(p.postponed is null, p.rgst_date, p.postponed) as decide_date
                        from
                            decisions d
                            join sto_items si on si.id = d.sto_item_id
                            join sto_groups sg on sg.id = si.sto_group_id
                            join programs pr on pr.id = si.program_id
                            join points p on p.id = d.point_id
                        where pr.deleted_at is null
                        and si.deleted_at is null
                        and sg.deleted_at is null
                        and ((d.point_id not in (
                            select tmp.* from (
                                select id from points
                                where points.sto_item_id = d.sto_item_id
                                and sg.type = 0
                                order by if(points.postponed is null, points.rgst_date, points.postponed) desc, points.updated_at desc
                                limit 1
                            ) as tmp
                        ) and si.finish in (1, 2)) or si.finish = 0)
                    ) sub3 on sub3.".$condition." and sub3.decide_date = sub1.real_date
                    group by end_date
                ) ss";

            $res = DB::connection($this->myDB)->select(DB::raw($query));
        } else if($graph == 5) {
            $query = "select
                    cal.*,
                    if(s.cnt1 is null, 0, s.cnt1) as cnt1,
                    if(s.cnt2 is null, 0, s.cnt2) as cnt2,
                    sum(if(s.cnt1 is null, 0, s.cnt1)) over(order by cal.end_date) as stack1,
                    sum(if(s.cnt2 is null, 0, s.cnt2)) over(order by cal.end_date) as stack2
                from (
                    select
                        d.end_date
                    from (
                        SELECT
                            DATE_FORMAT(DATE_SUB(v.c_date, INTERVAL (DAYOFWEEK(DATE_ADD(v.c_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date
                        FROM
                        (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                        (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                        (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                        (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                        (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                        (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                        WHERE c_date >= '".$from."' AND c_date <=  '".$to."'
                    ) d
                    group by d.end_date
                    order by d.end_date asc
                ) cal
                left join (
                    select
                        DATE_FORMAT(DATE_SUB(err.rgst_date, INTERVAL (DAYOFWEEK(DATE_ADD(err.rgst_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date,
                        sum(err.`count`) as cnt1,
                        sum(err.`count2`) as cnt2
                    from
                        user_errors err";
            if($standard == 3) {
                $query .= "
                    left join class_relations cr on cr.user_id = err.user_id
                    where cr.flag = 1
                    and pr.personal = 0
                    and cr.class_id = ".$target;
            } else if($standard == 4) {
                $query .= "
                    left join class_relations cr on cr.user_id = err.user_id
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 1
                    and kc.over_id = ".$target;
            } else { // $standard == 1
                $query .= " where err.user_id = ".$target;
            }
            $query .= " and err.deleted_at is null
                        and err.rgst_date >= '".$from."' AND err.rgst_date <=  '".$to."'
                        group by end_date
                    ) s on s.end_date = cal.end_date
                    order by cal.end_date asc";

            $res = DB::connection($this->myDB)->select(DB::raw($query));
        } else if($graph == 6) {
            $query = "select
                        sub1.*,
                        DATE_FORMAT(DATE_SUB(sub1.real_date, INTERVAL (DAYOFWEEK(DATE_ADD(sub1.real_date, INTERVAL +4 DAY))) -2 DAY), '%Y-%m-%d') as start_date,
                        DATE_FORMAT(DATE_SUB(sub1.real_date, INTERVAL (DAYOFWEEK(DATE_ADD(sub1.real_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date,
                        sub2.lto_name,
                        if(sub2.positive is null, 0, sub2.positive) as positive,
                        if(sub2.total is null, 0, sub2.total) as total,
                        if(sub2.reaches is null, 0, sub2.reaches) as sto_met,
                        if(sub2.reaches is null or sub2.reaches = 0, 0 ,round(if(sub2.total is null, 0, sub2.total) / if(sub2.reaches is null, 0, sub2.reaches), 1)) as criterion,
                        if(sub2.lto_reaches is null, 0, sub2.lto_reaches) as lto_met,
                        if(sub2.decs is null, 0, sub2.decs) + if(sub2.reaches is null, 0, sub2.reaches) + if(sub2.no_reaches is null, 0, sub2.no_reaches) as decs,
                        sub2.errorless,
                        sub2.witherror
                    from (
                        select
                            cal.*, stud.id as student_id, stud.name as student_name,
                            domains.id as domain_id, domains.name as domain_name
                        from (
                            select
                                d.real_date
                            from (
                                SELECT
                                    v.c_date as real_date
                                FROM
                                (SELECT ADDDATE('".$from."',t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                                (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                                (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                                (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                                (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                                (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                                WHERE c_date >= '".$from."' AND c_date <= '".$to."'
                                and DAYOFWEEK(c_date) not in (1,4,7)
                            ) d
                            group by d.real_date
                            order by d.real_date asc
                        ) cal, (
                            select
                                st.id, st.name
                            from class_relations cr
                            left join students st on st.id = cr.student_id ";

            if($standard == 3) {
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 2
                    and cr.class_id = ".$target."
                    and kc.visible = 1
                    order by st.name asc ";
            } else if($standard == 4) {
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 2
                    and cr.user_id is null
                    and kc.over_id = ".$target."
                    and kc.visible = 1
                    group by st.id
                    order by st.name asc ";
            } else { // $standard == 1
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    join (
                        select * from class_relations
                        where flag = 1 and user_id = ".$target."
                    ) cr2 on cr2.class_id = cr.class_id
                    where cr.flag = 2
                    and cr.user_id is null
                    and kc.visible = 1
                    order by st.name asc ";
            }

            $query .= " ) stud, (
                            select * from long_term_goals lto2 where lto2.`using` = 1
                        ) domains
                    ) sub1
                    left join (
                        select
                            s.stick_date as real_date, pr.student_id, lto.id as lto_id, lto.name as lto_name,
                            count(DISTINCT if(s.value = 1, s.id, null)) as positive,
                            count(DISTINCT s.id) as total,
                            count(DISTINCT if(si.finish = 1 and si.reach_date = s.stick_date, si.id, null)) as reaches,
                            count(DISTINCT if(si.finish = 2 and si.reach_date = s.stick_date, si.id, null)) as no_reaches,
                            count(DISTINCT if(sg.finish = 1, sg.id, null)) as lto_reaches,
                            count(DISTINCT d.decision_id) as decs,
                            sum(e.`count2`) / count(DISTINCT s.id) as errorless,
                            sum(e.`count`) / count(DISTINCT s.id) as witherror
                        from points p
                            left join sticks s on p.id = s.point_id
                            left join sto_items si on si.id = p.sto_item_id
                            left join sto_groups sg on sg.id = si.sto_group_id
                            left join programs pr on pr.id = si.program_id
                            left join long_term_goals lto on lto.id = pr.lto_id
                            left join (
                                select
                                    decisions.id as decision_id, decisions.point_id,
                                    if(p.postponed is null, p.rgst_date, p.postponed) as decide_date
                                from decisions
                                join points p on p.id = decisions.point_id
                                join sto_items on sto_items.id = decisions.sto_item_id
                                join sto_groups on sto_groups.id = sto_items.sto_group_id
                                where sto_items.deleted_at is null
                                and sto_groups.deleted_at is null
                                and if(p.postponed is null, p.rgst_date, p.postponed) >= '".$from."'
                                and if(p.postponed is null, p.rgst_date, p.postponed) <= '".$to."'
                                and (decisions.point_id not in (
                                    select tmp.* from (
                                        select id from points
                                        where points.sto_item_id = decisions.sto_item_id
                                        order by if(points.postponed is null, points.rgst_date, points.postponed) desc, points.updated_at desc
                                        limit 1
                                    ) as tmp
                                ) and sto_groups.`type` = 0 )
                            ) d on d.point_id = p.id
                            left join user_errors e on e.student_id = pr.student_id and e.rgst_date = s.stick_date
                        where pr.personal = 0
                        and p.deleted_at is null
                        and s.deleted_at is null
                        and s.stick_date >= '".$from."'
                        and s.stick_date <= '".$to."'
                        and DAYOFWEEK(s.stick_date) not in (1,4,7)
                        and si.deleted_at is null
                        and sg.deleted_at is null
                        and pr.deleted_at is null
                        and e.deleted_at is null
                        group by real_date, pr.student_id, lto.id
                    ) sub2 on sub2.real_date = sub1.real_date and sub2.student_id = sub1.student_id and sub2.lto_id = sub1.domain_id
                    where sub1.student_id is not null
                    order by sub1.real_date asc, sub1.student_name asc";

            $list = DB::connection($this->myDB)->select(DB::raw($query));
            $res = [];
            if(count($list) > 0) {
                foreach($list as $key => $row) {
                    $label = $row->end_date;
                    if(!array_key_exists($label, $res)) {
                        $res[$label] = [];
                    }
                    if(!array_key_exists($row->real_date, $res[$label])) {
                        $res[$label][$row->real_date] = [];
                    }
                    if(!array_key_exists($row->student_id, $res[$label][$row->real_date])) {
                        $res[$label][$row->real_date][$row->student_id] = [];
                    }
                    $res[$label][$row->real_date][$row->student_id] [] = $row;
                }
            }
        }
        return $res;
    }

    private function userDataForGraph($standard = 0, $target, $from, $to) {
        $res = [];
        $query = "select
                sub1.*,
                DATE_FORMAT(DATE_SUB(sub1.real_date, INTERVAL (DAYOFWEEK(DATE_ADD(sub1.real_date, INTERVAL +4 DAY))) -2 DAY), '%Y-%m-%d') as start_date,
                DATE_FORMAT(DATE_SUB(sub1.real_date, INTERVAL (DAYOFWEEK(DATE_ADD(sub1.real_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as end_date,
                if(sub2.positive is null, 0, sub2.positive) as positive,
                if(sub2.total is null, 0, sub2.total) as total,
                if(sub2.reaches is null, 0, sub2.reaches) as sto_met,
                if(sub2.reaches is null or sub2.reaches = 0, 0 ,round(if(sub2.total is null, 0, sub2.total) / if(sub2.reaches is null, 0, sub2.reaches), 1)) as criterion,
                if(sub2.lto_reaches is null, 0, sub2.lto_reaches) as lto_met,
                if(sub2.decs is null, 0, sub2.decs) + if(sub2.reaches is null, 0, sub2.reaches) + if(sub2.no_reaches is null, 0, sub2.no_reaches) as decs,
                if(sub3.errorless is null, 0, sub3.errorless) as errorless,
                if(sub3.witherror is null, 0, sub3.witherror) as witherror
            from (
                select
                    cal.*, uu.id as user_id, uu.name as user_name
                from (
                    select
                        d.real_date
                    from (
                        SELECT
                            DATE_FORMAT(DATE_SUB(v.c_date, INTERVAL (DAYOFWEEK(DATE_ADD(v.c_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as real_date
                        FROM
                        (SELECT ADDDATE('".$from."' ,t4*10000 + t3*1000 + t2*100 + t1*10 + t0) c_date FROM
                        (SELECT 0 t0 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
                        (SELECT 0 t1 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
                        (SELECT 0 t2 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
                        (SELECT 0 t3 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
                        (SELECT 0 t4 UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4) v
                        WHERE c_date >= '".$from."'  AND c_date <= '".$to."'
                        and DAYOFWEEK(c_date) not in (1,4,7)
                    ) d
                    group by d.real_date
                    order by d.real_date asc
                ) cal, (
                    select
                        u.id, u.name
                    from class_relations cr
                    left join users u on u.id = cr.user_id";

            if($standard == 3) {
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 1
                    and cr.class_id = ".$target."
                    and kc.visible = 1
                    order by u.name asc ";
            } else if($standard == 4) {
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    where cr.flag = 1
                    and kc.over_id = ".$target."
                    and kc.visible = 1
                    group by u.id
                    order by u.name asc ";
            } else { // $standard == 1
                $query .= "
                    left join kavba_classes kc on kc.id = cr.class_id
                    join (
                        select * from class_relations
                        where flag = 1 and user_id = ".$target."
                    ) cr2 on cr2.class_id = cr.class_id
                    where cr.flag = 1
                    and kc.visible = 1
                    order by u.name asc";
            }
            // count(DISTINCT if(si.finish = 1 and si.reach_user_id = p.target_user_id, si.id, null)) as reaches,
            // count(DISTINCT if(sg.finish = 1 and si.reach_user_id = p.target_user_id, sg.id, null)) as lto_reaches,
            $query .= ") uu
                ) sub1
                left join (
                    select
                        DATE_FORMAT(DATE_SUB(s.stick_date,
                            INTERVAL (DAYOFWEEK(DATE_ADD(s.stick_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as real_date,
                        s.user_id as user_id,
                        count(DISTINCT if(s.value = 1, s.id, null)) as positive,
                        count(DISTINCT s.id) as total,
                        count(DISTINCT if(si.finish = 1 and si.reach_date = s.stick_date, si.id, null)) as reaches,
                        count(DISTINCT if(si.finish = 2 and si.reach_date = s.stick_date, si.id, null)) as no_reaches,
                        count(DISTINCT if(sg.finish = 1, sg.id, null)) as lto_reaches,
                        count(DISTINCT d.decision_id) as decs
                    from sticks s
                        left join points p on p.id = s.point_id
                        left join sto_items si on si.id = p.sto_item_id
                        left join sto_groups sg on sg.id = si.sto_group_id
                        left join programs pr on pr.id = si.program_id
                        left join (
                            select
                                decisions.id as decision_id, decisions.point_id,
                                if(p.postponed is null, p.rgst_date, p.postponed) as decide_date
                            from decisions
                            join points p on p.id = decisions.point_id
                            join sto_items on sto_items.id = decisions.sto_item_id
                            join sto_groups on sto_groups.id = sto_items.sto_group_id
                            where sto_items.deleted_at is null
                            and sto_groups.deleted_at is null
                            and if(p.postponed is null, p.rgst_date, p.postponed) >= '".$from."'
                            and if(p.postponed is null, p.rgst_date, p.postponed) <= '".$to."'
                            and (decisions.point_id not in (
                                select tmp.* from (
                                    select id from points
                                    where points.sto_item_id = decisions.sto_item_id
                                    order by if(points.postponed is null, points.rgst_date, points.postponed) desc, points.updated_at desc
                                    limit 1
                                ) as tmp
                            ) and sto_groups.`type` = 0 )
                        ) d on d.point_id = p.id
                        left join class_relations cr on cr.student_id = pr.student_id
                        left join kavba_classes kc on kc.id = cr.class_id
                    where pr.personal = 0
                        and cr.flag = 2
                        and cr.user_id is null
                        and kc.visible = 1
                    and p.deleted_at is null
                    and s.deleted_at is null
                    and s.stick_date >= '".$from."'
                    and s.stick_date <= '".$to."'
                    and DAYOFWEEK(s.stick_date) not in (1,4,7)
                    and si.deleted_at is null
                    and sg.deleted_at is null
                    and pr.deleted_at is null
                    group by real_date, s.user_id
                ) sub2 on sub2.real_date = sub1.real_date and sub2.user_id = sub1.user_id
                left join (
                    select
                        DATE_FORMAT(DATE_SUB(e.rgst_date,
                            INTERVAL (DAYOFWEEK(DATE_ADD(e.rgst_date, INTERVAL +4 DAY))) -7 DAY), '%Y-%m-%d') as real_date,
                        e.user_id,
                        sum(e.`count2`) as errorless, sum(e.`count`) as witherror
                    from user_errors e
                    where e.deleted_at is null
                    and DAYOFWEEK(e.rgst_date) not in (1,4,7)
                    group by real_date, e.user_id
                ) sub3 on sub3.real_date = sub1.real_date and sub3.user_id = sub1.user_id
                where sub1.user_id is not null
                order by sub1.real_date asc, sub1.user_name asc";

            $list = DB::connection($this->myDB)->select(DB::raw($query));
            $res = [];
            if(count($list) > 0) {
                foreach($list as $key => $row) {
                    $label = $row->end_date;
                    if(!array_key_exists($label, $res)) {
                        $res[$label] = [];
                    }
                    $res[$label] [] = $row;
                }
            }

        return $res;
    }
}
