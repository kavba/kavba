<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\KavbaClass;
use App\Models\OverClass;
use App\Models\ClassRelation;

class KlassController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getClasses(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        if(!is_null($request->user)) {
            $returnData['klass'] = KavbaClass::leftJoin('over_classes', 'over_classes.id', 'kavba_classes.over_id')
                ->where('kavba_classes.id', '!=', 0)
                ->selectRaw('kavba_classes.*, over_classes.name as over_name')
                ->orderBy('name', 'asc')
                ->get();
            $returnData['over'] = OverClass::orderBy('name', 'asc')->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getKlass(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        if(!is_null($request->user)) {
            $returnData['list'] = KavbaClass::orderBy('name', 'asc')->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function createKlass(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        $name = $request->name;
        $center = $request->center;
        if(!is_null($name)) {
            $returnData['inserted'] = KavbaClass::create([
                'name'      =>  $name,
                'over_id'   =>  $center
            ]);
            if(!is_null($center)) {
                $center = OverClass::where('id', $center)->first();
                $returnData['inserted']['over_name'] = $center->name;
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function editKlass(Request $request, $id = null) {
        $returnData = [
            'result'    =>  'false',
        ];
        $name = $request->name;
        $center = $request->center;
        if(!is_null($id)) {
            KavbaClass::where('id', $id)->update([
                'name'      =>  $name,
                'over_id'   =>  $center
            ]);
            $returnData['edited'] = KavbaClass::leftJoin('over_classes', 'over_classes.id', 'kavba_classes.over_id')
                ->selectRaw('kavba_classes.*, over_classes.name as over_name')
                ->where('kavba_classes.id', $id)
                ->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function removeKlass(Request $request, $id = null) {
        $returnData = [
            'result'    =>  'false',
        ];
        if(!is_null($id)) {
            KavbaClass::where('id', $id)->delete();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function toggleVisible(Request $request, $id = null) {
        $returnData = [
            'result'    =>  'false',
        ];
        $visible = $request->visible;
        if(!is_null($id)) {
            KavbaClass::where('id', $id)->update([
                'visible'   =>  $visible
            ]);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getOverClasses(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        if(!is_null($request->user)) {
            $returnData['list'] = OverClass::orderBy('name', 'asc')->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function createOverClass(Request $request) {
        $returnData = [
            'result'    =>  'false',
        ];
        $name = $request->name;
        if(!is_null($name)) {
            $returnData['inserted'] = OverClass::create([
                'name'      =>  $name,
            ]);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function editOverClass(Request $request, $id = null) {
        $returnData = [
            'result'    =>  'false',
        ];
        $name = $request->name;
        if(!is_null($id)) {
            OverClass::where('id', $id)->update([
                'name'      =>  $name
            ]);
            $returnData['edited'] = OverClass::where('id', $id)->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function removeOverClass(Request $request, $id = null) {
        $returnData = [
            'result'    =>  'false',
        ];
        if(!is_null($id)) {
            OverClass::where('id', $id)->delete();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }
}
