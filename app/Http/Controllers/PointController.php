<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\Program;
use App\Models\StoItem;
use App\Models\StoGroup;
use App\Models\Point;
use App\Models\Stick;
use App\Models\Decision;
use App\Models\KavbaClass;
use App\Models\ClassRelation;
use App\Models\LongTermGoal;
use App\Models\ShortTermGoal;

class PointController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function createPoint(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $stoItemId = $request->stoItemId;
        $targetUserId = $request->targetUserId;
        $rgstDate = $request->rgstDate;
        $count = $request->count;

        if(!is_null($stoItemId) && !is_null($targetUserId) && !is_null($rgstDate) && !is_null($count)) {
            $stoItem = StoItem::where('id', $stoItemId)
                ->where('finish', 0)
                ->first();
            if(is_null($stoItem)) {
                $returnData['error'] = 'sto 정보 오류';
                $returnData['error_code'] = 'S01';
                return response()->json($returnData);
            }
            $program = Program::where('id', $stoItem->program_id)->first();
            if(!Student::checkManager($request->user->id, $program->student_id)) {
                $returnData['error'] = '정보 수정 권한 없음';
                $returnData['error_code'] = 'A01';
                return response()->json($returnData);
            }

            $returnData['point'] = Point::create([
                'target_user_id'    =>  $targetUserId,
                'sto_item_id'       =>  $stoItemId,
                'rgst_date'         =>  $rgstDate,
                'count'             =>  $count,
                'last_update_user_id' =>  $request->user->id,
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function editPoint(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $stoItemId = $request->stoItemId;
        $targetUserId = $request->targetUserId;
        $rgstDate = $request->rgstDate;
        $count = $request->count;
        $status = $request->status;

        if(!is_null($id) && !is_null($stoItemId) && !is_null($targetUserId) && !is_null($rgstDate) && !is_null($count) && !is_null($status)) {
            $stoItem = StoItem::where('id', $stoItemId)
                ->where('finish', 0)
                ->first();
            if(is_null($stoItem)) {
                $returnData['error'] = 'sto 정보 오류';
                $returnData['error_code'] = 'S01';
                return response()->json($returnData);
            }
            $program = Program::where('id', $stoItem->program_id)->first();
            if(!Student::checkManager($request->user->id, $program->studentId)) {
                $returnData['error'] = '정보 수정 권한 없음';
                $returnData['error_code'] = 'A01';
                return response()->json($returnData);
            }
            $point = Point::where('id', $id)->first();
            if($point->count != $count) {
                // **reach, decision 갱신하기
            }
            if($status == 1) {
                // **포인트 완료처리
            }

            $point->update([
                'target_user_id'    =>  $targetUserId,
                'rgst_date'         =>  $rgstDate,
                'count'             =>  $count,
                'status'            =>  $status
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function removePoint(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $tmp = Point::where('id', $id);
            $point = $tmp->first();
            // **상위 sto_item의 reach 값 갱신
            Stick::where('point_id', $id)->delete();
            // 해당 스틱 전부 제거
            if(!is_null($point)) {
                // 디시전 재계산 버튼으로 대채
                // StoItem::setDecision($request->user, $point);
                // decision 재계산
            }
            $tmp->delete();
            $sto = StoItem::where('id', $point->sto_item_id);
            $stoItem = $sto->first();
            $isReach = StoItem::isReach($stoItem);
            if($isReach != "") {
                $sto->update([ 'total_reach' => 1, 'finish' => 1, 'reach_date' => $isReach->real_date, 'reach_user_id' => $isReach->target_user_id ]);
            } else {
                $sto->update([ 'total_reach' => 0, 'finish' => 0, 'reach_date' => null, 'reach_user_id' => null ]);
            }
            $returnData['stoItem'] = $sto->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPoint(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $returnData['point'] = Point::where('id', $id)->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsAndSticksList(Request $request, $stoItemId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($stoItemId)) {
            $points = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                ->where('points.sto_item_id', $stoItemId)
                ->selectRaw('users.name as target_user_name, points.*,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    sum(if(sticks.value = 1, 1, 0)) as positive,
                    sum(if(sticks.value = 0, 1, 0)) as negative,
                    sum(if(sticks.value = 2, 1, 0)) as pass,
                    count(sticks.id) as ing
                ')
                ->groupBy('points.id')
                ->orderBy('real_date', 'desc')
                ->orderBy('points.updated_at', 'desc')
                ->get();
            foreach($points as $point) {
                $point['stick'] = Stick::where('point_id', $point->id)->get();
                $point['history'] = Stick::leftJoin('points', 'points.id', 'sticks.point_id')
                    ->leftJoin('users', 'users.id', 'sticks.user_id')
                    ->where('point_id', $point->id)
                    ->selectRaw('
                        sticks.stick_date, users.name as user_name, sticks.user_id, count(distinct sticks.id) as stick_cnt,
                        sum(if(sticks.value = 1, 1, 0)) as positive,
                        sum(if(sticks.value = 0, 1, 0)) as negative,
                        sum(if(sticks.value = 2, 1, 0)) as pass
                    ')
                    ->groupBy('sticks.user_id', 'sticks.stick_date')
                    ->orderByRaw('sticks.stick_date desc, users.name asc')
                    ->get();
            }
            $sto = StoItem::leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->where('sto_items.id', $stoItemId)
                ->selectRaw('sto_items.*, sto_groups.type as reach_type')
                ->first();
            $sto['points'] = [];
            $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
            $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
            $returnData['stoItem'] = $sto;

            $returnData['points'] = $points;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointAndSticks(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $returnData['point'] = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->where('points.id', $id)
                ->selectRaw('users.name as target_user_name, points.*')
                ->first();
            $returnData['sticks'] = Stick::where('point_id', $id)->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPoints(Request $request, $stoItemId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($stoItemId)) {
            $points = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->where('points.sto_item_id', $stoItemId)
                ->selectRaw('users.name as target_user_name, points.*')
                ->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsAndSticks(Request $request, $stoItemId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($stoItemId)) {
            $sub = DB::table('users')->select('id', 'name');
            $points = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                ->leftJoin('decisions', 'decisions.point_id', 'points.id')
                ->leftJoinSub($sub, 'u2', function($j) {
                    $j->on('u2.id', '=', 'points.last_update_user_id');
                })
                ->where('points.sto_item_id', $stoItemId)
                ->where('points.status', 1)
                ->whereNull('points.deleted_at')
                ->whereNull('sticks.deleted_at')
                ->selectRaw('users.name as target_user_name, points.*,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    sum(if(sticks.value = 1, 1, 0)) as positive,
                    sum(if(sticks.value = 0, 1, 0)) as negative,
                    sum(if(sticks.value = 2, 1, 0)) as pass,
                    count(sticks.id) as ing,
                    group_concat(distinct decisions.id) as decision_id,
                    group_concat(distinct u2.name) as decision_user_name
                ')
                ->groupBy('points.id')
                ->groupBy('decisions.id')
                ->orderBy('real_date', 'asc')
                ->orderBy('points.updated_at', 'asc')
                ->get();
            foreach($points as $point) {
                $point['stick'] = Stick::where('point_id', $point->id)->get();
            }
            $sto = StoItem::leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('students', 'students.id', 'programs.student_id')
                ->where('sto_items.id', $stoItemId)
                ->selectRaw("sto_items.*,
                    case
                        when sto_items.prompt_code = 1 then '독립(independent)'
                        when sto_items.prompt_code = 2 then '전체 신체(full physical)'
                        when sto_items.prompt_code = 3 then '부분 신체(partial physical)'
                        when sto_items.prompt_code = 4 then '전체 음성(full aditory)'
                        when sto_items.prompt_code = 5 then '부분 음성(partial aditory)'
                        when sto_items.prompt_code = 6 then '몸짓(gestual)'
                        when sto_items.prompt_code = 7 then '글자(textual)'
                        when sto_items.prompt_code = 8 then '그림(picture)'
                    end as prompt,
                    long_term_goals.name as domain_name,
                    students.name as student_name
                ")
                ->first();
            $sto['points'] = [];
            $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
            $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
            $returnData['stoItem'] = $sto;
            $returnData['stoGroup'] = StoGroup::where('id', $sto->sto_group_id)->whereNull('deleted_at')->first();
            $returnData['program'] = Program::leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('students', 'students.id', 'programs.student_id')
                ->where('programs.id', $sto->program_id)
                ->selectRaw("
                    programs.id, students.name as student_name, long_term_goals.name as domain_name
                ")
                ->first();
            $returnData['points'] = $points;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsAndSticksSto(Request $request, $stoItemId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($stoItemId)) {
            $temp = StoItem::where('id', $stoItemId)->first();
            $stoItems = StoItem::where('sto_group_id', $temp->sto_group_id)
                ->whereIn('finish', [0, 1, 2])
                ->orderBy('order', 'asc')
                ->get();
            $stoGroup = StoGroup::where('id', $temp->sto_group_id)->whereNull('deleted_at')->first();
            if($stoGroup->type != 0) {
                $list = DB::connection($this->myDB)->select(DB::raw('
                    select
                        sum(if(s.reaches is null, 0, s.reaches)) over(order by s.updated_at) as stack,
                        s.*
                    from (
                        select
                            sum(if(sub.reach > 0, 1, 0)) as reaches, sub.real_date,
                            group_concat(distinct sub.sto_item_id) as sto_item_ids,
                            group_concat(distinct sub.id) as points,
                            group_concat(distinct sub.target_user_name) as directors,
                            group_concat(distinct sub.decision_id) as decision_id,
                            group_concat(distinct sub.decision_user_name) as decision_user_name,
                            sub.updated_at
                        from
                            (
                                select
                                    p.reach, p.status, p.sto_item_id, p.id, u.name as target_user_name,
                                    d.id as decision_id, u2.name as decision_user_name,
                                    if(p.postponed is null, p.rgst_date, p.postponed) as real_date,
                                    p.updated_at
                                from points p
                                    left join users u on u.id = p.target_user_id
                                    left join sto_items si on si.id = p.sto_item_id
                                    left join decisions d on d.point_id = p.id
                                    left join users u2 on u2.id = p.last_update_user_id
                                    where si.sto_group_id = '.$temp->sto_group_id.'
                                    and si.id = '.$stoItemId.'
                                    and p.status > 0
                                    and p.deleted_at is null
                                    order by p.updated_at asc
                            ) sub
                        group by sub.real_date, sub.id) s
                    order by s.real_date asc, s.updated_at asc
                '));
                foreach($list as $item) {
                    $ids = explode(",", $item->sto_item_ids);
                    $item->info = [];
                    foreach($ids as $sto_item_id) {
                        $sto = StoItem::where('id', $sto_item_id)
                            ->whereNull('deleted_at')
                            ->selectRaw('sto_items.id, sto_items.target,
                                case
                                    when sto_items.prompt_code = 1 then "독립(independent)"
                                    when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                                    when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                                    when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                                    when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                                    when sto_items.prompt_code = 6 then "몸짓(gestual)"
                                    when sto_items.prompt_code = 7 then "글자(textual)"
                                    when sto_items.prompt_code = 8 then "그림(picture)"
                                end as prompt
                            ')
                            ->first();
                        $sto->target = (!is_null($sto->target) ? json_decode($sto->target) : []);
                        $item->info [] = $sto;
                    }
                }
                $returnData['stoItems'] = $list;
            } else {
                foreach($stoItems as $sto) {
                    $sub = DB::table('users')->select('id', 'name');
                    $sto['points'] = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                        ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                        ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                        ->leftJoin('decisions', 'decisions.point_id', 'points.id')
                        ->leftJoinSub($sub, 'u2', function($j) {
                            $j->on('u2.id', '=', 'points.last_update_user_id');
                        })
                        ->where('points.sto_item_id', $sto->id)
                        ->where('points.status', 1)
                        ->whereNull('points.deleted_at')
                        ->whereNull('sticks.deleted_at')
                        ->selectRaw('
                            points.id, points.target_user_id, points.sto_item_id, points.count, points.reach, points.status, points.last_update_user_id,
                            points.rgst_date,
                            points.created_at, points.updated_at,
                            users.name as target_user_name,
                            if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                            sum(if(sticks.value = 1, 1, 0)) as positive,
                            sum(if(sticks.value = 0, 1, 0)) as negative,
                            sum(if(sticks.value = 2, 1, 0)) as pass,
                            count(sticks.id) as ing,
                            case
                                when sto_items.prompt_code = 1 then "독립(independent)"
                                when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                                when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                                when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                                when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                                when sto_items.prompt_code = 6 then "몸짓(gestual)"
                                when sto_items.prompt_code = 7 then "글자(textual)"
                                when sto_items.prompt_code = 8 then "그림(picture)"
                            end as prompt,
                            group_concat(distinct decisions.id) as decision_id,
                            group_concat(distinct u2.name) as decision_user_name
                        ')
                        ->groupBy('points.id')
                        ->groupBy('decisions.id')
                        ->orderBy('real_date', 'asc')
                        ->orderBy('points.updated_at', 'asc')
                        ->get();

                    $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
                    $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
                }
                $returnData['stoItems'] = $stoItems;
            }
            $returnData['stoGroup'] = $stoGroup;
            $returnData['program'] = Program::leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('students', 'students.id', 'programs.student_id')
                ->where('programs.id', $temp->program_id)
                ->selectRaw("
                    programs.id, students.name as student_name, long_term_goals.name as domain_name
                ")
                ->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsAndSticksGroup(Request $request, $groupId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($groupId)) {
            $stoGroup = StoGroup::where('id', $groupId)->whereNull('deleted_at')->first();
            $stoItems = StoItem::where('sto_group_id', $groupId)
                ->whereIn('finish', [0, 1, 2])
                ->selectRaw('sto_items.*,
                    case
                        when sto_items.prompt_code = 1 then "독립(independent)"
                        when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                        when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                        when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                        when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                        when sto_items.prompt_code = 6 then "몸짓(gestual)"
                        when sto_items.prompt_code = 7 then "글자(textual)"
                        when sto_items.prompt_code = 8 then "그림(picture)"
                    end as prompt')
                ->orderBy('order', 'asc')
                ->get();

            if($stoGroup->type != 0) {
                $list = DB::connection($this->myDB)->select(DB::raw('
                    select
                        sum(if(s.reaches is null, 0, s.reaches)) over(order by s.updated_at) as stack,
                        s.*
                    from (
                        select
                            sum(if(sub.reach > 0, 1, 0)) as reaches, sub.real_date, sub.updated_at,
                            group_concat(distinct sub.sto_item_id) as sto_item_ids,
                            group_concat(distinct sub.id) as points,
                            group_concat(distinct sub.target_user_name) as directors,
                            group_concat(distinct sub.decision_id) as decision_id,
                            group_concat(distinct sub.decision_user_name) as decision_user_name
                        from
                            (
                                select
                                    p.reach, p.status, p.sto_item_id, p.id, u.name as target_user_name,
                                    d.id as decision_id, u2.name as decision_user_name,
                                    if(p.postponed is null, p.rgst_date, p.postponed) as real_date,
                                    p.updated_at
                                from points p
                                    left join users u on u.id = p.target_user_id
                                    left join sto_items si on si.id = p.sto_item_id
                                    left join decisions d on d.point_id = p.id
                                    left join users u2 on u2.id = p.last_update_user_id
                                    where si.sto_group_id = '.$groupId.'
                                    and p.status > 0
                                    and p.deleted_at is null
                                    order by p.updated_at asc
                            ) sub
                        group by sub.real_date, sub.id
                        ) s
                    order by s.real_date asc, s.updated_at asc
                '));
                // $sub = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                //     ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                //     ->where('sto_items.sto_group_id', $groupId)
                //     ->whereNull('points.deleted_at')
                //     ->selectRaw('
                //         points.status, points.sto_item_id, points.id, users.name as target_user_name,
                //         if(points.postponed is null, points.rgst_date, points.postponed) as real_date
                //     ');
                // $sub2 = DB::table(DB::raw("({$sub->toSql()}) as sub"))
                //     ->mergeBindings($sub->getQuery())
                //     ->selectRaw('
                //         sum(if(sub.status = 1, 1, 0)) as reaches, sub.real_date,
                //         group_concat(distinct sub.sto_item_id) as sto_item_ids,
                //         group_concat(distinct sub.id) as points,
                //         group_concat(distinct sub.target_user_name) as directors
                //     ')
                //     ->groupBy('sub.real_date');

                // $list = DB::table(DB::raw("({$sub2->toSql()}) as sub2"))
                //     ->selectRaw('
                //             sub2.*,
                //             sum(if(sub2.reaches is null, 0, sub2.reaches)) over(order by sub2.real_date) as stack
                //         ')
                //         ->orderBy('sub2.real_date', 'asc')
                //         ->get();
                foreach($list as $item) {
                    $ids = explode(",", $item->sto_item_ids);
                    $item->info = [];
                    foreach($ids as $sto_item_id) {
                        $sto = StoItem::where('id', $sto_item_id)
                            ->whereNull('deleted_at')
                            ->selectRaw('sto_items.id, sto_items.target,
                                case
                                    when sto_items.prompt_code = 1 then "독립(independent)"
                                    when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                                    when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                                    when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                                    when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                                    when sto_items.prompt_code = 6 then "몸짓(gestual)"
                                    when sto_items.prompt_code = 7 then "글자(textual)"
                                    when sto_items.prompt_code = 8 then "그림(picture)"
                                end as prompt
                            ')
                            ->first();
                        $sto->target = (!is_null($sto->target) ? json_decode($sto->target) : []);
                        $item->info [] = $sto;
                    }
                }
                $returnData['stoItems'] = $list;
            } else {
                foreach($stoItems as $sto) {
                    $sub = DB::table('users')->select('id', 'name');
                    $sto['points'] = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                        ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                        ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                        ->leftJoin('decisions', 'decisions.point_id', 'points.id')
                        ->leftJoinSub($sub, 'u2', function($j) {
                            $j->on('u2.id', '=', 'points.last_update_user_id');
                        })
                        ->where('points.sto_item_id', $sto->id)
                        ->where('points.status', 1)
                        ->whereNull('points.deleted_at')
                        ->whereNull('sticks.deleted_at')
                        ->selectRaw('
                            points.id, points.target_user_id, points.sto_item_id, points.count, points.reach, points.status, points.last_update_user_id,
                            points.rgst_date,
                            points.created_at, points.updated_at,
                            users.name as target_user_name,
                            if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                            sum(if(sticks.value = 1, 1, 0)) as positive,
                            sum(if(sticks.value = 0, 1, 0)) as negative,
                            sum(if(sticks.value = 2, 1, 0)) as pass,
                            count(sticks.id) as ing,
                            case
                                when sto_items.prompt_code = 1 then "독립(independent)"
                                when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                                when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                                when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                                when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                                when sto_items.prompt_code = 6 then "몸짓(gestual)"
                                when sto_items.prompt_code = 7 then "글자(textual)"
                                when sto_items.prompt_code = 8 then "그림(picture)"
                            end as prompt,
                            group_concat(distinct decisions.id) as decision_id,
                            group_concat(distinct u2.name) as decision_user_name
                        ')
                        ->groupBy('points.id')
                        ->groupBy('decisions.id')
                        ->orderBy('real_date', 'asc')
                        ->orderBy('points.updated_at', 'asc')
                        ->get();

                    $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
                    $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
                }
                $returnData['stoItems'] = $stoItems;
            }
            $returnData['stoGroup'] = $stoGroup;
            $returnData['program'] = Program::leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('students', 'students.id', 'programs.student_id')
                ->where('programs.id', $stoGroup->program_id)
                ->selectRaw("
                    programs.id, students.name as student_name, long_term_goals.name as domain_name
                ")
                ->first();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsAndSticksProgram(Request $request, $programId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($programId)) {
            $stoItems = StoItem::where('program_id', $programId)
                ->whereIn('finish', [0, 1, 2])
                ->orderBy('order', 'asc')
                ->get();

            foreach($stoItems as $sto) {
                $sto['points'] = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                    ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                    ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                    ->where('points.sto_item_id', $sto->id)
                    ->where('points.status', 1)
                    ->whereNull('points.deleted_at')
                    ->whereNull('sticks.deleted_at')
                    ->selectRaw('
                        points.id, points.target_user_id, points.sto_item_id, points.count, points.reach, points.status, points.last_update_user_id,
                        if(points.postponed is null, points.rgst_date, points.postponed) as rgst_date,
                        points.created_at, points.updated_at,
                        users.name as target_user_name,
                        if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                        sum(if(sticks.value = 1, 1, 0)) as positive,
                        sum(if(sticks.value = 0, 1, 0)) as negative,
                        sum(if(sticks.value = 2, 1, 0)) as pass,
                        count(sticks.id) as ing
                    ')
                    ->groupBy('points.id')
                    ->orderBy('real_date', 'asc')
                    ->orderBy('rgst_date', 'asc')
                    ->get();
                $sto['rows'] = (!is_null($sto->rows) ? json_decode($sto->rows) : []);
                $sto['target'] = (!is_null($sto->target) ? json_decode($sto->target) : []);
            }
            $returnData['stoItems'] = $stoItems;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getPointsByTarget(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $targetUserId = ($request->targetUserId == 0 ? $request->user->id : $request->targetUserId);
        $userId = $request->user->id;
        $userLv = $request->user->level;
        $rgstDate = $request->rgstDate;
        if(!is_null($targetUserId) || $targetUserId == 0) {
            if(!is_null($rgstDate)) {
                $points = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                // ->orWhereRaw('(points.target_user_id = '.$targetUserId.' or programs.user_id = '.$userId.')')
                ->when($userLv < 2, function($q) use($targetUserId, $userId) {
                    return $q->whereRaw('(points.target_user_id = '.$targetUserId.' or programs.user_id = '.$userId.')');
                })
                ->when($userLv == 2, function($q) use($targetUserId, $userId, $rgstDate) {
                    $tmp = ClassRelation::where('class_relations.flag', 1)
                        ->where('class_relations.user_id', $userId)
                        ->where('class_relations.class_id', '!=', 0)
                        ->select('class_relations.class_id')
                        ->get();

                    $cTxt = "";
                    if(count($tmp) > 0) {
                        $cTxt .= "(programs.personal = 0 and class_relations.class_id in (";
                        foreach($tmp as $i=>$tItem) {
                            if($i > 0) $cTxt .= ",";
                            $cTxt .= $tItem->class_id;
                        }
                        $cTxt .= ")) or ";
                    }

                    return $q->leftJoin('class_relations', function($j) use($userId) {
                        $j->on('class_relations.student_id', 'programs.student_id')
                            ->where('class_relations.flag', 2)
                            ->whereNull('class_relations.user_id');
                    })
                    ->whereRaw("(".$cTxt."
                        (programs.personal = 1 and (points.target_user_id = ".$targetUserId." or programs.user_id = ".$userId.")))
                        and ((points.postponed is null and points.rgst_date = '".$rgstDate."') or
                        (points.postponed is not null and points.postponed = '".$rgstDate."'))
                    ");
                    // return $q->whereIn('programs.student_id', $c)
                    //     ->orWhereRaw('(points.target_user_id = '.$targetUserId.' or programs.user_id = '.$userId.')');

                    // return $q->join('class_relations', function($j) use($userId) {
                    //     $j->on('class_relations.student_id', 'programs.student_id')
                    //         ->where('class_relations.flag', 2)
                    //         ->whereNull('class_relations.user_id')
                    //         //->where('programs.personal', 0)
                    //         ->whereRaw('class_relations.class_id in (
                    //             select cr2.class_id from class_relations cr2
                    //             where cr2.user_id = '.$userId.'
                    //             and cr2.flag = 1
                    //             group by cr2.class_id
                    //         )');
                    // })
                    // ->orWhereRaw('(points.target_user_id = '.$targetUserId.' or programs.user_id = '.$userId.')');
                })
                ->when($userLv > 2, function($q) {
                    return $q;
                })
                ->whereIn('sto_items.finish', [0, 1])
                ->whereNotNull('long_term_goals.id')
                ->whereRaw("
                    ((points.postponed is null and points.rgst_date = '".$rgstDate."') or
                        (points.postponed is not null and points.postponed = '".$rgstDate."'))
                ")
                ->selectRaw('points.*, users.name as user_name, programs.student_id, sto_items.standard_perc, sto_items.standard_cnt,
                    long_term_goals.name as lto_name, sto_items.program_id, sto_items.name as sto_item_name, sto_items.finish,
                    sto_items.prompt_memo, sto_items.schedule_memo, sto_items.program_memo, sto_items.target, sto_groups.name as sto_group_name,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    case
                        when sto_items.prompt_code = 1 then "독립(independent)"
                        when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                        when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                        when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                        when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                        when sto_items.prompt_code = 6 then "몸짓(gestual)"
                        when sto_items.prompt_code = 7 then "글자(textual)"
                        when sto_items.prompt_code = 8 then "그림(picture)"
                    end as prompt, sto_groups.type as reach_type')
                ->groupBy('points.id')
                ->orderBy('real_date', 'desc')
                ->orderBy('points.id', 'desc')
                ->get();
            } else {
                $points = Point::leftJoin('users', 'users.id', 'points.target_user_id')
                ->leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->whereNotNull('long_term_goals.id')
                ->when($userLv < 10, function($q) use($targetUserId, $userId) {
                    return $q->whereRaw('(points.target_user_id = '.$targetUserId.' or points.last_update_user_id = '.$userId.')');
                })
                ->when($userLv >= 10, function($q) {
                    return $q;
                })
                ->selectRaw('points.*, users.name as user_name, programs.student_id, sto_items.standard_perc, sto_items.standard_cnt,
                    long_term_goals.name as lto_name, sto_items.program_id, sto_items.name as sto_item_name, sto_items.finish,
                    sto_items.prompt_memo, sto_items.schedule_memo, sto_items.program_memo, sto_items.target, sto_groups.name as sto_group_name,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    case
                        when sto_items.prompt_code = 1 then "독립(independent)"
                        when sto_items.prompt_code = 2 then "전체 신체(full physical)"
                        when sto_items.prompt_code = 3 then "부분 신체(partial physical)"
                        when sto_items.prompt_code = 4 then "전체 음성(full aditory)"
                        when sto_items.prompt_code = 5 then "부분 음성(partial aditory)"
                        when sto_items.prompt_code = 6 then "몸짓(gestual)"
                        when sto_items.prompt_code = 7 then "글자(textual)"
                        when sto_items.prompt_code = 8 then "그림(picture)"
                    end as prompt, sto_groups.type as reach_type')
                ->groupBy('points.id')
                ->orderBy('real_date', 'asc')
                ->orderBy('points.id', 'asc')
                ->get();
            }

            $p = [];
            $students = [];
            $studentInfo = [];
            foreach($points as $point) {
                $point['stick'] = Stick::where('point_id', $point->id)->orderBy('order', 'asc')->whereNull('deleted_at')->get();

                if(!in_array($point->student_id, $students)) {
                    $s = Student::where('id', intval($point->student_id))->first();
                    $students [] = $point->student_id;
                    $studentInfo [] = [
                        'id' => $s->id,
                        'name'  =>  $s->name,
                        'birth_date'    =>  $s->birth_date
                    ];
                }
                $point['target'] = (!is_null($point->target) ? json_decode($point->target) : []);
                $p[$point->student_id] [] = $point;
            }
            //$returnData['points'] = $points;
            $returnData['points'] = $p;
            $returnData['students'] = $studentInfo;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getStudentLtoPoints(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $ltoId = $request->ltoId;
        $studentId = $request->studentId;
        if(!is_null($ltoId) && !is_null($studentId)) {
            // **특정 lto, sto 에 대해서만 points 조회
            // $ltos = Program::where('lto_id', $ltoId)
            //     ->where('student_id', $studentId)
            //     ->get();

            // $points = Point::where('sto_item_id', $stoItemId)->get();
            // foreach($points as $point) {
            //     $point['stick'] = Stick::where('point_id', $point->id)->get();
            // }
            // $returnData['points'] = $points;
            // $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function setStatus(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $status = $request->status;
        if(!is_null($id) && (!is_null($status) && ($status == 0 || $status == 1 || $status == 2))) {
            $point = Point::where('id', $id)->whereNull('deleted_at')->first();
            $sto = StoItem::where('sto_items.id', $point->sto_item_id);
            $stoItem = $sto->leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->selectRaw('sto_items.*, programs.user_id as master_id')
                ->whereNull('sto_items.deleted_at')
                ->whereNull('programs.deleted_at')
                ->first();
            $stoGroup = StoGroup::where('id', $stoItem->sto_group_id)->whereNull('deleted_at')->first();
            $nowDate = date('Y-m-d');

            if(!is_null($point) && !is_null($stoItem) && !is_null($stoGroup)) {
                if($stoGroup->type == 0) {
                    $returnData['reach'] = StoItem::calcReachOnPoint($id);
                    //**** 도달 연속성으로 도달여부 확인 > dispatch 예정
                    $isReach = StoItem::isReach($stoItem);
                    if($isReach != "") {
                        $stoItem->update([
                            'total_reach' => 1,
                            'finish'    =>  1,
                            'reach_date' => $isReach->real_date,
                            'reach_user_id' => $isReach->target_user_id,
                            'master_id' =>  $stoItem->master_id
                        ]);
                        $returnData['total_reach'] = 1;
                    } else {
                        $stoItem->update([
                            'total_reach' => 0,
                            'finish'    =>  0,
                            'reach_date' => null,
                            'reach_user_id' => null,
                            'master_id' => null
                        ]);
                        $returnData['total_reach'] = 0;
                    }
                    $point->update([ 'status' => $status ]);
                    // **decision
                    //$returnData['decision'] = StoItem::setDecision($request->user, $point);
                    $last =  Point::where('sto_item_id', $point->sto_item_id)
                        ->whereNull('points.deleted_at')
                        ->selectRaw('
                            points.*,
                            if(points.postponed is null, points.rgst_date, points.postponed) as real_date
                        ')
                        ->orderBy('real_date', 'desc')
                        ->orderBy('created_at', 'desc')
                        ->limit(1)
                        ->first();
                    if($status > 0) {
                        if(!is_null($last) && $last->id == $id) {
                            $returnData['decision'] = StoItem::setDecisionByLastPoint($request->user, $point);
                        }
                    } else {
                        $td = Decision::where('point_id', $point->id);
                        $dec = $td->first();
                        if(!is_null($td->first())) {
                            Decision::where('point_id', $point->id)->delete();
                            $sto->update([
                                'decision'  =>  ($stoItem->decision - 1 >= 0 ? $stoItem->decision - 1 : 0)
                            ]);
                        }
                        $returnData['decision'] = 0;
                    }
                } else {
                    $returnData['reach'] = StoItem::calcReachOnStackPoint($id, $stoItem->master_id);
                    if($status > 0) {
                        if($point->status == 0) {
                            $point->update([ 'status' => $status ]);
                        }
                    } else { // 0으로 전환시
                        if($point->status == 1) { // 도달해서 완료가 된 포인트라면,
                            $lastStick = Stick::where('point_id', $point->id)
                                ->whereNull('deleted_at')
                                ->orderBy('order', 'desc')
                                ->limit(1)
                                ->first();
                            if($lastStick->value == 1) {
                                $returnData['deleted'] = $lastStick->id;
                                $lastStick->delete();
                            }
                            $point->update([ 'reach' => 0, 'status' => 0 ]);
                            $stoItem->update([
                                'total_reach' => 0,
                                'finish'    => 0,
                                'reach_date' => null,
                                'reach_user_id' => null
                            ]);
                            $returnData['total_reach'] = 0;
                            Decision::where('sto_item_id', $stoItem->id)
                                ->where('point_id', $point->id)
                                ->delete(); // 이 포인트로 추가되어있는 디씨전 제거
                        } else {
                            $point->update([ 'reach' => 0, 'status' => 0 ]);
                        }
                    }
                }

                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function setReach(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $reach = $request->reach;
        if(!is_null($id) && (!is_null($reach) && ($reach == 0 || $reach == 1))) {
            $point = Point::where('id', $id)->whereNull('deleted_at')->first();
            $stoItem = StoItem::where('id', $point->sto_item_id)->whereNull('deleted_at')->first();
            $stoGroup = StoGroup::where('id', $stoItem->sto_group_id)->whereNull('deleted_at')->first();
            if(!is_null($point) && !is_null($stoItem) && !is_null($stoGroup)) {
                $nowDate = date('Y-m-d');
                if($reach == 0) {
                    if($stoGroup->type == 0) {
                        if($point->reach == 2) {
                            $point->update([ 'reach' => 0 ]);
                            $returnData['reach'] = 0;
                        } else {
                            $returnData['reach'] = 1;
                        }
                        $returnData['status'] = $point->status;
                    } else {
                        if($point->status == 2) { // 강제 도달시킨 상태만 미도달로 전환 가능
                            $point->update([ 'reach' => 0, 'status' => 0 ]);
                            $stoItem->update([ 'total_reach' => 0, 'finish' => 0, 'reach_date' => null, 'reach_user_id' => null ]);
                            Decision::where('sto_item_id', $stoItem->id)
                                ->where('point_id', $point->id)
                                ->delete();
                            $returnData['total_reach'] = 0;
                            $returnData['reach'] = 0;
                            $returnData['status'] = 0;
                        }
                    }
                } else if($reach == 1) {
                    if($stoGroup->type == 0) {
                        //$returnData['reach'] = StoItem::calcReachOnPoint($id);
                        $point->update([ 'reach' => 2 ]);
                        $returnData['reach'] = 2;
                        $returnData['status'] = $point->status;
                    } else {
                        $point->update([ 'reach' => 2, 'status' => 2 ]);
                        $stoItem->update([ 'total_reach' => 1, 'finish' => 1, 'reach_date' => $nowDate, 'reach_user_id' => $point->target_user_id  ]);
                        $d = Decision::where('sto_item_id', $stoItem->id)->where('point_id', $point->id)->first();
                        if(is_null($d)) {
                            Decision::create([
                                'decide_user_id'    =>  $request->user->id,
                                'sto_item_id'       =>  $stoItem->id,
                                'point_id'          =>  $point->id
                            ]);
                        }
                        $returnData['total_reach'] = 1;
                        $returnData['reach'] = 2;
                        $returnData['status'] = 2;
                    }
                }
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function resetDate(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        $postponed = $request->postponed;
        $targetId = $request->target;

        if(!is_null($id) && !is_null($postponed)) {
            $point = Point::where('id', $id)->first();
            if(!is_null($point)) {
                // if(!is_null($point->postponed) && ($point->postponed != $postponed || ($point->postponed == $postponed && $point->target_user_id != $targetId && !is_null($targetId)))) {
                //     $tmp = Stick::where('point_id', $point->id);
                //     $tmp->where('stick_date', $point->postponed)->where('target_user_id', $point->target_user_id)->delete();
                //     $sticks = $tmp->orderBy('order', 'asc')->get();
                //     foreach($sticks as $i=>$s) {
                //         $s->update([ 'order' => $i ]);
                //     }
                // }

                if(is_null($targetId) || $targetId == $point->target_user_id) {
                    $point->update([ 'postponed' => $postponed ]);
                } else {
                    $point->update([
                        'postponed'         => $postponed,
                        'target_user_id'    =>  $targetId
                    ]);
                    // Stick::where('point_id', $point->id)
                    //     ->update([ 'user_id' => $targetId ]);
                }
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function duplicate(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $point = Point::where('id', $id)->first();
            $stoItem = StoItem::where('id', $point->sto_item_id)->first();
            $stoGroup = StoGroup::where('id', $stoItem->sto_group_id)->first();
            if(!is_null($point)) {
                unset($point->id);
                $returnData['inserted'] = Point::create([
                    'target_user_id'    =>  $point->target_user_id,
                    'sto_item_id'       =>  $point->sto_item_id,
                    'rgst_date'         =>  $point->postponed ? $point->postponed : $point->rgst_date,
                    'count'             =>  $point->count,
                    'last_update_user_id' =>  $point->last_update_user_id
                ]);
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }
}
