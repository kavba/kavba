<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\Program;
use App\Models\StoGroup;
use App\Models\StoItem;
use App\Models\Point;
use App\Models\Stick;
use App\Models\KavbaClass;
use App\Models\LongTermGoal;
use App\Models\ShortTermGoal;
use App\Models\ClassRelation;
use App\Models\AssignProgram;

class ProgramController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function createProgram(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];
        $classId = $request->classId;
		$ltoId = $request->ltoId;
        $studentId = $request->studentId;
        $classRelationId = $request->classRelationId;
        $buildAll = $request->buildAll;
		if(!is_null($studentId) && !is_null($classId)) {
            // 담당자 체크
            // $ck = ClassRelation::where('id', $classRelationId)->first();
	        // if(is_null($ck) || ($ck->class_id && !Student::checkManager($request->user, $studentId))) {
			// 	if($request->user->level <= 2) {
			// 		$returnData['error'] = '프로그램 권한 없음';
			// 		return response()->json($returnData);
			// 	}
            // }
            // 개별인지 판단
            $user = $request->user;
            if($buildAll == "true") {
                Program::buildAllLtosForStudent($request->user->id, $studentId, $classId);
            } else {
				if(is_null($ltoId)) {
					$returnData['error'] = '파라미터 부재';
				} else {
					$returnData['inserted'] = Program::create([
						'user_id'		    =>	$request->user->id,
						'student_id'	    =>	$studentId,
                        'lto_id'		    =>	$ltoId,
                        'personal'          =>  ($classId == 0 ? 1 : 0)
					]);
				}
            }

			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function ducheckProgramLto(Request $request) {
	    $ltoId = $request->ltoId;
		$studentId = $request->studentId;

	    $duck = Program::where('user_id', $request->user->id)
			->where('student_id', $studentId)
			->where('lto_id', $ltoId)
			->count();
		return response()->json($duck);
    }

    public function editProgram(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
		$ltoId = $request->ltoId;
		$assignId = $request->assignId;
		if(!is_null($id) && !is_null($ltoId)) {
			$program = Program::where('id', $id)->first();
			// 담당자 체크
	        // $ck = ClassRelation::where('id', $program->class_relation_id)->first();
	        // if(is_null($ck) || ($ck->class_id && !Student::checkManager($request->user, $program->student_id))) {
		    //     if($request->user->level <= 2) {
			// 		$returnData['error'] = '프로그램 권한 없음';
			// 		return response()->json($returnData);
			// 	}
            // }
            if($request->user->level < 100) {
                $returnData['error'] = '100';
                return response()->json($returnData);
            }
			if(is_null($assignId) || $program->user_id == $assignId) {
				Program::where('id', $id)->update([
					'lto_id'		=>	$ltoId
				]);
			} else {
				AssignProgram::create([
					'program_id'	=>	$id,
					'from_user_id'	=>	$program->user_id,
					'to_user_id'	=>	$assignId
				]);
				Program::where('id', $id)->update([
					'lto_id'		=>	$ltoId,
					'user_id'		=>	$assignId
				]);
			}

			$returnData['result'] = 'true';
		}

        return response()->json($returnData);
    }

    public function removeProgram(Request $request, $id = null) {
	    $returnData = [
            'result' => 'false'
        ];
		if(!is_null($id)) {
			$program = Program::where('id', $id)->first();
			// 담당자 체크
	        // $ck = ClassRelation::where('id', $program->class_relation_id)->first();
	        // if(is_null($ck) || ($ck->class_id && !Student::checkManager($request->user, $program->student_id))) {
		    //     if($request->user->level <= 2) {
			// 		$returnData['error'] = '프로그램 권한 없음';
			// 		return response()->json($returnData);
			// 	}
            // }
            if($request->user->level < 100) {
                $returnData['error'] = '100';
                return response()->json($returnData);
            }
            StoGroup::where('program_id', $id)->delete();
            $stos = StoItem::where('program_id', $id)->get();
            foreach($stos as $sto) {
                $points = Point::where('sto_item_id', $sto->id)->get();
                foreach($points as $point) {
                    $sticks = Stick::where('point_id', $point->id)->delete();
                    $point->delete();
                }
                $sto->delete();
            }
            Program::where('id', $id)->delete();
			$returnData['result'] = 'true';
		}

        return response()->json($returnData);
	}

	public function getPrograms(Request $request) {
		$user = $request->user;
		$perPage = (!is_null($request->perPage) ? $request->perPage : 10);
		$page = (!is_null($request->page) ? $request->page : 0);
        $keyword = (!is_null($request->keyword) ? $request->keyword : "");

		$list =  ClassRelation::where('class_relations.flag', 2)
            ->join('students', function($j) {
                $j->on('students.id', 'class_relations.student_id')
                    ->where('students.is_valid', '1');
            })
            ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
            ->when($user->level < 10, function($q) use($user) {
                // lv 1,2,3 : 배정된 반 혹은 개별 학생
                // lv 4,5 : 모든 학생
                return $q->whereRaw('(
                    class_relations.class_id in (
                        select cr.class_id from class_relations cr
                        where cr.user_id = '.$user->id.'
                        and cr.flag = 1
                        group by cr.class_id
                    ) or (class_relations.class_id = 0 and class_relations.user_id = '.$user->id.'))
                ');
            })
            ->when($keyword != "", function($q) use($keyword) {
                return $q->whereRaw("students.name like '%".$keyword."%'");
            })
            ->where('kavba_classes.visible', 1)
            ->whereNull('students.deleted_at')
            ->selectRaw('distinct students.id as student_id, students.name as student_name, students.birth_date as student_birth,
                group_concat(distinct(class_relations.class_id)) as class_ids,
                group_concat(distinct(kavba_classes.name)) as class_names')
            ->groupBy('students.id')
            ->groupBy('kavba_classes.id')
			->orderBy('students.name', 'asc'); // 여기서 페이징처리

		$tmp = $list->get();
		$returnData['total'] = count($tmp);
		$studentList = $list->skip($page * $perPage)
			->take($perPage)
			->get();
		foreach($studentList as $key=>$s) {
            $programs = Program::join('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
                ->leftJoin('sto_groups', function($j) {
                    $j->on('sto_groups.program_id', 'programs.id')
                        ->whereNull('sto_groups.deleted_at');
                })
                //->leftJoin('sto_items', 'sto_items.program_id', 'programs.id')
                ->leftJoin('users', 'users.id', 'programs.user_id')
                ->where('programs.student_id', $s->student_id)
                ->when($s->class_ids == "0", function($q){
                    return $q->where('programs.personal', 1);
                })
                ->when($s->class_ids != "0", function($q){
                    return $q->where('programs.personal', 0);
                })
				->whereNull('programs.deleted_at')
                ->whereNotNull('long_term_goals.id')
				->selectRaw('
                    programs.*,
                    count(distinct sto_groups.id) as lto_count,
                    sum(if(sto_groups.finish = 1, 1, 0)) as finish_lto,
                    group_concat(distinct sto_groups.name) as finish_lto_names,
                    long_term_goals.name as lto_name, users.name as user_name
				')
                ->groupBy('programs.id')
				->orderBy('long_term_goals.id', 'desc')
                ->get();
            $s['programs'] = $programs;
            $total = 0;
            $reaches = 0;
            foreach($programs as $p) {
                $total += $p->lto_count;
                //if($p->finish_lto == 1) $reaches++;
                $reaches += $p->finish_lto;
            }
            // $temp = Program::join('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
            //     ->leftJoin('sto_groups', function($j) {
            //         $j->on('sto_groups.program_id', 'programs.id')
            //             ->whereNull('sto_groups.deleted_at');
            //     })
            //     ->where('programs.student_id', $s->student_id)
            //     ->when($s->class_ids == "0", function($q){
            //         return $q->where('programs.personal', 1);
            //     })
            //     ->when($s->class_ids != "0", function($q){
            //         return $q->where('programs.personal', 0);
            //     })
            //     ->whereNull('programs.deleted_at')
            //     ->where('sto_groups.finish', 1)
            //     ->whereNotNull('long_term_goals.id')
            //     ->selectRaw('programs.id,
            //         count(DISTINCT sto_groups.id) as total,
            //         sum(if(sto_groups.finish = 1, 1, 0)) as finish_lto
            //     ')
            //     ->groupBy('sto_groups.id')
            //     ->first();
            // // $reaches = DB::table(DB::raw("({$sub->toSql()}) as sub"))
            // //     ->mergeBindings($sub->getQuery())
            // //     ->selectRaw('sum(sub.cnt) as total')
            // //     ->first();

            $s['total'] = $total;
            $s['total_reach'] = $reaches;
        }
		$returnData['students'] = $studentList;
	    return response()->json($returnData);
    }

    public function getPrograms_bk(Request $request) {
		$user = $request->user;
		$perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
	    $list = Program::leftJoin('students', 'students.id', 'programs.student_id')
            ->leftJoin('long_term_goals', 'long_term_goals.id', 'programs.lto_id')
            ->leftJoin('class_relations', 'class_relations.student_id', 'programs.student_id')
            ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
			->leftJoin('users', 'users.id', 'programs.user_id')
			->when($user->level < 100, function($q){
				return $q->where('class_relations.flag', 2)
					->whereRaw('(class_relations.user_id is null or class_relations.user_id = '.$user->id.')')
					->whereRaw('
						class_relations.class_id in (
							select cr.class_id from class_relations cr
							where cr.user_id = '.$user->id.'
							and cr.flag = 1
							group by cr.class_id
						)
					')
					->whereNull('class_relations.user_id');
			})
	    	->whereNull('programs.deleted_at')
            ->selectRaw('programs.*, students.name as student_name, students.birth_date as student_birth,
                long_term_goals.name as lto_name, class_relations.class_id, kavba_classes.name as class_name, users.name as user_name')
            ->orderByRaw('field(programs.user_id, '.$user->id.') desc, programs.lto_id asc, students.name asc, programs.created_at')
	    	->get();
	    $programs = [];
	    $students = [];
	    foreach($list as $program) {
            $program['sto_count'] = StoItem::where('program_id', $program->id)->whereNull('deleted_at')->count();
		    if(!in_array($program->student_id, $students)) $students [] = $program->student_id;
		    $programs[$program->student_id] [] = $program;
	    }
	    $returnData = [
            'programs'	=> $programs,
            'students'	=>	$students
        ];
	    return response()->json($returnData);
    }

    public function getProgram(Request $request, $id = null, $flag = 0) {
	    // 0 : 하나의 프로그램, 1 : 프로그램 & sto 목록, 2 : 1 & 각 포인트 & 스틱
	    $returnData = [
            'result' => 'false'
        ];
	    $program = Program::where('id', $id)->first();
	    if(!is_null($program)) {
		    if($flag == 1) {
			    $stoItems = StoItem::where()->get();
			    $returnData['stos'] = $stoItems;

		    } else if($flag == 2) {
			    $stoItems = StoItem::where()->get();
			    foreach($stoItems as $item) {
				    $points = Point::where('sto_item_id', $item->id)->get();
				    foreach($point as $point) {
					    $sticks = Stick::where('point_id', $point->id)->get();
					    $point['sticks'] = $sticks;
				    }
				    $item['points'] = $points;
			    }
			    $returnData['stos'] = $stoItems;

		    }
		    $returnData['program'] = $program;
	    }
	    return response()->json($returnData);
    }


    public function sample(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];

        return response()->json($returnData);
    }

}
