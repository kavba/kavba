<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Student;

class PublicController extends Controller
{
    public function __construct(Request $request)
    {
        
    }

    public function test() {
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e );
        }
        return "test";
    }

    public function webJoin(Request $request) {
        return true;
    }

    public function appJoin(Request $request) {
        return true;
    }

    public function webLogin(Request $request) {
        return true;
    }

    public function appLogin(Request $request) {
        return true;
    }

    public function getSticksForUserCommon(Request $request, $date, $userId) {
        $dd = explode('-', $date);
        $db = "kavba";
        if($dd[0] == "2020") $db = "kavba2020";

        $user = User::where('id', $userId)->first();
        if(!is_null($user) && !is_null($date)) {
            $query = "select
                    p.rgst_date, if(p.postponed is null, '-', p.postponed) as postponed,
                    lto.name as domain, sg.name as lto, si.target, st.name as stick_student_name, 
                    count(distinct s.id) as cnt
                from sticks s
                left join points p on p.id = s.point_id 
                left join sto_items si on si.id = p.sto_item_id 
                left join programs pr on pr.id = si.program_id 
                left join long_term_goals lto on lto.id = pr.lto_id 
                left join sto_groups sg on sg.id = si.sto_group_id
                left join students st on st.id = pr.student_id 
                left join users u on u.id = s.user_id 
                where s.user_id = ".$userId."
                and si.deleted_at is null 
                and sg.deleted_at is null
                and p.deleted_at is null
                and pr.deleted_at is null
                and pr.personal = 0
                and s.stick_date='".$date."'
                group by s.stick_date, p.rgst_date, p.postponed, lto.id, sg.id, si.target, p.id, u.id";
            $list = DB::connection($db)->select(DB::raw($query));
            $total = 0;
            foreach($list as $item) {
                if($item->target) {
                    $tmp = json_decode($item->target);
                    $txt = "";
                    foreach($tmp as $key => $t) {
                        if($key > 0) $txt .= " | ";
                        $txt .= $t->text;
                    }
                    $item->target = $txt;
                }
                $total += intval($item->cnt);
            }
        }

        return view ('public/studentList')->with('date', $date)->with('name', $user->name)->with('sticks', $list)->with('total', $total);
    }

    public function getSticksForStudentCommon(Request $request, $date, $studentId) {
        $dd = explode('-', $date);
        $db = "kavba";
        if($dd[0] == "2020") $db = "kavba2020";

        $student = DB::connection($db)->select('
            select * from students s where s.id = '.$studentId.' and s.deleted_at is null
        ');
        // $student = Student::where('id', $studentId)->whereNull('deleted_at')->first();
        if(count($student) > 0 && !is_null($date)) {
            $query = "select
                    p.rgst_date, if(p.postponed is null, '-', p.postponed) as postponed,
                    lto.name as domain, sg.name as lto, si.target, u.name as stick_user_name, 
                    count(distinct s.id) as cnt
                from sticks s
                left join points p on p.id = s.point_id 
                left join sto_items si on si.id = p.sto_item_id 
                left join programs pr on pr.id = si.program_id 
                left join long_term_goals lto on lto.id = pr.lto_id 
                left join sto_groups sg on sg.id = si.sto_group_id
                left join students st on st.id = pr.student_id 
                left join users u on u.id = s.user_id 
                where pr.student_id = ".$studentId."
                and si.deleted_at is null 
                and sg.deleted_at is null
                and p.deleted_at is null
                and pr.deleted_at is null
                and pr.personal = 0
                and s.stick_date='".$date."'
                group by s.stick_date, p.rgst_date, p.postponed, lto.id, sg.id, si.target, p.id, u.id";
            $list = DB::connection($db)->select(DB::raw($query));
            $total = 0;
            foreach($list as $item) {
                if($item->target) {
                    $tmp = json_decode($item->target);
                    $txt = "";
                    foreach($tmp as $key => $t) {
                        if($key > 0) $txt .= " | ";
                        $txt .= $t->text;
                    }
                    $item->target = $txt;
                }
                $total += intval($item->cnt);
            }
        }
        return view ('public/userList')->with('date', $date)->with('name', $student[0]->name)->with('sticks', $list)->with('total', $total);
    }
}
