<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\Program;
use App\Models\StoItem;
use App\Models\Point;
use App\Models\Stick;
use App\Models\KavbaClass;
use App\Models\LongTermGoal;
use App\Models\ShortTermGoal;

class StickController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getSticks(Request $request, $programId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($programId)) {
            $sticks = Stick::where('program_id', $programId)
                ->orderBy('order', 'desc')
                ->get();
            $returnData['list'] = $sticks;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function checkStick(Request $request, $pointId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $val = $request->val;
        if(!is_null($pointId) && !is_null($val)) {
            $point = Point::leftJoin('sto_items', 'sto_items.id', 'points.sto_item_id')
                ->leftJoin('sto_groups', 'sto_groups.id', 'sto_items.sto_group_id')
                ->leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->where('points.id', $pointId)
                ->selectRaw('points.*, sto_groups.type as reach_type, programs.user_id as master_id')
                ->first();
            $cnt = Stick::where('point_id', $pointId)->count();
            try {
                if(!is_null($point) && $point->status == 0 && $point->count > $cnt) { // 맥시멈을 넘지 않아야만 체크 가능
                    $userId = $request->user->id;
                    $returnData['inserted'] = Stick::create([
                        'user_id'           =>  $userId,
                        'target_user_id'    =>  $point->target_user_id,
                        'point_id'          =>  $pointId,
                        'value'             =>  $val,
                        'order'             =>  $cnt,
                        'memo'              =>  (isset($request->memo) ? $request->memo : null),
                        'stick_date'        =>  (is_null($point->postponed) ? $point->rgst_date : $point->postponed)
                    ]);
                    if($val == 1) {
                        if($point->reach_type == 0) {
                            $returnData['reach'] = StoItem::calcReachOnPoint($pointId);
                        } else { // 누적형 n번 연속 정반응 체크
                            $returnData['reach'] = StoItem::calcReachOnStackPoint($pointId, $userId, $point->master_id);
                        }
                    }
                    $returnData['current'] = $cnt + 1;
                    $returnData['point'] = Point::where('id',$pointId)->first();
                    $returnData['result'] = 'true';
                }
            } catch(Exception $e) {
                if($e->getCode() == 500) {
                    $returnData['reload'] = "true";
                }
            }
        }

        return response()->json($returnData);
    }

    public function editStick(Request $request, $id = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $val = $request->val;
        if(!is_null($id) && !is_null($val) && $val >= 0 && $val < 3) {
            Stick::where('id', $id)->update([
                'value'             =>  $val,
                //'memo'              =>  (isset($request->memo) ? $request->memo : null),
            ]);
            $stick = Stick::where('id', $id)->first();
            $returnData['updated'] = $stick;
            $returnData['result'] = 'true';
            $returnData['reach'] = StoItem::calcReachOnPoint($stick->point_id);
        }
        return response()->json($returnData);
    }

    public function insertStick(Request $request, $id = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $position = $request->position;
        $idx = $request->idx;
        if(!is_null($id) && !is_null($idx)) {
            $s = Stick::where('id', $id)->first();
            $point = Point::where('points.id', $s->point_id)->whereNull('deleted_at')->first();
            if(!is_null($point)) {
                if($position == "true") {
                    DB::connection($this->myDB)->select('
                        update sticks set `order` = `order` + 1 where point_id = '.$s->point_id.' and `order` >= '.$idx);
                    Stick::create([
                        'user_id'           =>  $request->user->id,
                        'target_user_id'    =>  $point->target_user_id,
                        'point_id'          =>  $s->point_id,
                        'value'             =>  2,
                        'order'             =>  $idx,
                        'stick_date'        =>  (is_null($point->postponed) ? $point->rgst_date : $point->postponed)
                    ]);
                } else {
                    DB::connection($this->myDB)->select('
                        update sticks set `order` = `order` + 1 where point_id = '.$s->point_id.' and `order` > '.$idx);
                    Stick::create([
                        'user_id'           =>  $request->user->id,
                        'target_user_id'    =>  $point->target_user_id,
                        'point_id'          =>  $s->point_id,
                        'value'             =>  2,
                        'order'             =>  $idx + 1,
                        'stick_date'        =>  (is_null($point->postponed) ? $point->rgst_date : $point->postponed)
                    ]);
                }
                $sticks = Stick::where('point_id', $s->point_id)->orderBy('order', 'asc')->get();
                $returnData['sticks'] = $sticks;
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function deleteStick(Request $request, $id = 0) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            $s = Stick::where('id', $id)->first();
            DB::connection($this->myDB)->select('
                update sticks set `order` = `order` - 1 where point_id = '.$s->point_id.' and `order` > '.$s->order);
            $s->delete();
            $sticks = Stick::where('point_id', $s->point_id)->orderBy('order', 'asc')->get();
            $returnData['sticks'] = $sticks;
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function resetAllSticks(Request $request, $pointId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($pointId)) {
            Stick::where('point_id', $pointId)->delete();
            $returnData['result'] = 'true';
            Point::where('id', $pointId)->update([ 'reach' => 0 ]);
            $returnData['reach'] = StoItem::calcReachOnPoint($pointId);
        }
        return response()->json($returnData);
    }

    public function removeLastStick(Request $request, $pointId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($pointId)) {
            $s = Stick::where('point_id', $pointId)->orderBy('order', 'desc')->limit(1)->first();
            if(!is_null($s)) $s->delete();
            $returnData['reach'] = StoItem::calcReachOnPoint($pointId);
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function removeHistoryStick(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $pointId = $request->pointId;
        $userId = $request->userId;
        $stickDate = $request->stickDate;
        if(!is_null($pointId) && !is_null($userId) && !is_null($stickDate)) {
            $point = Point::where('id', $pointId)->whereNull('deleted_at')->first();
            if(!is_null($point)) {
                $tmp = Stick::where('point_id', $point->id);
                $tmp->where('stick_date', $stickDate)->where('user_id', $userId)->delete();
                $sticks = $tmp->orderBy('order', 'asc')->get();
                foreach($sticks as $i=>$s) {
                    $s->update([ 'order' => $i ]);
                }
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function setHistoryStickUser(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $pointId = $request->pointId;
        $fromUserId = $request->fromUserId;
        $toUserId = $request->toUserId;
        $date = $request->date;
        if(!is_null($pointId) && !is_null($fromUserId) && !is_null($toUserId) && !is_null($date)) {
            Stick::where('point_id', $pointId)->where('stick_date', $date)->where('user_id', $fromUserId)->update([ 'user_id' => $toUserId ]);
            $returnData['history'] = Stick::leftJoin('points', 'points.id', 'sticks.point_id')
                ->leftJoin('users', 'users.id', 'sticks.user_id')
                ->where('point_id', $pointId)
                ->selectRaw('
                    sticks.stick_date, users.name as user_name, sticks.user_id, count(distinct sticks.id) as stick_cnt,
                    sum(if(sticks.value = 1, 1, 0)) as positive,
                    sum(if(sticks.value = 0, 1, 0)) as negative,
                    sum(if(sticks.value = 2, 1, 0)) as pass
                ')
                ->groupBy('sticks.user_id', 'sticks.stick_date')
                ->orderByRaw('sticks.stick_date desc, users.name asc')
                ->get();
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    // for graph
    public function getSticksForUser(Request $request, $userId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $ltoId = $request->ltoId;
        if(!is_null($fromDate) && !is_null($toDate)) {
            $ltoCheck = (is_null($ltoId) ? false : true);
            $pros = Program::where('user_id', $userId)
                ->when($ltoCheck, function($q) use ($ltoId) {
                    return $q->where('lto_id', $ltoId);
                })
                ->where('rgst_date', '>=', $fromDate)
                ->where('rgst_date', '<=', $toDate)
                ->whereNull('deleted_at')
                ->get();
            foreach($pros as $program) {
                $stick = Stick::leftJoin('users', 'users.id', 'sticks.update_user_id')
                    ->where('program_id', $program->id)
                    ->selectRaw('sticks.*, users.name as update_user_name')
                    ->orderBy('order', 'asc')
                    ->get();
                $program['sticks'] = $stick;
            }
            $returnData['programs'] = $pros;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getSticksForStudent(Request $request, $studentId = 0) {
        $returnData = [
            'result' => 'false'
        ];
        $fromDate = $request->fromDate;
        $toDate = $request->toDate;
        $ltoId = $request->ltoId;
        if(!is_null($fromDate) && !is_null($toDate)) {
            $ltoCheck = (is_null($ltoId) ? false : true);
            $pros = Program::leftJoin('users', 'users.id', 'programs.user_id')
                ->where('student_id', $studentId)
                ->when($ltoCheck, function($q) use ($ltoId) {
                    return $q->where('lto_id', $ltoId);
                })
                ->where('rgst_date', '>=', $fromDate)
                ->where('rgst_date', '<=', $toDate)
                ->whereNull('deleted_at')
                ->selectRaw('programs.*, users.name as register_user_name')
                ->get();
            foreach($pros as $program) {
                $stick = Stick::leftJoin('users', 'users.id', 'sticks.update_user_id')
                    ->where('program_id', $program->id)
                    ->selectRaw('sticks.*, users.name as update_user_name')
                    ->orderBy('order', 'asc')
                    ->get();
                $program['sticks'] = $stick;
            }
            $returnData['programs'] = $pros;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getAllUsersOnDate(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $date = $request->date;
        $classId = $request->classId;
        $studentId = $request->studentId;
        if(!is_null($date) && !is_null($studentId)) {
            $query = "select
                    p.rgst_date, if(p.postponed is null, '-', p.postponed) as postponed,
                    lto.name as domain, sg.name as lto, si.target, u.name as stick_user_name,
                    count(distinct s.id) as cnt
                from sticks s
                left join points p on p.id = s.point_id
                left join sto_items si on si.id = p.sto_item_id
                left join programs pr on pr.id = si.program_id
                left join long_term_goals lto on lto.id = pr.lto_id
                left join sto_groups sg on sg.id = si.sto_group_id
                left join students st on st.id = pr.student_id
                left join users u on u.id = s.user_id
                where pr.student_id = ".$studentId."
                and si.deleted_at is null
                and sg.deleted_at is null
                and p.deleted_at is null
                and pr.deleted_at is null
                and pr.personal = 0
                and s.stick_date='".$date."'
                group by s.stick_date, p.rgst_date, p.postponed, lto.id, sg.id, si.target, p.id, u.id";
            $list = DB::connection($this->myDB)->select(DB::raw($query));

            foreach($list as $item) {
                if($item->target) {
                    $tmp = json_decode($item->target);
                    $txt = "";
                    foreach($tmp as $key => $t) {
                        if($key > 0) $txt .= " | ";
                        $txt .= $t->text;
                    }
                    $item->target = $txt;
                }
            }
            $returnData['points'] = $list;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }
}
