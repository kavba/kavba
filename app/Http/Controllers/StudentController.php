<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\User;
use App\Models\KavbaClass;
use App\Models\ClassRelation;
use App\Models\Program;
use App\Models\StoItem;
use App\Models\Point;
use App\Models\Stick;

class StudentController extends Controller
{
    protected $myDB = "kavba";
    public function __construct(Request $request)
    {
        $this->middleware('kavba');
        $this->middleware(function ($request, $next) {
            $ds = Auth::user()->data_set;
            $this->myDB = dbSetHelpers::setDbData($ds);
            return $next($request);
        });
    }

    public function getStudents(Request $request, $userId = 0) {
        $returnData = [
            'list' => null
        ];
        if($userId == 0) {
	    	$userId = $request->user->id;
        }
        $returnData['list'] = Student::when(!is_null($userId), function($q) use ($userId) {
            return $q->where('user_id', $userId);
        })
        ->orderBy('name', 'asc')
        ->get();

        return response()->json($returnData);
    }

    public function getStudentsByClass(Request $request, $classId = 0) {
        $returnData = [
            'list' => null
        ];
        $returnData['list'] = ClassRelation::leftJoin('students', 'students.id', 'class_relations.student_id')
            ->when($classId == 0, function($q) use($request) {
                return $q->where('class_relations.user_id', $request->user->id)
                    ->whereNotNull('class_relations.student_id');
            })
            ->where('class_relations.class_id', $classId)
            ->where('class_relations.flag', 2)
            ->selectRaw('students.*, class_relations.id as class_relation_id')
        	->orderBy('students.name', 'asc')
        	->get();

        return response()->json($returnData);
    }

    public function getAllByAuth(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $user = $request->user;
        $qry = $request->qry;
        $my = $request->my;
        $sort = ($request->sort=="true" ? "desc" : "asc");
        $overId = $request->overId;
        $classId = $request->classId;
        $perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
        if(!is_null($user)) {
            if($my == "true") {
                $sub = ClassRelation::where('flag', 1)->where('user_id', $user->id);

                $list = ClassRelation::joinSub($sub, 'cr2', function($join) use($user) {
                        $join->on('cr2.class_id', 'class_relations.class_id')
                            ->where('class_relations.flag', 2)
                            ->orWhereRaw('(class_relations.class_id = 0 and class_relations.user_id = '.$user->id.')');
                        })
                        ->leftJoin('students', 'students.id', 'class_relations.student_id')
                        ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                        ->leftJoin('over_classes', 'over_classes.id', 'kavba_classes.over_id')
                        ->leftJoin('student_parent_relations', 'student_parent_relations.student_id', 'students.id')
                        ->leftJoin('parents', 'parents.id', 'student_parent_relations.parent_id')
                        ->when((!is_null($qry)), function($q) use($qry) {
                            return $q->where('students.name', 'like', '%'.$qry.'%')
                                    ->orWhere('parents.name', 'like', '%'.$qry.'%')
                                    ->orWhere('students.phone', 'like', '%'.$qry.'%');
                        })
                        ->when((!is_null($classId)), function($q) use($classId) {
                            return $q->where('class_relations.class_id', $classId);
                        })
                        ->where('kavba_classes.visible', 1)
                        ->whereNotNull('students.id')
                        ->orderByRaw('field(students.is_valid, "1", "0"), students.name ' . $sort)
                        ->groupBy('students.id', 'kavba_classes.id', 'over_classes.id');

                $returnData['students'] = $list->selectRaw('students.*,
                    kavba_classes.id as class_id, kavba_classes.name as class_name,
                    over_classes.id as over_id, over_classes.name as over_name,
                    group_concat(distinct parents.id) as parent_id, group_concat(distinct parents.name) as parent_name,
                    group_concat(distinct parents.phone) as parent_phone')
                    ->skip($page * $perPage)
                    ->take($perPage)
                    ->get();
                $returnData['total'] = $list->count();

            } else {
                $list = Student::leftJoin('class_relations', function($q) use($user) {
                    $q->on('class_relations.student_id', 'students.id')
                        ->where('class_relations.flag', 2)
                        ->whereRaw('
                            ((class_relations.class_id = 0 and class_relations.user_id = '.$user->id.')
                             or (class_relations.class_id != 0))
                        ');
                    })
                    ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                    ->leftJoin('over_classes', 'over_classes.id', 'kavba_classes.over_id')
                    ->leftJoin('student_parent_relations', 'student_parent_relations.student_id', 'students.id')
                    ->leftJoin('parents', 'parents.id', 'student_parent_relations.parent_id')
                    ->when((!is_null($qry)), function($q) use($qry) {
                        return $q->where('students.name', 'like', '%'.$qry.'%')
                                ->orWhere('parents.name', 'like', '%'.$qry.'%')
                                ->orWhere('students.phone', 'like', '%'.$qry.'%');
                    })
                    ->when((!is_null($classId)), function($q) use($classId) {
                        return $q->where('class_relations.class_id', $classId);
                    })
                    ->selectRaw('students.*,
                        kavba_classes.id as class_id, kavba_classes.name as class_name,
                        over_classes.id as over_id, over_classes.name as over_name,
                        group_concat(distinct parents.id separator ",") as parent_id,
                        group_concat(distinct parents.name separator ",") as parent_name,
                        group_concat(distinct parents.phone) as parent_phone,
                        if(class_relations.user_id is null, 0, 1) as special')
                    ->orderByRaw('field(students.is_valid, "1", "0"), students.name ' . $sort)
                    ->groupBy('students.id', 'special', 'class_id', 'over_id');

                $tmp = $list->get();
                $returnData['total'] = count($tmp);
                $returnData['students'] = $list->skip($page * $perPage)
                    ->take($perPage)
                    ->get();
                if($user->level >= 2) {

                    $returnData['type'] = 2;
                } else {
                    $returnData['type'] = 0;
                }
            }

            $returnData['result'] = "true";
        }

        return response()->json($returnData);
    }

    public function getInfo(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];

        if(!is_null($id)) {
            $returnData['student'] = Student::where('id', $id)->first();
            //$returnData['teachers'] = Teacher::where('')
            if(!is_null($returnData['student'])) {
                $returnData['result'] = 'true';
            }
        }

        return response()->json($returnData);
    }

    public function createStudent(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        if($request->user->level < 10) {
            $returnData['error'] = '권한이 없습니다';
            return response()->json($returnData);
        }
        $name = $request->name;
        $birthDate = $request->birthDate;
        $parentIds = ($request->parentIds ? $request->parentIds : []);
        $phone = $request->phone;
        $classId = $request->classId;
        $buildAll = $request->buildAll;
        $valid = $request->isValid;
        if(!is_null($name) && !is_null($birthDate)) {
            $stud = Student::where('name', $name)->where('birth_date', $birthDate)->first();
            if(!is_null($stud)) {
                $returnData['result_code'] = '001';
            } else {
                // 학생 등록
                $stud = Student::create([
                    "name"          =>  $name,
                    "birth_date"    =>  $birthDate,
                    "user_id"       =>  $request->user->id,
                    "phone"         =>  $phone,
                    "is_valid"      =>  $valid,
                ]);
                foreach($parentIds as $parentId) {
                    DB::connection($this->myDB)->table('student_parent_relations')->insert([
                        'student_id'    => $stud->id,
                        'parent_id'     => $parentId
                    ]);
                }
                $crRes = null;
                if(!is_null($classId)) {
                    // 클래스 등록
                    $crRes = ClassRelation::registerToClass($classId, $stud->id, 2);
                    // foreach($classIds as $classId) {
                    //     ClassRelation::registerToClass($classId, $stud->id, 2);
                    // }
                }
                if($buildAll == 'true') {
                    // 자동으로 모든 LTO 프로그램 생성
                    Program::buildAllLtosForStudent($request->user->id, $stud->id, $crRes->id);
                }
                $returnData['result'] = 'true';
            }
        }
        return response()->json($returnData);
    }

    public function editStudent(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        $name = $request->name;
        $birthDate = $request->birthDate;
        $parentIds = ($request->parentIds ? $request->parentIds : []);
        $phone = $request->phone;
        $classId = $request->classId;
        $valid = $request->isValid;
        if(!is_null($id) && !is_null($name) && !is_null($birthDate)) {
            Student::where('id', $id)->update([
                "name"          =>  $name,
                "birth_date"    =>  $birthDate,
                "phone"         =>  $phone,
                "is_valid"      =>  $valid
            ]);

            DB::connection($this->myDB)->table('student_parent_relations')->where('student_id', $id)->delete();
            foreach($parentIds as $parentId) {
                DB::connection($this->myDB)->table('student_parent_relations')->insert([
                    'student_id'    => $id,
                    'parent_id'     => $parentId
                ]);
            }

            if(!is_null($classId)) {
                $crList = ClassRelation::where('student_id', $id)->where('flag', 2)->first();
                $res = ClassRelation::registerToClass($classId, $id, 2);
                if(!is_null($crList)) {
                    $crId = $crList->id;
                    $crList->delete();

                    Program::where('class_relation_id', $crId)->update([ 'class_relation_id' => $res->id ]);
                }
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function removeStudent(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($id)) {
            Student::where('id', $id)->delete();
            ClassRelation::where('student_id', $id)->where('flag', 2)->forceDelete();
            DB::connection($this->myDB)->table('student_parent_relations')->where('student_id', $id)->delete();
            // 학생 해당 프로그램도 모조리 삭제(표면상으로만)
            $programs = Program::where('student_id', $id)->get();
            foreach($programs as $p) {
                $stos = StoItem::where('program_id', $p->id)->get();
                foreach($stos as $sto) {
                    $points = Point::where('sto_item_id', $sto->id)->get();
                    foreach($points as $point) {
                        $sticks = Stick::where('point_id', $point->id)->delete();
                        $point->delete();
                    }
                    $sto->delete();
                }
                $p->delete();
            }
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function setClass(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $studentId = $request->studentId;
        $classIds = $request->classIds;
        if(!is_null($studentId) && !is_null($classIds)) {
            // 클래스 등록
            ClassRelation::where('student_id', $studentId)->where('flag', 2)->delete();
            foreach($classIds as $classId) {
                ClassRelation::registerToClass($classId, $studentId, 2);
            }
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getPyramid(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];

        if(!is_null($id)) {
            $returnData['rows'] = [];
            $temp = DB::connection($this->myDB)->table('pyramid_relations')->where('student_id', $id)->first();
            if(!is_null($temp) && !is_null($temp->rows)) $returnData['rows'] = json_decode($temp->rows);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function setPyramid(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $studentId = $request->studentId;
        $pyramid = $request->pyramid;
        if(!is_null($studentId) && !is_null($pyramid)) {
            $pyramidData = DB::connection($this->myDB)->table('pyramid_relations')
                ->where('student_id', $studentId);
            $temp = $pyramidData->first();
            if(!is_null($temp)) {
                $pyramidData->update([
                    'rows'  =>  json_encode($pyramid)
                ]);
            } else {
                DB::connection($this->myDB)->table('pyramid_relations')->insert([
                    'student_id'    =>  $studentId,
                    'rows'          =>  json_encode($pyramid)
                ]);
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getCircle(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        $special = $request->special;
        if(!is_null($id)) {
            $res = [];
            $user = $request->user;
            $list = DB::connection($this->myDB)->table('circle_relations')
                ->where('student_id', $id)
                ->when($special == 1, function($q) use($user) {
                    return $q->where('user_id', $user->id)
                        ->where('special', 1);
                })
                ->when($special == 0, function($q) {
                    return $q->where('special', 0);
                })
                ->get();
            foreach($list as $c) {
                if(!is_null($c) && !is_null($c->data))
                    $res[$c->parent_id] = json_decode($c->data);
            }
            $returnData['studentCircles'] = $res;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function setCircle(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $studentId = $request->studentId;
        $circle = $request->circle;
        $special = $request->special;
        if(!is_null($studentId)) {
            $user = $request->user;
            DB::connection($this->myDB)->table('circle_relations')
                ->where('student_id', $studentId)
                ->when($special == 1, function($q) use($user) {
                    return $q->where('user_id', $user->id)
                        ->where('special', 1);
                })
                ->when($special == 0, function($q) {
                    return $q->where('special', 0);
                })
                ->delete();
            if(!is_null($circle)) {
                $data = [];
                foreach($circle as $key => $cp) {
                    $data [] = [
                        'student_id'    => $studentId,
                        'parent_id'     => $key,
                        'data'          => json_encode($cp),
                        'user_id'       =>  ($special != 0 ? $user->id : null),
                        'special'       =>  $special
                    ];
                }
                DB::connection($this->myDB)->table('circle_relations')->insert($data);
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getMemo(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        $special = $request->special;
        if(!is_null($id)) {
            $user = $request->user;
            $returnData['personal'] = DB::connection($this->myDB)->table('class_memos')
                ->where('student_id', $id)
                ->where('user_id', $request->user->id)
                ->where('flag', 0)
                ->when($special == 1, function($q) use($user) {
                    return $q->where('user_id', $user->id)
                        ->where('special', 1);
                })
                ->when($special == 0, function($q) {
                    return $q->where('special', 0);
                })
                ->first();

            $returnData['all'] = DB::connection($this->myDB)->table('class_memos')
                ->where('student_id', $id)
                ->where('flag', 1)
                ->when($special == 1, function($q) use($user) {
                    return $q->where('user_id', $user->id)
                        ->where('special', 1);
                })
                ->when($special == 0, function($q) {
                    return $q->where('special', 0);
                })
                ->first();

            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function setMemo(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $studentId = $request->studentId;
        $type = $request->type;
        $memo = $request->memo;
        $flag = ($type == "0" ? 0 : 1);
        $special = $request->special;
        if(!is_null($studentId)) {
            $user = $request->user;
            $data = DB::connection($this->myDB)->table('class_memos')
                ->where('student_id', $studentId)
                ->where('flag', $flag)
                ->when($special == 1, function($q) use($user) {
                    return $q->where('user_id', $user->id)
                        ->where('special', 1);
                })
                ->when($special == 0, function($q) {
                    return $q->where('special', 0);
                });
            $temp = $data->first();
            if(!is_null($temp)) {
                $data->update([
                    'memo'  =>  $memo
                ]);
            } else {
                DB::connection($this->myDB)->table('class_memos')->insert([
                    'user_id'       =>  $request->user->id,
                    'student_id'    =>  $studentId,
                    'memo'          =>  $memo,
                    'flag'          =>  $flag,
                    'special'       =>  $special
                ]);
            }
            $returnData['result'] = 'true';
        }
        return response()->json($returnData);
    }

    public function getAllDomainReaches(Request $request, $id = null) {
        $returnData = [
            'result' => 'false'
        ];
        return response()->json($returnData);
    }
}
