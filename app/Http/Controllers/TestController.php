<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function __construct(Request $request) {
        $from = $request->header('Accessing-From');
        if($from == "web") { // calling api from web
            $this->middleware('csrf');
        }
        $this->middleware('kavba:api');
    }

    public function test1(Request $request) {
        $res = [
            'data' => "this is test script!",
            'user' => $request['user']
        ];
        return response()->json(
            $res
        );
    }

    public function test2(Request $request) {
        $res = [
            'data' => "this is test script2222!",
            'user' => $request['user']
        ];
        return response()->json(
            $res
        );
    }
}
