<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Student;
use App\Models\User;
use App\Models\StoItem;
use App\Models\KavbaClass;
use App\Models\ClassRelation;
use App\Models\UserStudentInactive;

class UserController extends Controller
{

    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('kavba');
    }

    public function setClass(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $userId = $request->userId;
        $classIds = $request->classIds;
        if(!is_null($userId) && !is_null($classIds)) {
            // 클래스 등록
            ClassRelation::where('user_id', $userId)->where('flag', 1)->delete();
            foreach($classIds as $classId) {
                ClassRelation::registerToClass($classId, $userId, 1);
            }
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getClasses(Request $request) {
        $user = $request->user;
        $list = [];
        // if($user->level > 10) {
        // } else if($user->level > 2) {
        // } else if($user->level > 1) {
        // } else {
        // }
        if($user->level > 2) {
            $list = KavbaClass::whereNull('deleted_at')
                ->where('kavba_classes.visible', 1)
                ->selectRaw('id, name, over_id')
                ->get();
        } else {
            $list = ClassRelation::leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                ->where('class_relations.user_id', $request->user->id)
                //->where('class_relations.flag', 1)
                ->where('kavba_classes.visible', 1)
                ->selectRaw('kavba_classes.id, kavba_classes.name, kavba_classes.over_id')
                ->groupBy('class_relations.class_id')
                ->orderBy('kavba_classes.name', 'asc')
                ->get();
        }
        return response()->json($list);
    }

    public function getAllClasses(Request $request) {
        // $returnData['list'] = ClassRelation::leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
        //     ->where('class_relations.user_id', $request->user->id)
        // 	->where('class_relations.flag', 1)
        // 	->selectRaw('kavba_classes.id, kavba_classes.name')
        //     ->get();

        $returnData['list'] = KavbaClass::whereNull('deleted_at')
            ->where('kavba_classes.visible', 1)
            ->get();

        return response()->json($returnData);
    }

    public function activeStudent(Request $request, $studentId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($studentId)) {
            // 삭제해서 활성화
            DB::table('user_student_inactives')->where('user_id', $request->user->id)->where('student_id', $studentId)->delete();
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function inactiveStudent(Request $request, $studentId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($studentId)) {
            // 비활성화목록에 추가
            DB::table('user_student_inactives')->insert([
                "user_id"   =>  $request->user->id,
                "student_id"    =>  $studentId
            ]);
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function createUser(Request $request) {
	    $returnData = [
            'result' => 'false'
        ];

		$name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $level = $request->level;
        $password = $request->password;
		if(!is_null($name) && !is_null($email)) {
            if(User::where('email', $email)->count() > 0) {
                $returnData['error'] = '이미 가입된 이메일입니다';
                return response()->json($returnData);
            }
            $token = str_random('60');
            $user = User::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'level' => $level,
                'password' => Hash::make($password),
                //'password' => bcrypt($data['password']),
                'api_token' => $token
            ]);
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function editUser(Request $request, $userId = null) {
	    $returnData = [
            'result' => 'false'
        ];

		$name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $level = $request->level;
        $password = $request->password;
        $valid = $request->is_valid;
		if(!is_null($userId) && !is_null($name) && !is_null($email)) {
            $user = User::where('id', $userId);
            $userOthers = [];
            foreach(dbSetHelpers::$defaultDates as $year) {
                $userOthers [] = DB::connection('kavba20'.$year)->table('users')->where('id', $userId);
            }
            if($password != '') {
                $user->update([
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'level' => $level,
                    'password' => Hash::make($password),
                    'is_valid' => $valid
                ]);
                foreach($userOthers as $users) {
                    $users->update([
                        'name' => $name,
                        'email' => $email,
                        'phone' => $phone,
                        'level' => $level,
                        'password' => Hash::make($password),
                        'is_valid' => $valid
                    ]);
                }
            } else {
                $user->update([
                    'name' => $name,
                    'email' => $email,
                    'phone' => $phone,
                    'level' => $level,
                    'is_valid' => $valid
                ]);
                foreach($userOthers as $users) {
                    $users->update([
                        'name' => $name,
                        'email' => $email,
                        'phone' => $phone,
                        'level' => $level,
                        'is_valid' => $valid
                    ]);
                }
            }
			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function deleteUser(Request $request, $userId = null) {
	    $returnData = [
            'result' => 'false'
        ];

        if(!is_null($userId)) {
            User::where('id', $userId)->delete();
            $user = DB::table('users')->where('id', $userId)->first();
            $deleted_at = $user->deleted_at;
            DB::connection('kavba')->table('users')->where('id', $userId)->update([
                "deleted_at" => $deleted_at
            ]);

            $userOthers = [];
            foreach(dbSetHelpers::$defaultDates as $year) {
                DB::connection('kavba20'.$year)->table('users')->where('id', $userId)->update([
                    "deleted_at" => $deleted_at
                ]);
            }
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getAll(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        $me = $request->user;
        $qry = $request->qry;
        $sort = ($request->sort=="true" ? "desc" : "asc");
        $classId = $request->classId;
        $perPage = (!is_null($request->perPage) ? $request->perPage : 10);
        $page = (!is_null($request->page) ? $request->page : 0);
        if(!is_null($me)) {
            $list = User::leftJoin('class_relations', 'class_relations.user_id', 'users.id')
                ->when((!is_null($qry)), function($q) use($qry) {
                    return $q->where('users.email', 'like', '%'.$qry.'%')
                            ->orWhere('users.name', 'like', '%'.$qry.'%');
                })
                ->when((!is_null($classId)), function($q) use($classId) {
                    return $q->where('class_relations.class_id', intval($classId));
                })
                ->selectRaw('users.id, users.name, users.email, users.phone, users.level, users.created_at, users.is_valid')
                ->groupBy('users.id')
                ->orderByRaw('field(users.is_valid, "1", "0"), users.name ' . $sort);

            $tmp = $list->get();
            $returnData['total'] = count($tmp);
            $users = $list->skip($page * $perPage)
                ->take($perPage)
                ->get();

            foreach($users as $user) {
                $user['selectedClasses'] = ClassRelation::leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                    ->where('class_relations.user_id', $user->id)
                    ->where('class_relations.flag', 1)
                    ->whereNull('class_relations.student_id')
                    ->where('class_relations.class_id', '!=', 0)
                    ->selectRaw('kavba_classes.*')
                    ->get();
                $user['selectedStudents'] = ClassRelation::leftJoin('students', 'students.id', 'class_relations.student_id')
                    ->where('class_relations.user_id', $user->id)
                    ->where('class_relations.flag', 2)
                    ->where('class_relations.class_id', 0)
                    ->selectRaw('students.*')
                    ->get();
            }

            $returnData['users'] = $users;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getByStoItem(Request $reques, $stoItemId = null) {
        $returnData = [
            'result' => 'false'
        ];
        if(!is_null($stoItemId)) {
            $classInfo = StoItem::leftJoin('programs', 'programs.id', 'sto_items.program_id')
                ->leftJoin('class_relations', 'class_relations.student_id', 'programs.student_id')
                ->where('class_relations.flag', 2)
                ->where('sto_items.id', $stoItemId)
                ->selectRaw('class_relations.class_id, programs.student_id, programs.personal')
                ->first();
            if(!is_null($classInfo)) {
                if($classInfo->personal == 1) {
                    $users = ClassRelation::leftJoin('users', 'users.id', 'class_relations.user_id')
                        ->where('class_relations.class_id', 0)
                        ->where('class_relations.flag', 2)
                        ->where('class_relations.student_id', $classInfo->student_id)
                        ->orWhere('users.level', 100)
                        ->selectRaw('users.*')
                        ->groupBy('users.id')
                        ->orderBy('users.name', 'asc')
                        ->get();
                    // if(is_null($classInfo->class_id)) {
                    //     $temp = ClassRelation::where('class_relations.student_id', $classInfo->student_id)
                    //         ->where('class_relations.flag', 2)
                    //         ->whereNull('class_relations.user_id')
                    //         ->first();
                    //     $users = ClassRelation::leftJoin('users', 'users.id', 'class_relations.user_id')
                    //         ->where('class_relations.flag', 1)
                    //         ->where('class_relations.class_id', $temp->class_id)
                    //         ->selectRaw('users.*')
                    //         ->orderBy('users.name', 'asc')
                    //         ->groupBy('users.id')
                    //         ->get();
                    // } else {
                    //     $users = ClassRelation::leftJoin('users', 'users.id', 'class_relations.user_id')
                    //         ->where('class_relations.class_id', 0)
                    //         ->where('class_relations.flag', 2)
                    //         ->where('class_relations.student_id', $classInfo->student_id)
                    //         ->selectRaw('users.*')
                    //         ->orderBy('users.name', 'asc')
                    //         ->get();
                    // }
                } else {
                    $users = ClassRelation::leftJoin('users', 'users.id', 'class_relations.user_id')
                        ->where('class_relations.class_id', $classInfo->class_id)
                        ->where('class_relations.flag', 1)
                        ->selectRaw('users.*')
                        ->orderBy('users.name', 'asc')
                        ->get();
                }
                $returnData['users'] = $users;
                $returnData['result'] = 'true';
                $returnData['test'] = $classInfo;
            }
        }
        return response()->json($returnData);
    }

    public function getInfo(Request $reques, $user = null) {
        $returnData = [
            'result' => 'false'
        ];

        return response()->json($returnData);
    }

    public function setClassesAndStudents(Request $request, $userId = null) {
	    $returnData = [
            'result' => 'false'
        ];

		$selectedClasses = (!is_null($request->selectedClasses) ? $request->selectedClasses : []);
        $selectedStudents = (!is_null($request->selectedStudents) ? $request->selectedStudents : []);
		if(!is_null($userId)) {
            $temp = [
                'class_id' => null, 'student_id' => null, 'user_id' => $userId, 'flag' => 1
            ];
            $arr = [];
            foreach($selectedClasses as $cId) {
                $temp['class_id'] = $cId;
                $arr [] = $temp;
            }
            ClassRelation::where('user_id', $userId)->where('flag', 1)->whereNull('student_id')->delete();
            ClassRelation::insert($arr);
            $arr = [];
            $temp['class_id'] = 0;
            $temp['flag'] = 2;
            foreach($selectedStudents as $sId) {
                $temp['student_id'] = $sId;
                $arr [] = $temp;
            }
            ClassRelation::where('user_id', $userId)->where('flag', 2)->delete();
            ClassRelation::insert($arr);

			$returnData['result'] = 'true';
		} else {
			$returnData['error'] = '파라미터 부재';
		}

        return response()->json($returnData);
    }

    public function getAllByStudent(Request $request, $studentId = null) {
	    $returnData = [
            'result' => 'false'
        ];

        if(!is_null($studentId)) {
        }

        return response()->json($returnData);
    }

    public function getMyClassTeachers(Request $request, $userId = null) {
        $returnData = [
            'result' => 'false'
        ];

        if(!is_null($userId)) {
            $list = ClassRelation::where('class_relations.flag', 1)
                ->leftJoin('users', 'users.id', 'class_relations.user_id')
                ->whereRaw('
                    class_relations.class_id in (
                        select cr.class_id from class_relations cr
                        where cr.user_id = '.$userId.'
                        and cr.flag = 1
                        group by cr.class_id
                    )
                ')
                ->selectRaw('distinct users.id, users.name')
                ->orderBy('users.name')
                ->get();
            $returnData['users'] = $list;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getStudentClassTeachers(Request $request, $studentId = null) {
        $returnData = [
            'result' => 'false'
        ];

        if(!is_null($studentId)) {
            $list = ClassRelation::where('class_relations.flag', 1)
                ->leftJoin('users', 'users.id', 'class_relations.user_id')
                ->whereRaw('
                    class_relations.class_id in (
                        select cr.class_id from class_relations cr
                        where cr.student_id = '.$studentId.'
                        and cr.flag = 2
                        group by cr.class_id
                    )
                ')
                ->selectRaw('distinct users.id, users.name')
                ->orderBy('users.name')
                ->get();
            $returnData['users'] = $list;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }

    public function getUsersClasses(Request $request, $userId = null) {
        $returnData = [
            'result' => 'false'
        ];

        if(!is_null($userId)) {
            $list =  ClassRelation::where('class_relations.flag', 2)
                ->join('students', 'students.id', 'class_relations.student_id')
                ->leftJoin('kavba_classes', 'kavba_classes.id', 'class_relations.class_id')
                ->whereRaw('(
                    class_relations.class_id in (
                        select cr.class_id from class_relations cr
                        where cr.user_id = '.$userId.'
                        and cr.flag = 1
                        group by cr.class_id
                    ) or (class_relations.class_id = 0 and class_relations.user_id = '.$userId.'))
                ')
                ->whereNull('students.deleted_at')
                ->selectRaw('distinct students.id as id, students.name as name, students.birth_date as birth_date')
                ->groupBy('students.id')
                ->groupBy('kavba_classes.id')
                ->orderBy('students.name', 'asc')
                ->get();
            $returnData['students'] = $list;
            $returnData['result'] = 'true';
        }

        return response()->json($returnData);
    }
}
