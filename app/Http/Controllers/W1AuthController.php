<?php

namespace App\Http\Controllers;

use App\Http\Helpers\dbSetHelpers;
use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class W1AuthController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('kavba:web');
    }

    public function join(Request $request) {
        $data = $request->only('name', 'email','password','passwordCheck');
        $messages = [
            'required' => '입력값 필수',
            'string' => '문자 형식 오류',
            'name.min' => '입력값 최소 2글자 이상',
            'password.min' => '비밀번호 최소 8글자 이상',
            'max' => '입력값 너무 깁니다',
            'email' => '잘못된 이메일 형식',
            'email.unique' => '이미 등록된 이메일',
            'same' => '비밀번호 확인 실패'
        ];

        $validator = Validator::make($data, [
            'name' => ['required', 'string', 'min:2', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'passwordCheck' => 'same:password',
        ], $messages);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $token = str_random('60');

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            //'password' => bcrypt($data['password']),
            'api_token' => $token
        ];

        $user = User::create($data);
        // 지난 DB 에 사용자 동시 추가
        $data['id'] = $user->id;
        $data['created_at'] = $user->created_at;
        $data['updated_at'] = $user->updated_at;
        DB::connection('kavba2020')->table('users')->insert($data);
        DB::connection('kavba2021')->table('users')->insert($data);

        $returnData = [
            'result' => 'true',
            'user'   => $user
        ];
        return $returnData;
    }

    public function login(Request $request) {
        $returnData = [
            'result' => 'false'
        ];
        if(is_null($request->email)) {
            $returnData['fail_code'] = "001";
        } else if(is_null($request->password)) {
            $returnData['fail_code'] = "002";
        } else {
            $user = User::where('email', $request->email)->first();
            if(!is_null($user)) {
                if($user->is_valid == '0') {
                    $returnData['fail_code'] = "005";
                } else {
                    $validCredentials = Hash::check($request->password, $user->getAuthPassword());
                    if($validCredentials){
                        Auth::login($user, $request->remember);
                        //Session::put();
                        $returnData['result'] = "true";
                        $returnData['session'] = Auth::user();
                    } else {
                        $returnData['fail_code'] = "004";
                    }
                }
            } else {
                $returnData['fail_code'] = "003";
            }
        }
        return response()->json($returnData);
    }

    public function knock(Request $request) {
        $returnData = [];
        $returnData['check'] = Auth::check();
        $returnData['user'] = Auth::user();

        return response()->json($returnData);
    }

    public function logout(Request $request) {
        Auth::logout();
        return response()->json(['result' => 'true']);
    }

    public function setDataSet(Request $request) {
        $user = Auth::user();
        if(!is_null($user)) {
            $ds = $user->data_set;
            if($request->dataSet != "") {
                $user->data_set = $request->dataSet;
                $user->save();
                $ds = $request->dataSet;
            }

            return response()->json([
                'result' => 'true',
                'userId' => $user->id,
                'dataSet' => $ds
            ]);
        }
        return response()->json([
            'result' => 'false',
        ]);
    }

    public function getDbYears() {
        $arr = dbSetHelpers::$defaultDates;
        $arr [] = strval(intval($arr[count($arr) - 1]) + 1);
        $arr = array_reverse($arr);
        $arr = array_slice($arr, 0, 2); // 마지막 숫자가 자를 갯수
        return response()->json([
            'arr' => $arr
        ]);
    }
}
