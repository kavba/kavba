<?php

namespace App\Http\Controllers;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\LongTermGoal;
use App\Models\ShortTermGoal;

class W1GoalController extends Controller
{
    public function __construct()
    {

        $this->middleware('kavba');
    }

    public function getLtos(Request $request) {
        $returnData = [
            'list' => null
        ];
        $ltos = LongTermGoal::get();
        if(!is_null($ltos)) {
            $returnData['list'] = $ltos;
        }

        return response()->json($returnData);
    }

    public function getStos(Request $request) {
        $returnData = [
            'list' => null
        ];
        $stos = ShortTermGoal::get();
        if(!is_null($stos)) {
            $returnData['list'] = $stos;
        }

        return response()->json($returnData);
    }
}
