<?php

namespace App\Http\Helpers;

class dbSetHelpers
{
    public static $defaultDates = ['20', '21'];

    public static function setDbData(string $ds = "") : string
    {
        $result = "kavba";
        $defaultDates = dbSetHelpers::$defaultDates;
        if($ds == "") {
            // to nothing
        } else if(in_array($ds, $defaultDates)) {
            $result .= "20" . $ds;
        }
        return $result;
    }
}
