<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class CheckApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if($request->api_token) {
            $token = $request->api_token;
            $user = User::where('api_token', $token)->select('id', 'name', 'email', 'phone', 'level')->first();
            if(!is_null($user)) {
                $request['user'] = $user;
                return $next($request);
            }
        }
        return "token_err";
    }
}
