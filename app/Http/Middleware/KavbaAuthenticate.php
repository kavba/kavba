<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class KavbaAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    
    public function handle($request, Closure $next, $guard = null)
    {
        if(isset($_SERVER['HTTP_ACCESS_FROM'])) {
            if($_SERVER['HTTP_ACCESS_FROM'] == 'web') {
                $request->user = Auth::user();
                return $next($request);
            }
        } else {
            if($request->api_token) {
                $token = $request->api_token;
                $user = User::where('api_token', $token)->select('id', 'name', 'email', 'phone', 'level', 'data_set')->first();
                if(!is_null($user)) {
                    $request->user = $user;
                    return $next($request);
                }
            } else {
                $request->user = Auth::user();
                return $next($request);
            }
        }
        
        return "auth_error";
    }
}
