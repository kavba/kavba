<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AssignProgram extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id', 'from_user_id', 'to_user_id'
    ];

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }
}
