<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ClassRelation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class_id', 'student_id', 'user_id', 'flag'
    ];

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }

    public static function registerToClass($classId, $id, $flag) {
        $res = null;
        if($flag == 1) {
            $res = self::create([
                'class_id'      =>  $classId,
                'user_id'       =>  $id,
                'flag'          =>  $flag
            ]);
        } else if($flag == 2) {
            $res = self::create([
                'class_id'      =>  $classId,
                'student_id'    =>  $id,
                'flag'          =>  $flag
            ]);
        }
        return $res;
    }
}
