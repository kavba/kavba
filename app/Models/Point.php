<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Point extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'target_user_id', 'sto_item_id', 'rgst_date', 'postponed', 'count', 'reach', 'status', 'last_update_user_id'
    ];

    use SoftDeletes;

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }
}
