<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Program extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'class_relation_id', 'student_id', 'lto_id', 'personal'
    ];

    use SoftDeletes;

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }

    public static function buildAllLtosForStudent($userId = null, $studentId = null, $classId = null) {
        if(!is_null($userId) && !is_null($studentId) && !is_null($classId)) {
            $ltos = LongTermGoal::where('using', 1)->get();
            foreach($ltos as $lto) {
                $p = new Program();
                $p->user_id = $userId;
                $p->student_id = $studentId;
                $p->lto_id = $lto->id;
                $p->personal = ($classId == 0 ? 1 : 0);
                $p->save();
            }
        }
    }
}
