<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class StoGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'type', 'program_id', 'order'
    ];

    use SoftDeletes;

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }

    public static function setFinish($groupId = null, $finish = 0) {
        $res = [ 'result'   =>  'false' ];
        $stoGroup = StoGroup::where('id', $groupId)->first();
        if(!is_null($stoGroup)) {
            if($finish >= -1 && $finish <= 2) {
                // 0:진행중, 1:완료, 2:중지, -1:취소
                $stoGroup->where('id', $groupId)->update([ 'finish' =>  $finish ]);
                $res['result'] = 'true';
            }
        }
        return $res;
    }

    public static function resetOrder($programId = null) {
        if(!is_null($programId)) {
            $stos = StoGroup::where('program_id', $programId)
                ->whereNull('deleted_at')
                ->orderBy('order', 'asc')
                ->get();
            $index = 0;
            foreach($stos as $sto) {
                $sto->update([ 'order' => $index ]);
                $index++;
            }
        }
    }
}
