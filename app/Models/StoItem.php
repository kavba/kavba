<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class StoItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'sto_group_id', 'program_id', 'user_id', 'parent_id', 'rows', 'target',
        'standard_perc', 'standard_cnt', 'total_reach', 'finish', 'order', 'decision',
        'schedule_memo', 'prompt_code', 'prompt_memo', 'program_memo', 'reach_date', 'reach_user_id', 'master_id'
    ];

    use SoftDeletes;

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }

    public static function setFinish($stoId = null, $finish = 0) {
        $res = [ 'result'   =>  'false' ];
        $stoItem = StoItem::leftJoin('programs', 'programs.id', 'sto_items.program_id')
            ->where('sto_items.id', $stoId)
            ->selectRaw('sto_items.*, programs.user_id as program_user_id')
            ->first();
        if(!is_null($stoItem)) {
            if($finish >= -1 && $finish <= 2) {
                // 0:진행중, 1:완료, 2:중지, -1:취소
                if($stoItem->total_reach < 1) {
                    if($finish == 1) {
                        $nowDate = date('Y-m-d');
                        $stoItem->where('id', $stoId)->update([
                            'finish' =>  $finish,
                            'reach_date' => $nowDate,
                            'reach_user_id' => $stoItem->program_user_id,
                            'master_id' =>  $stoItem->program_user_id
                        ]);
                    } else {
                        $stoItem->where('id', $stoId)->update([
                            'finish' =>  $finish,
                            'reach_date' => null,
                            'reach_user_id' => null,
                            'master_id' => null
                        ]);
                    }
                } else {
                    $stoItem->where('id', $stoId)->update([ 'finish' =>  $finish ]);
                }

                $res['result'] = 'true';
            }
        }
        return $res;
    }

    public static function calcReachOnPoint($pointId = null) {
        $res = 0;
        $point = Point::where('id', $pointId)->first();
        if(!is_null($point)) {
            $reach = 0;
            $stoItem = StoItem::where('id', $point->sto_item_id)->first();
            // $stoGroup = StoGroup::where('id', $stoItem->sto_group_id)->first();
            // 해당 sto_item 아래의 point, stick을 조사
            // 준거 도달 갯수를 다시 계산해 reach 값 갱신
            $positiveCnt = Stick::where('point_id', $pointId)
                ->where('value', 1)
                ->count();
            if(!is_null($stoItem)) {
                $standard = floatval($point->count * $stoItem->standard_perc / 100);
                if($positiveCnt >= $standard) {
                    if($point->reach == 0) {
                        $point->update([ 'reach' => 1 ]);
                    }
                    $reach = 1;
                } else {
                    if($point->reach == 1) {
                        $point->update([ 'reach' => 0 ]);
                    }
                    $reach = 0;
                }
            }

            //$stoItem->update([ 'total_reach' =>  $reach ]);
            $res = $reach;
        }
        return $res;
    }

    public static function calcReachOnStackPoint($pointId = null, $masterId) {
        $res = 0;
        $point = Point::where('id', $pointId)->first();
        if(!is_null($point)) {
            $reach = 0;
            $stoItem = StoItem::where('id', $point->sto_item_id)->whereNull('deleted_at')->first();
            $stoGroup = StoGroup::where('id', $stoItem->sto_group_id)->whereNull('deleted_at')->first();
            // 해당 sto_item 아래의 point, stick을 조사
            // 준거 도달 갯수를 다시 계산해 reach 값 갱신
            // decision 에 추가
            if(!is_null($stoItem) && !is_null($stoGroup)) {
                $curr = Stick::where('point_id', $pointId)->count() - 1;
                $to = $curr - $stoItem->standard_cnt + 1;
                if($curr > 0 && $to >= 0) {
                    $sticks = Stick::where('point_id', $point->id)
                        ->orderBy('order', 'asc')
                        ->get();
                    $reach = 1;
                    for($i=$curr ; $i >= $to ; $i--) {
                        if($sticks[$i]->value != 1) {
                            $reach = 0;
                            break;
                        }
                    }
                    $nowDate = date('Y-m-d');
                    if($reach > 0) {
                        $point->update([ 'reach' => 1, 'status' => 1 ]);
                        $stoItem->update([
                            'total_reach' => 1,
                            'finish' => 1,
                            'reach_date' => $nowDate,
                            'reach_user_id' => $stoItem->program_user_id,
                            'master_id'     =>  $masterId
                        ]);
                        Decision::create([
                            'decide_user_id'    =>  $stoItem->program_user_id,
                            'sto_item_id'       =>  $stoItem->id,
                            'point_id'          =>  $point->id
                        ]);
                    } else {

                    }
                }
            }
            if($reach > 0) {
                $stoItem->update([ 'total_reach' => 1, 'finish' => 1, 'reach_date' => $nowDate, 'reach_user_id' => $stoItem->program_user_id ]);
            } else {
                $stoItem->update([ 'total_reach' => 0, 'finish' => 0, 'reach_date' => null, 'reach_user_id' => null ]);
            }
            $res = $reach;
        }
        return $res;
    }

    public static function calcReachOnStoItem($stoItemId = null) {
        $res = [ 'result'   =>  'false' ];
        $stoItem = StoItem::where('id', $stoItemId)->first();
        if(!is_null($stoItem)) {
            $nowDate = date('Y-m-d');
            $reachCnt = Point::where('sto_item_id', $stoItemId)
                ->where('reach', 1)
                ->count();
            $stoItem->update([ 'total_reach' => $reachCnt, 'reach_date' => $nowDate ]);
            $res['result'] = 'true';
        }
        return $res;
    }

    public static function calcAllDecision($stoId = null, $standardPerc = 0, $standardCnt = 0) {
        $res = [ 'result'   =>  'false' ];
        $stoItem = StoItem::where('id', $stoId)->first();
        if(!is_null($stoItem)) {
            // 해당 sto_item 아래의 point, stick을 조사
            // 각 point의 decision 갱신
        }
        return $res;
    }

    public static function setDecisionByLastPoint($user = null, $point = null) {
        $res = 0;
        if(!is_null($point) && !is_null($user)) {
            $stoId = $point->sto_item_id;
            $sto = StoItem::where('id', $stoId);
            $stoItem = $sto->first();
            $points = Point::leftJoin('decisions', 'decisions.point_id', 'points.id')
                ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                ->where('points.sto_item_id', $stoId)
                ->whereNull('points.deleted_at')
                ->selectRaw('
                    points.*,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    sum(if(sticks.value = 1, 1, 0)) as positive,
                    decisions.point_id as decision_point_id
                ')
                ->groupBy('points.id')
                ->orderBy('real_date', 'asc')
                ->orderBy('points.updated_at', 'asc')
                ->get();

            $idx = 0;
            $dId = null;
            foreach($points as $key=>$p) {
                if($p->decision_point_id) {
                    $idx = $key;
                    $dId = $p->decision_point_id;
                }
                if($p->id == $point->id) break;
            }
            $decisionPoint = self::calcRealDecisions($idx, $points);
            $td = Decision::where('point_id')->first();
            if(!is_null($decisionPoint)) {
                $program = Program::where('id', $stoItem->program_id)->first();
                if(is_null($td)) {
                    //return $decisionPoint->id;
                    Decision::create([
                        'decide_user_id'    =>  $user->id,
                        'master_id'         =>  $program->user_id,
                        'sto_item_id'       =>  $stoId,
                        'point_id'          =>  $decisionPoint->id
                    ]);
                    $sto->update([
                        'decision'  =>  $stoItem->decision + 1
                    ]);
                } else {
                    Decision::where('point_id')->update([
                        'decide_user_id'    =>  $user->id,
                        'master_id'         =>  $program->user_id
                    ]);
                }
                $res = 1;
            }
        }
        return $res;
    }

    public static function setAllDecisions($user = null, $stoItemId = null) {
        $res = [];
        $FIRSTINDEX = 3;
        $SECONDINDEX = 5;
        $BASICGAP = 2;
        if(!is_null($stoItemId) && !is_null($user)) {
            $sto = StoItem::where('id', $stoItemId);
            $stoItem = $sto->first();
            $points = Point::leftJoin('decisions', 'decisions.point_id', 'points.id')
                ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                ->where('points.sto_item_id', $stoItemId)
                ->where('points.status', 1)
                ->whereNull('points.deleted_at')
                ->whereNull('sticks.deleted_at')
                ->selectRaw('
                    points.*,
                    if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                    sum(if(sticks.value = 1, 1, 0)) as positive,
                    decisions.point_id as decision_point_id
                ')
                ->groupBy('points.id')
                ->orderBy('real_date', 'asc')
                ->orderBy('points.updated_at', 'asc')
                ->get();

            // 1. 해당 sto의 모든 디시전 삭제
            Decision::where('sto_item_id', $stoItemId)->delete();
            // 2. sto 포인트 루프내에서 디시전 발생
            $end = count($points) - 1;

            if($end+1 > $FIRSTINDEX) {
                $idx = $BASICGAP;
                $sIdx = 0;
                $a = 0; $b = 0;
                $test = []; $test2 = [];
                $zeroCk = false;
                while($idx <= $FIRSTINDEX) { // all 0
                    $b = $points[$idx]->positive - $points[$idx-1]->positive;
                    $a = $points[$idx-1]->positive - $points[$idx-2]->positive;
                    if($a == 0 && $b == 0) {
                        if($sIdx + $FIRSTINDEX == $idx) {
                            $tmp = $sIdx + $FIRSTINDEX;
                            //$zeroCk = true;
                            $zeroCk = false;
                            if(isset($points[$tmp])) {
                                $res [] = $points[$tmp];
                                $sIdx = $tmp;
                                $idx = $sIdx + $BASICGAP;
                            } else {
                                break;
                            }
                        } else {
                            $idx++;
                        }
                    } else {
                        $zeroCk = false;
                        break;
                    }
                }
                if(!$zeroCk) {
                    while($idx <= $end) { // changed
                        $b = $points[$idx]->positive - $points[$idx-1]->positive;
                        $a = $points[$idx-1]->positive - $points[$idx-2]->positive;
                        if($a * $b <= 0) {
                            $tmp = $sIdx + $SECONDINDEX;
                            if(isset($points[$tmp])) {
                                $res [] = $points[$tmp];
                                $sIdx = $tmp;
                                $idx = $sIdx + $BASICGAP;
                            } else {
                                break;
                            }
                        } else {
                            if($sIdx + $FIRSTINDEX == $idx) {
                                $tmp = $sIdx + $FIRSTINDEX;
                                if(isset($points[$tmp])) {
                                    $res [] = $points[$tmp];
                                    $sIdx = $tmp;
                                    $idx = $sIdx + $BASICGAP;
                                } else {
                                    break;
                                }
                            } else {
                                $idx++;
                            }
                        }
                    }
                }
            }
            foreach($res as $decsPoint) {
                // 3. 발생한 디시전위치 저장
                if(!is_null($decsPoint)) {
                    $program = Program::where('id', $stoItem->program_id)->first();
                    Decision::create([
                        'decide_user_id'    =>  $user->id,
                        'master_id'         =>  $program->user_id,
                        'sto_item_id'       =>  $stoItemId,
                        'point_id'          =>  $decsPoint->id
                    ]);
                }
            }
        }
        return $res;
    }

    public static function setDecision($user = null, $point = null) {
        $res = 0;
        if(!is_null($point) && !is_null($user)) {
            $stoId = $point->sto_item_id;
            $sto = StoItem::where('id', $stoId)->first();
            $res = $sto->decision;
            if(!is_null($sto)) {
                self::calcDecisionAllPoints();
                $points = Point::where('sto_item_id', $stoId)
                    ->leftJoin('sticks', 'sticks.point_id', 'points.id')
                    ->where('points.status', 1)
                    ->whereNull('points.deleted_at')
                    ->whereNull('sticks.deleted_at')
                    ->selectRaw('points.*,
                        if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                        sum(if(sticks.value = 1, 1, 0)) as positive')
                    ->groupBy('points.id')
                    ->orderBy('real_date', 'asc')
                    ->orderBy('points.created_at', 'asc')
                    ->get();
                $l = count($points);
                $userId = $user->id;
                $temp = Decision::where('decide_user_id', $userId)->where('sto_item_id', $stoId);
                $decs = $temp->get();
                if($l > 3) {
                    // 해당 sto의 기존 디시전을 업데이트하는지, 삭제 > 추가 하는지도 고려
                    $startIdx = 0;
                    $endPoint = null;
                    while($startIdx >= $l-1) {
                        $end = self::calcRealDecision2($startIdx, $points);
                        $endPoint = $end->point;
                        $startIdx = $end->idx;
                        // doing after
                        if(!is_null($endPoint)) {

                        }

                    }
                    if(!is_null($endPoint) && is_null($decs)) {
                        $program = Program::where('id', $sto->program_id)->first();
                        Decision::create([
                            'decide_user_id'    =>  $userId,
                            'master_id'         =>  $program->user_id,
                            'sto_item_id'       =>  $stoId,
                            'point_id'          =>  $endPoint->id
                        ]);
                        $sto->update([ 'decision' => 1 ]);
                        $res = 1;
                    } else if(is_null($endPoint) && !is_null($decs)) {
                        $temp->delete();
                        $sto->update([ 'decision' => 0 ]);
                        $res = 0;
                    }
                }
                // if($l > 5) {
                //     if(self::calcRealDecision($points, 5))  {
                //         $res = 1;
                //         if(is_null($decs)) {
                //             Decision::create([
                //                 'decide_user_id'    =>  $userId,
                //                 'sto_item_id'       =>  $stoId,
                //                 'point_id'          =>  $point->id
                //             ]);
                //         }
                //         $sto->update([ 'decision' => 1 ]);
                //     }
                // } else if($l > 3) {
                //     if(self::calcRealDecision($points, 4)) {
                //         $res = 1;
                //         if(is_null($decs)) {
                //             Decision::create([
                //                 'decide_user_id'    =>  $userId,
                //                 'sto_item_id'       =>  $stoId,
                //                 'point_id'          =>  $point->id
                //             ]);
                //         }
                //         $sto->update([ 'decision' => 1 ]);
                //     };
                // } else {
                //     $sto->update([ 'decision' => 0 ]);
                //     $res = 0;
                //     $temp->delete();
                // }
            }
        }
        return $res;
    }

    private static function calcRealDecisions($startIdx, $points) {
        $l = count($points);
        if($l - $startIdx > 3) {
            $end = $startIdx + 3;
            $tmp1 = 0;
            $zeroCk = false;
            for($i = $startIdx+1 ; $i < $end ; $i++) {
                $tmp2 = $points[$i]->positive - $points[$i-1]->positive;
                if($i == $startIdx+1) $tmp1 = $tmp2;
                if($tmp1 == 0 && $tmp2 == 0) {
                    $zeroCk = true;
                } else {
                    $zeroCk = false;
                    break;
                }
            }
            if(!$zeroCk) {
                for($i = $startIdx+1 ; $i < $l ; $i++) {
                    $tmp2 = $points[$i]->positive - $points[$i-1]->positive;
                    if($i == $startIdx+1) $tmp1 = $tmp2;
                    if($tmp1 * $tmp2 <= 0) {
                        $end = $startIdx + 5;
                        break;
                    }
                }
            }
            if($l > $end) {
                return $points[$end];
            }
            else return null;
        }
        return null;
    }

    private static function calcRealDecision2($points) {
        $end = 5;
        $tmp1 = 0;
        foreach($points as $i=>$point) {
            if($i > 0) {
                $tmp2 = $points[$i]->positive - $points[$i-1]->positive;
                if($i == 1) $tmp1 = $tmp2;
                if($tmp1 * $tmp2 < 0) {
                    $end = 3;
                    break;
                }
            }
        }
        if(count($points) > $end) return $points[$end];
        else return null;
    }

    private static function calcRealDecision($arr, $start) {
        $res = false;
        $d = [];
        $end = $start - 3;
        $l = count($arr);
        for($i = $start; $i > $end; $i--) {
            $t = $l - $i;
            $r = $arr[$t]->positive - $arr[$t + 1]->positive;
            if($r > 0) {
                $d [] = 1;
            } else if($r < 0) {
                $d [] = -1;
            } else { // r == 0
                $d [] = 0;
            }
        }
        if(($arr[0] != $arr[1]) || ($arr[1] != $arr[2])) {
            $res = true;
        }
        return $res;
    }

    public static function resetOrder($groupId = null) {
        if(!is_null($groupId)) {
            $stos = StoItem::where('sto_group_id', $groupId)
                ->whereNull('deleted_at')
                ->orderBy('order', 'asc')
                ->get();
            $index = 0;
            foreach($stos as $sto) {
                $sto->update([ 'order' => $index ]);
                $index++;
            }
        }
    }

    public static function isReach($stoItem = null) {
        if(!is_null($stoItem)) {
            $points = Point::where('sto_item_id', $stoItem->id)
                ->whereNull('deleted_at')
                ->selectRaw('points.reach, points.target_user_id, if(points.postponed is null, points.rgst_date, points.postponed) as real_date')
                ->orderBy('real_date', 'asc')
                ->get();
            $l = count($points);
            $cnt = $stoItem->standard_cnt-1;
            $res = "";
            if($l >= $cnt+1) {
                for($i = $cnt ; $i < $l ; $i++) {
                    if($points[$i]->reach > 0) {
                        if($cnt == 0) { // 기준 횟수가 1일때 조건
                            $res = $points[$i];
                            break;
                        }
                        $end = $i-($stoItem->standard_cnt-1);
                        for($j=$i-1 ; $j >= $end ; $j--) { // 기타 연속된 값 비교
                            if($points[$j]->reach == 0) {
                                break;
                            } else if($j == $end) {
                                $res = $points[$i];
                            }
                        }
                        if($res != "") break;
                    }
                }
            }
            return $res;
        }
    }

    public static function getLastDecisionPoint($stoItemId = null) {
        $sto = StoItem::where('id', $stoItemId);
        $stoItem = $sto->first();
        if(is_null($stoItem)) return false;
        $points = Point::leftJoin('decisions', 'decisions.point_id', 'points.id')
            ->where('points.sto_item_id', $stoItemId)
            ->whereNull('points.deleted_at')
            ->selectRaw('points.*,
                if(points.postponed is null, points.rgst_date, points.postponed) as real_date,
                decisions.decide_user_id
            ')
            ->orderBy('real_date', 'desc')
            ->orderBy('points.created_at', 'desc')
            ->first();

        if(!is_null($points)) {
            if(is_null($points->decide_user_id)) return false;
            else return true;
        } else {
            return false;
        }
    }
}
