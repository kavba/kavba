<?php

namespace App\Models;

use App\Http\Helpers\dbSetHelpers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Student extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'birth_date', 'user_id', 'phone', 'is_valid'
    ];

    use SoftDeletes;

    protected $connection;
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $ds = Auth::user()->data_set;
        $this->connection = 'kavba';
        if(in_array($ds, dbSetHelpers::$defaultDates)) {
            $this->connection = 'kavba20' . $ds;
        }
    }

    public static function checkManager($user = null, $studentId = null) {
        return true;
	    // true 반환하면 담당자
	    if(!is_null($user) && $user->level >= 2) {
		    $userClasses = ClassRelation::where('user_id', $userId)
		    	->where('flag', 1)
		    	->get();
		    foreach($userClasses as $cls) {
			    $ck = ClassRelation::where('student_id', $studentId)
			    	->where('flag', 2)
			    	->where('class_id', $cls->class_id)
			    	->count();
			    if($ck > 0) {
				    return true;
			    }
		    }

	    }
	    return false;
    }

}
