import 'core-js/es6/';
import "@babel/polyfill";
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { observable, action } from 'mobx';
import { Provider, observer } from 'mobx-react';
import MainStore from './stores/main';
import FuncStore from './stores/func';
import App from './shared/App';

const mainStore = new MainStore();
const funcStore = new FuncStore();

export default class Index extends Component {

    componentDidMount() {
    }

    render() {
        return (
            <BrowserRouter>
                <Provider mainStore={mainStore} funcStore={funcStore}>
                    <App />
                </Provider>
            </BrowserRouter>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
