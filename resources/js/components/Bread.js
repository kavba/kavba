import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { withStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core/';

class Bread extends Component {

    controllTopFlex = flag => e => {
        const { subMenus } = this.props;
        if(subMenus) {
            if(flag) {
                $("#left_box").css('flex', 'unset');
                $("#right_box").css('flex', '1');
            } else {
                $("#left_box").css('flex', '1');
                $("#right_box").css('flex', 'unset');
            }
        }
    }

    render() {
        const { classes, title, subTitle, subMenus } = this.props;
        return (
            <div className={classes.titleRoot}>
                <div id="left_box" className={classes.leftBox}>
                    <div className={classes.titleBox}>
                        <span className={classes.title}>{title ? title.toUpperCase() : ""}</span>
                        {subTitle && (
                            <span className={classes.subTitle}>{`\u00A0\u00A0>\u00A0\u00A0` + subTitle.toUpperCase()}</span>
                        )}
                    </div>
                    <div className={classes.rb}></div>
                </div>
                <div id="right_box" className={classes.rightBox} >
                    <React.Fragment>
                        <div className={classes.rb2}></div>
                        <ToggleButtonGroup
                            value={subTitle}
                            exclusive
                            onChange={this.props.handleSubMenu}
                            classes={{
                                root: classes.btnRoot,
                            }}
                        >
                            {subMenus && subMenus.map((m,i) => (
                                <ToggleButton key={"m_"+i} value={m.value} classes={{ root: classes.btns, selected: classes.selected }}>
                                    {m.title}
                                </ToggleButton>
                            ))}
                        </ToggleButtonGroup>
                    </React.Fragment>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    titleRoot: {
        flex: 1,
        display: 'flex',
        background: 'linear-gradient(to right, #865995 0%,#ca5388 36%,#f55f5c 67%,#f68a15 100%)',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    titleBox: {
        flex: 1,
        padding: '10px 45px 10px 45px',
        height: 60,
        background: 'rgba(51,51,51,.7)',
    },
    rb: {
        width: 0,
        height: 0,
        borderLeft: '20px solid rgba(51,51,51,.7)',
        borderRight: 'none',
        borderTop: '0px solid rgba(51,51,51,.7)',
        borderBottom: '60px solid transparent'
    },
    rb2: {
        width: 0,
        height: 0,
        borderLeft: 'none',
        borderRight: '20px solid #333',
        borderTop: '60px solid transparent',
        borderBottom: '0px solid #333'
    },
    title: {
        fontFamily: 'JungBold',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        background: 'linear-gradient(to right, #ca5388 0%,#f55f5c 60%,#f68a15 100%)',
        WebkitBackgroundClip: 'text',
        textFillColor: 'transparent',
        color: '#fff',
        fontSize: '1.6rem',
        marginBottom: -5
    },
    subTitle: {
        fontFamily: 'JungNormal',
        color: '#fff',
        fontSize: '1.2rem',
    },
    leftBox: {
        display: 'flex',
        flex: 'unset',
        transition: 'all .35s ease'
    },
    rightBox: {
        display: 'flex',
        flex: '1',
        transition: 'all .35s ease',
    },
    btnRoot: {
        borderRadius: 'unset',
        border: 'none',
        flex: 1,
        minWidth: 100,
        justifyContent: 'flex-end',
        backgroundColor: '#333',
    },
    btns: {
        borderRadius: 'unset',
        minWidth: 100,
        height: 60,
        borderTop: 0,
        borderBottom: 0,
        backgroundColor: '#333',
        color: '#fff!important',
        '&:first-child': {
            borderLeft: 0
        },
        '&:last-child': {
            borderRight: 0
        },
        borderColor: '#666!important',
        '&:hover': {
            backgroundColor: '#454545'
        }
    },
    selected: {
        backgroundColor: '#454545!important',
    }
});

export default withRouter(withStyles(styles)(Bread));