import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { useTheme } from '@material-ui/core/styles';
import { purple } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({
    root: {
        flex:'none',display:'flex',alignItems:'center',padding:'8px 24px',backgroundColor:'#666'
    },
    title: {
        flex:1,fontFamily:'jungBold',fontSize:'1.24rem',color:'#efefef'
    },
    button: {
        width: 'auto',
        '& button': {
            margin: '0px 0px 0px 8px'
        }
    },
    footer: {
        backgroundColor: '#ededed'
    }
}));

const theme2 = createMuiTheme({
    palette: {
        primary: {
            light: '#aaa',
            main: '#676767',
            dark: '#333',
            contrastText: '#fff',
        },
    },
});

export default function Dialogs(props) {
    const theme = useTheme();
    const classes = useStyles();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const handleClose = () => {
        props.handleOpen(false);
    }
    const leftFunc = () => {
        if(props.leftFunc.func) props.leftFunc.func();
    }
    const rightFunc = () => {
        if(props.rightFunc.func) props.rightFunc.func();
    }
    let actions = null;
    if(props.kinds == "content") {
        if(props.leftFunc || props.rightFunc) {
            actions = (
                <DialogActions className={classes.footer}>
                    {props.leftFunc ? (
                        <Button onClick={leftFunc} variant="contained" color="primary">
                            {props.leftFunc.label}
                        </Button>
                    ) : null}
                    {props.rightFunc ? (
                        <Button onClick={rightFunc} variant="contained" color="primary">
                            {props.rightFunc.label}
                        </Button>
                    ) : null}
                </DialogActions>
            );
        }
    } else if(props.kinds == "confirm") {
        actions = (
            <DialogActions className={classes.footer}>
                <Button onClick={leftFunc} variant="contained" color="primary">
                    추가
                </Button>
                <Button onClick={handleClose} variant="contained" color="primary" autoFocus>
                    닫기
                </Button>
            </DialogActions>
        );
    } else if(props.kinds == "alert") {
        actions = (
            <DialogActions className={classes.footer}>
                {props.rightFunc ? (
                    <Button onClick={rightFunc} variant="contained" color="primary">
                        {props.rightFunc.label}
                    </Button>
                ) : (
                    <Button onClick={handleClose} variant="contained" color="primary">
                        {"닫기"}
                    </Button>
                )}
            </DialogActions>
        );
    }

    return (
        <div>
            <Dialog
                classes={props.classes}
                fullScreen={fullScreen}
                fullWidth={props.fullWidth == true || props.fullWidth == false ? props.fullWidth : true}
                maxWidth={(props.maxWidth ? props.maxWidth : "md")}
                open={props.open}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <div id="responsive-dialog-title" className={classes.root}>
                    <div className={classes.title}>
                        {props.title}
                    </div>
                    <div className={classes.button}>
                        {props.subButton}
                    </div>
                </div>
                <DialogContent className="dialog-content-custom">
                    {props.content}
                </DialogContent>
                <ThemeProvider theme={theme2}>
                    {actions}
                </ThemeProvider>
            </Dialog>
        </div>
    );
}