import React from 'react';
import { inject } from 'mobx-react';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { ExpandLess, ExpandMore, StarBorder, SwapHoriz, RotateRight, Edit, Delete, Add } from '@material-ui/icons';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Button, Fab, IconButton, ListSubheader, List, ListItem, ListItemSecondaryAction, TextField, CircularProgress,
    FormControl, InputLabel, OutlinedInput, Select, MenuItem } from '@material-ui/core';
import ReactPaginate from 'react-paginate';

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Errors extends React.Component {
    state = {
        rDate: this.props.funcStore.convertToymd(),
        rCount: 0,
        rCount2: 0,
        rStudent: '',
        selectedError: '',
        data: [],
        studentList: [],
        page: 0,
        perPage: 10,
        total: 0,
        sort: true,
        studentLabelWidth: 0
    }

    componentDidMount() {
        this.loadMyUses();
        this.loadErrors();
    }

    loadMyUses = () => {
        const { user, funcStore } = this.props;
        if(!user) {
            return;
        }
        funcStore.conn('get', '/user/'+user.id+'/getUsersClasses', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    studentList: res.students,
                });
            } else {
                console.log("학생 로딩 실패");
            }
        });
    }

    loadErrors = () => {
        const { user, funcStore } = this.props;
        const { page, perPage, sort } = this.state;
        if(!user) {
            return;
        }
        const param = {
            sort: sort,
            page: page,
            perPage: perPage,
        }
        funcStore.conn('get', '/errors/'+user.id, false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    data: res.errors,
                    total: res.total
                });
            } else {
                console.log("에러 로딩 실패");
            }
        });
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        });
    };

    handleDateChange = date => {
        const d = date.target.value;
        this.setState({
            rDate: d
        });
    }

    handlePage = e => {
        const t = e.selected;
        this.setState({ page: t });
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadErrors();
            }, 0);
        });
    }

    handleRow = err => {
        const { selectedError, studentList } = this.state;
        if(selectedError.id == err.id) {
            this.setState({
                selectedError: '',
                rCount: 0,
                rCount2: 0,
                rDate: this.props.funcStore.convertToymd(),
                rStudent: ''
            });
        } else {
            this.setState({
                selectedError: err,
                rCount: err.count,
                rCount2: err.count2,
                rDate: err.rgst_date,
                rStudent: studentList.indexOf(err.student_id) !== -1 ? err.student_id : ''
            });
        }
    }

    addError = () => {
        const { user, funcStore } = this.props;
        const { rDate, rCount, rCount2, rStudent } = this.state;
        if(!user) {
            return;
        }
        const param = {
            rgstDate: rDate,
            count: rCount,
            count2: rCount2,
            studentId: rStudent
        }
        funcStore.conn('post', '/error/'+user.id+'/post', false, true, param, (res)=>{
            if(res.result == "true") {
                this.loadErrors();
            } else {
                if(res.duplicate) alert("동일 날짜 데이터가 존재합니다");
                else console.log("에러 추가 실패");
            }
        });
    }

    editError = () => {
        const { user, funcStore } = this.props;
        const { selectedError, data, rDate, rCount, rCount2, rStudent } = this.state;
        if(!user) {
            return;
        }
        const param = {
            rgstDate: rDate,
            count: rCount,
            count2: rCount2,
            studentId: rStudent
        }
        funcStore.conn('post', '/error/'+selectedError.id+'/put', false, true, param, (res)=>{
            if(res.result == "true" && res.errorData) {
                // this.setState({
                //     data: data.map(v => v.id == selectedError.id ? {
                //         ...v,
                //         rgst_date: rDate, count: rCount, count2: rCount2
                //     } : v)
                // });
                this.loadErrors();
            } else {
                if(res.duplicate) alert("해당 날짜에 데이터가 이미 존재합니다");
                else console.log("에러 수정 실패");
            }
        });
    }

    removeError = err => {
        if(!confirm("에러 데이터를 삭제하시겠습니까?")) return;
        const { funcStore } = this.props;
        funcStore.conn('post', '/error/'+err.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                this.loadErrors();
            } else {
                console.log("에러 삭제 실패");
            }
        });
    }

    setLabelWidth = (kind, ref) => {
        const { studentLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != studentLabelWidth) {
            this.setState({ studentLabelWidth: ref.offsetWidth });
        }
    }

    render() {
        const { classes } = this.props;
        const { rDate, rCount, rCount2, rStudent, data, selectedError, total, page, perPage, sort, studentLabelWidth, studentList } = this.state;
        return(
            <div className={classes.root}>
                <div className={classes.topBox}>
                    <form noValidate autoComplete="off" className={classes.formRoot}>
                        <TextField
                            id="date-picker"
                            label="날짜"
                            type="date"
                            value={rDate}
                            style={{ width:180 }}
                            className={classes.dateForm}
                            onChange={this.handleDateChange}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true
                            }}
                        />
                        <TextField
                            id="without-error"
                            label="미오류"
                            type="number"
                            className={classes.inputForm}
                            style={{ width:80 }}
                            value={rCount2}
                            onChange={this.handleChange('rCount2')}
                            variant="outlined"
                        />
                        <TextField
                            id="with-error"
                            label="오류"
                            type="number"
                            className={classes.inputForm}
                            style={{ width:80 }}
                            value={rCount}
                            onChange={this.handleChange('rCount')}
                            variant="outlined"
                        />
                        <FormControl variant="outlined" className={classes.inputForm} style={{ width:160 }}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(1, ref);
                                }}
                                htmlFor="register-student"
                            >
                                학생
                            </InputLabel>
                            <Select
                                value={rStudent}
                                onChange={this.handleChange('rStudent')}
                                input={
                                <OutlinedInput
                                    labelWidth={studentLabelWidth}
                                    name="rStudent"
                                    id="register-student"
                                />
                                }
                            >
                                <MenuItem value="">
                                    <em>선택</em>
                                </MenuItem>
                                {studentList.map((v,i) => {
                                    return (
                                        <MenuItem key={i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </form>
                    {selectedError ? (
                        <Fab variant="extended" aria-label="edit" className={classes.newErrorBtn}
                            onClick={() => this.editError()}>
                            <Edit className={classes.margin} />
                            {"Edit Error"}
                        </Fab>
                    ) : (
                        <Fab variant="extended" aria-label="add" className={classes.newErrorBtn}
                            onClick={() => this.addError()}>
                            <Add className={classes.margin} />
                            {"New Error"}
                        </Fab>
                    )}
                    
                </div>
                {data.length ? (
                    <React.Fragment>
                        <List
                            component="nav"
                            aria-labelledby="points-list"
                            className={classes.listRoot}
                            subheader={
                                <ListSubheader component="div" id="nested-list-subheader" className={classes.listContainer}>
                                    <ul className={classes.listHeader}>
                                        <li>날짜</li>
                                        <li>미오류</li>
                                        <li>오류</li>
                                        <li>학생</li>
                                    </ul>
                                </ListSubheader>
                            }
                        >
                            {data.map((v,i) => {
                                return (
                                    <ListItem
                                        key={"l_"+i}
                                        component="div"
                                        className={v.id == selectedError.id ? classes.selectedItem : classes.listItem}
                                        button
                                        onClick={()=>this.handleRow(v)}
                                    >
                                        <ul className={classes.listTitle}>
                                            <li>{v.rgst_date}</li>
                                            <li>{v.count2 ? v.count2 : "0"}</li>
                                            <li>{v.count ? v.count : "0"}</li>
                                            <li>{v.student_name ? v.student_name : "-"}</li>
                                        </ul>
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" aria-label="delete" className={classes.listItemDelete}
                                                onClick={() => this.removeError(v)}
                                            >
                                                <Delete />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                );
                                
                            })}
                        </List>
                        <ReactPaginate
                            previousLabel={"<"}
                            nextLabel={">"}
                            breakLabel={<span className="gap">...</span>}
                            pageCount={parseInt((total - 1) / perPage) + 1}
                            pageRangeDisplayed={20}
                            marginPagesDisplayed={10}
                            onPageChange={this.handlePage}
                            containerClassName={"pagination"}
                            previousLinkClassName={"previous_page"}
                            nextLinkClassName={"next_page"}
                            disabledClassName={"disabled"}
                            activeClassName={"active"}
                        />
                    </React.Fragment>
                ) : null}
            </div>
        );
    }
}

const styles = theme => ({
    root: {
        '& .pagination': {
            justifyContent: 'center',
            padding: '10px 0',
            borderTopLeftRadius: 0,
            borderTopRightRadius: 0,
            marginBottom: 0,
            backgroundColor: '#333',
            '& a' : {
                fontFamily: 'JungBold',
                fontSize: 16,
                cursor: 'pointer',
                padding: '0 5px',
                color: '#aaa',
                '&:hover': {
                    fontWeight: 'bold',
                    color: '#fff'
                },
                '&:focus': {
                    outline: 'none!important',
                    boxShadow: 'none!important'
                },
            },
            '& > li.active a': {
                color: '#fff'
            }
        }
    },
    topBox: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: '30px 0 15px'
    },
    formRoot: {
        display: 'flex'
    },
    newErrorBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        marginLeft: 15,
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    margin: {
        marginRight: theme.spacing(1),
    },
    dateForm: {
        marginRight: theme.spacing(1),
    },
    inputForm: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    listRoot: {
        width: '100%',
        paddingBottom: 0
    },
    listItemRoot: {
        borderTop: '1px solid #bbb'
    },
    listContainer: {
        paddingRight: 5,
        backgroundColor: '#333',
        color: '#fff',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
    },
    listHeader: {
        listStyle: 'none',
        padding: '0 35px 0 0',
        margin: 0,
        display: 'flex',
        '& > li': {
            flex: 1,
            textAlign: 'center',
        }
    },
    listItem: {
        minHeight: 48,
        display: 'flex',
        justifyContent: 'center',
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
        borderBottom: '1px solid #eee',
        color: '#fff',
    },
    selectedItem: {
        minHeight: 48,
        display: 'flex',
        justifyContent: 'center',
        borderBottom: '1px solid #eee',
        color: '#fff',
        backgroundImage: 'linear-gradient(to right, #f55f5c 0%,#f55f5c 100%)',
    },
    listTitle: {
        flex: 1,
        listStyle: 'none',
        padding: 0,
        margin: 0,
        display: 'flex',
        fontSize: '14px',
        '& > li': {
            flex: 1,
            textAlign: 'center',
        }
    },
    listItemDelete: {
        '& span': {
            color: '#fff'
        }
    }
});

export default withStyles(styles)(Errors);