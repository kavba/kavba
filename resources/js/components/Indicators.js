import 'isomorphic-fetch';
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

function sleep(delay = 0) {
    return new Promise(resolve => {
        setTimeout(resolve, delay);
    });
}

export default function Indicators(props) {
    const [open, setOpen] = React.useState(false);
    const loading = open && props.userList.length === 0;

    React.useEffect(() => {
        let active = true;

        if (!loading) {
            return undefined;
        }

        (async () => {
            const url = window.location.origin + '/web/users/'+props.stoItemId+'/getStoUsers';
            const doing = {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN' : document.getElementById('csrf').getAttribute('content'),
                    'ACCESS-FROM' : 'web'
                }
            }
            const response = await fetch(url, doing);
            await sleep(5e2);
            const data = await response.json();

            if (active) {
                props.setIndicators(data.users);
            }
        })();

        return () => {
            active = false;
        };
    }, [loading]);

    return (
        <Autocomplete
            id="asynchronous-demo"
            style={{ width: 180 }}
            open={open}
            onChange={(e, v)=> {
                props.handleIndicator(v);
            }}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            getOptionLabel={option => option.name}
            options={props.userList}
            loading={loading}
            renderInput={params => (
                <TextField
                {...params}
                label="지시자"
                fullWidth
                variant="outlined"
                InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                    <React.Fragment>
                        {loading ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                    </React.Fragment>
                    ),
                }}
                />
            )}
            classes={{
                root: props.classes.textField
            }}
        />
    );
}