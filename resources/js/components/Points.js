import React from 'react';
import { inject } from 'mobx-react';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { RotateRight, Save, Edit, Delete, Add, RadioButtonUnchecked, RadioButtonChecked, Clear } from '@material-ui/icons';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Typography, Button, IconButton, Fab, ListSubheader, List, ListItem, ListItemIcon, TextField, CircularProgress,
    ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import { Indicators, Dialogs } from '../components/';

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
class Points extends React.Component {
    constructor(props) {
        super();
        this.state = {
            stoItem: '',
            data: [],
            loadFunc: this.loadPoints.bind(this),
            reachType: 0,
            rDate: props.funcStore.convertToymd(),
            rCount: 15,
            rTarget: '',
            userList: [],
            flag: false,
            selectedPoint: null,
            dialogOpen: false,
            resetDialogOpen: false,
            resetFromUser: '',
            resetToUser: '',
            resetDate: '',
            resetPoint: ''
        }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //     if(nextProps.stoItem.id !== this.props.stoItem.id) {
    //         return true;
    //     } else if(nextProps.flag != this.state.flag) {
    //         return true;
    //     }
    //     return false;
    // }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.stoItem.id !== prevState.stoItem.id) {
            prevState.loadFunc(nextProps.stoItem.id);
            return {
                stoItem: nextProps.stoItem,
                userList: []
            };
        }
        if(nextProps.flag !== prevState.flag) {
            prevState.loadFunc(nextProps.stoItem.id);
            return {
                flag: nextProps.flag
            }
        }
        return null;
    }

    loadPoints = stoItemId => {
        const { funcStore } = this.props;
        if(!stoItemId) {
            return;
        }
        funcStore.conn('get', '/points/'+stoItemId+'/getAllPoints', false, true, null, (res)=>{
            if(res.result == "true") {
                const tmp = res.points.map(v => {
                    v.open = v.status == 0 ? true : false;
                    return v;
                });
                this.setState({
                    data: tmp,
                    reachType: res.stoItem.reach_type
                });
                this.forceUpdate();
            } else {
                console.log("포인트 로딩 실패");
            }
        });
    }

    handleExpand = pointId => {
        this.setState({
            data: this.state.data.map(v => v.id === pointId ? {...v, open: !v.open} : v)
        });
        this.forceUpdate();
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        });
        this.forceUpdate();
    };

    handleDateChange = date => {
        const d = date.target.value;
        this.setState({
            rDate: d
        });
    }

    changeStatus = point => {
        const { funcStore } = this.props;
        const status = point.status;
        const param = {
            status: (status ? 0 : 1),
        }
        funcStore.conn('post', '/point/'+point.id+'/status', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    data: this.state.data.map(v => v.id === point.id ? {...v, status: !v.status}: v)
                });
                this.forceUpdate();
            } else {
                console.log("포인트 추가 실패");
            }
        });
    }

    changeDate = point => {
        const { funcStore, stoItem } = this.props;
        const { rDate, rTarget } = this.state;
        if(!confirm("지시(포인트) 날짜를 변경하시겠습니까?\n(연장 날짜에 해당하는 체크는 모두 삭제됩니다)")) return;

        const c1 = new Date(rDate);
        const c2 = new Date(point.rgst_date);
        if(+c1 <= +c2) {
            alert("시작일자 이후로 변경가능합니다");
            return;
        }
        
        const param = {
            postponed: rDate,
            target: rTarget.id ? rTarget.id : null
        }
        funcStore.conn('post', '/point/'+point.id+'/postpone', false, true, param, (res)=>{
            if(res.result == "true") {
                // this.setState({
                //     data: this.state.data.map(v => v.id === point.id ? {...v, postponed: rDate}: v)
                // });
                this.loadPoints(stoItem.id);
                alert("포인트 시작일자가 변경되었습니다");
                this.forceUpdate();
            } else {
                console.log("포인트 날짜 변경 실패");
            }
        });
    }

    validation = () => {
        const { rDate, rCount, rTarget } = this.state;
        if(rDate == '') {
            console.log("날짜 입력 필수");
            return false;
        } else if(rCount == '') {
            console.log("회기 당 시도수 입력 필수");
            return false;
        } else if(rCount <= 0) {
            console.log("회기 당 시도수 입력 오류");
            return false;
        } else if(rTarget == '') {
            console.log("지시자 입력 필수");
            return false;
        }
        return true;
    }

    register = () => {
        if(!this.validation()) return;
        const { funcStore, stoItem, standardForStack } = this.props;
        const { reachType, rDate, rCount, rTarget } = this.state;
        const status = this.props.funcStore.conn('get', '/stoItem/'+stoItem.id+'/completed', false, false, null, null);
        if(reachType == 1 && rCount < standardForStack) {
            alert("회기 당 시도수는 도달 기준 횟수보다 커야합니다"); return;
        }
        if(status == 1) {
            alert("완료된 STO 입니다"); return;
        } else if(status == 2) {
            alert("중지된 STO 입니다"); return;
        } else if(status == -1) {
            alert("취소된 STO 입니다"); return;
        }
        
        //const res = this.props.funcStore.conn('get', '/stoItem/'+stoItem.id+'/getDecision', false, false, null, null);
        const res = this.props.funcStore.conn('get', '/stoItem/'+stoItem.id+'/getLastDecision', false, false, null, null);
        if(res && res.result == true) {
            if(!confirm("마지막 포인트에서 디시전이 발생하였습니다.\n이어서 새로운 포인트를 추가하시겠습니까?")) return;
        }
        const param = {
            stoItemId: stoItem.id,
            targetUserId: rTarget.id,
            rgstDate: rDate,
            count: rCount,
        }
        funcStore.conn('post', '/point/post', false, true, param, (res)=>{
            if(res.result == "true") {
                this.loadPoints(stoItem.id);
            } else {
                console.log("포인트 추가 실패");
            }
        });
    }

    setIndicators = users => {
        this.setState({
            userList: users
        });
        this.forceUpdate();
    }

    handleIndicator = user => {
        this.setState({
            rTarget: user
        });
    }

    handleResetIndicator = user => {
        this.setState({
            resetToUser: user
        });
    }

    openEdit = point => {
        this.setState({ selectedPoint: point });
        this.handleOpen(true);
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
        this.forceUpdate();
    }

    resetDialog = () => {
        this.setState({
            selectedPoint: null
        });
    }

    handleResetDialog = flag => {
        this.setState({ resetDialogOpen: flag });
    }

    editPoint = () => {

    }

    removePoint = point => {
        if(!confirm("포인트를 삭제하시겠습니까?")) return;
        this.props.funcStore.conn('post', '/point/'+point.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    data: this.state.data.filter(v => v.id != point.id)
                });
                this.props.updateSelectedStoItem(res.stoItem);
                alert("포인트가 삭제되었습니다");
                //this.forceUpdate();
            } else {
                console.log("포인트 삭제 실패");
            }
        });
    }

    removeHistory = (history, point, index) => {
        const { funcStore, stoItem } = this.props;
        if(!confirm("연장에 있는 해당 시도를 삭제하시겠습니까?")) return;
        const param = {
            pointId: point.id,
            userId: history.user_id,
            stickDate: history.stick_date
        }
        this.props.funcStore.conn('post', '/stick/deleteHistory', false, true, param, (res)=>{
            if(res.result == "true") {
                // this.setState({
                //     data: this.state.data.map(v => v.id == point.id ? {
                //         ...v, history: v.history.filter((h,i) => i != index)
                //     } : v)
                // });
                this.loadPoints(stoItem.id);
                alert("연장 시도가 삭제되었습니다");
                //this.forceUpdate();
            } else {
                console.log("시도 삭제 실패");
            }
        });
    }

    resetStickUser = () => {
        const { resetFromUser, resetToUser, resetDate, resetPoint } = this.state;
        if(!resetFromUser || !resetToUser.id) {
            alert("지시자를 선택하세요");
            return;
        }
        if(resetFromUser == resetToUser.id) {
            alert("다른 지시자를 선택하세요");
            return;
        }
        // reset sticks user
        const param = {
            pointId: resetPoint.id,
            fromUserId: resetFromUser,
            toUserId: resetToUser.id,
            date: resetDate
        }
        this.props.funcStore.conn('post', '/stick/resetHistoryUser', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    data: this.state.data.map(v => v.id == resetPoint.id ? {
                        ...v, history: res.history
                    } : v)
                });
                alert("연장 시도 지시자가 변경되었습니다(complete)");
                this.handleResetDialog(false);
            } else {
                console.log("시도 지시자 변경 실패(fail)");
            }
        });
    }

    render() {
        const { classes, stoItem, auth, mainStore } = this.props;
        const { data, reachType, rDate, rCount, rTarget, userList, selectedPoint, dialogOpen, resetDialogOpen,
            resetUser, resetDate, resetPoint } = this.state;
        const lang = mainStore.getLang(mainStore.country);
        const l = data.length;
        const dialogForm = (
            <div>
                <div className={classes.container}>

                </div>
            </div>
        );
        const editButton = (
            <div className={classes.dialogBtn}>
                <Fab size="small" variant="round" aria-label="save" className={classNames(classes.fab)}
                    onClick={() => this.editPoint()}>
                    <Save />
                </Fab>
                <Fab size="small" variant="round" aria-label="remove" className={classNames(classes.fab)}
                    onClick={
                        () => this.removePoint()}>
                    <Delete />
                </Fab>
            </div>
        );

        const resetDialogForm = (
            <div>
                <div className={classes.resetContainer}>
                    <Indicators
                        classes={classes}
                        stoItemId={stoItem.id}
                        userList={userList}
                        setIndicators={this.setIndicators}
                        handleIndicator={this.handleResetIndicator}
                    />
                </div>
            </div>
        );

        const resetButton = (
            <div className={classes.dialogBtn}>
                <Fab size="small" variant="round" aria-label="save" className={classNames(classes.fab)}
                    onClick={() => this.resetStickUser()}>
                    <Save />
                </Fab>
            </div>
        );

        return (
            <React.Fragment>
                <div className={classes.addRoot}>
                    <form noValidate autoComplete="off" className={classes.addFormRoot}>
                        <TextField
                            id="date-picker"
                            label={lang['stoitem5']}
                            type="date"
                            value={rDate}
                            className={classes.textField}
                            onChange={this.handleDateChange}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true
                            }}
                        />
                        <TextField
                            id="outlined-name"
                            label={lang['stoitem6']}
                            type="number"
                            className={classes.textField}
                            value={rCount}
                            onChange={this.handleChange('rCount')}
                            variant="outlined"
                        />
                        <Indicators
                            classes={classes}
                            stoItemId={stoItem.id}
                            userList={userList}
                            setIndicators={this.setIndicators}
                            handleIndicator={this.handleIndicator}
                        />
                    </form>
                    {auth && (mainStore.user.level > 0) && (
                        <Fab variant="extended" aria-label="add" className={classes.newPointBtn}
                            onClick={
                                () => this.register()}>
                            <Add className={classes.extendedIcon} />
                            {"New Point"}
                        </Fab>
                    )}
                </div>
                <List
                    component="nav"
                    aria-labelledby="points-list"
                    className={classes.root}
                    subheader={
                        <ListSubheader component="div" id="nested-list-subheader" className={classes.listContainer}>
                        <ul className={classes.listHeader}>
                                <li style={{width:120}}>{lang['stoitem8']}</li>
                                <li style={{width:120}}>{lang['stoitem9']}</li>
                                <li style={{width:120}}>{lang['stoitem10']}</li>
                                <li style={{flex:1}}>{lang['stoitem11']}</li>
                                <li style={{width:100}}>{lang['stoitem12']}</li>
                                <li style={{width:80}}>{lang['stoitem13']}</li>
                                <li style={{width:50}}>{lang['stoitem14']}</li>
                                <li style={{width:50}}></li>
                        </ul>
                        </ListSubheader>
                    }
                >
                    { data.map((v,i) => {
                        const sticks = v.stick;
                        const history = v.history;
                        let a=0, b=0, c=0, hTxt = "";
                        if(sticks.length) {
                            sticks.map((s,idx) => {
                                if(s.value == 0) {
                                    b++;
                                } else if(s.value == 1) {
                                    a++;
                                } else if(s.value == 2) {
                                    c++;
                                }
                            })
                        }
                        const popText = "+ " + a + " / - " + b + " / P " + c;
                        history.map((h,i) => {
                            hTxt = h.stick_cnt + (i != 0 ? " + " : "") + hTxt;
                        });
                        return (
                            <div key={i} className={classNames(classes.listRoot, reachType == 0 ? classes.ingRow : classes.ingStackRow)}>
                                <ExpansionPanel className={classes.listItem}>
                                    <ExpansionPanelSummary
                                        aria-label="Expand"
                                        aria-controls={"additional-actions"+i+"-content"}
                                        id={"additional-actions"+i+"-header"}
                                        classes={{
                                            content: classes.listItemSummary,
                                            expanded: classes.expandedSummary
                                        }}
                                    >
                                        <ul className={classes.listTitle}>
                                            <li style={{width:120}}>{v.rgst_date}</li>
                                            <li style={{width:120}}>{v.postponed ? v.postponed : "X"}</li>
                                            <li style={{width:120}}>{v.target_user_name}</li>
                                            <li style={{flex:1}}>
                                                {v.count + " / " + (sticks ? sticks.length : 0) + (hTxt ? " ("+hTxt+")" : "")}
                                            </li>
                                            <li style={{width:60}}>{v.status == 1 ? lang['stoitem16'] : lang['stoitem15']}</li>
                                            <li style={{width:50}}>
                                                {v.reach == 1 ? <RadioButtonUnchecked/> : v.reach == 2 ? <RadioButtonChecked/> : <Clear/>}
                                            </li>
                                            <li style={{width:50}}>
                                                <IconButton className={classes.listIconBox}
                                                    edge="end" aria-label="postpone"
                                                    onClick={e => {e.stopPropagation();this.changeDate(v);}}
                                                    onFocus={e => {e.stopPropagation();}}
                                                    disabled={mainStore.user.level > 0 ? false : true}>
                                                    <RotateRight className={classes.listIcon} />
                                                </IconButton>
                                            </li>
                                            <li style={{width:50}}>
                                                <IconButton className={classes.listIconBox}
                                                    edge="end" aria-label="edit"
                                                    onClick={e => {e.stopPropagation();this.removePoint(v);}}
                                                    onFocus={e => {e.stopPropagation();}}
                                                    disabled={mainStore.user.level > 0 ? false : true}>
                                                    <Delete className={classes.listIcon} />
                                                </IconButton>
                                            </li>
                                        </ul>
                                    </ExpansionPanelSummary>
                                    {sticks.length ? (
                                        <ExpansionPanelDetails className={classes.listItemDetails}>
                                            <div className={classes.listDetailTitle}>
                                                {lang['point5'] + " : " + popText}
                                            </div>
                                            <ul className={classNames(classes.listHistoryTitleRoot, classes.listHistoryTitle)}>
                                                <li>{lang['point1']}</li>
                                                <li>{lang['point2']}</li>
                                                <li>{lang['point3']} (+ / - / P)</li>
                                                <li>{lang['point4']}</li>
                                            </ul>
                                            {history.map((h,i2) => {
                                                return (
                                                    <ul key={"history_"+i+"_"+i2} className={classes.listHistoryTitle}>
                                                        <li>{h.stick_date}</li>
                                                        <li>
                                                            <Button className={classes.resetBtn} onClick={()=>{
                                                                this.setState({ 
                                                                    resetFromUser: h.user_id, 
                                                                    resetToUser: '', 
                                                                    resetDate: h.stick_date, 
                                                                    resetPoint: v 
                                                                });
                                                                this.handleResetDialog(true);
                                                            }}>{h.user_name}</Button>
                                                        </li>
                                                        <li>{h.stick_cnt + " ("+h.positive+" / "+h.negative+" / "+h.pass+")"}</li>
                                                        <li>
                                                            <IconButton className={classes.listIconBox}
                                                                edge="end" aria-label="edit" onClick={()=>this.removeHistory(h, v, i2)}
                                                                disabled={mainStore.user.level > 0 ? false : true}>
                                                                <Delete className={classes.listIcon}/>
                                                            </IconButton>
                                                        </li>
                                                    </ul>
                                                );
                                            })}
                                        </ExpansionPanelDetails>
                                    ) : null}
                                </ExpansionPanel>
                            </div>
                        );
                    })}
                </List>
                <Dialogs
                    kinds={"content"}
                    open={dialogOpen}
                    handleOpen={this.handleOpen}
                    title={"Edit"}
                    content={dialogForm}
                    subButton={mainStore.user.level > 0 ? editButton : ''}
                    rightFunc={{
                        label: "close",
                        func: ()=> {
                            this.handleOpen(false);
                        }
                    }}
                />
                <Dialogs
                    kinds={"content"}
                    open={resetDialogOpen}
                    handleOpen={this.handleResetDialog}
                    title={lang['point6'] + " : " + resetDate}
                    content={resetDialogForm}
                    subButton={mainStore.user.level > 0 ? resetButton : ''}
                    maxWidth={"xs"}
                    rightFunc={{
                        label: "close",
                        func: ()=> {
                            this.handleResetDialog(false);
                        }
                    }}
                />
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    addRoot: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: '30px 0 15px'
    },
    btn: {
        margin: theme.spacing(1),
    },
    newPointBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        marginLeft: 15,
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    listRoot: {
        borderTop: '1px solid #eee'
    },
    listContainer: {
        backgroundColor: '#333',
        color: '#fff',
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
    },
    listHeader: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        display: 'flex',
        '& > li': {
            textAlign: 'center'
        }
    },
    listItem: {
        paddingTop: 0,
        paddingBottom: 0,
        color: '#fff',
        background: 'none'
    },
    listItemSummary: {
        margin: 0
    },
    expandedSummary: {
        margin: '0!important',
        minHeight: '48px!important'
    },
    listItemDetails: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255,255,255,.14)'
    },
    listTitle: {
        flex: 1,
        listStyle: 'none',
        padding: 0,
        margin: 0,
        display: 'flex',
        fontSize: '14px',
        '& > li': {
            display: 'flex',
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center'
        }
    },
    listHistoryTitle: {
        width: '80%',
        minWidth: 756,
        listStyle: 'none',
        padding: 0,
        margin: 0,
        display: 'flex',
        fontSize: '14px',
        backgroundColor: 'rgba(0,0,0,.34)',
        padding: 0,
        marginBottom: 1,
        '& > li': {
            display: 'flex',
            flex: 1,
            textAlign: 'center',
            alignItems: 'center',
            justifyContent: 'center'
        }
    },
    listHistoryTitleRoot: {
        padding: '8px 0!important'
    },
    resetContainer: {
        display: 'flex',
        justifyContent: 'center',
        margin: '8px 0',
    },
    resetBtn: {
        color: '#fff'
    },
    listDetailTitle: {
        color: '#000',
        fontWeight: 'bold',
        marginBottom: 8
    },
    fab: {
        marginLeft: 8
    },
    subBtns: {
    },
    stickGrid: {
        flex: 'inherit',
        paddingLeft: 10,
        paddingRight: 10
    },
    stickNext: {
        borderLeft: '1px solid #000'
    },
    stickInfo: {
        alignitems: 'center',
        justifyContent: 'center'
    },
    textField: {
        marginRight: 16
        //margin: theme.spacing(1),
    },
    ingRow: {
        //background: 'cornsilk'
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
        color: '#fff'
    },
    ingStackRow: {
        //background: 'cornsilk'
        backgroundImage: 'linear-gradient(to right, #ca5388 0%,#f55f5c 65%,#f68a15 100%)',
        color: '#fff'
    },
    finishedRow: {
        background: 'tan'
    },
    addFormRoot: {
        display: 'flex'
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    listIcon: {
        color: '#fff',
    },
    listIconBox: {
        
    },
    dialogBtn: {
        // position: 'absolute',
        // top: 10,
        // right: 10
    },
    popRoot: {
		display: 'flex',
		flexDirection: 'column',
		overflow: 'visible',
		borderRadius: '7px'
	},
    popRow: {
		flex: 1,
		display: 'flex',
		background: '#fff',
        overflow: 'hidden',
        borderRadius: 7,
	},
	popGrid: {
		cursor: 'pointer',
		display: 'flex',
		height: 30,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
        fontSize: 14,
        padding: '8px 12px',
		color: '#444',
		'&:not(:first-child)': {
			borderLeft: '1px solid #444'
		},
	},
	popSub: {
		borderTop: '10px solid #fff',
		borderBottom: 'none',
		borderLeft: '10px solid transparent',
		borderRight: '10px solid transparent',
		width: 0,
		height: 0,
		position: 'absolute',
		bottom: '-10px',
		alignSelf: 'center'
    },
    popButton: {
        color: '#fff'
    }
});

export default withStyles(styles)(Points);