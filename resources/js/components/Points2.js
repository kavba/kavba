import React from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// import { Button, Fab, Dialog, ListItem, List, Divider, AppBar, Toolbar, IconButton, OutlinedInput,
//     Typography, Slide, Grid, InputLabel, MenuItem, FormControl, Select, TextField } from '@material-ui/core';
import MaterialTable from 'material-table';

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Points2 extends React.Component {
    constructor(props) {
        super();
        this.state = {
            stoItem: '',
            columns: [
                { title: '지시자', field: 'target_user_id' },
                { title: '날짜', field: 'rgst_date' },
                { title: '등록일', field: 'updated_at' },
                { title: '도달갯수', field: 'reach', type: 'numeric' },
                { title: '상태', field: 'status', lookup: { 0: '진행중', 1: '완료' } },
            ],
            data: [],
            loadFunc: this.loadPoints.bind(this)
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.stoItem.id !== prevState.stoItem.id) {
            prevState.loadFunc(nextProps.stoItem.id);
            return {
                stoItem: nextProps.stoItem
            };
        }
        return null;
    }

    loadPoints = stoItemId => {
        const { funcStore } = this.props;
        if(!stoItemId) {
            return;
        }
        funcStore.conn('get', '/points/'+stoItemId+'/getAll', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    data: res.points
                });
                //this.forceUpdate();
            } else {
                console.log("포인트 로딩 실패");
            }
        });
    }

    render() {
        return (
            <MaterialTable
            title="POINTS list"
            columns={this.state.columns}
            data={this.state.data}
            editable={{
                onRowUpdate: (newData, oldData) =>
                new Promise(resolve => {
                    setTimeout(() => {
                        resolve();
                        const data = [...this.state.data];
                        data[data.indexOf(oldData)] = newData;
                        this.setState({
                            data: data
                        });
                    }, 600);
                }),
                onRowDelete: oldData =>
                new Promise(resolve => {
                    setTimeout(() => {
                        resolve();
                        const data = [...this.state.data];
                        data.splice(data.indexOf(oldData), 1);
                        this.setState({
                            data: data
                        });
                    }, 600);
                }),
            }}
            options={{
                showTitle: false,
                search: false,
                toolbar: false,
                paging: false,
                pageSize: 100,
                emptyRowsWhenPaging: false,
            }}
            />
        );
    }
}

const styles = theme => ({
});

export default withStyles(styles)(Points2);