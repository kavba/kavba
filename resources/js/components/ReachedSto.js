import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import clsx from 'clsx';
import { lighten, makeStyles } from '@material-ui/core/styles';
import { Button, Fab, TextField, OutlinedInput, Input, InputLabel, InputAdornment, IconButton,
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, Toolbar, Typography,
    Paper, Checkbox, Chip,
    FormControl, Select, MenuItem, Tooltip
} from '@material-ui/core';
import ReactPaginate from 'react-paginate';
import FilterListIcon from '@material-ui/icons/FilterList';
import ChangeHistoryIcon from '@material-ui/icons/ChangeHistory';
import ForumIcon from '@material-ui/icons/Forum';
import PieChartIcon from '@material-ui/icons/PieChart';

function arrowGenerator(color) {
    return {
        '&[x-placement*="bottom"] $arrow': {
            top: 0,
            left: 0,
            marginTop: '-0.95em',
            width: '2em',
            height: '1em',
            '&::before': {
            borderWidth: '0 1em 1em 1em',
            borderColor: `transparent transparent ${color} transparent`,
            },
        },
        '&[x-placement*="top"] $arrow': {
            bottom: 0,
            left: 0,
            marginBottom: '-0.95em',
            width: '2em',
            height: '1em',
            '&::before': {
            borderWidth: '1em 1em 0 1em',
            borderColor: `${color} transparent transparent transparent`,
            },
        },
    };
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
    { id: 'number', numeric: false, disablePadding: true, label: '순번' },
    { id: 'domain', numeric: false, disablePadding: true, label: '도메인' },
    { id: 'lto', numeric: false, disablePadding: true, label: 'LTO' },
    { id: 'sto', numeric: false, disablePadding: true, label: 'STO' },
    { id: 'finish', numeric: false, disablePadding: true, label: '상태' },
    { id: 'type', numeric: false, disablePadding: true, label: '구분' },
    { id: 'user', numeric: false, disablePadding: true, label: '담당자' },
    { id: 'rgstDate', numeric: false, disablePadding: true, label: '도달일' },
];

const useStylesBootstrap = makeStyles(theme => ({
    arrow: {
        position: 'absolute',
        fontSize: 6,
        '&::before': {
            content: '""',
            margin: 'auto',
            display: 'block',
            width: 0,
            height: 0,
            borderStyle: 'solid',
        },
    },
    popper: arrowGenerator(theme.palette.common.black),
    tooltip: {
        position: 'relative',
        backgroundColor: theme.palette.common.black,
        fontSize: 10
    },
    tooltipPlacementLeft: {
        margin: '0 8px',
    },
    tooltipPlacementRight: {
        margin: '0 8px',
    },
    tooltipPlacementTop: {
        margin: '8px 0',
    },
    tooltipPlacementBottom: {
        margin: '8px 0',
    },
}));
  
function BootstrapTooltip(props) {
    const { arrow, ...classes } = useStylesBootstrap();
    const [arrowRef, setArrowRef] = React.useState(null);

    return (
    <Tooltip
        classes={classes}
        placement="top"
        PopperProps={{
        popperOptions: {
                modifiers: {
                    arrow: {
                        enabled: Boolean(arrowRef),
                        element: arrowRef,
                    },
                },
            },
        }}
        {...props}
        title={
        <React.Fragment>
            {props.title}
            <span className={arrow} ref={setArrowRef} />
        </React.Fragment>
        }
    />
    );
}

BootstrapTooltip.propTypes = {
    title: PropTypes.node,
};

function EnhancedTableHead(props) {
    const { classes, onSelectAllClick, order, orderBy, rowCount, onRequestSort } = props;
    const createSortHandler = property => event => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {headCells.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align={'center'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        classes={(headCell.id == 'info' || headCell.id == 'edit') ? {
                            root: classes.rowBtnHeader
                        } : null}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
    sortBtn: {
        transition: 'transform 300ms ease'
    },
    sortOn: {
        transform: 'rotate(-180deg)'
    },
    sortOff: {
        transform: 'rotate(0deg)'
    }
}));

const EnhancedTableToolbar = props => {
    const classes = useToolbarStyles();

    return (
        <Toolbar
            className={clsx(classes.root)}
        >
            <Typography className={classes.title} variant="h6" id="tableTitle"></Typography>
            <Tooltip title={props.sort ? "오름차순으로" : "내림차순으로"}>
                <IconButton aria-label="sort"
                    onClick={()=>props.handleSort()}
                >
                    <FilterListIcon className={classNames(classes.sortBtn, (props.sort ? classes.sortOff : classes.sortOn))} />
                </IconButton>
            </Tooltip>
        </Toolbar>
    );
};

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& .pagination': {
            justifyContent: 'center',
            padding: '10px 0 15px',
            '& a' : {
                fontFamily: 'JungBold',
                fontSize: 16,
                cursor: 'pointer',
                padding: '0 5px',
                color: '#aaa',
                '&:hover': {
                    fontWeight: 'bold',
                    color: '#fff'
                },
                '&:focus': {
                    outline: 'none!important',
                    boxShadow: 'none!important'
                },
            },
            '& > li.active a': {
                color: '#fff'
            }
        }
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
        backgroundColor: '#333',
        color: '#fff',
        '& svg, & span': {
            color: '#fff'
        }
    },
    table: {
        minWidth: 750,
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
        '& thead': {
            backgroundColor: '#333'
        },
        '& th': {
            color: '#fff'
        },
        '& tbody td': {
            lineHeight: "2.6",
            color: '#fff'
        },
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    margin: {
        margin: theme.spacing(1),
    },
    textField: {
        flexBasis: 200,
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    formControl: {
        margin: theme.spacing(1),
        width: 120
    },
    rowBtn: {

    },
    rowBtnHeader: {
        width: 130
    },
    tableRow: {
        cursor: 'pointer'
    }
}));

export default function ReachedSto(props) {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [rowsPerPage, setRowsPerPage] = React.useState(props.perPage);
    const [classLabelWidth, setClassLabelWidth] = React.useState(0);
    const [overLabelWidth, setOverLabelWidth] = React.useState(0);
    const [searchLabelWidth, setSearchLabelWidth] = React.useState(0);
    const rows = props.data;

    const handleRequestSort = (event, property) => {
        const isDesc = orderBy === property && order === 'desc';
        setOrder(isDesc ? 'asc' : 'desc');
        setOrderBy(property);
    };

    const handleSelectAllClick = event => {
        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.name + n.class_name);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleChangePage = e => {
        props.handlePage(e.selected);
    };

    const isSelected = name => selected.indexOf(name) !== -1;

    const emptyRows = rowsPerPage - (rows ? rows.length : 0);

    const setLabelWidth = (kind, ref) => {
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            setClassLabelWidth(ref.offsetWidth);
        } else if(kind == 2 && ref.offsetWidth != overLabelWidth) {
            setOverLabelWidth(ref.offsetWidth);
        } else if(kind == 3 && ref.offsetWidth != searchLabelWidth) {
            setSearchLabelWidth(ref.offsetWidth);
        }
    }

    const returnText = flag => {
        let finish = "진행중";
        if(flag == 1) {
            finish = "완료";
        } else if(flag == 2) {
            finish = "중지";
        } else if(flag == -1) {
            finish = "취소";
        }
        return finish;
    }

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar sort={props.sort} handleSort={props.handleSort} />
                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size='small'
                        aria-label="enhanced table"
                    >
                        <EnhancedTableHead
                            classes={classes}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows ? rows.length : 0}
                        />
                        <TableBody>
                        {rows ? stableSort(rows, getSorting(order, orderBy))
                            .map((row, index) => {
                                const isItemSelected = isSelected(row.name + row.class_name);
                                const ltoFinish = returnText(row.lto_finish);
                                const stoFinish = returnText(row.finish);
                                return (
                                    <TableRow
                                        hover
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={"r_" + index}
                                        selected={isItemSelected}
                                        classes={{
                                            root: classes.tableRow
                                        }}
                                    >
                                        <TableCell align="center">{props.total - (props.page * props.perPage) - index}</TableCell>
                                        <TableCell align="center" padding="none" style={{width:240}}>{row.lto_name}</TableCell>
                                        <TableCell align="center" padding="none">{row.sto_group_name + "("+ltoFinish+")"}</TableCell>
                                        <TableCell align="center" padding="none" style={{maxWidth:360}}>{(row.target ? row.target.map((v,i) => (i>0 ? " / " : "") + v.text) : "")}</TableCell>
                                        <TableCell align="center" padding="none">{stoFinish}</TableCell>
                                        <TableCell align="center" padding="none">{row.type == 0 ? "일반" : "누적"}</TableCell>
                                        <TableCell align="center" padding="none">{row.user_name}</TableCell>
                                        <TableCell align="center" padding="none">{row.reach_date}</TableCell>
                                    </TableRow>
                                );
                            }) : null}
                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                            <TableCell colSpan={6} />
                            </TableRow>
                        )}
                        </TableBody>
                    </Table>
                </div>
                <ReactPaginate
                    previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={<span className="gap">...</span>}
                    pageCount={parseInt((props.total - 1) / props.perPage) + 1}
                    pageRangeDisplayed={20}
                    marginPagesDisplayed={10}
                    onPageChange={handleChangePage}
                    containerClassName={"pagination"}
                    previousLinkClassName={"previous_page"}
                    nextLinkClassName={"next_page"}
                    disabledClassName={"disabled"}
                    activeClassName={"active"}
                />
            </Paper>
        </div>
    );
}