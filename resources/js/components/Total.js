import React from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab, Box, Typography, Divider } from '@material-ui/core';

// const AntTabs = withStyles({
//     root: {
//         borderBottom: '1px solid #fff',
//     },
//     indicator: {
//         backgroundColor: '#333',
//     },
// })(Tabs);

// const AntTab = withStyles(theme => ({
//     root: {
//         color: '#fff',
//     },
//     selected: {},
// }))(props => <Tab {...props} />);

function calcCriterion(total, reach) {
    if(total != 0 && reach != 0) {
        return parseFloat(total/reach).toFixed(1);
    }
    return 0;
}

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
            >
            {value === index && <Box p={3}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Total extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: 0,
            tabs: [],
            panels: [],
            func: null,
        }
    }

    componentDidMount() {
        const drawingFunc = this.drawTable;
        this.setState({
            func: drawingFunc
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.refresh && nextProps.data) {
            if(nextProps.refresh && prevState.func) prevState.func(nextProps.data, nextProps.userData);
            nextProps.afterRefresh();
        }
        if(nextProps.value != prevState.value) {
            return {
                value: nextProps.value
            }
        }
        return true;
    }

    drawTable = (data, userData) => {
        const { value } = this.state;
        const { classes } = this.props;
        let tabs = [], panels = [];
        Object.keys(data).forEach((k, i) => {
            tabs.push(
                <Tab key={"t_"+i} label={k} {...a11yProps(i)} classes={{
                    root: classes.tab,
                    selected: classes.selectedTab
                }}/>
            );
            const tabCont = data[k];
            let content = [];
            let weekTotal = {
                positive: 0,
                total: 0,
                sto_met: 0,
                criterion: 0,
                lto_met: 0,
                cor_decs: 0,
                incor_decs: 0,
                err: 0,
                with_err: 0,
            };
            Object.keys(tabCont).forEach((d, idx) => {
                const oneDay = tabCont[d];
                let inside = [];
                let dayTotal = {
                    positive: 0,
                    total: 0,
                    sto_met: 0,
                    criterion: 0,
                    lto_met: 0,
                    cor_decs: 0,
                    incor_decs: 0,
                    err: 0,
                    with_err: 0,
                }
                Object.keys(oneDay).forEach((r, j) => {
                    const item = oneDay[r];
                    let rows = [];
                    let total = {
                        positive: 0,
                        total: 0,
                        sto_met: 0,
                        criterion: 0,
                        lto_met: 0,
                        cor_decs: 0,
                        incor_decs: 0,
                        err: 0,
                        with_err: 0,
                    };
                    const l = item.length;
                    item.map((row, idx2) => {
                        if(row.domain_name) {
                            const key = d+"_"+j+"_"+idx2;
                            rows.push(
                                <tr key={key}>
                                    <td>{row.domain_name}</td>
                                    <td>{row.positive}</td>
                                    <td>{row.total}</td>
                                    <td>{row.sto_met}</td>
                                    <td>{row.criterion}</td>
                                    <td>{row.lto_met}</td>
                                    <td>{row.decs}</td>
                                    <td>{0}</td>
                                    <td>{0}</td>
                                    <td>{0}</td>
                                </tr>
                            );
                            total.positive += parseInt(row.positive);
                            total.total += parseInt(row.total);
                            total.sto_met += parseInt(row.sto_met);
                            total.criterion += parseFloat(row.criterion);
                            total.lto_met += parseInt(row.lto_met);
                            total.cor_decs += parseInt(row.decs);
                            // total.incor_decs += parseInt(row.decs);
                            // total.errorless += row.errorless;
                            // total.with_err += row.witherror;
                        }
                    });
                    rows.push(
                        <tr key={d+"_"+j+"_total"}>
                            <td>{"Totals"}</td>
                            <td>{total.positive}</td>
                            <td>{total.total}</td>
                            <td>{total.sto_met}</td>
                            <td>{total.criterion}</td>
                            <td>{total.lto_met}</td>
                            <td>{total.cor_decs}</td>
                            <td>{total.incor_decs}</td>
                            <td>{item[0].errorless ? parseInt(item[0].errorless) : 0}</td>
                            <td>{item[0].witherror ? parseInt(item[0].witherror) : 0}</td>
                        </tr>
                    );
                    dayTotal.positive += total.positive;
                    dayTotal.total += total.total;
                    dayTotal.sto_met += total.sto_met;
                    dayTotal.criterion += total.criterion;
                    dayTotal.lto_met += total.lto_met;
                    dayTotal.cor_decs += total.cor_decs;
                    dayTotal.incor_decs += total.incor_decs;
                    dayTotal.err += item[0].errorless ? parseInt(item[0].errorless) : 0;
                    dayTotal.with_err += item[0].witherror ? parseInt(item[0].witherror) : 0;
                    // <td rowSpan={l + 3}>{item[0].student_name}</td>
                    // <Typography>{item[0].student_name}</Typography>
                    inside.push(
                        <React.Fragment key={d+"_"+j}>
                            <table className={classes.tableRoot}>
                                <thead>
                                    <tr>
                                        <th rowSpan={2}>{item[0].student_name}</th>
                                        <th rowSpan={2}>{"# of Trials correct L.U."}</th>
                                        <th rowSpan={2}>{"Total # of L.U."}</th>
                                        <th rowSpan={2}>{"STO Met"}</th>
                                        <th rowSpan={2}>{"L.U. to Criterion"}</th>
                                        <th rowSpan={2}>{"LTO Met"}</th>
                                        <th colSpan={2}>{"Total # of Decisions"}</th>
                                        <th colSpan={2}>{"Total # TPRA"}</th>
                                    </tr>
                                    <tr>
                                        <th>{"Correct"}</th>
                                        <th>{"Incorrect"}</th>
                                        <th>{"errorless"}</th>
                                        <th>{"with errors"}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {rows}
                                </tbody>
                            </table>
                        </React.Fragment>
                    );
                });
                content.push(
                    <div key={d} className={classes.tableBox}>
                        <Typography>{this.props.funcStore.dateToHangul(d)}</Typography>
                        {inside}
                        <table className={classNames(classes.tableRoot, classes.dailyRow)}>
                            <thead>
                                <tr>
                                    <th rowSpan={2}>{"Daily"}</th>
                                    <th rowSpan={2}>{"# of Trials correct L.U."}</th>
                                    <th rowSpan={2}>{"Total # of L.U."}</th>
                                    <th rowSpan={2}>{"STO Met"}</th>
                                    <th rowSpan={2}>{"L.U. to Criteron"}</th>
                                    <th rowSpan={2}>{"LTO Met"}</th>
                                    <th colSpan={2}>{"Total # of Decisions"}</th>
                                    <th colSpan={2}>{"Total # TPRA"}</th>
                                </tr>
                                <tr>
                                    <th>{"Correct"}</th>
                                    <th>{"Incorrect"}</th>
                                    <th>{"errorless"}</th>
                                    <th>{"with errors"}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr></tr>
                                <tr>
                                    <td>{"Totals"}</td>
                                    <td>{dayTotal.positive}</td>
                                    <td>{dayTotal.total}</td>
                                    <td>{dayTotal.sto_met}</td>
                                    <td>{calcCriterion(dayTotal.total, dayTotal.sto_met)}</td>
                                    <td>{dayTotal.lto_met}</td>
                                    <td>{dayTotal.cor_decs}</td>
                                    <td>{dayTotal.incor_decs}</td>
                                    <td>{dayTotal.err}</td>
                                    <td>{dayTotal.with_err}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                );
                weekTotal.positive += dayTotal.positive;
                weekTotal.total += dayTotal.total;
                weekTotal.sto_met += dayTotal.sto_met;
                weekTotal.criterion += dayTotal.criterion;
                weekTotal.lto_met += dayTotal.lto_met;
                weekTotal.cor_decs += dayTotal.cor_decs;
                weekTotal.incor_decs += dayTotal.incor_decs;
                weekTotal.err += dayTotal.err;
                weekTotal.with_err += dayTotal.with_err;
            });
            const userArr = userData[k];
            const userTotal = {
                positive: 0,
                total: 0,
                sto_met: 0,
                criterion: 0,
                lto_met: 0,
                cor_decs: 0,
                incor_decs: 0,
                err: 0,
                with_err: 0,
            }
            content.push(
                <div key={k+"_total"} className={classes.tableBox}>
                    <Divider variant="middle" />
                    <table className={classNames(classes.tableRoot, classes.weeklyRow)}>
                        <thead>
                            <tr>
                                <th rowSpan={2}>{"Weekly"}</th>
                                <th rowSpan={2}>{"# of Trials correct L.U."}</th>
                                <th rowSpan={2}>{"Total # of L.U."}</th>
                                <th rowSpan={2}>{"STO Met"}</th>
                                <th rowSpan={2}>{"L.U. to Criteron"}</th>
                                <th rowSpan={2}>{"LTO Met"}</th>
                                <th colSpan={2}>{"Total # of Decisions"}</th>
                                <th colSpan={2}>{"Total # TPRA"}</th>
                            </tr>
                            <tr>
                                <th>{"Correct"}</th>
                                <th>{"Incorrect"}</th>
                                <th>{"errorless"}</th>
                                <th>{"with errors"}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr></tr>
                            <tr>
                                <td>{"Student Totals"}</td>
                                <td>{weekTotal.positive}</td>
                                <td>{weekTotal.total}</td>
                                <td>{weekTotal.sto_met}</td>
                                <td>{calcCriterion(weekTotal.total, weekTotal.sto_met)}</td>
                                <td>{weekTotal.lto_met}</td>
                                <td>{weekTotal.cor_decs}</td>
                                <td>{weekTotal.incor_decs}</td>
                                <td>{weekTotal.err}</td>
                                <td>{weekTotal.with_err}</td>
                            </tr>
                            {userArr.map((u, y) => {
                                userTotal.positive += parseInt(u.positive);
                                userTotal.total += parseInt(u.total);
                                userTotal.sto_met += parseInt(u.sto_met);
                                userTotal.criterion += parseFloat(u.criterion);
                                userTotal.lto_met += parseInt(u.lto_met);
                                userTotal.cor_decs += parseInt(u.decs);
                                //userTotal.incor_decs += parseInt(u.decs);
                                userTotal.err += parseInt(u.errorless);
                                userTotal.with_err += parseInt(u.witherror);
                                return (
                                    <tr key={k+"_"+y+"_"+u.user_id}>
                                        <td>{u.user_name}</td>
                                        <td>{u.positive}</td>
                                        <td>{u.total}</td>
                                        <td>{u.sto_met}</td>
                                        <td>{u.criterion}</td>
                                        <td>{u.lto_met}</td>
                                        <td>{u.decs}</td>
                                        <td>{0}</td>
                                        <td>{u.errorless}</td>
                                        <td>{u.witherror}</td>
                                    </tr>
                                );
                            })}
                            <tr>
                                <td>{"Teacher Totals"}</td>
                                <td>{userTotal.positive}</td>
                                <td>{userTotal.total}</td>
                                <td>{userTotal.sto_met}</td>
                                <td>{calcCriterion(userTotal.total, userTotal.sto_met)}</td>
                                <td>{userTotal.lto_met}</td>
                                <td>{userTotal.cor_decs}</td>
                                <td>{userTotal.incor_decs}</td>
                                <td>{userTotal.err}</td>
                                <td>{userTotal.with_err}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
            panels.push(content);
        });
        this.setState({
            value: 0,
            tabs: tabs,
            panels: panels,
        });
    }

    render() {
        const { value, tabs, panels } = this.state;
        const { classes, data } = this.props;

        return tabs.length ? (
            <div className={classes.root}>
                <AppBar position="static" color="default" className={classes.barRoot}>
                    <Tabs
                        value={value}
                        onChange={this.props.handleTabChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                        classes={{
                            root: classes.tabs,
                            indicator: classes.tabIndicator
                        }}
                    >
                        {tabs}
                    </Tabs>
                </AppBar>
                {panels.map((p,i) => <TabPanel key={"p_"+i} value={value} index={i}>{p}</TabPanel> )}
            </div>
        ) : (
            <div>Empty Data</div>
        );
    }
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    barRoot: {
        backgroundColor: '#333',
        color: '#fff'
    },
    tabs: {
        backgroundColor: '#333',
        color: '#ccc'
    },
    tabIndicator: {
        backgroundColor: '#fff',
    },
    tab: {
        color: '#ccc'
    },
    selectedTab: {
        color: '#fff!important'
    },
    tableBox: {
        marginBottom: 30
    },
    tableRoot: {
        width: '100%',
        marginTop: 15,
        '& td, & th': {
            borderCollapse: 'collapse',
            border: '0.5px solid #666',
            padding: 3,
        },
        '& thead': {
            '& th': {
                fontSize: 12,
                textAlign: 'center',
                fontFamily: 'jungBold'
            },
            '& > tr:first-child > th:first-child': {
                width: 240,
                textAlign: 'left',
            }
        },
        '& tbody': {
            '& td': {
                fontSize: 13,
                textAlign: 'right',
            },
            '& td:first-child': {
                textAlign: 'left',
                paddingLeft: 6
            },
            '& > tr:last-child > td:first-child': {
                fontFamily: 'jungBold',
                textAlign: 'left',
            }
        }
    },
    dailyRow: {
        '& td, & th': {
            backgroundColor: '#ededed'
        }
    },
    weeklyRow: {
        marginTop: 30,
        '& td, & th': {
            backgroundColor: '#ddd'
        },
        '& tbody': {
            '& > tr > td:first-child': {
                fontFamily: 'jungBold',
                textAlign: 'left',
            }
        }
    }
});

export default withStyles(styles)(Total);