import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import classNames from 'classnames';
import { lighten, makeStyles } from '@material-ui/core/styles';
import { Button, Fab, TextField, OutlinedInput, Input, InputLabel, InputAdornment, IconButton,
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, Toolbar, Typography,
    Paper, Checkbox, Chip,
    FormControl, Select, MenuItem, Tooltip
} from '@material-ui/core';
import ReactPaginate from 'react-paginate';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import SearchIcon from '@material-ui/icons/Search';
import DoneIcon from '@material-ui/icons/Done';
import ChangeHistoryIcon from '@material-ui/icons/ChangeHistory';
import DetailsIcon from '@material-ui/icons/Details';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ErrorIcon from '@material-ui/icons/Error';

function arrowGenerator(color) {
    return {
        '&[x-placement*="bottom"] $arrow': {
            top: 0,
            left: 0,
            marginTop: '-0.95em',
            width: '2em',
            height: '1em',
            '&::before': {
            borderWidth: '0 1em 1em 1em',
            borderColor: `transparent transparent ${color} transparent`,
            },
        },
        '&[x-placement*="top"] $arrow': {
            bottom: 0,
            left: 0,
            marginBottom: '-0.95em',
            width: '2em',
            height: '1em',
            '&::before': {
            borderWidth: '1em 1em 0 1em',
            borderColor: `${color} transparent transparent transparent`,
            },
        },
    };
}

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const headCells = [
    { id: 'number', numeric: false, disablePadding: true, label: '순번' },
    { id: 'name', numeric: false, disablePadding: true, label: '이름' },
    { id: 'email', numeric: false, disablePadding: true, label: '메일' },
    { id: 'classes', numeric: false, disablePadding: true, label: '반' },
    { id: 'level', numeric: false, disablePadding: true, label: '레벨' },
    { id: 'btn', numeric: false, disablePadding: true, label: '배정' },
    { id: 'err_btn', numeric: false, disablePadding: true, label: '에러' },
];

function EnhancedTableHead(props) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;

    return (
        <TableHead>
            <TableRow>
                {headCells.map(headCell => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.id=="title" ? 'left' : 'center'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        classes={(headCell.id == 'info') ? {
                            root: classes.rowBtnHeader
                        } : null}
                        style={{
                            width: headCell.id == 'number' ? 50 : 
                            headCell.id == 'id' ? 70 : 
                            headCell.id == 'name' ? 120 : 
                            headCell.id == 'email' ? 220 : 
                            headCell.id == 'classes' ? '*%' : 
                            headCell.id == 'phone' ? 180 : 
                            headCell.id == 'level' ? 100 : 
                            headCell.id == 'btn' ? 70 : 
                            headCell.id == 'err_btn' ? 70 : 
                            headCell.id == 'created_at' ? 200 : 'auto'
                        }}
                    >
                        {headCell.label}
                    </TableCell>
                ))}
            </TableRow>
        </TableHead>
    );
}

EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.oneOf(['asc', 'desc']).isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const useToolbarStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight:
        theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: '1 1 100%',
    },
    sortBtn: {
        transition: 'transform 300ms ease'
    },
    sortOn: {
        transform: 'rotate(-180deg)'
    },
    sortOff: {
        transform: 'rotate(0deg)'
    }
}));

const useStylesBootstrap = makeStyles(theme => ({
    arrow: {
        position: 'absolute',
        fontSize: 6,
        '&::before': {
            content: '""',
            margin: 'auto',
            display: 'block',
            width: 0,
            height: 0,
            borderStyle: 'solid',
        },
    },
    popper: arrowGenerator(theme.palette.common.black),
    tooltip: {
        position: 'relative',
        backgroundColor: theme.palette.common.black,
        fontSize: 10
    },
    tooltipPlacementLeft: {
        margin: '0 8px',
    },
    tooltipPlacementRight: {
        margin: '0 8px',
    },
    tooltipPlacementTop: {
        margin: '8px 0',
    },
    tooltipPlacementBottom: {
        margin: '8px 0',
    },
}));

function BootstrapTooltip(props) {
    const { arrow, ...classes } = useStylesBootstrap();
    const [arrowRef, setArrowRef] = React.useState(null);

    return (
    <Tooltip
        classes={classes}
        placement="top"
        PopperProps={{
        popperOptions: {
                modifiers: {
                    arrow: {
                        enabled: Boolean(arrowRef),
                        element: arrowRef,
                    },
                },
            },
        }}
        {...props}
        title={
        <React.Fragment>
            {props.title}
            <span className={arrow} ref={setArrowRef} />
        </React.Fragment>
        }
    />
    );
}

BootstrapTooltip.propTypes = {
    title: PropTypes.node,
};

const EnhancedTableToolbar = props => {
    const classes = useToolbarStyles();
    const { numSelected } = props;

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
        {numSelected > 0 ? (
            <Typography className={classes.title} color="inherit" variant="subtitle1">
            {numSelected} selected
            </Typography>
        ) : (
            <Typography className={classes.title} variant="h6" id="tableTitle"></Typography>
        )}

        {numSelected > 0 ? (
            <Tooltip title="Delete">
            <IconButton aria-label="delete">
                <DeleteIcon />
            </IconButton>
            </Tooltip>
        ) : (
            <Tooltip title={props.sort ? "오름차순으로" : "내림차순으로"}>
                <IconButton aria-label="sort"
                    onClick={()=>props.handleSort()}
                >
                    <FilterListIcon className={classNames(classes.sortBtn, (props.sort ? classes.sortOff : classes.sortOn))} />
                </IconButton>
            </Tooltip>
        )}
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
};

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& .pagination': {
            justifyContent: 'center',
            padding: '10px 0 15px',
            '& a' : {
                fontFamily: 'JungBold',
                fontSize: 16,
                cursor: 'pointer',
                padding: '0 5px',
                color: '#aaa',
                '&:hover': {
                    fontWeight: 'bold',
                    color: '#fff'
                },
                '&:focus': {
                    outline: 'none!important',
                    boxShadow: 'none!important'
                },
            },
            '& > li.active a': {
                color: '#fff'
            }
        }
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
        backgroundColor: '#333',
        color: '#fff',
        '& svg, & span': {
            color: '#fff'
        }
    },
    table: {
        minWidth: 750,
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
        '& thead': {
            backgroundColor: '#333'
        },
        '& td': {
            color: '#fff',
            lineHeight: "2.6",
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
            overflow: "hidden"
        },
        '& th': {
            color: '#fff'
        },
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    margin: {
        margin: theme.spacing(1),
    },
    textField: {
        //flexBasis: 200,
        width: 'calc(50% - 120px)',
        minWidth: 360,
        maxWidth: 600
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    formControl: {
        margin: theme.spacing(1),
        width: 120
    },
    rowBtn: {
        margin: 0
    },
    rowBtnHeader: {
        width: 130
    },
    diabledBtn: {
        '& svg': {
            color: 'rgba(0, 0, 0, 0.26)!important'
        }
    },
    tableRow: {
        cursor: 'pointer'
    },
    tableRowInvalid: {
        cursor: 'pointer',
        backgroundColor: 'rgba(0,0,0,.4)'
    },
    wrappingColumn: {
        whiteSpace: 'normal!important'
    }
}));

export default function Users(props) {
    const classes = useStyles();
    const [order, setOrder] = React.useState('asc');
    const [orderBy, setOrderBy] = React.useState('calories');
    const [selected, setSelected] = React.useState([]);
    const [rowsPerPage, setRowsPerPage] = React.useState(props.perPage);
    const [classLabelWidth, setClassLabelWidth] = React.useState(0);
    const rows = props.data;

    const handleRequestSort = (event, property) => {
        const isDesc = orderBy === property && order === 'desc';
        setOrder(isDesc ? 'asc' : 'desc');
        setOrderBy(property);
    };

    const handleSelectAllClick = event => {
        if (event.target.checked) {
            const newSelecteds = rows.map(n => n.id);
            setSelected(newSelecteds);
            return;
        }
        setSelected([]);
    };

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }
        setSelected(newSelected);
    };

    const handleChangePage = e => {
        props.handlePage(e.selected);
    };

    const handleMouseDownSearching = e => {
        e.preventDefault();
    };

    const handleClickSearching = e => {
        if(!props.keyword) return;
        props.reloadUsers();
    }

    const handleKeyPress = e => {
        if(e.key === 'Enter') {
            props.reloadUsers();
        }
    }

    const isSelected = name => selected.indexOf(name) !== -1;

    const emptyRows = rowsPerPage - rows.length;

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <EnhancedTableToolbar numSelected={selected.length} sort={props.sort} handleSort={props.handleSort} />
                <div className={classes.tableWrapper}>
                <Table
                    className={classes.table}
                    aria-labelledby="tableTitle"
                    size='small'
                    aria-label="enhanced table"
                >
                    <EnhancedTableHead
                        classes={classes}
                        numSelected={selected.length}
                        order={order}
                        orderBy={orderBy}
                        onSelectAllClick={handleSelectAllClick}
                        onRequestSort={handleRequestSort}
                        rowCount={rows.length}
                    />
                    <TableBody>
                    {stableSort(rows, getSorting(order, orderBy))
                        .map((row, index) => {
                        const isItemSelected = isSelected(row.id);
                        const labelId = `enhanced-table-checkbox-${index}`;
                        let t = "";
                        row.selectedClasses.map((v,i) => {
                            if(i > 0) t += ", ";
                            t += v.name;
                        });
                        const lv = row.level == 100 ? "Lv.5" : row.level == 10 ? "Lv.4" : row.level == 2 ? "Lv.3" : row.level == 1 ? "Lv.2" : "Lv.1";

                        return (
                            <TableRow
                                hover
                                role="checkbox"
                                aria-checked={isItemSelected}
                                tabIndex={-1}
                                key={"post_" + index}
                                selected={isItemSelected}
                                classes={{
                                    root: row.is_valid == '1' ? classes.tableRow : classes.tableRowInvalid
                                }}
                            > 
                                <TableCell align="center" component="th" id={labelId} scope="row" padding="none"
                                onClick={()=>props.openRow(row)}>
                                    {props.total - (props.page * props.perPage) - index}
                                </TableCell>
                                <TableCell align="center" padding="none"
                                onClick={()=>props.openRow(row)}>{row.name}</TableCell>
                                <TableCell align="center" padding="none"
                                onClick={()=>props.openRow(row)}>{row.email}</TableCell>
                                <TableCell className={classes.wrappingColumn} align="center" padding="none"
                                onClick={()=>props.openRow(row)}>{t}</TableCell>
                                <TableCell align="center" padding="none"
                                onClick={()=>props.openRow(row)}>{lv}</TableCell>
                                <TableCell align="center" padding="none">
                                    <IconButton aria-label="classes" className={classNames(classes.margin, classes.rowBtn, props.user.level < 10 ? classes.diabledBtn:'')}
                                        disabled={props.user.level < 10}
                                        onClick={() => props.openChild(row)}>
                                        <AssignmentIndIcon />
                                    </IconButton>
                                </TableCell>
                                <TableCell align="center" padding="none">
                                    <IconButton aria-label="classes" className={classNames(classes.margin, classes.rowBtn)}
                                        onClick={() => props.openError(row)}>
                                        <ErrorIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        );
                    })}
                    {emptyRows > 0 && (
                        <TableRow style={{ height: 37 * emptyRows }}>
                        <TableCell colSpan={10} />
                        </TableRow>
                    )}
                    </TableBody>
                </Table>
                </div>
                <ReactPaginate
                    previousLabel={"<"}
                    nextLabel={">"}
                    breakLabel={<span className="gap">...</span>}
                    pageCount={parseInt((props.total - 1) / props.perPage) + 1}
                    pageRangeDisplayed={20}
                    marginPagesDisplayed={10}
                    onPageChange={handleChangePage}
                    containerClassName={"pagination"}
                    previousLinkClassName={"previous_page"}
                    nextLinkClassName={"next_page"}
                    disabledClassName={"disabled"}
                    activeClassName={"active"}
                />
            </Paper>
        </div>
    );
}