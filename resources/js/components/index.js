export { default as Nav } from './nav/Nav';

export { default as Dialogs } from './Dialogs';

export { default as Points } from './Points';
export { default as Indicators } from './Indicators';
export { default as Errors } from './Errors';

export { default as StudentTable } from './Students';

export { default as ReachedSto } from './ReachedSto';

export { default as BoardTable } from './Boards';
export { default as UserTable } from './Users';

export { default as Bread } from './Bread';

export { default as Svgtext } from './Svgtext';

export { default as Total } from './Total';
