import React, { Component } from 'react';
import classNames from 'classnames';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, IconButton, Button, Menu, MenuItem, Badge, Select } from '@material-ui/core/';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';

const styles = makeStyles(theme => ({
    navContainer: {
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        minWidth: 960,
        zIndex: '1000'
    },
    subPaper: {
        background: 'rgba(47, 72, 88, .97)',
        color: '#fff',
        zIndex: '9999',
        '& li': {
            fontFamily: 'JungNormal!important',
            fontSize: '.8rem'
        }
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    topLogo: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'flex-end'
    },
    title: {
        color: '#232323',
        fontFamily: 'Ssangmun!important',
        fontSize: '1.72rem',
        lineHeight: '1.72rem'
    },
    subTitle: {
        color: '#343434',
        fontFamily: 'Ssangmun!important',
        fontSize: '0.9rem',
        lineHeight: '1.2rem',
        paddingLeft: 7
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    navRoot: {
        backgroundImage: 'linear-gradient(to right, #865995 0%,#ca5388 36%,#f55f5c 67%,#f68a15 100%)',
    },
    navToolbar: {
        color: '#232323',
        transition: 'all .24s ease'
    },
    menuText: {
        '& span': {
            fontFamily: 'JungNormal',
            color: '#232323'
        }
    },
    menuTextOn: {
        '& span': {
            fontFamily: 'JungBold',
            color: '#fff'
        }
    },
    selectFirst: {
        marginRight: 8,
        width: 105.44
    },
    conSelect: {
        paddingTop: 9,
        paddingBottom: 9,
        fontSize: 14,
    },
    userIcon: {
        marginRight: 8
    }
}));

const Nav = (props) => {
    const classes = styles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
    const [con, setCon] = React.useState(props.mainStore.country);

    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };
    const handleMenuClose = () => {
        setAnchorEl(null);
    };

    const handleMobileMenuClose = () => {
        setMobileMoreAnchorEl(null);
    };

    const handleMobileMenuOpen = event => {
        setMobileMoreAnchorEl(event.currentTarget);
    };

    const logout = () => {
        if(confirm("로그아웃 하시겠습니까?")) {
            props.logout();
        }
        handleMenuClose();
    }

    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
            classes={{
                paper: classes.subPaper
            }}
        >
            <MenuItem >{props.mainStore.user ? props.mainStore.user.name : ""}</MenuItem>
            <MenuItem onClick={()=>logout()}>{"로그아웃"}</MenuItem>
        </Menu>
    );
    // <MenuItem onClick={handleMenuClose}>{"내 정보"}</MenuItem>

    const changeCountry = e => {
        props.mainStore.setCountry(e.target.value);
        setCon(e.target.value);
    }

    const changeDataSet = e => {
        props.setData(e.target.value);
    }

    const mobileMenuItem = (title, tab) => {
        return (
            <MenuItem className={classes.menuText}
                onClick={()=>{
                    props.moveView(tab);
                    handleMobileMenuClose();
            }}>
                <p>{title}</p>
            </MenuItem>
        );
    }

    const webMenuItem = (title, tab) => {
        return (
            <Button className={props.view == tab ? classes.menuTextOn : classes.menuText}
                onClick={()=>props.moveView(tab)}
            >{title}</Button>
        );
    }

    const mobileMenuId = 'primary-search-account-menu-mobile';
    const renderMobileMenu = (
        <Menu
            anchorEl={mobileMoreAnchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={mobileMenuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMobileMenuOpen}
            onClose={handleMobileMenuClose}
        >
        {mobileMenuItem('CHART', 'chart')}
        {mobileMenuItem('ANALYSIS', 'analysis')}
        {mobileMenuItem('MANAGEMENT', 'management')}
        {mobileMenuItem('SETTING', 'setting')}
        <MenuItem onClick={handleProfileMenuOpen}>
            <IconButton
                aria-label="account of current user"
                aria-controls="primary-search-account-menu"
                aria-haspopup="true"
                color="inherit"
            >
                <AccountCircle />
            </IconButton>
            <p>PROFILE</p>
        </MenuItem>
        </Menu>
    );

    return (
        <div className={classNames(classes.grow, classes.navContainer)}>
            <AppBar position="static"
                classes={{
                    root: classes.navRoot
                }}>
                <Toolbar
                    variant={props.isTop ? "regular" : "dense"}
                    classes={{
                        root: classes.navToolbar
                }}>
                    <div className={classes.topLogo}>
                        <span className={classes.title}>KAVBA ABA</span>
                        <span className={classes.subTitle}>_LABORATARY</span>
                    </div>
                    <div className={classes.grow} />
                    <div className={classes.sectionDesktop}>
                        {webMenuItem('CHART', 'chart')}
                        {webMenuItem('MANAGEMENT', 'management')}
                        {webMenuItem('ANALYSIS', 'analysis')}
                        {webMenuItem('SETTING', 'setting')}
                        <IconButton
                            edge="end"
                            aria-label="account of current user"
                            aria-controls={menuId}
                            aria-haspopup="true"
                            onClick={handleProfileMenuOpen}
                            color="inherit"
                            className={classes.userIcon}
                            >
                            <AccountCircle />
                        </IconButton>
                        <Select
                            value={props.dataSet}
                            onChange={changeDataSet}
                            variant="outlined"
                            className={classes.selectFirst}
                            classes={{
                                root: classes.conSelect
                            }}
                        >
                            {props.dbYears.map((yy, index) => {
                                return (
                                    <MenuItem key={yy} value={yy}>
                                        {"20"+yy+"_DB"}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                        <Select
                            value={con}
                            onChange={changeCountry}
                            variant="outlined"
                            classes={{
                                root: classes.conSelect
                            }}
                        >
                            <MenuItem value="ko">
                                Korean
                            </MenuItem>
                            <MenuItem value="en">
                                English
                            </MenuItem>
                        </Select>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton
                        aria-label="show more"
                        aria-controls={mobileMenuId}
                        aria-haspopup="true"
                        onClick={handleMobileMenuOpen}
                        color="inherit"
                        >
                        <MoreIcon />
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
            {renderMobileMenu}
            {renderMenu}
        </div>
    );
}

export default Nav;
