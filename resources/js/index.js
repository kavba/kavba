import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

const general_url = window.location.origin;

export default class Index extends Component {

    componentDidMount() {
        $.ajax({
            type: 'POST',
            url: general_url + '/web/test2',
            headers: {
                'X-CSRF-TOKEN' : document.getElementById('csrf').getAttribute('content'),
                'Accessing-From':'web'
            },
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            async: true,
            dataType: 'json',
            success: function (res) {
                console.log(res);
            },
            error: function (data) {
                console.log('An error occurred.');
                console.log(data);
            }
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-header">Example Component</div>

                            <div className="card-body">I'm an example component!</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}
