import React, { Component } from 'react';
import { Bread } from '../../components/';
import { Graph, StoList } from '../';

const subMenus = [
    {title: "GRAPH", value: "graph"},
    {title: "준거도달 완료목록", value: "준거도달 완료목록"},
];

class Analysis extends Component {
    state = {
        view: 'graph'
    }

    handleView = (e, page) => {
        if(!page || this.state.view == page) return;
        this.setState({ view: page });
    }

    render() {
        const { view } = this.state;
        let page = null;
        if(view == 'graph') {
            page = (<Graph />);
        } else if(view == '준거도달 완료목록') {
            page = (<StoList />);
        }

        return (
            <div>
                <Bread title={"ANALYSIS"} subTitle={view} subMenus={subMenus} handleSubMenu={this.handleView} />
                {page}
            </div>
        );
    }
}

export default Analysis;