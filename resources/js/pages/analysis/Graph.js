import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, TextField, Button, Input, OutlinedInput, FilledInput,
    InputLabel, MenuItem, FormControl, Select, Divider, InputAdornment,
    List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, IconButton } from '@material-ui/core';
import { Cached, SaveAlt, Print } from '@material-ui/icons/';
import {
    LineChart, ComposedChart, Label, Bar, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine
} from 'recharts';
import { Total } from '../../components/';

import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';

function calcCriterion(total, reach) {
    if(total != 0 && reach != 0) {
        return parseFloat(total/reach).toFixed(1);
    }
    return 0;
}

let newWindow = null;

class CustomizedAxisTick extends Component {
    render() {
        const { x, y, stroke, payload, index, select, stickedUsersOrStudents } = this.props;
        const d = payload.value.split("-");
        let t = d[1]+"-"+d[2];
        if(index == 0 || (d[1] == "01" && (parseInt(d[2]) <= 7))) {
            t = payload.value;
        }
        return (
            <g transform={`translate(${x},${y})`} onClick={() => {
                if(select) stickedUsersOrStudents(payload.value);
            }} style={ select ? { cursor: 'pointer', '&:hover': { color:'#000', fontWeight:'bold' }} : {}}>
                <text x={0} y={0} dy={16} textAnchor="end" fill="#666" fontSize="12" transform="rotate(-35)">{t}</text>
            </g>
        );
    }
}

const CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        const data = payload[0].payload;
        return (
            <div className="custom-tooltip">
                <p className="label">{label}</p>
                <p className="total">{`총 런유닛 : ${data.total}`}</p>
                <p className="reach">{`준거도달 : ${data.reaches}`}</p>
                <p className="criterion">{`CRETERIA : ${data.criteria}`}</p>
            </div>
        );
    }
    return null;
};

const Custom1Tooltip = ({ active, payload, label, domainList }) => {
    if (active) {
        const data = payload[0].payload;
        return (
            <div className="custom-tooltip">
                <p className="label">{label}</p>
                <p className="total">{`총 런유닛 : ${data.positive}/${data.total}`}</p>
                {domainList.map((v,i) => {
                    if(data["t_"+v.id] != 0) {
                        const title = (v.name ? v.name.split("(")[0] + (v.name.indexOf("-") !== -1 ? "-"+v.name.split("-")[1] : "") : "");
                        return (
                            <p key={"dl_"+i} className="rununit">{`${title} : ${data["p_"+v.id]}/${data["t_"+v.id]}`}</p>
                        );
                    }
                })}
            </div>
        );
    }
    return null;
};

@inject(stores => ({
    funcStore: stores.funcStore,
    mainStore: stores.mainStore
}))
class Graph extends Component {
    state = {
        dialogFlag: true,
        dialogOpen: false,
        alertOpen: false,
        listLabelWidth: 0,
        standardLabelWidth: 0,
        standardSubLabelWidth: 0,
        classLabelWidth: 0,
        ostandardLabelWidth: 0,
        onlyLabelWidth: 0,
        domainLabelWidth: 0,
        graphList: [],
        classList: [],
        subList: [],
        centerList: [],
        userList: [],
        domainList: [],
        selectedGraph: "",
        selectedStandard: "1",
        selectedStandardSub: "",
        selectedClass: "",
        selectedCenter: "",
        selectedDomain: "",
        fromDate: this.props.funcStore.setThisDate(),
        toDate: this.props.funcStore.setThisDate(1),
        data: [],
        userData: [],
        //interval: [...Array(21)].map((v,i) => i*10 ),
        ticks: [],
        ticks2: [],
        domain: [0, 'dataMax'],
        domain2: [0, 'dataMax'],
        yLabel: '',
        yLabel2: '',
        subDataKey: "",
        dataKey: "",
        xDataKey: "end_date",
        lineColor: '#f68a15',
        subLineColor: '#865995',
        dotStyle: {
            r: 6, strokeWidth: 2
        },
        viewComponent: (
            <div>Empty Data</div>
        ),
        printComponent: null,
        totalRefresh: false,
        tabValue: 0
    }

    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        this.loadClasses();
        this.loadGraphList();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this._isMounted;
    }

    setLabelWidth = (kind, ref) => {
        const { listLabelWidth, standardLabelWidth, standardSubLabelWidth, classLabelWidth, ostandardLabelWidth, onlyLabelWidth, domainLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != listLabelWidth) {
            this.setState({ listLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != standardLabelWidth) {
            this.setState({ standardLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != standardSubLabelWidth) {
            this.setState({ standardSubLabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 5 && ref.offsetWidth != ostandardLabelWidth) {
            this.setState({ ostandardLabelWidth: ref.offsetWidth });
        } else if(kind == 6 && ref.offsetWidth != onlyLabelWidth) {
            this.setState({ onlyLabelWidth: ref.offsetWidth });
        } else if(kind == 7 && ref.offsetWidth != domainLabelWidth) {
            this.setState({ domainLabelWidth: ref.offsetWidth });
        }
    }

    handleGraphList = name => event => {
        const { selectedGraph, selectedStandard, userList, toDate } = this.state;
        const selected = event.target.value;
        if(selectedGraph == selected) return;
        if(selected > 3 && selected != 6 && selectedGraph < 5) {
            if(selected == 5) {
                const d = new Date(toDate);
                const monthOfYear = d.getMonth();
                d.setMonth(monthOfYear - 3);
                this.setState({
                    selectedGraph: selected,
                    selectedStandard: "1",
                    selectedStandardSub: "",
                    selectedClass: "",
                    selectedCenter: "",
                    subList: userList,
                    fromDate: this.props.funcStore.convertToymd(d),
                });
            } else {
                this.setState({
                    selectedGraph: selected,
                    selectedStandard: "1",
                    selectedStandardSub: "",
                    selectedClass: "",
                    selectedCenter: "",
                    subList: userList
                });
            }
            
        } else {
            this.setState({
                selectedGraph: selected,
            });
        }
    };

    handleSelection = name => e => {
        if(name == "selectedStandard") {
            const { mainStore } = this.props;
            const section = e.target.value;
            if(section == 1) {
                let selectedStandardSub = '';
                if(mainStore.user.level < 2) {
                    selectedStandardSub = mainStore.user.id;
                }
                this.setState({
                    selectedStandard: e.target.value,
                    selectedStandardSub: selectedStandardSub,
                    selectedClass: '',
                    selectedCenter: '',
                    selectedDomain: '',
                    subList: this.state.userList
                });
            } else {
                this.setState({
                    [name]: e.target.value,
                    selectedStandardSub: '',
                    selectedClass: '',
                    selectedCenter: '',
                    selectedDomain: '',
                    subList: []
                });
            }
        } else {
            this.setState({
                [name]: e.target.value
            });
        }
    }

    handleClass = () => e => {
        const { selectedStandard } = this.state;
        const selectedClass = e.target.value;
        if(selectedStandard == 3) {
            this.setState({
                selectedClass: selectedClass
            });
        } else {
            const param = {
                selectedStandard: selectedStandard,
                selectedClass: selectedClass
            }
            this.props.funcStore.conn('get', '/common/getGraphSubList', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        subList: res.list,
                        selectedClass: selectedClass
                    });
                } else {
                    console.log("데이터 로딩 실패");
                }
            });
        }
        
    }

    handleDateChange = name => date => {
        const { fromDate, toDate } = this.state;
        const d = date.target ? date.target.value : date;
        if(name == "fromDate" && (new Date(d) > new Date(toDate))) return;
        if(name == "toDate" && (new Date(d) < new Date(fromDate))) return;
        this.setState({
            [name]: d
        });
    }

    applyCondition = () => {
        this.reloadGraphData();
    }

    loadGraphList = () => {
        this.props.funcStore.conn('get', '/analysis/getGraphList', false, true, null, (res)=>{
            this.setState({
                graphList: res.list,
                selectedStandardSub: res.user,
            });
        });
    }

    loadClasses = () => {
        this.props.funcStore.conn('get', '/common/getClassesUsers', false, true, null, (res)=>{
            this.setState({
                classList: res.classes,
                userList: res.users,
                centerList: res.centers,
                subList: res.users,
                domainList: res.domains
            });
        });
    }

    loadSubList = () => {
        const { selectedStandard, selectedClass } = this.state;
        const param = {
            selectedStandard: selectedStandard,
            selectedClass: selectedClass
        }
        this.props.funcStore.conn('get', '/common/getGraphSubList', false, true, param, (list)=>{
            this.setState({ selectedStandardSub: list });
        });
    }

    loadGraphData = () => {
        const { selectedGraph, selectedStandard, selectedStandardSub, selectedCenter, selectedClass, selectedDomain,
            fromDate, toDate, graphList } = this.state;
        if(!selectedGraph) return;
        if(fromDate=="" || toDate=="") {
            const lang = this.props.mainStore.getLang(this.props.mainStore.country);
            alert(lang["graph14"]);
            return;
        }
        
        let param = {
            standard: selectedStandard,
            target: selectedStandardSub,
            graph: selectedGraph,
            center: selectedCenter,
            fromDate: fromDate,
            toDate: toDate,
            domain: selectedDomain,
            personal: ''
        }
        if(selectedStandard == 3) {
            param.target = selectedClass;
        } else if (selectedStandard == 4) {
            param.target = selectedCenter;
        }
        let tg = selectedStandardSub;
        let personal = '';
        if(selectedClass == 0 && tg) {
            const tmp = tg.toString().split("_");
            tg = tmp[0];
            personal = tmp[1];
            param.target = tg;
            param.personal = personal;
        }

        $('#loader_box').css('visibility', 'visible');
        this.props.funcStore.conn('get', '/analysis/getData', false, true, param, (res)=>{
            console.log("Graph Loaded");
            $('#loader_box').css('visibility', 'hidden');
            if(res.result == 'true' ) {
                if(selectedGraph == 6) {
                    const printBtn = (
                        <Fab size="small" aria-label="save" className={this.props.classes.saveBtn}
                            onClick={() => {
                                if(res.data) this.exportToExcel(res.data, res.user);
                            }}>
                            <SaveAlt />
                        </Fab>
                    );
                    this.setState({
                        data: res.data,
                        userData: res.user,
                        totalRefresh: true,
                        printComponent: printBtn,
                    });
                } else {
                    const CUSTOMCOUNT = 10;
                    const CUSTOMSTACKCOUNT = 30;
                    let max = 0;
                    let ticks = [], ticks2 = [], domain = [], domain2 = [];
                    const keys = this.selectKeys(selectedGraph);
    
                    if(res.data.length) {
                        if(selectedGraph == 6) {
                            // do something
                        } else if(selectedGraph != 5) {
                            max = this.props.funcStore.arrMax(res.data, keys[2]);
                            if(max < CUSTOMCOUNT * 10) {
                                ticks = [...Array(CUSTOMCOUNT+1)].map((v,i) => i*10 );
                                ticks2 = [...Array(CUSTOMCOUNT+1)].map((v,i) => i*15 );
                                domain = [0, (CUSTOMCOUNT * 10)];
                                domain2 = [0, (CUSTOMCOUNT * 15)];
                            } else {
                                const s = parseInt(max/10)+1;
                                ticks = [...Array(s+1)].map((v,i) => i*10 );
                                ticks2 = [...Array(s+1)].map((v,i) => i*15 );
                                domain = [0, s * 10];
                                domain2 = [0, s * 15];
                            }
                        } else {
                            let m = this.props.funcStore.arrMax(res.data, "stack1");
                            const m2 = this.props.funcStore.arrMax(res.data, "stack2");
                            if(parseInt(m) < parseInt(m2)) m = m2;
                            m = m ? parseInt(m) : 0;
                            const c = this.arrCountMax(res.data);
                            if(c < CUSTOMSTACKCOUNT) {
                                ticks = [...Array(Math.ceil(CUSTOMSTACKCOUNT/2)+1)].map((v,i) => i*2);
                                domain = [0, CUSTOMSTACKCOUNT];
                            } else {
                                ticks = [...Array(Math.ceil(c/2)+1)].map((v,i) => i*2);
                                domain = [0, c + 1];
                            }
                            if(m < CUSTOMSTACKCOUNT * 10) {
                                ticks2 = [...Array(CUSTOMSTACKCOUNT+1)].map((v,i) => i*10 );
                                domain2 = [0, (CUSTOMSTACKCOUNT * 10)];
                            } else {
                                ticks2 = [...Array(m+1)].map((v,i) => i*10);
                                domain2 = [0, m*10];
                            }
                        }
                    }
                    const item = graphList.filter(v=>v.id == selectedGraph);
                    let label = "", label2 = "";
                    if(item.length) {
                        label = item[0].label;
                        label2 = item[0].label2 ? item[0].label2 : "";
                    }
                    this.setState({
                        data: res.data,
                        userData: [],
                        ticks: ticks,
                        ticks2: ticks2,
                        domain: domain,
                        domain2: domain2,
                        yLabel: label,
                        yLabel2: label2,
                        dataKey: keys[0],
                        subDataKey: keys[1],
                    });

                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            this.setViewComponent();
                        }, 0);
                    });
                }
                
            } else {
                console.log("데이터 로딩 실패");
            }
        });
    }

    selectKeys = selectedGraph => {
        let keys = [];
        switch(selectedGraph) {
            case 2 :
                keys[0] = "criteria";
                keys[1] = "";
                keys[2] = "criteria";
                break;
            case 3 :
                keys[0] = "cnt";
                keys[1] = "stack";
                keys[2] = "stack";
                break;
            case 4 :
                keys[0] = "decisions";
                keys[1] = "stack";
                keys[2] = "stack";
                break;
            case 5 :
                keys[0] = "cnt";
                keys[1] = "stack";
                keys[2] = "stack";
                break;
            case 6 :
                keys[0] = "";
                keys[1] = "";
                keys[2] = "";
                break;
            default :
                keys[0] = "total";
                keys[1] = "positive";
                keys[2] = "total";
        }
        return keys;
    }

    reloadGraphData = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadGraphData();
            }, 0);
        });
    }

    arrCountMax = arr => {
        const tmp = arr.map(v => parseInt(v.cnt1) + parseInt(v.cnt2));
        const max = tmp.reduce( function (previous, current) { 
            return previous > current ? previous:current;
        });
        return max;
    }

    exportToExcel = async(data, userData) => {
        let standard = "", sub = "";
        const { selectedStandard, selectedStandardSub, selectedClass, selectedCenter, subList, classList, centerList } = this.state;
        if(selectedStandard == 1) {
            standard = "지시자";
            sub = subList.find(v => v.id == selectedStandardSub);
        } else if(selectedStandard == 3) {
            standard = "반";
            sub = classList.find(v => v.id == selectedClass);
        } else if(selectedStandard == 4) {
            standard = "센터";
            sub = centerList.find(v => v.id == selectedCenter);
        }
        if(!sub || !sub.name) {
            alert("기준 선택 오류입니다"); return;
        }
        const exportTitle = standard + "_" + sub.name + "_excel";

        const wb = new ExcelJS.Workbook();
        Object.keys(data).forEach((k, i) => {
            let cnt = 0;
            let maxWidth = 20;
            let weekTotal = {
                positive: 0,
                total: 0,
                sto_met: 0,
                criterion: 0,
                lto_met: 0,
                cor_decs: 0,
                incor_decs: 0,
                err: 0,
                with_err: 0,
            };

            const ws = wb.addWorksheet(k);
            ws.columns = [
                { header:'name', key:'col0' },
                { header:'ltos', key:'col1' },
                { header:"# of Trials correct L.U.", key:'col2' },
                { header:"Total # of L.U.", key:'col3' },
                { header:"STO Met", key:'col4' },
                { header:"L.U. to Criteron", key:'col5' },
                { header:"LTO Met", key:'col6' },
                { header:"Correct", key:'col7' },
                { header:"Incorrect", key:'col8' },
                { header:"errorless", key:'col9' },
                { header:"with errors", key:'col10' },
            ];cnt++;
            const colRow = ws.getRow(1);
            colRow.hidden = true;

            const tabCont = data[k];
            Object.keys(tabCont).forEach(d => {
                const dateTitleRow = ws.addRow([this.props.funcStore.dateToHangul(d)]);cnt++;
                dateTitleRow.font = { bold: true };

                const oneDay = tabCont[d];
                let dayTotal = {
                    positive: 0,
                    total: 0,
                    sto_met: 0,
                    criterion: 0,
                    lto_met: 0,
                    cor_decs: 0,
                    incor_decs: 0,
                    err: 0,
                    with_err: 0,
                }
                Object.keys(oneDay).forEach(r => {
                    const item = oneDay[r];
                    let total = {
                        positive: 0,
                        total: 0,
                        sto_met: 0,
                        criterion: 0,
                        lto_met: 0,
                        cor_decs: 0,
                        incor_decs: 0,
                        err: 0,
                        with_err: 0,
                    };
                    const l = item.length;
                    const colTemp1 = [,,,,,,,,"Total # of Decisions","","Total # TPRA",""];
                    const colTemp2 = [item[0].student_name,"",
                        "# of Trials correct L.U.",
                        "Total # of L.U.",
                        "STO Met",
                        "L.U. to Criteron",
                        "LTO Met",
                        "Correct","Incorrect","errorless","with errors"];
                    ws.addRow(colTemp1);cnt++;
                    const rr1 = ws.getRow(cnt);
                    colTemp1.map((r2,i2)=>{
                        if(i2 > 6) {
                            rr1.getCell(i2).border = {
                                top: {style:'thin'},
                                left: {style:'thin'},
                                bottom: {style:'thin'},
                                right: {style:'thin'}
                            };
                        }
                    });
                    ws.mergeCells('H'+cnt, 'I'+cnt);
                    ws.mergeCells('J'+cnt, 'K'+cnt);
                    ws.addRow(colTemp2);cnt++;
                    const rr2 = ws.getRow(cnt);
                    const start = cnt;
                    colTemp2.map((r2,i2)=>{
                        if(i2 == 0) rr2.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                        rr2.getCell(i2+1).border = {
                            top: {style:'thin'},
                            left: {style:'thin'},
                            bottom: {style:'thin'},
                            right: {style:'thin'}
                        };
                    });
                    item.map((addRow,i) => {
                        if(addRow.lto_name) {
                            let row = [];
                            row[1] = "";
                            row[2] = addRow.lto_name;
                            row[3] = addRow.positive;
                            row[4] = addRow.total;
                            row[5] = addRow.sto_met;
                            row[6] = addRow.criterion;
                            row[7] = addRow.lto_met;
                            row[8] = addRow.decs;
                            //row[9] = addRow.decs;
                            row[9] = '0';
                            row[10] = '0';
                            row[11] = '0';
                            total.positive += parseInt(addRow.positive);
                            total.total += parseInt(addRow.total);
                            total.sto_met += parseInt(addRow.sto_met);
                            total.criterion += parseFloat(addRow.criterion);
                            total.lto_met += parseInt(addRow.lto_met);
                            total.cor_decs += parseInt(addRow.decs);
                            // total.incor_decs += parseInt(addRow.decs);
                            // total.errorless += row.errorless;
                            // total.with_err += row.witherror;
                            ws.addRow(row);cnt++;
                            const rr3 = ws.getRow(cnt);
                            row.map((r2,i2)=>{
                                rr3.getCell(i2).border = {
                                    top: {style:'thin'},
                                    left: {style:'thin'},
                                    bottom: {style:'thin'},
                                    right: {style:'thin'}
                                };
                                if(i2 < 3) rr3.getCell(i2).alignment = {vertical: 'middle', horizontal: 'left'};
                                else rr3.getCell(i2).alignment = {vertical: 'middle', horizontal: 'right'};
                            });
                            if(row[2].length > maxWidth) {maxWidth = row[2].length}
                        }
                    });
                    let totalRow = [];
                    totalRow[1] = "";
                    totalRow[2] = "Totals";
                    totalRow[3] = total.positive;
                    totalRow[4] = total.total;
                    totalRow[5] = total.sto_met;
                    totalRow[6] = total.criterion;
                    totalRow[7] = total.lto_met;
                    totalRow[8] = total.cor_decs;
                    totalRow[9] = total.incor_decs;
                    totalRow[10] = item[0].errorless ? parseInt(item[0].errorless) : 0;
                    totalRow[11] = item[0].witherror ? parseInt(item[0].witherror) : 0;

                    dayTotal.positive += total.positive;
                    dayTotal.total += total.total;
                    dayTotal.sto_met += total.sto_met;
                    dayTotal.criterion += total.criterion;
                    dayTotal.lto_met += total.lto_met;
                    dayTotal.cor_decs += total.cor_decs;
                    dayTotal.incor_decs += total.incor_decs;
                    dayTotal.err += item[0].errorless ? parseInt(item[0].errorless) : 0;
                    dayTotal.with_err += item[0].witherror ? parseInt(item[0].witherror) : 0;

                    ws.addRow(totalRow);cnt++;
                    const rr4 = ws.getRow(cnt);
                    totalRow.map((r2,i2)=>{
                        rr4.getCell(i2).border = {
                            top: {style:'thin'},
                            left: {style:'thin'},
                            bottom: {style:'thin'},
                            right: {style:'thin'}
                        };
                        if(i2 < 3) rr4.getCell(i2).alignment = {vertical: 'middle', horizontal: 'left'};
                        else rr4.getCell(i2).alignment = {vertical: 'middle', horizontal: 'right'};
                    });
                    ws.mergeCells('A'+start, 'A'+cnt);
                    ws.addRow();cnt++;
                    
                    // <td rowSpan={l + 3}>{item[0].student_name}</td>
                    // <Typography>{item[0].student_name}</Typography>
                });
                const ct1 = [,,,,,,,,"Total # of Decisions","","Total # TPRA",""];
                const ct2 = ["Daily","",
                    "# of Trials correct L.U.",
                    "Total # of L.U.",
                    "STO Met",
                    "L.U. to Criteron",
                    "LTO Met",
                    "Correct","Incorrect","errorless","with errors"];
                ws.addRow(ct1);cnt++;
                const rr1 = ws.getRow(cnt);
                ct1.map((r2,i2)=>{
                    if(i2 > 6) {
                        rr1.getCell(i2).border = {
                            top: {style:'thin'},
                            left: {style:'thin'},
                            bottom: {style:'thin'},
                            right: {style:'thin'}
                        };
                    }
                });
                ws.mergeCells('H'+cnt, 'I'+cnt);
                ws.mergeCells('J'+cnt, 'K'+cnt);
                ws.addRow(ct2);cnt++;
                const rr2 = ws.getRow(cnt);
                const ss = cnt;
                ct2.map((r2,i2)=>{
                    if(i2 == 0) {
                        rr2.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                        rr2.getCell(i2+1).fill = {
                            type: 'pattern',
                            pattern:'darkTrellis',
                            fgColor:{argb:'FFFFFF00'},
                            bgColor:{argb:'FF0000FF'}
                        }
                    }
                    rr2.getCell(i2+1).border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    };
                });
                const dailyRow = [
                    "", "Totals", dayTotal.positive, dayTotal.total, dayTotal.sto_met, calcCriterion(dayTotal.total, dayTotal.sto_met), dayTotal.lto_met, dayTotal.cor_decs,
                    dayTotal.incor_decs, dayTotal.err, dayTotal.with_err
                ];
                ws.addRow(dailyRow);cnt++;
                const rr5 = ws.getRow(cnt);
                dailyRow.map((r2,i2)=>{
                    rr5.getCell(i2+1).border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    };
                    if(i2 < 2) rr5.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                    else rr5.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'right'};
                });
                ws.mergeCells('A'+ss, 'A'+cnt);
                ws.addRow();cnt++;

                weekTotal.positive += dayTotal.positive;
                weekTotal.total += dayTotal.total;
                weekTotal.sto_met += dayTotal.sto_met;
                weekTotal.criterion += dayTotal.criterion;
                weekTotal.lto_met += dayTotal.lto_met;
                weekTotal.cor_decs += dayTotal.cor_decs;
                weekTotal.incor_decs += dayTotal.incor_decs;
                weekTotal.err += dayTotal.err;
                weekTotal.with_err += dayTotal.with_err;
            });

            const ct3 = [,,,,,,,,"Total # of Decisions","","Total # TPRA",""];
            const ct4 = ["Weekly","",
                "# of Trials correct L.U.",
                "Total # of L.U.",
                "STO Met",
                "L.U. to Criteron",
                "LTO Met",
                "Correct","Incorrect","errorless","with errors"];
            ws.addRow(ct3);cnt++;
            const rr1 = ws.getRow(cnt);
            ct3.map((r2,i2)=>{
                if(i2 > 6) {
                    rr1.getCell(i2).border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    };
                }
            });
            ws.mergeCells('H'+cnt, 'I'+cnt);
            ws.mergeCells('J'+cnt, 'K'+cnt);
            ws.addRow(ct4);cnt++;
            const rr2 = ws.getRow(cnt);
            const ss2 = cnt;
            ct4.map((r2,i2)=>{
                if(i2 == 0) {
                    rr2.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                    rr2.getCell(i2+1).fill = {
                        type: 'pattern',
                        pattern:'darkTrellis',
                        fgColor:{argb:'FFFFFF00'},
                        bgColor:{argb:'FF0000FF'}
                    }
                }
                rr2.getCell(i2+1).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };
            });
            const weekRow = [
                "", "Student Totals", weekTotal.positive, weekTotal.total, weekTotal.sto_met, calcCriterion(weekTotal.total, weekTotal.sto_met), weekTotal.lto_met, weekTotal.cor_decs,
                weekTotal.incor_decs, weekTotal.err, weekTotal.with_err
            ];
            ws.addRow(weekRow);cnt++;
            const rr6 = ws.getRow(cnt);
            weekRow.map((r2,i2)=>{
                rr6.getCell(i2+1).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };
                if(i2 < 2) rr6.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                else rr6.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'right'};
            });
            const userArr = userData[k];
            const userTotal = {
                positive: 0,
                total: 0,
                sto_met: 0,
                criterion: 0,
                lto_met: 0,
                cor_decs: 0,
                incor_decs: 0,
                err: 0,
                with_err: 0,
            }
            userArr.map(u => {
                const weekRowItems = [
                    "", u.user_name, u.positive, u.total, u.sto_met, parseFloat(u.criterion), u.lto_met,
                    u.decs, 0, parseInt(u.errorless), parseInt(u.witherror)
                ];
                ws.addRow(weekRowItems);cnt++;
                const rt = ws.getRow(cnt);
                weekRow.map((r2,i2)=>{
                    rt.getCell(i2+1).border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    };
                    if(i2 < 2) rt.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                    else rt.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'right'};
                });
                userTotal.positive += parseInt(u.positive);
                userTotal.total += parseInt(u.total);
                userTotal.sto_met += parseInt(u.sto_met);
                userTotal.criterion += parseFloat(u.criterion);
                userTotal.lto_met += parseInt(u.lto_met);
                userTotal.cor_decs += parseInt(u.decs);
                // userTotal.incor_decs += parseInt(u.decs);
                userTotal.err += parseInt(u.errorless);
                userTotal.with_err += parseInt(u.witherror);
            });
            const userRow = [
                "", "Teacher Totals", userTotal.positive, userTotal.total, userTotal.sto_met, calcCriterion(userTotal.total, userTotal.sto_met), userTotal.lto_met,
                userTotal.cor_decs, userTotal.incor_decs, userTotal.err, userTotal.with_err
            ];
            ws.addRow(userRow);cnt++;
            const rt2 = ws.getRow(cnt);
            userRow.map((r2,i2)=>{
                rt2.getCell(i2+1).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                };
                if(i2 < 2) rt2.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'left'};
                else rt2.getCell(i2+1).alignment = {vertical: 'middle', horizontal: 'right'};
            });
            ws.mergeCells('A'+ss2, 'A'+cnt);
            ws.addRow();cnt++;

            ws.columns.forEach((column, i2) => {
                if(i2 == 0) column.width = 15;
                else if(i2 == 1) column.width = column.header.length < 20 ? 20 : column.header.length;
                else column.width = column.header.length < 12 ? 12 : column.header.length;
            });
            ws.columns[1].width = maxWidth;
        });
        
        const buf = await wb.xlsx.writeBuffer();
        saveAs(new Blob([buf]), exportTitle + '.xlsx');
    }

    stickedUsersOrStudents = date => {
        const { selectedGraph, selectedStandard, selectedStandardSub } = this.state;
        if((selectedGraph == 1 || selectedGraph == 2)) {
            let url = "/public/search/";
            if(selectedStandard == 1) url += "StudentSticks/";
            else if(selectedStandard == 2) url += "userSticks/";
            url += date + "/" + selectedStandardSub;
            window.open(url, "_blank");
        }
    }

    setViewComponent = () => {
        const { selectedGraph, graphList,
            yLabel, yLabel2, ticks, ticks2, domain, domain2, domainList, 
            data, dotStyle, dataKey, subDataKey, xDataKey, lineColor, subLineColor } = this.state;
        const { classes } = this.props;
        let graphView = null;
        let printBtn = null;
        if(data.length && selectedGraph != 6) {
            const h = ticks.length * 44 > 640 ? 640 : ticks.length * 44;
            if(selectedGraph == 1 || selectedGraph == 2) {
                graphView = (
                    <LineChart
                        id="graph_area"
                        className={classes.graphDraw}
                        width={data.length * 60 < 600 ? 600 : data.length * 60}
                        height={h}
                        data={data}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 40,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey={xDataKey}
                            minTickGap={0}
                            interval={0}
                            tick={<CustomizedAxisTick select={true} stickedUsersOrStudents={this.stickedUsersOrStudents} />}
                        />
                        <YAxis
                            tick={{
                                fontSize: 10
                            }}
                            ticks={ticks}
                            domain={domain}
                            orientation="left"
                        >
                            <Label value={yLabel} angle={-90} position="insideLeft" />
                        </YAxis>
                        {selectedGraph == 2 ? (
                            <Tooltip
                                labelFormatter={(c)=>{
                                    const d = data.filter(v => v.end_date == c);
                                    if(d.length) return d[0].end_date;
                                    else return "";
                                }}
                                content={<CustomTooltip />}
                            />
                        ) : (
                            <Tooltip
                                labelFormatter={(c)=>{
                                    const d = data.filter(v => v.end_date == c);
                                    if(d.length) return d[0].end_date;
                                    else return "";
                                }}
                                content={<Custom1Tooltip domainList={domainList} />}
                            />
                        )}
                        
                        <Legend align="left" iconSize={14} fontSize={11} wrapperStyle={{paddingLeft:60, bottom: 10}}
                            formatter={(value, entry, index)=>{
                                return value == "total" ? "총 런유닛" : value=="crieteria" ? "런유닛/도달" : "정반응";
                            }}
                        />
                        <Line
                            dataKey={dataKey}
                            stroke="#222"
                            strokeWidth={4}
                            strokeDasharray="4 4"
                            dot={{
                                r: 4, strokeWidth: 2, fill: "#222", stroke: "#222"
                            }}
                            activeDot={{
                                r: 6, strokeWidth: 2, fill: "#eee", stroke: "#222"
                            }}
                        />
                        {selectedGraph == 1 && (
                            <Line
                                dataKey={subDataKey}
                                stroke="#888"
                                strokeWidth={4}
                                dot={{
                                    r: 4, strokeWidth: 2, fill: "#888", stroke: "#888"
                                }}
                                activeDot={{
                                    r: 6, strokeWidth: 2, fill: "#eee", stroke: "#888"
                                }}
                            />
                        )}
                    </LineChart>
                );
            } else if(selectedGraph == 3 || selectedGraph == 4 || selectedGraph == 5) {
                graphView = (
                    <ComposedChart
                        id="graph_area"
                        className={classes.graphDraw}
                        width={data.length * 50 < 600 ? 600 : data.length * 50}
                        height={h}
                        data={data}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 40,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey={selectedGraph != 5 ? xDataKey : "end_date"}
                            minTickGap={0}
                            interval={0}
                            tick={<CustomizedAxisTick />}
                        />
                        <YAxis
                            yAxisId="1"
                            tick={{
                                fontSize: 10
                            }}
                            ticks={ticks}
                            domain={domain}
                            orientation="left"
                        >
                            <Label value={yLabel} angle={-90} position="insideLeft" />
                        </YAxis>
                        <YAxis
                            yAxisId="2"
                            tick={{
                                fontSize: 10
                            }}
                            ticks={ticks2}
                            domain={domain2}
                            orientation="right"
                        >
                            <Label value={yLabel2} angle={-90} position="insideRight" />
                        </YAxis>
                        <Tooltip
                            labelFormatter={(c)=>{
                                const d = data.filter(v => v.end_date == c);
                                if(d.length) return d[0].end_date;
                                else return "";
                            }}
                            formatter={(val, name, props)=>{
                                if(selectedGraph != 5) {
                                    return [val, (name=="cnt" ? "도달수" : name=="stack" ? "누적수" : name=="decisions" ? "디씨전" : name=="errors" ? "오류" : "미오류")];
                                } else {
                                    let text = "";
                                    if(name == "cnt1") {
                                        text = "오류";
                                    } else if(name == "cnt2") {
                                        text = "미오류";
                                    } else if(name == "stack1") {
                                        text = "오류 누적";
                                    } else if(name == "stack2") {
                                        text = "미오류 누적";
                                    }
                                    return [val, text];
                                }
                        }} />
                        <Legend align="left" iconSize={14} fontSize={11} wrapperStyle={{paddingLeft:60, bottom: 10}}
                            formatter={(value, entry, index)=>{
                                if(selectedGraph != 5) {
                                    return value=="cnt" ? "도달수" : value=="stack" ? "누적수" : value=="decisions" ? "디씨전" : value=="errors" ? "오류" : "미오류";
                                } else {
                                    let text = "";
                                    if(value == "cnt1") {
                                        text = "오류";
                                    } else if(value == "cnt2") {
                                        text = "미오류";
                                    } else if(value == "stack1") {
                                        text = "오류 누적";
                                    } else if(value == "stack2") {
                                        text = "미오류 누적";
                                    }
                                    return text;
                                }
                            }}
                        />
                        {selectedGraph != 5 && (
                            <Bar
                                yAxisId="1"
                                dataKey={dataKey}
                                fill="#fff"
                                stroke="#666"
                                barSize={12}
                            />
                        )}
                        {selectedGraph != 5 && (
                            <Line
                                yAxisId="2"
                                dataKey={subDataKey}
                                stroke="#222"
                                strokeWidth={4}
                                dot={{
                                    r: 4, strokeWidth: 2, fill: "#222", stroke: "#222"
                                }}
                                activeDot={{
                                    r: 6, strokeWidth: 2, fill: "#eee", stroke: "#222"
                                }}
                            />
                        )}
                        {selectedGraph == 5 && (
                            <Bar
                                yAxisId="1"
                                stackId="a"
                                dataKey="cnt2"
                                fill="#fff"
                                stroke="#666"
                                barSize={12}
                            />
                        )}
                        {selectedGraph == 5 && (
                            <Bar
                                yAxisId="1"
                                stackId="a"
                                dataKey="cnt1"
                                fill="#666"
                                stroke="#666"
                                barSize={12}
                            />
                        )}
                        {selectedGraph == 5 && (
                            <Line
                                yAxisId="2"
                                dataKey="stack2"
                                stroke="#222"
                                strokeWidth={4}
                                strokeDasharray="4 4"
                                dot={{
                                    r: 4, strokeWidth: 2, fill: "#222", stroke: "#222"
                                }}
                                activeDot={{
                                    r: 6, strokeWidth: 2, fill: "#eee", stroke: "#222"
                                }}
                            />
                        )}
                        {selectedGraph == 5 && (
                            <Line
                                yAxisId="2"
                                dataKey="stack1"
                                stroke="#222"
                                strokeWidth={4}
                                dot={{
                                    r: 4, strokeWidth: 2, fill: "#222", stroke: "#222"
                                }}
                                activeDot={{
                                    r: 6, strokeWidth: 2, fill: "#eee", stroke: "#222"
                                }}
                            />
                        )}
                    </ComposedChart>
                );
            }

            printBtn = (
                <Fab size="small" aria-label="save" className={classes.saveBtn}
                    onClick={() => {
                        const title = graphList.filter(g => g.id == selectedGraph);
                        if(title[0] && title[0].name) {
                            const svg = document.getElementsByClassName('recharts-surface');
                            if(svg) {
                                this.props.funcStore.printSvg(title[0].name, svg[0]);
                            }
                        }
                    }}>
                    <Print />
                </Fab>
            );
        } else {
            graphView = (
                <div>Empty Data</div>
            );
            printBtn = null;
        }
        this.setState({
            viewComponent: graphView,
            printComponent: printBtn,
        });
    }

    handleTabChange = (e, newValue) => {
        this.setState({
            tabValue: newValue
        });
    };

    render() {
        const { dialogFlag, dialogOpen, alertOpen, viewComponent, data, userData, tabValue, totalRefresh,
            listLabelWidth, standardLabelWidth, standardSubLabelWidth, classLabelWidth, ostandardLabelWidth, onlyLabelWidth, domainLabelWidth,
            graphList, selectedGraph, selectedStandard, selectedStandardSub, selectedClass, selectedCenter, selectedDomain,
            classList, centerList, subList, domainList, fromDate, toDate, printComponent,
        } = this.state;
        const { classes, mainStore } = this.props;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        return (
            <div className={classes.container}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputBoxRow}>
                        <FormControl variant="outlined" className={classes.selectGraphList}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(1, ref);
                                }}
                                htmlFor="graph-select"
                            >
                                {lang['graph4']}
                            </InputLabel>
                            <Select
                                value={selectedGraph}
                                onChange={this.handleGraphList('selectedGraph')}
                                input={
                                    <OutlinedInput
                                        labelWidth={listLabelWidth}
                                        name="selectedGraph"
                                        id="graph-select"
                                    />
                                }
                            >
                                <MenuItem value="">
                                    <em>{lang['etc1']}</em>
                                </MenuItem>
                                {graphList.map((v,i) => {
                                    return (
                                        <MenuItem key={"g_"+i} value={v.id}>
                                            {"#" + (i+1) + ". " + v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </div>
                    <div className={classes.subInputBoxRow}>
                        <div className={classes.subInputBoxLeft}>
                            <FormControl variant="outlined" className={classes.selectStandard}>
                                <InputLabel
                                    ref={ref => {
                                        if(ref) this.setLabelWidth(2, ref);
                                    }}
                                    htmlFor="standard-select"
                                >
                                    {lang['graph6']}
                                </InputLabel>
                                <Select
                                    value={selectedStandard}
                                    onChange={this.handleSelection('selectedStandard')}
                                    input={
                                        <OutlinedInput
                                            labelWidth={standardLabelWidth}
                                            name="selectedStandard"
                                            id="standard-select"
                                        />
                                    }
                                    // disabled={
                                    //     mainStore.user.level < 2 ? true : false
                                    // }
                                >
                                    <MenuItem value="1">
                                        {lang['stoitem7']}
                                    </MenuItem>
                                    {selectedGraph < 5 && (
                                        <MenuItem value="2">
                                            {lang['reach2']}
                                        </MenuItem>
                                    )}
                                    <MenuItem value="3">
                                        {lang['reach1']}
                                    </MenuItem>
                                    <MenuItem value="4">
                                        {lang['reach9']}
                                    </MenuItem>
                                </Select>
                            </FormControl>
                            {(selectedStandard == 2 || selectedStandard == 3) && (
                                <FormControl variant="outlined" className={classes.selectWideInput}>
                                    <InputLabel
                                        ref={ref => {
                                            if(ref) this.setLabelWidth(4, ref);
                                        }}
                                        htmlFor="class-select"
                                    >
                                        {lang['reach1']}
                                    </InputLabel>
                                    <Select
                                        value={selectedClass}
                                        onChange={this.handleClass()}
                                        input={
                                            <OutlinedInput
                                                labelWidth={classLabelWidth}
                                                name="selectedClass"
                                                id="class-select"
                                            />
                                        }
                                    >
                                        <MenuItem value="">
                                            <em>선택</em>
                                        </MenuItem>
                                        {classList.map((v,i) => {
                                            return (
                                                <MenuItem key={"c_"+i} value={v.id}>
                                                    {v.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>
                            )}
                            {selectedStandard < 3 && (
                                <FormControl variant="outlined" className={classes.selectStandardSub}>
                                    <InputLabel
                                        ref={ref => {
                                            if(ref) this.setLabelWidth(3, ref);
                                        }}
                                        htmlFor="standard-select-sub"
                                    >
                                        {selectedStandard == 1 ? lang['graph8'] : lang['graph13']}
                                    </InputLabel>
                                    <Select
                                        value={selectedStandardSub}
                                        onChange={this.handleSelection('selectedStandardSub')}
                                        input={
                                            <OutlinedInput
                                                labelWidth={standardSubLabelWidth}
                                                name="selectedStandardSub"
                                                id="standard-select-sub"
                                            />
                                        }
                                        // disabled={
                                        //     selectedStandard == 1 && mainStore.user.level < 2 ? true : false
                                        // }
                                    >
                                        <MenuItem value="">
                                            <em>{lang['etc1']}</em>
                                        </MenuItem>
                                        {subList.map((v,i) => {
                                            return (
                                                <MenuItem key={"sub_"+i} value={(selectedStandard==2 && selectedClass==0) ? v.re_id : v.id}>
                                                    {v.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>
                            )}
                            {selectedStandard == 4 && (
                                <FormControl variant="outlined" className={classes.selectStandardSub}>
                                    <InputLabel
                                        ref={ref => {
                                            if(ref) this.setLabelWidth(3, ref);
                                        }}
                                        htmlFor="standard-select-sub"
                                    >
                                        {lang['reach9']}
                                    </InputLabel>
                                    <Select
                                        value={selectedCenter}
                                        onChange={this.handleSelection('selectedCenter')}
                                        input={
                                            <OutlinedInput
                                                labelWidth={standardSubLabelWidth}
                                                name="selectedCenter"
                                                id="standard-select-sub"
                                            />
                                        }
                                    >
                                        <MenuItem value="">
                                            <em>{lang['etc1']}</em>
                                        </MenuItem>
                                        {centerList.map((v,i) => {
                                            return (
                                                <MenuItem key={"sub_"+i} value={v.id}>
                                                    {v.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>
                            )}
                        </div>
                        <div className={classes.subInputBoxRight}>
                            <TextField
                                id="date-picker1"
                                label={lang['graph9']}
                                type="date"
                                value={fromDate}
                                className={classes.selectDate}
                                onChange={this.handleDateChange('fromDate')}
                                variant="outlined"
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                            <TextField
                                id="date-picker2"
                                label={lang['graph10']}
                                type="date"
                                value={toDate}
                                className={classes.selectDate}
                                onChange={this.handleDateChange('toDate')}
                                variant="outlined"
                                InputLabelProps={{
                                    shrink: true
                                }}
                            />
                            <Fab variant="extended" aria-label="apply" className={classes.applyBtn}
                                onClick={
                                    () => this.applyCondition()}>
                                <Cached className={classes.extendedIcon} />
                                {"APPLY"}
                            </Fab>
                            {printComponent}
                        </div>
                    </div>
                    <div className={classes.subInputBoxRow}>
                        {(selectedGraph == 2 && selectedStandard == 2) && (
                            <FormControl variant="outlined" className={classes.selectDomain}>
                                <InputLabel
                                    ref={ref => {
                                        if(ref) this.setLabelWidth(7, ref);
                                    }}
                                    htmlFor="select-domain"
                                >
                                    {lang['domain6']}
                                </InputLabel>
                                <Select
                                    value={selectedDomain}
                                    onChange={this.handleSelection('selectedDomain')}
                                    input={
                                        <OutlinedInput
                                            labelWidth={domainLabelWidth}
                                            name="selectedDomain"
                                            id="select-domain"
                                        />
                                    }
                                >
                                    <MenuItem value="">
                                        <em>All</em>
                                    </MenuItem>
                                    {domainList.map((v,i) => {
                                        return (
                                            <MenuItem key={"d_"+i} value={v.id}>
                                                {v.name}
                                            </MenuItem>
                                        );
                                    })}
                                </Select>
                            </FormControl>
                        )}
                    </div>
                </div>
                <div className={classes.contentRoot}>
                    <div className={classes.loadingBox} id="loader_box">
                        <div id="loader"></div>
                    </div>
                    {(selectedGraph == 6) ? (
                        <div className={classes.totalArea}>
                            <Total
                                refresh={totalRefresh}
                                afterRefresh={()=>{
                                    this.setState({ totalRefresh:false });
                                }}
                                data={data}
                                userData={userData}
                                value={tabValue}
                                handleTabChange={this.handleTabChange}
                            />
                        </div>
                    ) : (
                        <div className={classes.graphArea}>
                            {viewComponent}
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    container: {
        padding: 30
    },
    extendedIcon: {
        marginRight: 8,
    },
    contentRoot: {
        marginTop: 15,
        overflow: 'auto',
        position: 'relative',
    },
    graphDraw: {
        marginTop: 0,
        '& .yAxis > text': {
            textAnchor: 'middle'
        },
        '& ul.recharts-tooltip-item-list *': {
            color: '#000'
        },
        '& .custom-tooltip': {
            backgroundColor: '#fff',
            border: '1px solid #444',
            padding: '12px 12px 6px 12px',
            '& p': {
                marginBottom: 6
            },
            '& .label': {
                fontSize: 16
            }
        }
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    subInputBoxRow: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 15
    },
    formControl: {
        width: 120,
        marginLeft: 15,
        marginRight: 15
    },
    subInputBoxLeft: {
        display: 'flex',
        flexDirection: 'row'
    },
    subInputBoxRight: {
        display: 'flex',
        flexDirection: 'row'
    },
    selectGraphList: {
        flex: 1,
    },
    selectStandard: {
        width: 100,
        marginRight: 8
    },
    selectoStandard: {
        width: 100,
        marginLeft: 8,
        marginRight: 8
    },
    selectDomain: {
        flex: 1,
        maxWidth: 380,
        marginRight: 8
    },
    selectClass: {
        width: 100,
        marginLeft: 8,
        marginRight: 8
    },
    selectWideInput: {
        width: 180,
        marginLeft: 8,
        marginRight: 8
    },
    selectStandardSub: {
        width: 220,
        marginLeft: 8,
        marginRight: 8
    },
    selectDate: {
        width: 180,
        marginLeft: 8,
        marginRight: 8
    },
    applyBtn: {
        backgroundColor: 'transparent',
        marginLeft: 8,
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    saveBtn: {
        width: 48,
        height: 48,
        backgroundColor: 'transparent',
        marginLeft: 16,
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    graphArea: {
        flex: 1,
        width: 'fit-content',
        minWidth: '100%',
        backgroundColor: '#fff',
        minHeight: 480,
        padding: '15px 0 0 0',
    },
    totalArea: {
        flex: 1,
        width: 'auto',
        minWidth: '100%',
        backgroundColor: '#fff',
        minHeight: 480,
        padding: 0
    },
    divined: {
        width: 1,
        height: 30,
        backgroundColor: '#bbb',
        alignSelf: 'center',
        margin: '0 8px'
    },
    loadingBox: {
        position: 'absolute',
        backgroundColor: 'rgba(0,0,0,.86)',
        flex: 1,
        height: '100%',
        width: '100%',
        minHeight: 480,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 9999,
        visibility: 'hidden',
        transition: 'visiblity .3s ease'
    }
});

export default withStyles(styles)(Graph);