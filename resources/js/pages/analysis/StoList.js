import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, TextField, Button, Input, OutlinedInput, FilledInput,
    InputLabel, MenuItem, FormControl, Select, Divider, InputAdornment,
    List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, IconButton } from '@material-ui/core';
import { Cached, Add, Comment, Delete, Save, HighlightOff } from '@material-ui/icons/';
import { ReachedSto } from '../../components/';

@inject(stores => ({
    funcStore: stores.funcStore
}))
class StoList extends Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            dialogFlag: true,
            dialogOpen: false,
            alertOpen: false,
            page: 0,
            perPage: 10,
            total: 0,
            sort: true,
            ltoList: [],
            classList: [],
            studentList: [],
            ltoId: '',
            classId: '',
            studentId: '',
            fromDate: this.props.funcStore.formatDate(),
            toDate: this.props.funcStore.convertToymd(),
            data: [],
            status: '',
            classLabelWidth: 0,
            studentLabelWidth: 0,
            statusLabelWidth: 0,
            ltoLabelWidth: 0
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.loadClassList();
        //this.loadReachedSto();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this._isMounted;
    }

    setLabelWidth = (kind, ref) => {
        const { classLabelWidth, studentLabelWidth, statusLabelWidth, ltoLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != studentLabelWidth) {
            this.setState({ studentLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != statusLabelWidth) {
            this.setState({ statusLabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != ltoLabelWidth) {
            this.setState({ ltoLabelWidth: ref.offsetWidth });
        }
    }

    loadClassList = () => {
        const { funcStore } = this.props;
        const { classList, ltoList } = this.state;
        // load class list
        // funcStore.conn('get', '/user/getClasses', false, true, null, (res)=>{
        //     if(res) {
	    //         if(JSON.stringify(res) != JSON.stringify(classList)) {
		//             this.setState({
        //                 //classList: res.list.filter(v => v.id != 0)
        //                 classList: res
	    //             });
	    //         }
        //     } else {
        //         console.log("CLASS 로딩 실패");
        //     }
        // });
        this.props.funcStore.conn('get', '/common/getClassesUsers', false, true, null, (res)=>{
            this.setState({
                classList: res.classes,
            });
        });
        // load lto list
        funcStore.conn('get', '/ltos', false, true, null, (res)=>{
            if(res.list) {
	            if(JSON.stringify(res.list) != JSON.stringify(ltoList)) {
		            this.setState({
	                    ltoList: res.list
	                });
	            }
            } else {
                console.log("DOMAIN 로딩 실패");
            }
        });
    }

    loadStudentList = classId => {
	    const { funcStore } = this.props;
	    const cId = classId ? classId : 0;
        // load student list
        funcStore.conn('get', '/students/' + cId + '/class', false, true, null, (res)=>{
            if(res.list) {
                this.setState({
                    studentList: res.list
                });
            } else {
                console.log("STUDENT 로딩 실패");
            }
        });
    }

    loadReachedSto = () => {
        const { classId, studentId, sort, page, perPage, status, ltoId, fromDate, toDate } = this.state;
        let sId = studentId;
        if(classId == 0) sId = sId ? sId.split("_")[0] : '';
        const param = {
            classId: classId,
            studentId: studentId,
            sort: sort,
            page: page,
            perPage: perPage,
            status: status,
            ltoId: ltoId,
            fromDate: fromDate,
            toDate: toDate
        }
        this.props.funcStore.conn('get', '/stoItem/getReached', false, true, param, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    data: res.stoItems,
                    total: res.total,
                });
            } else {
                console.log("STO 로딩 실패");
            }
        });
    }

    reloadReached = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadReachedSto();
                this.forceUpdate();
            }, 0);
        });
    }

    handleChange = name => e => {
	    if(name == 'classId') {
		    if(e.target.value != 0 && e.target.value == '') {
                this.setState({
				    studentId: '', studentList: []
			    });
		    } else {
                //this.loadStudentList(e.target.value);
                const param = {
                    selectedStandard: 2,
                    selectedClass: e.target.value
                }
                this.props.funcStore.conn('get', '/common/getGraphSubList', false, true, param, (res)=>{
                    if(res.result == "true") {
                        this.setState({
                            studentId: '',
                            studentList: res.list
                        });
                    } else {
                        console.log("학생 로딩 실패");
                    }
                });
		    }
	    }
	    this.setState({
            [name]: e.target.value,
        });
    }

    handleDateChange = name => date => {
        const { fromDate, toDate } = this.state;
        const d = date.target ? date.target.value : date;
        if(name == "fromDate" && (new Date(d) > new Date(toDate))) return;
        if(name == "toDate" && (new Date(d) < new Date(fromDate))) return;
        this.setState({
            [name]: d
        });
    }

    handlePage = page => {
        let t = page;
        if(!page) t = 0;
        this.setState({
            page: page
        });
        this.reloadReached();
    }

    handleSort = () => {
        this.setState({ sort: !this.state.sort });
        this.reloadReached();
    }
/* <FormControl variant="outlined" className={classes.selectClass}>
    <InputLabel
        ref={ref => {
            if(ref) this.setLabelWidth(3, ref);
        }}
        htmlFor="standard-select-status"
    >
        STO 상태
    </InputLabel>
    <Select
        value={status}
        onChange={this.handleChange('status')}
        input={
            <OutlinedInput
                labelWidth={statusLabelWidth}
                name="selectedStatus"
                id="standard-select-status"
            />
        }
    >
        <MenuItem value="">
            <em>전체</em>
        </MenuItem>
        <MenuItem value="0">
            <em>진행중</em>
        </MenuItem>
        <MenuItem value="1">
            <em>완료</em>
        </MenuItem>
        <MenuItem value="2">
            <em>중지</em>
        </MenuItem>
        <MenuItem value="-1">
            <em>취소</em>
        </MenuItem>
    </Select>
</FormControl> */
    render() { 
        const { classList, studentList, classId, studentId, fromDate, toDate, data,
            page, perPage, total, sort, ltoList, ltoId, status,
            classLabelWidth, studentLabelWidth, statusLabelWidth, ltoLabelWidth } = this.state;
        const { classes } = this.props;
        return (
            <div className={classes.container}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputBoxLeft}>
                        <FormControl variant="outlined" className={classes.selectClass}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(1, ref);
                                }}
                                htmlFor="class-select"
                            >
                                반
                            </InputLabel>
                            <Select
                                value={classId}
                                onChange={this.handleChange('classId')}
                                input={
                                    <OutlinedInput
                                        labelWidth={classLabelWidth}
                                        name="selectedClass"
                                        id="class-select"
                                    />
                                }
                            >
                                <MenuItem value="">
                                    <em>전체</em>
                                </MenuItem>
                                {classList.map((v,i) => {
                                    return (
                                        <MenuItem key={"c_"+i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" className={classes.selectStandardSub}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(2, ref);
                                }}
                                htmlFor="standard-select-sub"
                            >
                                학생
                            </InputLabel>
                            <Select
                                value={studentId}
                                onChange={this.handleChange('studentId')}
                                input={
                                    <OutlinedInput
                                        labelWidth={studentLabelWidth}
                                        name="selectedStandardSub"
                                        id="standard-select-sub"
                                    />
                                }
                            >
                                <MenuItem value="">
                                    <em>선택</em>
                                </MenuItem>
                                {studentList.map((v,i) => {
                                    return (
                                        <MenuItem key={"s_"+i} value={classId == 0 ? v.re_id : v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </div>
                    <div className={classes.subInputBoxRight}>
                        <TextField
                            id="date-picker1"
                            label="부터"
                            type="date"
                            value={fromDate}
                            className={classes.selectDate}
                            onChange={this.handleDateChange('fromDate')}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true
                            }}
                        />
                        <TextField
                            id="date-picker2"
                            label="까지"
                            type="date"
                            value={toDate}
                            className={classes.selectDate}
                            onChange={this.handleDateChange('toDate')}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true
                            }}
                        />
                        <Fab variant="extended" aria-label="apply" className={classes.applyBtn}
                            onClick={
                                () => this.reloadReached()}>
                            <Cached className={classes.extendedIcon} />
                            {"APPLY"}
                        </Fab>
                    </div>
                </div>
                <Divider />
                <div className={classes.subInputBox2}>
                    <FormControl variant="outlined" className={classes.selectDomain}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(4, ref);
                            }}
                            htmlFor="standard-lto"
                        >
                            도메인
                        </InputLabel>
                        <Select
                            value={ltoId}
                            onChange={this.handleChange('ltoId')}
                            input={
                                <OutlinedInput
                                    labelWidth={ltoLabelWidth}
                                    name="ltoId"
                                    id="standard-lto"
                                />
                            }
                        >
                            <MenuItem value="">
                                <em>전체</em>
                            </MenuItem>
                            {ltoList.map((v,i) => {
                                return (
                                    <MenuItem key={"lto_"+i} value={v.id}>
                                        {v.name}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                </div>
                <div className={classes.contentRoot}>
                    <div className={classes.listArea}>
                        <ReachedSto
                            page={page}
                            perPage={perPage}
                            sort={sort}
                            total={total}
                            data={data}
                            reloadReached={this.reloadReached}
                            handlePage={this.handlePage}
                            handleSort={this.handleSort}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    container: {
        padding: 30
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    formControl: {
        width: 120,
        marginLeft: 15,
        marginRight: 15
    },
    subInputBoxLeft: {
        display: 'flex',
        flexDirection: 'row'
    },
    subInputBoxRight: {
        display: 'flex',
        flexDirection: 'row'
    },
    subInputBox2: {
        display: 'flex',
        flex: 1,
        marginTop: 8
    },
    selectStandard: {
        width: 100,
        marginRight: 8
    },
    selectClass: {
        width: 180,
        marginRight: 8
    },
    selectStandardSub: {
        width: 220,
        marginLeft: 8,
        marginRight: 8
    },
    selectDomain: {
        flex: 1,
        maxWidth: 380,
    },
    selectDate: {
        width: 180,
        marginLeft: 8,
        marginRight: 8
    },
    extendedIcon: {
        marginRight: 8,
    },
    applyBtn: {
        backgroundColor: 'transparent',
        marginLeft: 8,
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    contentRoot: {
        marginTop: 15,
        overflow: 'auto'
    },
    listArea: {
        flex: 1,
    }
});

export default withStyles(styles)(StoList);