import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, ButtonGroup, Button,
    Card, CardActions, CardContent,
    ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { InsertChartOutlined, Refresh, FirstPage } from '@material-ui/icons/';
import { Bread } from '../../components/';
import { Daily } from '../';

class Chart extends Component {

    // handleView = page => {
    //     this.setState({ view: page });
    // }

    render() {
        const { classes } = this.props;
        const page = (<Daily />);

        return (
            <div>
                <Bread title={"CHART"} subTitle={"daily"} />
                {page}
            </div>
        );
    }
}

const styles = theme => ({
    onBtn: {
        
    },
    offBtn: {

    }
});

export default withStyles(styles)(Chart);
