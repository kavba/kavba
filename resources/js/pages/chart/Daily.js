import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import 'date-fns';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, TextField, ButtonGroup, Button, Fab, Popover,
    Card, CardActions, CardContent, FormControl,
    ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core/';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { InsertChartOutlined, FilterNone, Replay, FirstPage, ChevronLeft, ChevronRight, Undo, Add, Remove, LocalParking, MultilineChart, ShowChart, ZoomIn } from '@material-ui/icons/';
import { StoGraphSlide } from '../../pages';

let popInfo = null;

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
@observer
class Daily extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchDate: this.props.funcStore.convertToymd(),
            students: [],
            points: [],
            stoItems: [],
            expanded: "panel_0",
            subExpanded: "spanel_0",
            popOpen: false,
            stoOpen: false,
            stoInfo: {
                name: '', created_at: '', status: '', rows: null, standard_perc: '', standard_cnt: ''
            },
            anchorEl: null,
            slideOpen: false,
            slideStoItem: null,
            slideSto: null,
        }
    }

    componentDidMount() {
        const { searchDate } = this.state;
        this.loadPoints(searchDate);
    }

    loadPoints = date => {
        const { mainStore, funcStore } = this.props;
        const d = (date ? date : this.state.searchDate);
        const param = {
            targetUserId: 0,
            rgstDate: d,
            sort: "desc"
        }
        funcStore.conn('get', '/points/getAllByTarget', false, true, param, (res)=>{
            if(res.result == 'true') {
                let temp1 = {};
                res.students.map((st, idx1) => {
                    if(st) {
                        const p = res.points[st.id] ? res.points[st.id] : [];
                        let temp2 = [];
                        p.map((v,i)=>{
                            const tArr = temp2.filter(c => c.sto_item_id === v.sto_item_id);
                            if(!tArr.length) {
                                temp2.push({
                                    'id' : v.sto_item_id,
                                    'sto_item_id' : v.sto_item_id,
                                    'sto_group_name' : v.sto_group_name,
                                    'lto_name' : v.lto_name,
                                    'target' : v.target,
                                    'user_name' : v.user_name,
                                    'reach_type' : v.reach_type,
                                    'size' : 1
                                }); 
                            } else {
                                tArr[0].size++;
                            }
                        });
                        temp1[st.id] = temp2;
                    }
                });
                this.setState({
                    points: res.points,
                    students: res.students,
                    stoItems: temp1,
                    searchDate: d
                });
                this.forceUpdate();
            } else {
                console.log("포인트차트 로딩 실패");
            }
        });
    }

    handelExpand = panel => (event, isExpanded) => {
        const { students } = this.state;
        const temp = students;
        const index = Number(panel.replace("panel_", ""));
        if(index > 0) {
            const temp2 = temp[index];
            temp.splice(index, 1);
            temp.unshift(temp2);
        }
        this.setState({
            students: temp,
            expanded: (isExpanded ? "panel_0" : false)
        });
    }

    handelSubExpand = spanel => (event, isExpanded) => {
        this.setState({
            subExpanded: (isExpanded ? spanel : false)
        });
    }

    checkStick = (studentId, point, val) => {
        if(!studentId) return;
        if(!point.id) return;
        const { points } = this.state;
        const param = {
            val: val
        }
        const res = this.props.funcStore.conn('post', '/stick/'+point.id+'/check', false, false, param, null);
        if(res && res.result == 'true') {
            const t = [res.inserted];
            this.setState({
                points: {
                    ...points,
                    [studentId]: points[studentId].map(p => p.id === point.id ? {
                        ...p,
                        reach: res.point.reach,
                        status: res.point.status,
                        stick: p.stick.concat(t)
                    } : p)
                }
            });
        } else {
            console.log("포인트차트 로딩 실패");
            if(res && res.reload) {
                this.loadPoints(this.state.searchDate);
            }
        }
        // 비동기 > 동기
        // this.props.funcStore.conn('post', '/stick/'+point.id+'/check', false, true, param, (res)=>{
        //     if(res.result == 'true') {
        //         const t = [res.inserted];
        //         this.setState({
        //             points: {
        //                 ...points,
        //                 [studentId]: points[studentId].map(p => p.id === point.id ? {...p, stick: p.stick.concat(t)} : p)
        //             }
        //         });
        //     } else {
        //         console.log("포인트차트 로딩 실패");
        //     }
        // });
    }

    editStick = val => e => {
        const { points } = this.state;
        const p = points[popInfo.studentId].filter(p => p.id === popInfo.point.id);
        if(p[0]) {
            const s = p[0].stick[popInfo.idx];
            if(s.value != val) {
                // update
                const param = {
                    val: val
                };
                const res = this.props.funcStore.conn('post', '/stick/'+s.id+'/put', false, false, param, null);
                if(res.result == 'true') {
                    this.setState({
                        points: {
                            ...points,
                            [popInfo.studentId]: points[popInfo.studentId].map(p => p.id === popInfo.point.id ? {
                                ...p,
                                stick: p.stick.map((v,i)=> i == popInfo.idx ? {
                                    ...v, value: val
                                } : v)
                            } : p)
                        },
                    });
                    this.forceUpdate();
                } else {
                    console.log("포인트 스틱 취소 실패");
                }
            }
        }
        this.handlePopoverClose();
        return;
    }

    insertStick = position => e => {
        // insert into left
        const { points } = this.state;
        const p = points[popInfo.studentId].filter(p => p.id === popInfo.point.id);
        if(p[0] && p[0].stick.length < p[0].count) {
            const s = p[0].stick[popInfo.idx];
            const param = {
                position: position,
                idx: popInfo.idx
            };
            const res = this.props.funcStore.conn('post', '/stick/'+s.id+'/insert', false, false, param, null);
            if(res.result == 'true') {
                this.setState({
                    points: {
                        ...points,
                        [popInfo.studentId]: points[popInfo.studentId].map(p => p.id === popInfo.point.id ? {
                            ...p,
                            stick: res.sticks
                        } : p)
                    },
                });
                this.forceUpdate();
            } else {
                console.log("스틱 끼워넣기 실패");
            }
        }
        this.handlePopoverClose();
        return;
    }

    removeStick = () => {
	    const { points } = this.state;
        const p = points[popInfo.studentId].filter(p => p.id === popInfo.point.id);
        if(p[0]) {
	        const s = p[0].stick[popInfo.idx];
            const res = this.props.funcStore.conn('post', '/stick/'+s.id+'/delete', false, false, null, null);
            if(res.result == 'true') {
                this.setState({
                    points: {
                        ...points,
                        [popInfo.studentId]: points[popInfo.studentId].map(p => p.id === popInfo.point.id ? {
                            ...p,
                            stick: res.sticks
                        } : p)
                    },
                });
                this.forceUpdate();
            } else {
                console.log("스틱 지우기 실패");
            }
	    }
	    this.handlePopoverClose();
        return;
    }

    cancelStick = (studentId, point) => {
        if(!studentId || !point.id) return;
        const { points } = this.state;
        const res = this.props.funcStore.conn('post', '/stick/'+point.id+'/deleteLast', false, false, null, null);
        if(res.result == 'true') {
            this.setState({
                points: {
                    ...points,
                    [studentId]: points[studentId].map(p => p.id === point.id ? {...p, stick: p.stick.slice(0,-1), reach: res.reach} : p)
                }
            });
        } else {
            console.log("포인트 스틱 취소 실패");
        }
    }

    resetStick = (studentId, point) => {
        if(!confirm("모든 체크를 삭제하시겠습니까?")) return;
        if(!studentId || !point.id) return;
        const { points } = this.state;
        const res = this.props.funcStore.conn('post', '/stick/'+point.id+'/reset', false, false, null, null);
        if(res.result == 'true') {
            this.setState({
                points: {
                    ...points,
                    [studentId]: points[studentId].map(p => p.id === point.id ? {...p, stick:[], reach:0, } : p)
                }
            });
        } else {
            console.log("포인트 스틱 리셋 실패");
        }
    }
    
    handleDateChange = date => {
	    const d = date.target ? date.target.value : date;
        this.loadPoints(d);
    }
    
    handleOneDate = k => {
	    const { searchDate } = this.state;
	    let d = new Date(searchDate);
	    if(k) {
		    d.setDate(d.getDate() - 1);
	    } else {
		    d.setDate(d.getDate() + 1);
	    }
	    this.handleDateChange(this.props.funcStore.convertToymd(d));
    }

    chartStick = sto => {
        this.setState({
            slideOpen: true,
            slideStoItem: sto,
            slideSto: '',
        });
    }

    chartSto = sto => {
        this.setState({
            slideOpen: true,
            slideStoItem: '',
            slideSto: sto,
        });
    }

    changeStatus = (studentId, point, status) => {
        if(!studentId || !point.id) return;
        const { points, stoItems } = this.state;
        const param = {
            status: status
        }
        const res = this.props.funcStore.conn('post', '/point/'+point.id+'/status', false, false, param, null);
        if(res.result == 'true') {
            if(res.total_reach == 1) {
                if(status == 1) alert("STO가 준거 도달 하였습니다");
                //else alert("준거 도달한 STO입니다");
            }
            const temp = points[studentId].map(p => {
                if(p.id === point.id) {
                    if(status == 0 && res.deleted) {
                        return {
                            ...p, status:status, stick: p.stick.filter(s => s.id != res.deleted)
                        };
                    } else {
                        return {
                            ...p, status:status
                        };
                    }
                } else {
                    return p;
                }
            });
            this.setState({
                points: {
                    ...points,
                    [studentId]: temp
                },
            });
            this.forceUpdate();
        } else {
            console.log("포인트 완료 실패");
        }
    }

    setReach = (studentId, point, r) => {
        if(!point.id) return;
        const { points, stoItems } = this.state;
        const param = {
            reach: r
        }
        const res = this.props.funcStore.conn('post', '/point/'+point.id+'/reach', false, false, param, null);
        if(res.result == 'true') {
            if(res.total_reach == 1) {
                if(r == 1) alert("STO가 준거 도달 하였습니다");
            }
            const temp = points[studentId].map(p => {
                if(p.id === point.id) {
                    return {
                        ...p, status: (res.status != '' ? res.status : p.status), reach: res.reach
                    };
                } else {
                    return p;
                }
            });
            this.setState({
                points: {
                    ...points,
                    [studentId]: temp
                },
            });
            this.forceUpdate();
        } else {
            console.log("포인트 도달 수정 실패");
        }
    }
    
    handlePopoverOpen = (studentId, point, idx) => e => {
        if(idx >= point.stick.length || point.status == 1) return;
        popInfo = {
            studentId: studentId,
            point: point,
            idx: idx,
        };
	    this.setState({
            popOpen: true,
		    anchorEl: e.currentTarget
	    });
    }
    
    handlePopoverClose = e => {
	    this.setState({
            popOpen: false,
            stoOpen: false,
		    anchorEl: null
	    });
    }

    handleInfoOpen = sto => e => {
        if(!sto && !sto.sto_item_id) return;
        this.setState({
            stoOpen: true,
            anchorEl: e.currentTarget
        });
        this.props.funcStore.conn('get', '/stoItem/'+sto.sto_item_id+'/get', false, true, null, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    stoInfo: res.stoItem
                });
                //this.forceUpdate();
            } else {
                console.log("STO정보 로딩 실패");
            }
        });
    }

    handleSlide = flag => {
	    this.setState({
		    slideOpen: flag
	    });
    }

    restartPoint = (studentId, point) => {
        if(!confirm("지시를 복사하시겠습니까?")) return;
        const { points } = this.state;
        const sto = this.props.funcStore.conn('get', '/stoItem/'+point.sto_item_id+'/completed', false, false, null, null);
        let flag = 0;
        if(sto.stoGroup.type == 0) {
            flag = sto.stoItem.finish;
            if(flag == 1) {
                alert("완료된 STO 입니다"); return;
            } else if(flag == 2) {
                alert("중지된 STO 입니다"); return;
            } else if(flag == -1) {
                alert("취소된 STO 입니다"); return;
            }
        } else {
            flag = sto.stoItem.total_reach;
            if(flag == 1) {
                alert("도달한 STO 입니다"); return;
            }
        }
        
        // const res = this.props.funcStore.conn('get', '/stoItem/'+point.sto_item_id+'/getDecision', false, false, null, null);
        // if(res == 1) {
        //     if(!confirm("디시전 값이 존재합니다. 추가하시겠습니까?")) return;
        // }
        const res = this.props.funcStore.conn('get', '/stoItem/'+point.sto_item_id+'/getLastDecision', false, false, null, null);
        if(res && res.result == true) {
            if(!confirm("마지막 포인트에서 디시전이 발생하였습니다.\n이어서 새로운 포인트를 추가하시겠습니까?")) return;
        }
        this.props.funcStore.conn('post', '/point/'+point.id+'/restart', false, true, null, (res)=>{
            if(res.result == 'true') {
                let newPoint = null;
                const idx = points[studentId].indexOf(point);
                let du = points[studentId][idx];
                if(du) {
                    newPoint = {
                        ...du,
                        id: res.inserted.id,
                        decision: 0, //res.inserted.desicion - 계산된 디시젼으로
                        reach: 0,
                        status: 0,
                        stick: [],
                    }
                    this.setState({
                        points: {
                            ...points,
                            [studentId]: [
                                ...points[studentId].slice(0,idx),
                                newPoint,
                                ...points[studentId].slice(idx)
                            ]
                        }
                    });
                }
            } else {
                console.log("포인트 재시작 실패");
            }
        });    
    }

    render() {
        const { classes } = this.props;
        const { searchDate, points, students, stoItems, expanded, subExpanded, anchorEl, popOpen, stoOpen, stoInfo, slideOpen, slideStoItem, slideSto } = this.state;
        const id = popOpen ? 'btn-popover' : undefined;
        const stoId = stoOpen ? 'info-popover' : undefined;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        return (
            <div className={classes.container}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputRightBox}>
                        <TextField
                            id="date-picker"
                            label={lang['stoitem5']}
                            type="date"
                            value={searchDate}
                            className={classNames(classes.textField, classes.dialogInput)}
                            onChange={this.handleDateChange}
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true
                            }}
                        />
                        <Fab size="small" aria-label="prev" className={classNames(classes.margin, classes.calBtn)}
                            onClick={(e)=>{
                                e.preventDefault();
                                this.handleOneDate(true);
                            }}>
                            <ChevronLeft />
                        </Fab>
                        <Fab size="small" aria-label="next" className={classNames(classes.margin, classes.calBtn)}
                            onClick={(e)=>{
                                e.preventDefault();
                                this.handleOneDate(false);
                            }}>
                            <ChevronRight />
                        </Fab>
                    </div>
                </div>
                <div className={classes.contentRoot}>
                    {students.map((st, idx1) => {
                        if(st) {
                            const s = stoItems[st.id] ? stoItems[st.id] : [];
                            const p = points[st.id] ? points[st.id] : [];
                            return (
                                <ExpansionPanel key={idx1}
                                    expanded={expanded === ('panel_' + idx1)} onChange={this.handelExpand('panel_' + idx1)}
                                    classes={{ root: classes.penelRoot }}
                                >
                                    <ExpansionPanelSummary
                                        expandIcon={<ExpandMoreIcon className={classes.penelArrowIcon} />}
                                        aria-controls="panel1a-content"
                                        id={"panel-" + idx1 + "-header"}
                                        classes={{ content: classes.penelHeaderContent }}
                                    >
                                        <Typography className={classes.heading}>
                                            {st.name +" / "+ st.birth_date + " ["+this.props.funcStore.calcMonths(st.birth_date)+"]"}
                                        </Typography>
                                        <div className={classes.penelProgramCnt}>{"PROGRAMS : " + s.length}</div>
                                        <div className={classes.penelProgramCnt}>{"POINTS : " + p.length}</div>
                                    </ExpansionPanelSummary>
                                    <ExpansionPanelDetails className={classes.panelDetail}>
                                        {s.map((sto, idx3) => {
                                            const realPoint = p.filter(v => v.sto_item_id === sto.sto_item_id);
                                            const fullName = sto.target ? (
                                                sto.lto_name +" > "+ sto.sto_group_name +" > "+ sto.target.map((t,i) => {
                                                    return (i>0 ? " / " : "") + t.text;
                                                })
                                            ) : "";
                                            const targetName = sto.target ? (
                                                sto.target.map((t,i) => {
                                                    return (i>0 ? " / " : "") + t.text;
                                                })
                                            ) : "";
                                            return (
                                                <ExpansionPanel key={'sub_'+idx3}
                                                    classes={{ root: sto.reach_type == 0 ? classes.stoRoot : classes.stoStackRoot }}
                                                >
                                                    <ExpansionPanelSummary
                                                        expandIcon={<ExpandMoreIcon className={classes.penelArrowIcon} />}
                                                        aria-controls="panel1b-content"
                                                        id={"spanel-" + idx3 + "-header"}
                                                        classes={{ content: classes.spenelHeaderContent }}
                                                    >
                                                        <Typography className={classes.stoTitle}>
                                                            {fullName}
                                                        </Typography>
                                                        <FormControl
                                                            onClick={event => event.stopPropagation()}
                                                            onFocus={event => event.stopPropagation()}
                                                        >
                                                            <div className={classes.chartBox}>
                                                                <Button
                                                                    variant="outlined" className={classes.stoChartBtn}
                                                                    onClick={this.handleInfoOpen(sto)}
                                                                ><ZoomIn /></Button>
                                                                {sto.reach_type == 0 && (
                                                                    <Button
                                                                        variant="outlined" className={classes.stoChartBtn}
                                                                        onClick={(e)=>{
                                                                            e.preventDefault();
                                                                            this.chartStick(sto)
                                                                        }}
                                                                    ><ShowChart /></Button>
                                                                )}
                                                                <Button
                                                                    variant="outlined" className={classes.stoChartBtn}
                                                                    onClick={(e)=>{
                                                                        e.preventDefault();
                                                                        this.chartSto(sto)
                                                                    }}
                                                                ><MultilineChart /></Button>
                                                            </div>
                                                        </FormControl>
                                                    </ExpansionPanelSummary>
                                                    <ExpansionPanelDetails className={classes.spanelDetail}>
                                                    
                                                    {realPoint.map((point, idx2) => {
                                                        // <Fab size="small"
                                                        //     aria-label="details"
                                                        //     className={classNames(classes.margin, classes.zoomBtn)}
                                                        //     aria-describedby={stoId}
                                                        //     onClick={this.handleInfoOpen(point)}
                                                        // >
                                                        //     <ZoomIn />
                                                        // </Fab>
                                                        return (
                                                            <Card key={st.id + "_" + idx2} className={classes.card}>
                                                                <CardContent className={classNames(classes.cardContent,
                                                                    (point.count == point.stick.length ? classes.finishedCard : ''))}>
                                                                    <div className={classes.cardPointStatus}>
                                                                        <div className={classes.cardContentTitle}>
                                                                            <span className={classes.titleNumber}>
                                                                                {"#" + (idx2 + 1) +". "+ targetName}
                                                                            </span>
                                                                            <span className={classes.titleCount}>
                                                                                {point.stick.length + " / " + point.count}
                                                                            </span>
                                                                        </div>
                                                                        <ButtonGroup variant="outlined" color="primary"
                                                                            aria-label="Sub"
                                                                        >
                                                                            <Button
                                                                                className={classNames(classes.controlBtn1, (point.reach > 0 ? classes.controlBtn1On : ""))}
                                                                                onClick={(e)=>{
                                                                                    e.preventDefault();
                                                                                    if(point.reach > 0) {
                                                                                        this.setReach(st.id, point, 0);
                                                                                    } else {
                                                                                        this.setReach(st.id, point, 1);
                                                                                    }
                                                                                }}>{lang['stoitem13']}
                                                                            </Button>
                                                                            <Button
                                                                                className={classNames(classes.controlBtn1, (point.status>0 ? classes.controlBtn1On : ""))}
                                                                                onClick={(e)=>{
                                                                                e.preventDefault();
                                                                                if(point.status > 0) {
                                                                                    this.changeStatus(st.id, point, 0);
                                                                                } else {
                                                                                    if(!confirm("포인트를 종료 하시겠습니까?")) return;
                                                                                    if(point.reach_type == 0) 
                                                                                        this.changeStatus(st.id, point, 1);
                                                                                    else
                                                                                        this.changeStatus(st.id, point, 2);
                                                                                }
                                                                            }}>{point.status > 0 ? lang['stoitem16'] : lang['stoitem15']}</Button>
                                                                        </ButtonGroup>
                                                                    </div>
                                                                    <div className={classes.cardPointContent}>
                                                                        <div className={classes.cardContentLeft}>
                                                                            <Typography variant="h4" className={classes.title}>
                                                                                {"["+lang['stoitem7']+"] " + point.user_name}
                                                                            </Typography>
                                                                            <Typography variant="h4" className={classes.title}>
                                                                                {"["+lang['stoitem2']+"] " + (point.reach_type == 0 ? point.standard_perc+"%, " : "") + point.standard_cnt + "번"}
                                                                            </Typography>
                                                                            <Typography variant="h4" className={classes.title}>
                                                                                {"["+lang['daily5']+"] " + (point.prompt ? point.prompt + ", " : "-") + (point.prompt_memo ? point.prompt_memo : "")}
                                                                            </Typography>
                                                                            <Typography variant="h4" className={classes.title}>
                                                                                {"["+lang['daily13']+"] " + (point.schedule_memo ? point.schedule_memo : "-")}
                                                                            </Typography>
                                                                            <Typography variant="h4" className={classes.title}>
                                                                                {"["+lang['daily14']+"] " + (point.program_memo ? point.program_memo : "-")}
                                                                            </Typography>
                                                                        </div>
                                                                        <div className={classes.cardContentRight}>
                                                                            <div className={classes.countBox}>
                                                                                {[...Array(point.count)].map((x, i) => {
                                                                                    let cls = null;
                                                                                    let vl = "";
                                                                                    if(!point.stick[i]) {
                                                                                        cls = classNames(classes.countArea, classes.countNull);
                                                                                    } else {
                                                                                        if(point.stick[i].value == 0) {
                                                                                            cls = classNames(classes.countArea, classes.count0);
                                                                                            vl = <Remove />;
                                                                                        } else if(point.stick[i].value == 1) {
                                                                                            cls = classNames(classes.countArea, classes.count1);
                                                                                            vl = <Add />;
                                                                                        } else if(point.stick[i].value == 2) {
                                                                                            cls = classNames(classes.countArea, classes.count2);
                                                                                            vl = <LocalParking />;
                                                                                        }
                                                                                    }
                                                                                    return (
                                                                                        <Button
                                                                                            size="small"
                                                                                            variant="contained"
                                                                                            className={cls}
                                                                                            key={"p_" + idx2 + "_" +i}
                                                                                            onClick={this.handlePopoverOpen(st.id, point, i)}
                                                                                        >{vl}</Button>
                                                                                    );

                                                                                })}
                                                                            </div>
                                                                            <div className={classes.pointBtnBox}>
                                                                                <ButtonGroup variant="outlined" color="primary"
                                                                                    aria-label="Sub"
                                                                                    disabled={point.status ? true : false}
                                                                                    classes={{
                                                                                        disabled: classes.disabledColor
                                                                                    }}
                                                                                >
                                                                                    <Button
                                                                                        className={classNames(classes.controlBtn, classes.con1)}
                                                                                        onClick={(e)=>{
                                                                                        e.preventDefault();
                                                                                        this.checkStick(st.id, point, 1)
                                                                                    }}><Add /></Button>
                                                                                    <Button
                                                                                        className={classNames(classes.controlBtn, classes.con0)}
                                                                                        onClick={(e)=>{
                                                                                        e.preventDefault();
                                                                                        this.checkStick(st.id, point, 0)
                                                                                    }}><Remove /></Button>
                                                                                    <Button
                                                                                        className={classNames(classes.controlBtn, classes.con2)}
                                                                                        onClick={(e)=>{
                                                                                        e.preventDefault();
                                                                                        this.checkStick(st.id, point, 2)
                                                                                    }}><LocalParking /></Button>
                                                                                </ButtonGroup>
                                                                                <ButtonGroup variant="outlined" color="primary" aria-label="Sub">
                                                                                    {point.reach_type == 0 ? (
                                                                                        <Button
                                                                                            className={classNames(classes.controlBtnSub, classes.conSub0)}
                                                                                            onClick={()=>{this.restartPoint(st.id, point)}}
                                                                                            disabled={point.status ? false : true}
                                                                                            classes={{
                                                                                                disabled: classes.disabledColor
                                                                                            }}
                                                                                        ><FilterNone /></Button>
                                                                                    ) : (
                                                                                        <Button
                                                                                            className={classNames(classes.controlBtnSub, classes.conSub0)}
                                                                                            onClick={()=>{this.restartPoint(st.id, point)}}
                                                                                        ><FilterNone /></Button>
                                                                                    )}
                                                                                    <Button
                                                                                        className={classNames(classes.controlBtnSub, classes.conSub1)}
                                                                                        disabled={point.status ? true : false}
                                                                                        classes={{
                                                                                            disabled: classes.disabledColor
                                                                                        }}
                                                                                        onClick={(e)=>{
                                                                                            e.preventDefault();
                                                                                            this.resetStick(st.id, point)
                                                                                        }}
                                                                                    ><Replay /></Button>
                                                                                    <Button
                                                                                        className={classNames(classes.controlBtnSub, classes.conSub2)}
                                                                                        disabled={point.status ? true : false}
                                                                                        classes={{
                                                                                            disabled: classes.disabledColor
                                                                                        }}
                                                                                        onClick={(e)=>{
                                                                                            e.preventDefault();
                                                                                            this.cancelStick(st.id, point)
                                                                                        }}
                                                                                    ><Undo /></Button>
                                                                                </ButtonGroup>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </CardContent>
                                                            </Card>
                                                        );
                                                    })}
                                                    </ExpansionPanelDetails>
                                                </ExpansionPanel>
                                            );
                                        })}
                                        
                                    </ExpansionPanelDetails>
                                </ExpansionPanel>
                            );
                        }
                    })}
                </div>
                <Popover
			        id={id}
			        open={popOpen}
			        anchorEl={anchorEl}
			        onClose={this.handlePopoverClose}
			        anchorOrigin={{
				        vertical: 'top',
				        horizontal: 'center',
			        }}
			        transformOrigin={{
				        vertical: 'bottom',
				        horizontal: 'center',
			        }}
			        classes={{
				        root: classes.popRoot,
				        paper: classes.popRoot 
			        }}
			    >
			        <div className={classes.popRow}>
			        	<div className={classes.popGrid} onClick={this.editStick(1)}>{"+"}</div>
			        	<div className={classes.popGrid} onClick={this.editStick(0)}>{"-"}</div>
			        	<div className={classes.popGrid} onClick={this.editStick(2)}>{"P"}</div>
			        </div>
			        <div className={classes.popRow}>
			        	<div className={classes.popGrid} onClick={this.insertStick(true)}>{"<"}</div>
			        	<div className={classes.popGrid} onClick={this.insertStick(false)}>{">"}</div>
			        	<div className={classes.popGrid} onClick={()=>this.removeStick()}>{"X"}</div>
			        </div>
			        <div className={classes.popSub}></div>
			    </Popover>
                <Popover
			        id={stoId}
			        open={stoOpen}
			        anchorEl={anchorEl}
			        onClose={this.handlePopoverClose}
			        anchorOrigin={{
				        vertical: 'top',
				        horizontal: 'left',
			        }}
			        transformOrigin={{
				        vertical: 'top',
				        horizontal: 'right',
                    }}
                    classes={{
                        paper: classes.popoverPaper
                    }}
			    >
                    <div className={classes.stoInfoRoot}>
                        <div>{lang['daily1'] + " : " + stoInfo.sto_group_name}</div>
                        <div>{lang['daily2'] + " : " + stoInfo.user_name}</div>
                        <div>{lang['daily3'] + " : " + stoInfo.created_at}</div>
                        <div>{lang['daily4'] + " : " + (stoInfo.finish == 0 ? lang['stoitem15'] : (stoInfo.status == 1 ? lang['stoitem18'] : (stoInfo.status == 2 ? lang['stoitem17'] : lang['stoitem19'])))}</div>
                        <div>{lang['daily5'] + " : " + (stoInfo.prompt ? stoInfo.prompt : "")}</div>
                        <div>{lang['daily6'] + " : " + (stoInfo.prompt_memo ? stoInfo.prompt_memo : "")}</div>
                        <div>{lang['daily7'] + " : " + (stoInfo.schedule_memo ? stoInfo.schedule_memo : "")}</div>
                        <div>{lang['daily8'] + " : " + (stoInfo.program_memo ? stoInfo.program_memo : "")}</div>
                        <div>{lang['daily9'] + " : " + stoInfo.reach_type}</div>
                        {stoInfo.type == 0 ? (
                            <div>{lang['daily10'] + " : " + stoInfo.standard_perc + "% / "+lang['daily10'] + +" : " + stoInfo.standard_cnt}</div>
                        ) : (
                            <div>{lang['daily10'] + " : " + stoInfo.standard_cnt}</div>
                        )}
                        {stoInfo.target && (
                            <div>
                                {lang['daily11'] + " : " + stoInfo.target.map((t,i) => {
                                    return (i>0 ? " / " : "") + t.text;
                                })}
                            </div>
                        )}
                        <div>{lang['daily12'] + " : " + stoInfo.startDate}</div>
                        <div>{stoInfo.rows && stoInfo.rows.map((r,i) => {
                            return r.label + " : " + r.text;
                        })}</div>
                    </div>
			    </Popover>
                <StoGraphSlide
                    open={slideOpen}
                    stoItem={slideStoItem}
                    sto={slideSto}
                	handleSlide={this.handleSlide}
                />
            </div>
        );
    }
}

const styles = theme => ({
    container: {
        padding: 30
    },
    contentRoot: {
        marginTop: 15
    },
    margin: {
        margin: theme.spacing(1),
    },
    card: {
        minWidth: 275,
        margin: '15px'
    },
    stoRoot: {
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
        marginBottom: 15,
        borderRadius: 5,
    },
    stoStackRoot: {
        backgroundImage: 'linear-gradient(to right, #ca5388 0%,#f55f5c 65%,#f68a15 100%)',
        marginBottom: 15,
        borderRadius: 5,
    },
    stoTop: {
        display: 'flex',
        marginTop: 10,
        marginLeft: 15
    },
    stoTitle: {
        display: 'flex',
        alignItems: 'center',
        color: '#fff',
        lineHeight: '1.5'
    },
    stoChartBtn: {
        borderColor: '#fff',
        marginTop: 5,
        marginRight: 15,
        '& > span': {
            color: '#fff'
        }
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontFamily: 'JungNormal',
        fontSize: 13,
        color: '#232323',
        whiteSpace: 'pre-line',
        lineHeight: '1rem',
        textIndent: -18,
        paddingLeft: 24,
        marginBottom: 8
    },
    titleCount: {
    },
    titleNumber: {
    },
    cardContentTitle: {
        '& span': {
            fontFamily: 'JungBold!important',
            fontSize: 16,
        },
        flex: 1,
        padding: '5px 15px',
        background: '#485680',
        color: '#fff',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 18,
        marginBottom: 2,
    },
    cardContentTitleBottom: {
        '& span': {
            fontFamily: 'JungBold!important',
            fontSize: 13,
        },
        padding: '5px 15px',
        background: '#485680',
        color: '#fff',
        borderRadius: 5,
        display: 'flex',
        justifyContent: 'flex-start'
    },
    heading: {
        fontFamily: 'JungNormal',
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
    panelDetail: {
        flexDirection: 'column'
    },
    spanelDetail: {
        flexDirection: 'column',
        padding: 8
    },
    cardContent: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        paddingBottom: '16px!important'
    },
    finishedCard: {
        backgroundColor: '#FFF8F1'
    },
    delayedCard: {
        backgroundColor: 'silver'
    },
    cardContentLeft: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        background: '#ddd',
        borderRadius: 5,
        marginRight: 5,
        overflowX: 'hidden',
        overflowY: 'auto',
        maxHeight: 104,
        wordBreak: 'break-all',
        padding: 8
    },
    cardContentLeftSub1: {
        display: 'flex',
        flex: 1,
        wordBreak: 'break-all',
        borderRight: '1px solid #fff'
    },
    cardContentLeftSub2: {
        display: 'flex',
        flex: 2,
        wordBreak: 'break-word'
    },
    cardContentRight: {
        flexWrap: 'wrap',
        display: 'flex',
        alignItems: 'flex-start',
        flexDirection: 'row'
    },
    countBox: {
        width: 230,
        flexWrap: 'wrap',
        paddingLeft: 10,
        display: 'flex',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
    },
    countArea: {
        width: 40,
        minWidth: 40,
        height: 34,
        lineHeight: '30px',
        fontSize: 14,
        //borderRight: '1px solid #bbb',
        textAlign: 'center',
        borderRadius: 5,
        padding: '2px 2px',
        marginLeft: 2,
        marginBottom: 2,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        boxShadow: 'none',
        '&:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
        '& svg': {
            fontSize: '1.2rem'
        }
    },
    countNull: {
        background: '#ddd',
        border:'1px solid #ddd',
        '&:hover': {
            backgroundColor: '#ddd',
            borderColor: '#ddd',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#ddd',
            borderColor: '#ddd',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
    count0: {
        background: '#CA5388',
        border:'1px solid #CA5388',
        color : '#fff',
        border:'1px solid #CA5388',
        '&:hover': {
            backgroundColor: '#e3629b',
            color: '#fff',
            borderColor: '#e3629b',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#e3629b',
            borderColor: '#e3629b',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
    count1: {
        background: '#F68A15',
        border:'1px solid #F68A15',
        color : '#fff',
        '&:hover': {
            backgroundColor: '#ffa23d',
            color: '#fff',
            borderColor: '#ffa23d',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#ffa23d',
            borderColor: '#ffa23d',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
    count2: {
        background: '#865995',
        border:'1px solid #865995',
        color : '#fff',
        '&:hover': {
            backgroundColor: '#9568a3',
            color: '#fff',
            borderColor: '#9568a3',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#9568a3',
            borderColor: '#9568a3',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
    pointBtnBox: {
        height: '100%',
        marginLeft: 10,
        flexDirection: 'column',
        display: 'flex',
        justifyContent: 'flex-start',
        '& button:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
    },
    cardPointStatus: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between'
    },
    cardPointContent: {
        flex: 1,
        display: 'flex',
    },
    controlBtn: {
        width: 82,
        height: 70,
        marginBottom: 2
    },
    controlBtn1: {
        width: 121,
        height: 40,
        marginBottom: 2,
        background: '#fff',
        border:'1px solid #485680!important',
        color : '#485680',
        fontFamily: 'JungBold!important',
        '&:hover': {
            backgroundColor: '#5d6b94',
            color: '#fff',
            borderColor: '#5d6b94',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#5d6b94',
            borderColor: '#5d6b94',
        },
        '&:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
        '&:not(:first-child)': {
            marginLeft: 2,
        }
    },
    controlBtn1On: {
        background: '#485680',
        color : '#fff',
    },
    con1: {
        background: '#fff',
        border:'1px solid #F68A15',
        color : '#F68A15',
        '&:hover': {
            backgroundColor: '#F68A15',
            color: '#fff',
            borderColor: '#F68A15',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#F68A15',
            borderColor: '#F68A15',
        },
        '&:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
    },
    con0: {
        background: '#fff',
        color : '#CA5388',
        border:'1px solid #CA5388',
        '&:hover': {
            backgroundColor: '#CA5388',
            color: '#fff',
            borderColor: '#CA5388',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#CA5388',
            borderColor: '#CA5388',
        },
        '&:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
    },
    con2: {
        background: '#fff',
        border:'1px solid #865995',
        color : '#865995',
        '&:hover': {
            backgroundColor: '#865995',
            color: '#fff',
            borderColor: '#865995',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#865995',
            borderColor: '#865995',
        },
        '&:focus': {
            outline: 'none!important',
            boxShadow: 'none!important'
        },
    },
    controlBtnSub: {
        flex: 1,
        height: 34,
        color: '#fff',
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
        '& svg': {
            fontSize: '1.2rem'
        }
    },
    conSub0: {
        background: '#485680',
        '&:hover': {
            backgroundColor: '#5d6b94',
            color: '#fff',
            borderColor: '#5d6b94',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#5d6b94',
            borderColor: '#5d6b94',
        },
    },
    conSub1: {
        background: '#53618a',
        '&:hover': {
            backgroundColor: '#5d6b94',
            color: '#fff',
            borderColor: '#5d6b94',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#5d6b94',
            borderColor: '#5d6b94',
        },
    },
    conSub2: {
        background: '#485680',
        '&:hover': {
            backgroundColor: '#5d6b94',
            color: '#fff',
            borderColor: '#5d6b94',
            boxShadow: 'none',
        },
        '&:active': {
            boxShadow: 'none',
            color: '#fff',
            backgroundColor: '#5d6b94',
            borderColor: '#5d6b94',
        },
    },
    popover: {
	    pointerEvents: 'none',
	},
	popRoot: {
		display: 'flex',
		flexDirection: 'column',
		overflow: 'visible',
		borderRadius: '7px'
	},
	popRow: {
		flex: 1,
		display: 'flex',
		background: '#444',
		overflow: 'hidden',
		'&:not(:first-child)': {
			borderTop: '1px solid #fff'
		},
		'&:first-child': {
			borderTopLeftRadius: '7px',
			borderTopRightRadius: '7px',
		},
		'&:nth-child(2)': {
			borderBottomLeftRadius: '7px',
			borderBottomRightRadius: '7px',
		}
	},
	popGrid: {
		cursor: 'pointer',
		display: 'flex',
		width: 30,
		height: 30,
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: 14,
		color: '#fff',
		'&:not(:first-child)': {
			borderLeft: '1px solid #fff'
		},
		'&:hover': {
			background: '#000'
		}
	},
	popSub: {
		borderTop: '10px solid #444',
		borderBottom: 'none',
		borderLeft: '10px solid transparent',
		borderRight: '10px solid transparent',
		width: 0,
		height: 0,
		position: 'absolute',
		bottom: '-10px',
		alignSelf: 'center'
    },
    stoInfoRoot: {
        width: 420,
        height: 'auto',
        margin: 10
    },
    calBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        }
    },
    dialogInput: {
        marginRight: 8
    },
    textField: {
        '& input': {
            fontFamily: "JungBold"
        }
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-end'
    },
    subInputRightBox: {

    },
    penelRoot: {
        backgroundColor: '#333',
        color: '#fff'
    },
    penelArrowIcon: {
        color: '#fff'
    },
    penelProgramCnt: {
        backgroundColor: '#fff',
        color: '#232323',
        padding: '4px 10px 2px',
        marginLeft: 15,
        borderRadius: 5,
        fontFamily: 'JungNormal'
    },
    penelPointCnt: {

    },
    penelHeaderContent: {
        alignItems: 'center'
    },
    spenelHeaderContent: {
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    chartBox: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
    },
    popoverPaper: {
        backgroundColor: '#eee'
    },
    disabledColor: {
        color: '#aaa!important'
    }
});

export default withStyles(styles)(Daily);