import React from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ReferenceLine
  } from 'recharts';
import { Button, Fab, Dialog, ListItem, List, Divider, AppBar, Toolbar, IconButton, OutlinedInput,
    Typography, Slide, Grid, InputLabel, MenuItem, FormControl, Select, TextField, Popover,
    FormControlLabel, Checkbox } from '@material-ui/core';
import { Close, Cached, SaveAlt } from '@material-ui/icons';

import ExcelJS from 'exceljs';
import { saveAs } from 'file-saver';

const MYSIZE = 50;
const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

class CustomizedAxisTick extends React.Component {
    render() {
        const { x, y, stroke, payload, index, data, flag } = this.props;
        let label = "";
        const r = payload.value ? payload.value : "";
        if(flag) {
            const d = data.filter(v => v.id === r);
            label = (d.length ? d[0].date : "");
        } else {
            const d = data.filter(v => v.name === r);
            label = (d.length ? d[0].date : "");
        }
        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} textAnchor="end" fill="#666" fontSize="11" transform="rotate(-35)">{label}</text>
            </g>
        );
    }
}

let stackTemp = [0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38,40];
// for(let i=0;i<=40;i+2) {
//     stackTemp.push(i);
// }
const GraphTooltip = ({ active, payload, label, condition, flag }) => {
    if (active && payload && payload[0]) {
        let text1 = "";
        let text2 = "";
        const tmp = payload[0].payload;
        if(flag == 1) {
            text1 = condition=='perc' ? tmp.po + "% (" + tmp.po2 + "번)" : tmp.po2 + "번 (" + tmp.po + "%)";
            text2 = condition=='perc' ? tmp.pa + "% (" + tmp.pa2 + "번)" : tmp.pa2 + "번 (" + tmp.pa + "%)";
        } else {
            const c = condition == 'perc' ? parseInt(payload[0].name.replace("p_","")) : parseInt(payload[0].name.replace("p2_",""));
            text1 = condition=='perc' ? tmp["p_" + c] + "% (" + tmp["p2_" + c] + "번)" : tmp["p2_" + c] + "번 (" + tmp["p_" + c] + "%)"; 
            text2 = condition=='perc' ? tmp["p_" + (c+1)] + "% (" + tmp["p2_" + (c+1)] + "번)" : tmp["p2_" + (c+1)] + "번 (" + tmp["p_" + (c+1)] + "%)"; 
        }
        
        return (
            <div style={{
                whiteSpace: 'pre-line',
                lineHeight: '0.8rem',
                padding: '15px 15px 0 15px',
                backgroundColor: '#fff',
                border: '1px solid #bbb'
            }}>
                <p>{tmp.date}</p>
                <p>{"지시자(Instructor): " + tmp.user}</p>
                <p>{"시도수(Attempts): " + tmp.cnt}</p>
                <p>{"정반응(Correct response): " + text1}</p>
                <p>{"촉구(Prompting): " + text2}</p>
                {tmp.decs ? (
                    <p>{"디시전(Decisions): " + tmp.decs_name}</p>
                ) : null}
            </div>
        );
    }
    return null;
};

const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload[0]) {
        const tmp = payload[0].payload;
        const targets = tmp.target.map((t,i) => {
            let text = "";
            if(i > 0) text += "/";
            return text + t;
        });
        return (
            <div style={{
                whiteSpace: 'pre-line',
                lineHeight: '0.8rem',
                padding: '15px 15px 0 15px',
                backgroundColor: '#fff',
                border: '1px solid #bbb'
            }}>
                <p>{tmp.date}</p>
                <p>{"지시자(Instructor) : " + tmp.directors}</p>
                <p>{"도달(Completed) : " + tmp.reaches}</p>
                <p>{"도달누적(Consecutive) : " + tmp.stack}</p>
                <p>{"STO 개수(Number of STO) : " + tmp.stos}</p>
                <p>{"STO : " + targets}</p>
            </div>
        );
    }
    return null;
};

const PoCheckbox = withStyles({
    root: {
        color: "#444",
        '&$checked': {
            color: "#444",
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

const PiCheckbox = withStyles({
    root: {
        color: "#444",
        '&$checked': {
            color: "#444",
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
class GraphSlide extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            first: true,
            open: props.open,
            condition: 'perc',
            reachType: 0,
            data: [],
            decs: [],
            maxCount: 0,
            stoItems: [],
            loadStoItemFunc: null,
            loadStoFunc: null,
            loadStoGroupFunc: null,
            labelCount: 0,
            p1: true,
            p2: true,
            conditionLabelWidth: 0,
            stoInfoBoxes: [],
            anchorEl: null,
            popContent: "",
            studentName: "",
            program: '',
            domainName: "",
            ltoName: "",
            stoName: "",
            lang: null,
        }
    }

    componentDidMount() {
        const func1 = this.loadStoItemGraph;
        const func2 = this.loadStoGraph;
        const func3 = this.loadStoGroupGraph;
        this.setState({
            loadStoItemFunc: func1,
            loadStoFunc: func2,
            loadStoGroupFunc: func3,
            lang: this.props.mainStore.getLang(this.props.mainStore.country)
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.open !== prevState.open) {
            if(nextProps.open) {
                if(nextProps.stoItem) prevState.loadStoItemFunc(nextProps.stoItem);
                else if(nextProps.sto) prevState.loadStoFunc(nextProps.sto);
                else if(nextProps.stoGroup) prevState.loadStoGroupFunc(nextProps.stoGroup);
                return {
                    open: nextProps.open,
                    first: nextProps.open,
                    title: nextProps.stoGroup ? "_" + nextProps.stoGroup.name : ""
                }
            } else {
                return {
                    open: nextProps.open,
                    first: nextProps.open,
                    stoInfoBoxes: [],
                    data: [],
                    reachType: 0,
                    stoItems: [],
                    maxCount: 0,
                }
            }
        }
        return null;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(prevProps.open == this.props.open && this.state.open && this.state.data) {
            if(this.state.first) {
                return {
                    refresh: true
                }
            } else if($('#loading-graph').css('visibility') == 'visible') {
                return {
                    loadingOff: true
                }
            }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot) {
            if(snapshot.refresh) {
                if(this.props.stoItem) this.setLinesPositionForPoint();
                else this.setLinesPosition();
            }
        }
    }

    setLinesPositionForPoint = () => {
        const { stoItems, labelCount } = this.state;
        const t = $("g.recharts-cartesian-grid");
        if(t.length) {
            const grid = $("g.recharts-cartesian-grid")[0].getBBox();
            if(grid && labelCount) {
                $("g.recharts-reference-line tspan").each((i,v) => {
                    $(v).parent().attr({
                        'x': 88,
                    });
                    $(v).attr({
                        'x': 82,
                        'dy': '1.2em',
                        'fill': 'red'
                    });
                });
                this.setState({ first: false });
            }
        }
    }

    setLinesPosition = () => {
        const { stoItems, labelCount, decs } = this.state;
        const realArr = stoItems.filter(s => s.points.length);
        const t = $("g.recharts-cartesian-grid");
        if(t.length) {
            const grid = $("g.recharts-cartesian-grid")[0].getBBox();
            if(grid && labelCount) {
                let begin = 60, start = 60;
                const l = realArr.length - 1;
                let c = 0;
                const ll = decs.length ? decs.length : 0;
                const tmp = ll > 0 ? $("g.recharts-reference-line line").slice(ll) : $("g.recharts-reference-line line");
                tmp.each((i,v) => {
                    let p = 0, leng = 0;
                    if(i < l) {
                        p = realArr[i].points ? realArr[i].points.length : 0;
                        leng = (p - 1) * MYSIZE > 0 ? (p - 1) * MYSIZE : 0;

                        begin += leng + MYSIZE/2;
                        v.setAttribute('x1', begin);
                        v.setAttribute('x2', begin);
                        begin += MYSIZE/2;
                        c++;
                    } else {
                        p = realArr[i - l].points ? realArr[i - l].points.length : 0;
                        if(l == 0 && p < 4) {
                            leng = MYSIZE * 2;
                        } else {
                            leng = (p-1) * MYSIZE;
                            if(i > c) leng += MYSIZE/2;
                            if(i < c + l) leng += MYSIZE/2;
                        }

                        v.setAttribute('x1', start);
                        v.setAttribute('x2', start + leng);

                        const label = $(v).siblings();
                        if(label.length) {
                            label.attr({
                                'x': start + 28,
                            });
                            label.find('tspan').attr({
                                'x': start + 22,
                                'dy': '1.2em',
                                'fill': 'red'
                            });
                        }
                        start += leng;
                    }
                });
                this.setState({ first: false });
            }
        }
    }

    loadStoItemGraph = stoItem => { // sto 화면에서 sto item 그래프 버튼
        const { funcStore, classes } = this.props;
        let max = 0;
        funcStore.conn('get', '/points/' + stoItem.id + '/getAll', false, true, null, (res)=>{
            if(res.result == "true") {
                let data = [], decs = [];
                let l = 0;
                res.points.map(v => {
                    let tmp = {};
                    tmp.id = v.id;
                    tmp.date = v.real_date;
                    tmp.cnt = v.count;
                    tmp.po = Math.round((v.positive / v.count) * 100);
                    tmp.pa = Math.round((v.pass / v.count) * 100);
                    tmp.po2 = v.positive;
                    tmp.pa2 = v.pass;
                    tmp.user = v.target_user_name;
                    tmp.decs = v.decision_id;
                    tmp.decs_name = v.decision_user_name;
                    if(max < v.positive) max = parseInt(v.positive);
                    if(max < v.pass) max = parseInt(v.pass);
                    data.push(tmp);
                    l++;

                    if(v.decision_id) decs.push({
                        id: v.id,
                        date: v.real_date,
                        name: v.decision_user_name,
                    });
                });
                const len = res.points.length;
                const sto = res.stoItem;
                let w = 0;
                if(len > 3) {
                    w = (len - 1) * MYSIZE;
                } else w = MYSIZE * 2;
                
                let nm = "";
                if(sto.target.length) {
                    sto.target.map((s, i) => {
                        if(i > 0) nm += " / ";
                        nm += s.text ? s.text : "-";
                    });
                } else {
                    nm = "(Empty target)";
                }
                const box = (
                    <div className={classes.stoInfoItem}
                        style={{ width: w }} onClick={this.popClick(sto)} >{nm}</div>
                );
                this.setState({
                    reachType: res.stoGroup.type,
                    stoItems: [res.stoItem],
                    data: data,
                    decs: decs,
                    labelCount: l,
                    maxCount: max,
                    stoInfoBoxes: box,
                    studentName: res.stoItem.student_name,
                    domainName: res.stoItem.domain_name,
                    ltoName: res.stoGroup.name,
                    program: res.program,
                });
                this.showLoading(false);
            } else {
                console.log("포인트 그래프 로딩 실패");
            }
        });
    }

    loadStoGraph = stoItem => { // 차트에서 그래프 버튼
        const { funcStore, classes } = this.props;
        let max = 0;
        let box = [];
        funcStore.conn('get', '/points/' + stoItem.sto_item_id + '/getAllBySto', false, true, null, (res)=>{
            if(res.result == "true") {
                let data = [], decs = [];
                const tempArr = res.stoItems.filter(s => s.points.length ? true : false);
                const tempLength = tempArr.length;
                if(res.stoGroup.type == 0) {
                    tempArr.map((sto, i) => {
                        const n = i * 2;
                        sto.points.map((v,idx) => {
                            let tmp = {};
                            tmp.name = i + "_" + idx;
                            tmp.date = v.real_date;
                            tmp.cnt = v.count;
                            tmp["p_" + n] = Math.round((v.positive / v.count) * 100);
                            tmp["p_" + (n+1)] = Math.round((v.pass / v.count) * 100);
                            tmp["p2_" + n] = v.positive;
                            tmp["p2_" + (n+1)] = v.pass;
                            tmp.user = v.target_user_name;
                            tmp.decs = v.decision_id;
                            tmp.decs_name = v.decision_user_name;
                            if(max < v.positive) max = parseInt(v.positive);
                            if(max < v.pass) max = parseInt(v.pass);
                            data.push(tmp);

                            if(v.decision_id) decs.push({
                                key: i + "_" + idx,
                                date: v.real_date,
                                name: v.decision_user_name,
                            });
                        });
                        const l = sto.points.length;
                        let w = (l - 1) * MYSIZE;
                        if(i > 0) {
                            w += (MYSIZE/2 - 2.5);
                        }
                        if(i < tempLength - 1) w += (MYSIZE/2 - 2.5);
                        if(tempLength == 1 && l <= 3) w = MYSIZE * 2;

                        let nm = sto.name ? sto.name : "";
                        if(nm == "") {
                            if(sto.target.length) {
                                sto.target.map((s, i) => {
                                    if(i > 0) nm += " / ";
                                    nm += s.text ? s.text : "-";
                                });
                            } else {
                                nm = "(Empty target)";
                            }
                        }
                        box = box.concat(
                            <div key={"box_"+i}
                                className={classNames(classes.stoInfoItem, i>0 ? classes.stoInfoNextItem : null)}
                                style={{ width: w }} onClick={this.popClick(sto)} >{nm}</div>
                        );
                    });
                } else {
                    tempArr.map((d, i) => {
                        const tmp = {
                            name: "dot_" + i,
                            date: d.real_date,
                            dot: d.stack,
                            reaches: d.reaches,
                            stack: d.stack,
                            stos: (d.sto_item_ids ? d.sto_item_ids.split(",").length : 0),
                            directors: d.directors,
                            target: d.info.map(t => {
                                return t.target ? t.target.map(tt => {
                                    return tt.text;
                                }) : "";
                            })
                        }
                        if(i == tempArr.length - 1) max = d.stack;
                        //if(max < d.stack) max = d.stack;
                        data.push(tmp);
                    });
                }
                
                this.setState({
                    reachType: res.stoGroup.type,
                    data: data,
                    decs: decs,
                    stoItems: res.stoItems,
                    labelCount: data.length,
                    maxCount: max,
                    stoInfoBoxes: box,
                    studentName: res.program.student_name,
                    domainName: res.program.domain_name,
                    ltoName: res.stoGroup.name,
                    program: res.program
                });
                this.showLoading(false);
            } else {
                console.log("STO 그래프 로딩 실패");
            }
        });
    }

    loadStoGroupGraph = stoGroup => { // sto 화면에서 group 그래프 버튼
        const { funcStore, classes } = this.props;
        let max = 0;
        let box = [];
        funcStore.conn('get', '/points/' + stoGroup.id + '/getAllByStoGroup', false, true, null, (res)=>{
            if(res.result == "true") {
                let data = [], decs = [];
                const tempArr = res.stoItems.filter(s => s.points.length ? true : false);
                const tempLength = tempArr.length;
                if(res.stoGroup.type == 0) {
                    tempArr.map((sto, i) => {
                        const n = i * 2;
                        sto.points.map((v,idx) => {
                            let tmp = {};
                            tmp.name = i + "_" + idx;
                            tmp.date = v.real_date;
                            tmp.cnt = v.count;
                            tmp["p_" + n] = Math.round((v.positive / v.count) * 100);
                            tmp["p_" + (n+1)] = Math.round((v.pass / v.count) * 100);
                            tmp["p2_" + n] = v.positive;
                            tmp["p2_" + (n+1)] = v.pass;
                            tmp.user = v.target_user_name;
                            tmp.decs = v.decision_id;
                            tmp.decs_name = v.decision_user_name;
                            if(max < v.positive) max = parseInt(v.positive);
                            if(max < v.pass) max = parseInt(v.pass);
                            data.push(tmp);

                            if(v.decision_id) decs.push({
                                key: i + "_" + idx,
                                date: v.real_date,
                                name: v.decision_user_name,
                            });
                        });
                        const l = sto.points.length;
                        let w = (l - 1) * MYSIZE;
                        if(i > 0) {
                            w += (MYSIZE/2 - 2.5);
                        }
                        if(i < tempLength - 1) w += (MYSIZE/2 - 2.5);
                        if(tempLength == 1 && l <= 3) w = MYSIZE * 2;

                        let nm = sto.name ? sto.name : "";
                        if(nm == "") {
                            if(sto.target.length) {
                                sto.target.map((s, i) => {
                                    if(i > 0) nm += " / ";
                                    nm += s.text ? s.text : "-";
                                });
                            } else {
                                nm = "(Empty target)";
                            }
                        }
                        box = box.concat(
                            <div key={"box_"+i}
                                className={classNames(classes.stoInfoItem, i>0 ? classes.stoInfoNextItem : null)}
                                style={{ width: w }} onClick={this.popClick(sto)} >{nm}</div>
                        );
                    });
                } else {
                    tempArr.map((d, i) => {
                        const tmp = {
                            name: "dot_" + i,
                            date: d.real_date,
                            dot: d.stack,
                            reaches: d.reaches,
                            stack: d.stack,
                            stos: (d.sto_item_ids ? d.sto_item_ids.split(",").length : 0),
                            directors: d.directors,
                            target: d.info.map(t => {
                                return t.target ? t.target.map(tt => {
                                    return tt.text;
                                }) : "";
                            })
                        }
                        if(i == tempArr.length - 1) max = d.stack;
                        //if(max < d.stack) max = d.stack;
                        data.push(tmp);
                    });
                }
                
                this.setState({
                    reachType: res.stoGroup.type,
                    data: data,
                    decs: decs,
                    stoItems: res.stoItems,
                    labelCount: data.length,
                    maxCount: max,
                    stoInfoBoxes: box,
                    studentName: res.program.student_name,
                    domainName: res.program.domain_name,
                    ltoName: res.stoGroup.name,
                    program: res.program
                });
                this.showLoading(false);
            } else {
                console.log("프로그램 그래프 로딩 실패");
            }
        });
    }

    handleClose = () => {
        this.props.handleSlide(false);
    };

    handleChangeCheck = name => e => {
        const { stoItem } = this.props;
        const { p1, p2 } = this.state;
        if(!e.target.checked && !(p1 && p2)) return;
        this.setState({
            [name]: e.target.checked
        });
        this.showLoading(true);
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                if(stoItem) this.setLinesPositionForPoint();
                else this.setLinesPosition();
                this.showLoading(false);
            }, 0);
        });
    }

    handleChange = name => e => {
        this.setState({
            [name]: e.target.value
        });
        if(name == 'condition') {
            const { stoItem } = this.props;
            this.showLoading(true);
            new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                    if(stoItem) this.setLinesPositionForPoint();
                    else this.setLinesPosition();
                    this.showLoading(false);
                }, 0);
            });
            // this.showLoading(true);
            // new Promise(resolve => {
            //     setTimeout(() => {
            //         resolve();
            //         this.redrawGraph();
            //     }, 0);
            // });
        }
    }

    setLabelWidth = (kind, ref) => {
        const { conditionLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != conditionLabelWidth) {
            this.setState({ conditionLabelWidth: ref.offsetWidth});
        }
    }

    redrawGraph = () => {
        const { stoItem, sto, stoGroup } = this.props;
        this.showLoading(true);
        if(stoItem) {
            this.loadStoItemGraph(stoItem);
        } else if(sto) {
            this.loadStoGraph(sto);
        } else if(stoGroup) {
            this.loadStoGroupGraph(stoGroup);
        }
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                if(stoItem) this.setLinesPositionForPoint();
                else this.setLinesPosition();
            }, 0);
        });
    }

    stoExportToExcel = async() => {
        const { stoItems, studentName, domainName, ltoName, data } = this.state;
        if(!stoItems.length) return;
        const sto = stoItems[0];
        let targets = "";

        const wb = new ExcelJS.Workbook();
        const ws = wb.addWorksheet();
        ws.columns = [
            { header:'col0', key:'col0', width: 20 },
            { header:'col1', key:'col1' },
            { header:'col2', key:'col2' },
            { header:'col3', key:'col3' },
            { header:'col4', key:'col4' },
            { header:'col5', key:'col5' },
            { header:'col6', key:'col6' },
            { header:'col7', key:'col7' },
        ];
        ws.getRow(1).hidden = true;

        const std = ws.addRow(["학생명", studentName]);
        const dm = ws.addRow(["DOMAIN", domainName]);
        const lt = ws.addRow(["LTO", ltoName]);
        std.getCell(1).font={bold:true};
        dm.getCell(1).font={bold:true};
        lt.getCell(1).font={bold:true};
        if(sto.target.length) {
            sto.target.map((s, i) => {
                const st = ws.addRow(["STO_" + (i+1), s.text]);
                st.getCell(1).font={bold:true};
                targets += (i > 0 ? "/" : "") + targets;
            });
        } else {
            const st = ws.addRow(["STO", "없음"]);
            st.getCell(1).font={bold:true};
        }
        ws.addRow();

        let titleRows = [];
        titleRows.push(ws.addRow(["도달 기준 정반응 비율", sto.standard_perc + "%"]));
        titleRows.push(ws.addRow(["도달 기준 총 횟수", sto.standard_cnt]));
        titleRows.push(ws.addRow(["촉구 설정 선택", sto.prompt]));
        titleRows.push(ws.addRow(["촉구 관련 메모", sto.prompt_memo ? sto.prompt_memo : ""]));
        titleRows.push(ws.addRow(["강화 스케쥴", sto.schedule_memo ? sto.schedule_memo : ""]));
        titleRows.push(ws.addRow(["프로그램 관련 메모", sto.program_memo ? sto.program_memo : ""]));
        titleRows.map(t => {
            t.getCell(1).font={bold:true};
            t.getCell(2).alignment={vertical: 'middle', horizontal: 'left'};
        });
        ws.addRow();
        const tArr = ["날짜", "지시자", "총 시도", "정반응", "촉구", "정반응(%)", "촉구(%)", "디시전"];
        const title = ws.addRow(tArr);
        tArr.map((v,i) => {
            title.getCell(i+1).alignment={vertical: 'middle', horizontal: 'center'};
            title.getCell(i+1).border = {
                top: {style:'thin'},
                left: {style:'thin'},
                bottom: {style:'thin'},
                right: {style:'thin'}
            }
        });
        data.map(p => {
            const arr = [p.date, p.user, p.cnt, p.po2, p.pa2, p.po+"%", p.pa+"%", p.decs ? "O" : "-"];
            const row = ws.addRow(arr);
            arr.map((v,i) => {
                row.getCell(i+1).alignment={vertical: 'middle', horizontal: 'center'};
                row.getCell(i+1).border = {
                    top: {style:'thin'},
                    left: {style:'thin'},
                    bottom: {style:'thin'},
                    right: {style:'thin'}
                }
            });
        });
        
        const d = this.props.funcStore.convertToymd().replace(/\-/g,'');
        const nav = domainName + "_" + ltoName + "_" + targets;
        const exportTitle = d + "_" + studentName + "_" + nav;

        const buf = await wb.xlsx.writeBuffer();
        saveAs(new Blob([buf]), exportTitle + '.xlsx');
    }

    ltoExportToExcel = async() => {
        const { stoItems, studentName, domainName, ltoName, program, data } = this.state;
        if(!stoItems.length) return;

        const duNameCheck = [];

        const wb = new ExcelJS.Workbook();
        const iws = wb.addWorksheet('info');
        iws.columns = [
            { header:'col0', key:'col0', width: 20 },
            { header:'col1', key:'col1' },
        ];
        iws.getRow(1).hidden = true;

        const istd = iws.addRow(["학생명", studentName]);
        const idm = iws.addRow(["DOMAIN", domainName]);
        const ilt = iws.addRow(["LTO", ltoName]);
        istd.getCell(1).font={bold:true};
        idm.getCell(1).font={bold:true};
        ilt.getCell(1).font={bold:true};

        stoItems.map((si, i)=>{
            const sto = si;
            let targets = "";
            const ws = wb.addWorksheet();
            ws.columns = [
                { header:'col0', key:'col0', width: 20 },
                { header:'col1', key:'col1' },
                { header:'col2', key:'col2' },
                { header:'col3', key:'col3' },
                { header:'col4', key:'col4' },
                { header:'col5', key:'col5' },
                { header:'col6', key:'col6' },
                { header:'col7', key:'col7' },
            ];
            ws.getRow(1).hidden = true;

            if(sto.target.length) {
                sto.target.map((s, i2) => {
                    let sn = s.text ? s.text.replace(/[\/]/gi, ",").replace(/[\[\{]/gi, "(").replace(/[\]\}]/gi, ")") : "";
                    sn = sn ? sn.replace(/[?;:|*~`!^\-+<>@\#$%&\\\=\'\"]/gi, "") : "";
                    const st = ws.addRow(["STO_" + (i2+1), s.text]);
                    st.getCell(1).font={bold:true};
                    targets += (i2 > 0 ? "," : "") + sn;
                });
            } else {
                const st = ws.addRow(["STO", "(없음)"]);
                st.getCell(1).font={bold:true};
                targets = "STO 없음";
            }
            let tn = this.props.funcStore.cutString(targets, 20);
            if(duNameCheck.indexOf(tn) !== -1) {
                duNameCheck.push(tn);
                let nn = 0;
                duNameCheck.map((v)=>{
                    if(v == tn) nn++;
                });
                tn = this.props.funcStore.cutString(targets, 16);
                tn += "(" + nn + ")";
            } else {
                duNameCheck.push(tn);
            }
            
            ws.name = tn;
            ws.addRow();

            let titleRows = [];
            titleRows.push(ws.addRow(["도달 기준 정반응 비율", sto.standard_perc + "%"]));
            titleRows.push(ws.addRow(["도달 기준 총 횟수", sto.standard_cnt]));
            titleRows.push(ws.addRow(["촉구 설정 선택", sto.prompt ? sto.prompt : ""]));
            titleRows.push(ws.addRow(["촉구 관련 메모", sto.prompt_memo ? sto.prompt_memo : ""]));
            titleRows.push(ws.addRow(["강화 스케쥴", sto.schedule_memo ? sto.schedule_memo : ""]));
            titleRows.push(ws.addRow(["프로그램 관련 메모", sto.program_memo ? sto.program_memo : ""]));
            titleRows.map(t => {
                t.getCell(1).font={bold:true};
                t.getCell(2).alignment={vertical: 'middle', horizontal: 'left'};
            });

            if(si.points.length) {
                ws.addRow();
                const tArr = ["날짜", "지시자", "총 시도", "정반응", "촉구", "정반응(%)", "촉구(%)", "디시전"];
                const title = ws.addRow(tArr);
                tArr.map((v,i2) => {
                    title.getCell(i2+1).alignment={vertical: 'middle', horizontal: 'center'};
                    title.getCell(i2+1).border = {
                        top: {style:'thin'},
                        left: {style:'thin'},
                        bottom: {style:'thin'},
                        right: {style:'thin'}
                    }
                });
    
                si.points.map((p, i2)=>{
                    const arr = [
                        p.real_date,
                        p.target_user_name, 
                        p.count, 
                        p.positive, p.pass, 
                        parseInt(p.positive / p.count * 100) + "%", parseInt(p.pass / p.count * 100) + "%", 
                        p.decision_id ? "O" : "-"
                    ];
                    const row = ws.addRow(arr);
                    arr.map((v,i3) => {
                        row.getCell(i3+1).alignment={vertical: 'middle', horizontal: 'center'};
                        row.getCell(i3+1).border = {
                            top: {style:'thin'},
                            left: {style:'thin'},
                            bottom: {style:'thin'},
                            right: {style:'thin'}
                        }
                    });
                });
            }
        });
        
        const d = this.props.funcStore.convertToymd().replace(/\-/g,'');
        const nav = domainName + "_" + ltoName;
        const exportTitle = d + "_" + studentName + "_" + nav;

        const buf = await wb.xlsx.writeBuffer();
        saveAs(new Blob([buf]), exportTitle + '.xlsx');
    }

    popClick = sto => e => {
        const { lang } = this.state;
        let t = "";
        t += "STO: " + sto.target.map((s, i) => {
            return s.text ? s.text : "-";
        });
        t += "\n"+lang['stoitem1']+": " + sto.standard_perc + "% / " + sto.standard_cnt;
        t += "\n"+lang['sto7']+": " + (sto.prompt ? sto.prompt + " / " + (sto.prompt_memo ? sto.prompt_memo : "") : "-");
        t += "\n"+lang['daily13']+": " + (sto.schedule_memo ? sto.schedule_memo : "-");
        t += "\n"+lang['daily14']+": " + (sto.program_memo ? sto.program_memo : "-");

        this.setState({
            anchorEl: e.currentTarget,
            popContent: t
        });
    }

    popClose = () => {
        this.setState({
            anchorEl: null,
        });
    }

    showLoading = flag => {
        if(flag) {
            $('#loading-graph').show();
        } else {
            new Promise(resolve => {
                setTimeout(() => {
                    resolve();
                    $('#loading-graph').hide();
                }, 0);
            });
        }
    }

    render() {
        const { classes, open, stoItem, sto, stoGroup } = this.props;
        const { title, data, decs, reachType, stoItems, labelCount, p1, p2, maxCount, condition, conditionLabelWidth, stoInfoBoxes, anchorEl, popContent } = this.state;
        const popOpen = Boolean(anchorEl);
        const anchorId = popOpen ? 'popover' : undefined;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        let realArr = [];
        if(sto || stoGroup) {
            realArr = stoItems.filter(s => s.points.length);
        }
        let idxArr = null;
        let tempCount = null;
        if(reachType == 0) {
            if(condition == 'perc') {
                idxArr = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
                tempCount = [0, 100];
            } else {
                //tempCount = maxCount % 5 > 0 ? maxCount - (maxCount % 5) + 5 : maxCount;
                tempCount = maxCount - (maxCount % 5) + 5;
                idxArr = [...Array(tempCount + 1)].map((v,i) => i);
            }
        } else {
            idxArr = stackTemp;
            tempCount = [0, 40];
        }
        const graphWidth = (labelCount > 3) ? ((labelCount-1) * 50 + 60) : 160;
        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}
                classes={reachType == 0 ? {
                    root: classes.dialogRoot,
				    paper: classes.dialogRoot
                } : {
                    root: classes.dialogStackRoot,
				    paper: classes.dialogStackRoot
                }}
            >
                <AppBar className={classes.appBar}>
                    <Toolbar>
                    <Typography variant="h6" color="inherit" className={classes.flex}>
                        {stoItem ? "LEARN UNIT GRAPH" : "LTO LEARN UNIT GRAPH" + title}
                    </Typography>
                    <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                        <Close />
                    </IconButton>
                    </Toolbar>
                </AppBar>
                <div className={classes.root}>
                    <div className={classes.graphFilterBox}>
                        <div className={classes.graphFilterLeft}>
                            <FormControl variant="outlined" className={(classes.formControl, classes.standardBox)}>
                                <InputLabel
                                    ref={ref => {
                                        if(ref) this.setLabelWidth(1, ref);
                                    }}
                                    htmlFor="register-class"
                                >
                                    {lang['graph6']}
                                </InputLabel>
                                {reachType == 0 ? (
                                    <Select
                                        value={condition}
                                        onChange={this.handleChange('condition')}
                                        input={
                                        <OutlinedInput
                                            labelWidth={conditionLabelWidth}
                                            name="condition"
                                            id="filter-condition"
                                        />
                                        }
                                    >
                                        <MenuItem value="perc">
                                            {"Percentage"}
                                        </MenuItem>
                                        <MenuItem value="cnt">
                                            {"Count"}
                                        </MenuItem>
                                    </Select>
                                ) : (
                                    <Select
                                        value={"stack"}
                                        input={
                                        <OutlinedInput
                                            labelWidth={conditionLabelWidth}
                                            name="condition"
                                            id="filter-condition"
                                        />
                                        }
                                        disabled={true}
                                    >
                                        <MenuItem value="stack">
                                            {"Stacked"}
                                        </MenuItem>
                                    </Select>
                                )}
                                
                            </FormControl>
                            {reachType == 0 && (
                                <React.Fragment>
                                    <FormControlLabel
                                        control={
                                            <PoCheckbox
                                                checked={p1}
                                                onChange={this.handleChangeCheck('p1')}
                                                value="p1"
                                            />
                                        }
                                        classes={{
                                            root: classes.checkboxLabel
                                        }}
                                        label={lang['graph1']}
                                    />
                                    <FormControlLabel
                                        control={
                                            <PiCheckbox
                                                checked={p2}
                                                onChange={this.handleChangeCheck('p2')}
                                                value="p2"
                                            />
                                        }
                                        classes={{
                                            root: classes.checkboxLabel
                                        }}
                                        label={lang['graph2']}
                                    />
                                </React.Fragment>
                            )}
                        </div>
                        <Fab className={classes.exportBtn} aria-label="export" onClick={()=>{
                            if(stoItem) this.stoExportToExcel();
                            else if(sto || stoGroup) this.ltoExportToExcel();
                        }}>
                            <SaveAlt />
                        </Fab>
                        <Fab className={classes.refreshBtn} aria-label="refresh" onClick={()=>this.redrawGraph()}>
                            <Cached />
                        </Fab>
                    </div>
                    <div id="loading-graph" className={classes.loadingRoot}></div>
                    <div className={classes.stoInfoRoot}>
                        <div className={classes.stoInfoBox}>{stoInfoBoxes}</div>
                        {stoItem && (
                            labelCount > 0 ? (
                                <LineChart
                                    className={classes.graphDraw}
                                    width={graphWidth}
                                    height={450}
                                    data={data}
                                    margin={{
                                        top: 5, right: 0, left: 0, bottom: 40,
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="2 2" />
                                    <XAxis dataKey="id"
                                        minTickGap={0}
                                        interval={0}
                                        tick={<CustomizedAxisTick data={data} flag={true}/>}
                                    />
                                    <YAxis unit={condition=='perc' ? "%" : ''}
                                        tick={{
                                            fontSize: 11
                                        }}
                                        domain={tempCount.length ? tempCount : []}
                                        ticks={idxArr.length ? idxArr : []}
                                    />
                                    <Tooltip formatter={(val, name, props)=>{
                                        const c = condition == 'perc' ? '2' : '';
                                        const tmp2 = name.indexOf('po') !== -1 ? 'po' : 'pa';
                                        const tmp = props.payload;
                                        const text = condition=='perc' ? "% (" + tmp[tmp2 + c] + "번)" : "번 (" + tmp[tmp2 + c] + "%)";
                                        return val + text;
                                        
                                    }} content={<GraphTooltip condition={condition} flag={1}/>}/>
                                    {decs.length ? decs.map((d,i) => {
                                        return (
                                            <ReferenceLine
                                                key={"decs_"+i}
                                                x={d.id}
                                                stroke={"#444"}
                                                strokeWidth="4"
                                                strokeOpacity="0.6"
                                            />
                                        );
                                    }) : null}
                                    {p1 == true && (
                                        <Line dataKey={condition == 'perc' ? "po" : "po2"}
                                            stroke="#222"
                                            strokeWidth={4}
                                            dot={{ r: 4, strokeWidth: 2, fill: '#222' }}
                                            activeDot={{ r: 4, strokeWidth: 2, stroke: "#222", fill: '#bbb' }}
                                        />
                                    )}
                                    {p2 == true && (
                                        <Line dataKey={condition == 'perc' ? "pa" : "pa2"}
                                            stroke="#444"
                                            strokeWidth={4}
                                            strokeDasharray="4 4"
                                            dot={{ r: 4, strokeWidth: 2, strokeDasharray: "0" }}
                                            activeDot={{ r: 4, strokeWidth: 2, strokeDasharray: "0", stroke: "#bbb" }}
                                        />
                                    )}
                                    {condition == 'perc' && stoItems.map((v,i)=>{
                                        // 가로
                                        return (<ReferenceLine
                                            key={"standard_"+i}
                                            y={v.standard_perc}
                                            label={v.standard_perc + "%"}
                                            stroke={"red"}
                                            strokeWidth="3"
                                            strokeOpacity="0.3"
                                        />);
                                    })}
                                </LineChart>
                            ) : ''
                        )}
                        {(sto || stoGroup) && (
                            (labelCount > 0 ? 
                                reachType == 0 ? (
                                    <LineChart
                                        className={classes.graphDraw}
                                        width={graphWidth}
                                        height={450}
                                        data={data}
                                        margin={{
                                            top: 5, right: 0, left: 0, bottom: 40,
                                        }}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="name"
                                            minTickGap={0}
                                            interval={0}
                                            tick={<CustomizedAxisTick data={data} flag={false}/>}
                                        />
                                        <YAxis unit={condition=='perc' ? "%" : ''}
                                            tick={{
                                                fontSize: 11
                                            }}
                                            domain={tempCount.length ? tempCount : []}
                                            ticks={idxArr.length ? idxArr : []}
                                        />
                                        <Tooltip labelFormatter={(c)=>{
                                            const d = data.filter(v => v.name == c);
                                            if(d.length) return d[0].date;
                                            else return "";
                                        }} formatter={(val, name, props)=>{
                                            const c = condition == 'perc' ? parseInt(name.replace("p_","")) : parseInt(name.replace("p2_",""));
                                            const tmp = props.payload;
                                            const text = condition=='perc' ? "% (" + tmp["p2_" + c] + "번)" : "번 (" + tmp["p_" + c] + "%)"; 
                                            return [val + text, (c%2 == 0 ? "정반응" : "촉구")];
                                        }}
                                        content={<GraphTooltip condition={condition} flag={2} />}
                                        />
                                        {decs.length ? decs.map((d,i) => {
                                            return (
                                                <ReferenceLine
                                                    key={"decs_"+i}
                                                    x={d.key}
                                                    stroke={"#444"}
                                                    strokeWidth="4"
                                                    strokeOpacity="0.6"
                                                />
                                            );
                                        }) : null}
                                        {[...Array(stoItems.length * 2)].map((v,i)=>{
                                            if(p1 == false && i%2==0) return null;
                                            if(p2 == false && i%2==1) return null;
                                            return (
                                                <Line
                                                    key={"line_"+i}
                                                    dataKey={condition == "perc" ? "p_"+i : "p2_"+i}
                                                    stroke={(i%2==0 ? "#444" : "#222")}
                                                    strokeDasharray={i%2!=0 ? "4 4" : ""}
                                                    strokeWidth={4}
                                                    dot={{ r: 4, strokeWidth: 2, strokeDasharray: "0", fill: i%2!=0 ? "#fff" : "#222" }}
                                                    activeDot={{ r: 4, strokeWidth: 2, strokeDasharray: "0",
                                                        stroke: i%2!=0 ? "#bbb" : "#222",
                                                        fill: i%2!=0 ? "#444" : "#fff"
                                                    }}
                                                />
                                            );
                                        })}
                                        {stoItems.map((v,i)=>{
                                            // 세로
                                            if(i > 0) {
                                                const ck = realArr[i-1] ? realArr[i-1].finish : 1;
                                                return (<ReferenceLine
                                                    key={"border_"+i}
                                                    x={i + "_0"}
                                                    stroke={ck == 0 ? "#bbb" : ck == 1 ? "red" : "cornflowerblue"}
                                                    strokeWidth={ck > 0 ? "4" : "8"}
                                                    strokeDasharray={ck > 0 ? "" : "8 8"}
                                                />);
                                            }
                                        })}
                                        {condition == 'perc' && realArr.map((v,i)=>{
                                            // 가로
                                            return (<ReferenceLine
                                                key={"standard_"+i}
                                                x={i + "_0"}
                                                y={v.standard_perc}
                                                label={v.standard_perc + "%"}
                                                stroke={"red"}
                                                strokeWidth="3"
                                                strokeOpacity="0.3"
                                            />);
                                        })}
                                        
                                    </LineChart>
                                ) : (
                                    <LineChart
                                        className={classes.graphDraw}
                                        width={graphWidth}
                                        height={450}
                                        data={data}
                                        margin={{
                                            top: 5, right: 0, left: 0, bottom: 40,
                                        }}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="name"
                                            minTickGap={0}
                                            interval={0}
                                            tick={<CustomizedAxisTick data={data} flag={false}/>}
                                        />
                                        <YAxis unit=""
                                            tick={{
                                                fontSize: 11
                                            }}
                                            domain={tempCount.length ? tempCount : []}
                                            ticks={idxArr.length ? idxArr : []}
                                        />
                                        <Tooltip labelFormatter={(c)=>{
                                            const d = data.filter(v => v.name == c);
                                            if(d.length) return d[0].date;
                                            else return "";
                                        }} content={<CustomTooltip />} />
                                        <Line
                                            dataKey={"dot"}
                                            stroke="#222"
                                            strokeDasharray=""
                                            strokeWidth={4}
                                            dot={{ r: 4, strokeWidth: 2, fill: '#222' }}
                                            activeDot={{ r: 4, strokeWidth: 2, stroke: "#222", fill: '#bbb' }}
                                        />
                                    </LineChart>
                            ) : '')
                        )}
                    </div>
                    <Popover
                        id={anchorId}
                        open={popOpen}
                        anchorEl={anchorEl}
                        onClose={this.popClose}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'left',
                        }}
                        classes={{
                            paper: classes.popRoot
                        }}
                    >
                        <Typography variant="body2" color="inherit" className={classes.popText}>
                            {popContent}
                        </Typography>
                    </Popover>
                </div>
            </Dialog>
        );
    }
}

const styles = theme => ({
    container: {
        flexWrap: 'wrap',
    },
    appBar: {
      position: 'relative',
      backgroundColor: '#f68a15'
    },
    flex: {
      flex: 1,
    },
    root: {
        flexGrow: 1,
        display: 'flex',
        overflowX: 'auto',
        overflowY: 'hidden',
        padding: '0 30px'
    },
    dialogRoot: {
        height: 670
    },
    dialogStackRoot: {
        height: 630
    },
    graphFilterBox: {
        position: 'absolute',
        width: 'calc(100% - 60px)',
        display: 'flex',
        alignItems: 'center',
        padding: '15px 0',
        borderBottom: '1px solid #bbb'
    },
    graphFilterLeft: {
        display: 'flex',
        flex: 1
    },
    graphDraw: {
        marginTop: 0,
        marginLeft: -20,
        '& > svg': {
            overflow: 'visible'
        }
    },
    checkboxLabel: {
        marginBottom: 0
    },
    standardBox: {
        width: 140,
        marginRight: 15
    },
    loadingRoot: {
        visibility: 'visible',
        position: 'absolute',
        left: 0,
        bottom: 0,
        width: '100%',
        height: 'calc(100% - 160px)',
        backgroundColor: '#fff',
        zIndex: '9999',
        marginTop: 100,
        transition: 'all .3s ease'
    },
    stoInfoRoot: {
        marginTop: 100,
    },
    stoInfoBox: {
        flex: 1,
        display: 'flex',
        paddingLeft: 40
    },
    stoInfoItem: {
        cursor: 'pointer',
        backgroundColor: 'lightslategrey',
        border: '1px solid lightslategrey',
        borderRadius: '7px',
        padding: '4px 7px',
        marginBottom: 5,
        color: '#fff',
        opacity: '0.8',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        transition: 'all .2s ease',
        '&:hover': {
            backgroundColor: 'slategray',
            opacity: '0.9',
        },
        '&:active': {
            backgroundColor: 'slategray',
            opacity: '1',
        }
    },
    stoInfoNextItem: {
        marginLeft: 5
    },
    popRoot: {
        width: 300,
        padding: 15,
        backgroundColor: 'rgba(0,0,0,.8)',
        color: '#fff'
    },
    popText: {
        whiteSpace: 'pre-line',
        lineHeight: '1.2rem'
    },
    refreshBtn: {
        color: '#444',
        backgroundColor: 'transparent',
        '&:hover': {
            backgroundColor: '#ddd',
        },
        '&:active': {
            backgroundColor: '#ddd',
        },
    },
    exportBtn: {
        marginRight: 8,
        color: '#444',
        backgroundColor: 'transparent',
        '&:hover': {
            backgroundColor: '#ddd',
        },
        '&:active': {
            backgroundColor: '#ddd',
        },
    }
});

export default withStyles(styles)(GraphSlide);
