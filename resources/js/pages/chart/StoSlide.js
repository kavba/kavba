import React from 'react';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { ButtonGroup, Button, Fab, Dialog, ListItem, List, Divider, AppBar, Toolbar, IconButton, OutlinedInput, Collapse,
    Typography, Slide, Grid, InputLabel, MenuItem, FormControl, Select, TextField, InputAdornment } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import PlaylistAdd from '@material-ui/icons/PlaylistAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import { FileCopy, SubdirectoryArrowRight, InsertChartOutlined, HighlightOff, Edit, Delete, ZoomIn, Save, MultilineChart, ShowChart, Replay, ChevronRight, ChevronLeft } from '@material-ui/icons';

import { Points, Dialogs } from '../../components/';
import { StoGraphSlide } from '../../pages';

let optionOrder = 0;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const promptArr = [
    {id:1, name: "독립(independent)"},
    {id:2, name: "전체 신체(full physical)"},
    {id:3, name: "부분 신체(partial physical)"},
    {id:4, name: "전체 음성(full aditory)"},
    {id:5, name: "부분 음성(partial aditory)"},
    {id:6, name: "몸짓(gestual)"},
    {id:7, name: "글자(textual)"},
    {id:8, name: "그림(picture)"},
];

@inject(stores => ({
    funcStore: stores.funcStore,
    mainStore: stores.mainStore,
}))
class StoSlide extends React.Component {
    state = {
        open: this.props.open,
        stoItems: [],
        stoGroups: [],
        view: 'intro',
        selectedSto: '',
        selectedGroup: '',
        name: '',
        rows: [],
        target: [{
            text: '', status: '0'
        }],
        points: [],
        stoTargets: [],
        userTargets: [],
        searchTarget: '',
        parentId: '',
        standardPerc: 90,
        standardCnt: 1,
        promptCode: '',
        promptMemo: '',
        scheduleMemo: '',
        programMemo: '',
        percLabelWidth: 0,
        cntLabelWidth: 0,
        parentLabelWidth: 0,
        targetLabelWidth: 0,
        promptLabelWidth: 0,
        typeLabelWidth: 0,
        graphOpen: false,
        graphStoItem: null,
        graphStoGroup: null,
        dialogOpen: false,
        dialogSort: true,
        pointFlag: false,
        groupOpen: false,
        groupSort: true,
        gName: '',
        gType: 0,
        groupMenuOpen: '',
        extendSide: true,
    };
  
//   shouldComponentUpdate(nextProps, nextState) {
// 	if(this.props.open == nextProps.open) {
//         if(this.state.view ) {

//         }
// 		return false;
//     }
// 	return true;
//   }

//   static getDerivedStateFromProps(nextProps, prevState) {
//       if(prevState.open !== nextProps.open && nextProps.open) {
//         nextProps.funcStore.conn('get', '/stoItems/' + nextProps.program.id + '/get', false, true, null, (res)=>{
//             if(res.result == "true") {
//                 return {
//                     stoItems: res.stoItems
//                 }
//             } else {
//                 return [];
//             }
//         });
//       }
//       return null;
//   }
    static getDerivedStateFromProps(nextProps, prevState) {
        if(prevState.open == nextProps.open && !nextProps.open) {
            return {
                stoItems: [],
                stoChilds: [],
                view: 'new',
                selectedSto: '',
                name: '',
                rows: [],
                target: [{
                    text: '', status: '0'
                }],
                points: [],
                standardPerc: 90,
                standardCnt: 1,
                promptCode: '',
                promptMemo: '',
                scheduleMemo: '',
                programMemo: '',
                extendSide: true,
            }
        }
        return true;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.open !== this.props.open && this.props.open && this.props.program) {
            return {
                refresh: true
            }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot && snapshot.refresh) {
            //this.loadStoItems();
            this.loadStoGroups();
        }
    }

    loadStoGroups = () => {
        const { funcStore } = this.props;
        funcStore.conn('get', '/stoGroup/' + this.props.program.id + '/get', false, true, null, (res)=>{
            if(res.result == "true" && res.stoGroups[0]) {
                const t = res.stoGroups[0].stoItems ? res.stoGroups[0].stoItems[0] : '';
                this.setState({
                    stoGroups: res.stoGroups,
                    selectedGroup: (res.stoGroups.length ? res.stoGroups[0] : ''),
                    groupMenuOpen: (res.stoGroups.length ? res.stoGroups[0].id : ''),
                    selectedSto: (res.stoGroups.length && t ? t : ''),
                    view: (res.stoGroups.length ? 'detail' : 'empty'),
                });
                // this.setState({
                //     stoItems: res.stoItems,
                //     stoChilds: res.stoChilds,
                //     stoTargets: res.stos,
                //     selectedSto: (res.stoItems.length ? res.stoItems[0] : ''),
                //     view: (res.stoItems.length ? 'detail' : 'empty')
                // });
                // this.forceUpdate();
            } else {
                this.setState({
                    stoGroups: [],
                    selectedGroup: '',
                    groupMenuOpen: '',
                    selectedSto: '',
                    view: 'empty',
                });
                console.log("STO 그룹 로딩 실패(fail)");
            }
        });
    }

    loadStoItems = () => {
        const { funcStore } = this.props;
        funcStore.conn('get', '/stoItems/' + this.props.program.id + '/get', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoItems: res.stoItems,
                    stoChilds: res.stoChilds,
                    stoTargets: res.stos,
                    selectedSto: (res.stoItems.length ? res.stoItems[0] : ''),
                    view: (res.stoItems.length ? 'detail' : 'empty')
                });
                this.forceUpdate();
            } else {
                this.setState({
                    stoItems: [],
                    stoTargets: [],
                });
                console.log("STO 로딩 실패(fail)");
            }
        });
    }

    handleView = view => {
        this.setState({
            view: view
        });
    }

    handleClose = () => {
        this.props.handleSlide(false);
    };

    handleOrder = (kind, id) => {
        const { stoGroups, selectedGroup } = this.state;
        let stoItems = selectedGroup.stoItems.slice();
        const index = stoItems.findIndex(v => v.id == id);
        if (index === -1) return;
        let original = stoItems[index];
        let newPos = 0, target = '';
        if(kind == 'up') {
            if(original) original.order += 1;
            newPos = index - 1;
            if (newPos < 0) {
                newPos = 0;
                original.order = stoItems.length;
            }
            stoItems.splice(index, 1);
            if(stoItems[newPos]) {
                stoItems[newPos].order -= 1;
                target = stoItems[newPos].id;
            }
        } else if(kind == 'down') {
            if(original) original.order -= 1;
            newPos = index + 1;
            if (newPos >= stoItems.length) {
                newPos = stoItems.length;
                original.order = 0;
            }
            stoItems.splice(index, 1);
            if(stoItems[index]) {
                stoItems[index].order += 1;
                target = stoItems[index].id;
            }
        }
        stoItems.splice(newPos, 0, original);
        // switch both
        const param = {
            groupId: selectedGroup.id,
            myId: original.id,
            targetId: target
        }
        this.props.funcStore.conn('post', '/stoItem/switchOrder', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoGroups: stoGroups.map(s => s.id == selectedGroup.id ? {
                        ...s, stoItems: stoItems
                    } : s),
                    selectedGroup: {
                        ...selectedGroup, stoItems: stoItems
                    }
                });
                this.forceUpdate();
            } else {
                console.log("순서 교체 실패(fail)");
            }
        });
    }

    handleGroupOrder = (kind, id) => {
        const { stoGroups } = this.state;
        const index = stoGroups.findIndex(v => v.id == id);
        if (index === -1) return;
        let original = stoGroups[index];
        let newPos = 0, target = '';
        if(kind == 'up') {
            if(original) original.order += 1;
            newPos = index - 1;
            if (newPos < 0) {
                newPos = 0;
                original.order = stoGroups.length;
            }
            stoGroups.splice(index, 1);
            if(stoGroups[newPos]) {
                stoGroups[newPos].order -= 1;
                target = stoGroups[newPos].id;
            }
        } else if(kind == 'down') {
            if(original) original.order -= 1;
            newPos = index + 1;
            if (newPos >= stoGroups.length) {
                newPos = stoGroups.length;
                original.order = 0;
            }
            stoGroups.splice(index, 1);
            if(stoGroups[index]) {
                stoGroups[index].order += 1;
                target = stoGroups[index].id;
            }
        }
        stoGroups.splice(newPos, 0, original);
        // switch both
        const param = {
            myId: original.id,
            targetId: target
        }
        
        this.props.funcStore.conn('post', '/stoGroup/switchOrder', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoGroups: stoGroups
                });
                this.forceUpdate();
            } else {
                console.log("순서 교체 실패(fail)");
            }
        });
    }

    handleChildOrder = (kind, id, parentId) => {
        const { stoChilds } = this.state;
        const arr = stoChilds[parentId];
        const index = arr.findIndex(v => v.id == id);
        if (index === -1) return;
        let original = arr[index];
        let newPos = 0, target = '';
        if(kind == 'up') {
            if(original) original.order += 1;
            newPos = index - 1;
            if (newPos < 0) {
                newPos = 0;
                original.order = arr.length;
            }
            arr.splice(index, 1);
            if(arr[newPos]) {
                arr[newPos].order -= 1;
                target = arr[newPos].id;
            }
        } else if(kind == 'down') {
            if(original) original.order -= 1;
            newPos = index + 1;
            if (newPos >= arr.length) {
                newPos = arr.length;
                original.order = 0;
            }
            arr.splice(index, 1);
            if(arr[index]) {
                arr[index].order += 1;
                target = arr[index].id;
            }
        }
        arr.splice(newPos, 0, original);
        // switch both
        const param = {
            myId: original.id,
            targetId: target
        }
        
        this.props.funcStore.conn('post', '/stoItem/switchOrder', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoChilds: {
                        ...stoChilds,
                        [parentId] : arr
                    }
                });
                this.forceUpdate();
            } else {
                console.log("순서 교체 실패(fail)");
            }
        });
    }

    hcandleChangeParent = e => {
        const v = e.target.value;
        if(v) {
            const t = v.split("?=");
            this.setState({
                parentId: parseInt(t[0]),
                parentName: t[1]
            });
        } else {
            this.setState({
                parentId: '',
                parentName: ''
            });
        }
    }

    handleChange = name => e => {
        if(name == "searchTarget") {
            const { stoTargets } = this.state;
            const tId = e.target.value;
            const t = stoTargets.filter(v => v.id == tId);
            if(t.length && t[0].target) {
                this.setState({
                    target: t[0].target
                });
            }
        }
        this.setState({
            [name]: e.target.value
        });
    };

    handleChangeTarget = (idx, name) => e => {
        const { target } = this.state;
        if(idx == target.length - 1) {
            const t = target.map((t,i) => i == idx ?
            {
                ...t,
                [name]: e.target.value
            } : t);

            this.setState({
                target: t.concat({ text: '', status: 0 })
            });
        } else {
            if(!e.target.value) {
                this.handleChangeTargetRemove(idx);
            } else {
                this.setState({
                    target: target.map((t,i) => i == idx ?
                        {
                            ...t,
                            [name]: e.target.value
                        } : t
                    )
                });
            }
        }
    };

    handleChangeTargetRemove = idx => {
        const { target } = this.state;
        this.setState({
            target: target.filter((t,i) => i !== idx)
        });
    }

    handleChangeSelectedTarget = (idx, name) => e => {
        const { selectedGroup, selectedSto, stoGroups, stoItems } = this.state;
        const target = selectedSto.target;
        if(idx == target.length - 1) {
            const tmp = target.map((t,i) => i == idx ? {
                    ...t,
                    [name]: e.target.value
                } : t);
            const t2 = tmp.concat({ text: '', status: 0 });
            this.setState({
                stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                    ...s, stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                        ...v, target: t2.concat({ text: '', status: 0 })
                    } : v)
                } : s),
                selectedSto: {
                    ...selectedSto,
                    target: t2.concat({ text: '', status: 0 })
                }
            });
        } else {
            if(!e.target.value) {
                this.handleChangeSelectedTargetRemove(idx);
            } else {
                const tmp = target.map((t,i) => i == idx ? {
                    ...t,
                    [name]: e.target.value
                } : t);
                this.setState({
                    stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                        ...s, stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                            ...v, target: tmp
                        } : v)
                    } : s),
                    selectedSto: {
                        ...selectedSto,
                        target: tmp
                    }
                });
            }
        }
    };

    handleChangeSelectedTargetRemove = idx => {
        const { selectedGroup, selectedSto, stoGroups, stoItems } = this.state;
        const tmp = selectedSto.target.filter((t,i) => i !== idx);
        this.setState({
            stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                ...s, stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                    ...v, target: tmp
                } : v)
            } : s),
            selectedSto: {
                ...selectedSto,
                target: tmp
            }
        });
    }

    handleChangeOption = (order, name) => event => {
        const { rows } = this.state;
        this.setState({
            rows: rows.map(row => row.order == order ?
                {
                    ...row,
                    [name]: event.target.value
                } : row
            )
        });
    };

    removeOption = order => {
        const { rows } = this.state;
        this.setState({
            rows: rows.filter(row => row.order !== order)
        });
    };

    setLabelWidth = (kind, ref) => {
        const { percLabelWidth, cntLabelWidth, parentLabelWidth, targetLabelWidth, promptLabelWidth, typeLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != percLabelWidth) {
            this.setState({ percLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != cntLabelWidth) {
            this.setState({ cntLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != parentLabelWidth) {
            this.setState({ parentLabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != targetLabelWidth) {
            this.setState({ targetLabelWidth: ref.offsetWidth });
        } else if(kind == 5 && ref.offsetWidth != promptLabelWidth) {
            this.setState({ promptLabelWidth: ref.offsetWidth });
        } else if(kind == 6 && ref.offsetWidth != typeLabelWidth) {
            this.setState({ typeLabelWidth: ref.offsetWidth });
        }
    }

    addRow = () => {
        const { rows } = this.state;
        this.setState({
            rows: rows.concat({
                order: ++optionOrder,
                label: '',
                text: ''
            })
        });
    }

    handleGraphSlide = flag => {
	    this.setState({
		    graphOpen: flag 
	    });
    }

    openStoItemGraph = () => {
        const { selectedSto } = this.state;
        this.setState({
            graphOpen: true,
            graphStoItem: selectedSto,
            graphStoGroup: '',
        });
    }

    openStoGroupGraph = () => {
        const { selectedGroup } = this.state;
        this.setState({
            graphOpen: true,
            graphStoItem: '',
            graphStoGroup: selectedGroup,
        });
    }

    handleFinish = kind => {
        const { selectedGroup, selectedSto, stoGroups } = this.state;
        if(selectedSto.finish == kind) return;
        if(!confirm("STO 상태를 변경하시겠습니까?\nChange state of Short Term Objective?")) return;
        
        if(selectedSto.id) {
            const param = {
                status: kind
            }
            this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/status', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        selectedSto: {
                            ...selectedSto,
                            finish: kind
                        },
                        stoGroups: stoGroups.map(v => v.id == selectedGroup.id ? {
                            ...v, stoItems: v.stoItems ? v.stoItems.map(sto => sto.id == selectedSto.id ? {
                                ...sto, finish: kind
                            } : sto) : []
                        } : v),
                    });
                } else {
                    console.log("STO 그룹 상태 수정 오류(rrror)");
                }
            });
        } else return;
    }

    handleGroupFinish = kind => {
        const { selectedGroup, stoGroups } = this.state;
        if(selectedGroup.finish == kind) return;
        if(!confirm("LTO 상태를 변경하시겠습니까?\nChange state of Long Term Objective?")) return;
        
        if(selectedGroup.id) {
            const param = {
                status: kind
            }
            this.props.funcStore.conn('post', '/stoGroup/'+selectedGroup.id+'/status', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        selectedGroup: {
                            ...selectedGroup,
                            finish: kind
                        },
                        stoGroups: stoGroups.map(v => v.id == selectedGroup.id ? {
                            ...v, finish: kind
                        } : v),
                    });
                } else {
                    console.log("LTO 상태 수정 오류(error)");
                }
            });
        } else return;
    }

    validation = () => {
        return true;
    }

    register = () => {
        if(!this.validation()) return;
        const { selectedGroup, stoGroups, rows, target, standardPerc, standardCnt, parentId, parentName,
            promptCode, promptMemo, scheduleMemo, programMemo } = this.state;
        const param = {
            programId: this.props.program.id,
            groupId: selectedGroup.id,
            stoId: '',
            parentId: parentId,
            parentName: parentName,
            standardPerc: standardPerc ? standardPerc : 0,
            standardCnt: standardCnt,
            rows: rows,
            target: target ? target.filter(v => v.text != "") : [],
            promptCode: promptCode ? promptCode : '',
            promptMemo: promptMemo,
            scheduleMemo: scheduleMemo,
            programMemo: programMemo
        }
        this.props.funcStore.conn('post', '/stoItem/post', false, true, param, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoItem];
                this.setState({
                    stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                        ...s, stoItems: s.stoItems ? temp.concat(s.stoItems) : temp
                    } : s),
                    selectedGroup: {
                        ...selectedGroup,
                        stoItems: selectedGroup.stoItems ? temp.concat(selectedGroup.stoItems) : temp
                    },
                    selectedSto: res.stoItem
                });
                this.handleView('detail');
                this.handleOpen(false);
                //this.loadStoItems();
            } else {
                console.log("STO 추가 오류(error)");
            }
        });
    }

    duplicateSto = stoItem => {
        const { selectedGroup, stoGroups } = this.state;
        this.props.funcStore.conn('post', '/stoItem/'+stoItem.id+'/duplicate', false, true, null, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoItem];
                this.setState({
                    stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                        ...s, stoItems: temp.concat(s.stoItems)
                    } : s),
                    selectedGroup: {
                        ...selectedGroup,
                        stoItems: selectedGroup.stoItems ? temp.concat(selectedGroup.stoItems) : temp
                    },
                    selectedSto: res.stoItem
                });
                alert("복사 완료");
                this.handleView('detail');
                this.handleOpen(false);
                this.forceUpdate();
            } else {
                console.log("STO 복사 오류");
            }
        });
    }

    editSto = () => {
        if(!this.validation()) return;
        const { selectedGroup, selectedSto, stoGroups, rows, target, standardPerc, standardCnt, pointFlag,
            promptCode, promptMemo, scheduleMemo, programMemo } = this.state;
        const param = {
            groupId: selectedGroup.id,
            standardPerc: standardPerc,
            standardCnt: standardCnt,
            rows: rows,
            target: target ? target.filter(v => v.text != "") : [],
            promptCode: promptCode ? promptCode : '',
            promptMemo: promptMemo,
            scheduleMemo: scheduleMemo,
            programMemo: programMemo
        }
        this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/put', false, true, param, (res)=>{
            if(res.result == "true" && res.stoItem) {
                //this.loadStoGroups();
                this.setState({ selectedSto: res.stoItem });
                this.forceUpdate();
                alert("수정을 완료하였습니다");
                if(selectedSto.standardPerc != standardPerc) {
                    this.setState({ pointFlag: !pointFlag });
                }
                this.handleOpen(false);
            } else {
                console.log("STO 수정 오류(error)");
            }
        });
    }

    removeSto = () => {
        if(!confirm("해당 STO를 삭제하시겠습니까?\nRemove STO?")) return;
        const { selectedGroup, selectedSto, stoGroups, stoItems, pointFlag } = this.state;
        this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                //this.loadStoGroups();
                let tmp = selectedGroup.stoItems.filter(sto => sto.id != selectedSto.id);
                this.setState({
                    stoGroups: stoGroups.map(s => s.id === selectedGroup.id ? {
                        ...s, stoItems: tmp
                    } : s),
                    selectedGroup: {
                        ...selectedGroup,
                        stoItems: tmp
                    },
                    selectedSto: (tmp && tmp[0]) ? tmp[0] : ''
                });
                this.handleOpen(false);
                this.forceUpdate();
            } else {
                console.log("STO 삭제 오류(error)");
            }
        });
    }

    addNewGroup = () => {
        if(!this.validation()) return;
        const { stoGroups, gName, gType } = this.state;
        const param = {
            programId: this.props.program.id,
            name: gName,
            type: gType
        }
        this.props.funcStore.conn('post', '/stoGroup/post', false, true, param, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoGroup];
                this.setState({
                    stoGroups: temp.concat(stoGroups),
                    selectedGroup: res.stoGroup,
                    groupMenuOpen: res.stoGroup.id
                });
                this.handleView('detail');
                this.handleGroupOpen(false);
            } else {
                console.log("STO 그룹 추가 오류(error)");
            }
        });
    }

    editStoGroup = () => {
        if(!this.validation()) return;
        const { selectedGroup, stoGroups, gName, gType } = this.state;
        const param = {
            name: gName,
            type: gType
        }
        this.props.funcStore.conn('post', '/stoGroup/'+selectedGroup.id+'/put', false, true, param, (res)=>{
            if(res.result == "true") {
                const newGroup = {
                    ...selectedGroup, name: gName, type: gType
                }
                this.setState({
                    stoGroups: stoGroups.map(s => s.id == selectedGroup.id ? newGroup : s),
                    selectedGroup: newGroup
                });
                alert("수정 완료");
                this.handleGroupOpen(false);
            } else {
                console.log("STO 그룹 수정 오류(error)");
            }
        });
    }

    removeStoGroup = () => {
        if(!confirm("해당 LTO를 삭제하시겠습니까?\nRemove LTO?")) return;
        const { selectedGroup, stoGroups } = this.state;
        this.props.funcStore.conn('post', '/stoGroup/'+selectedGroup.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                //this.loadStoGroups();
                let tmp = stoGroups.filter(sg => sg.id != selectedGroup.id);console.log(tmp[0]);
                this.setState({
                    stoGroups: tmp,
                    groupMenuOpen: tmp && tmp[0] ? tmp[0].id : '',
                    selectedGroup: tmp && tmp[0] ? tmp[0] : '',
                    selectedSto: (tmp && tmp[0]) ? (tmp[0].stoItems && tmp[0].stoItems[0]) ? tmp[0].stoItems[0] : '' : ''
                });
                this.handleGroupOpen(false);
                this.forceUpdate();
            } else {
                console.log("STO 삭제 오류(error)");
            }
        });
    }

    duplicateStoGroup = stoGroup => {
        const { stoGroups } = this.state;
        this.props.funcStore.conn('post', '/stoGroup/'+stoGroup.id+'/duplicate', false, true, null, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoGroup];
                this.setState({
                    stoGroups: temp.concat(stoGroups),
                    selectedGroup: res.stoGroup
                });
                alert("복사 완료(Complete)");
                this.handleView('detail');
                this.handleOpen(false);
                this.forceUpdate();
            } else {
                console.log("LTO 복사 오류");
            }
        });
    }

    stoStatusClass = (status) => {
        const { classes } = this.props;
        let clname = "";
        if(status == -1) {
            clname = classes.canceledSto;
        } else if(status == 1) {
            clname = classes.finishedSto;
        } else if(status == 2) {
            clname = classes.stoppedSto;
        }
        return clname;
    }

    openNewDialog = () => {
        this.setState({
            dialogOpen: true,
            dialogSort: true,
            standardCnt: this.state.selectedGroup.type == 0 ? 1 : 3
        });
    }

    openEditDialog = () => {
        const { selectedSto } = this.state;
        const tmp = {
            text: '', status: '0'
        };
        this.setState({
            dialogOpen: true,
            dialogSort: false,
            name: selectedSto.name,
            parentId: selectedSto.parent_id,
            parentName: selectedSto.parent_name,
            standardPerc: selectedSto.standard_perc,
            standardCnt: selectedSto.standard_cnt,
            rows: selectedSto.rows ? selectedSto.rows : [],
            target: selectedSto.target ? selectedSto.target.concat(tmp) : [tmp],
            promptCode: selectedSto.prompt_code ? selectedSto.prompt_code : '',
            promptMemo: selectedSto.prompt_memo,
            scheduleMemo: selectedSto.schedule_memo,
            programMemo: selectedSto.program_memo,
        });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    openNewGroupDialog = () => {
        this.setState({
            groupOpen: true,
            groupSort: true
        });
    }

    openEditGroupDialog = () => {
        const { selectedGroup } = this.state;
        this.setState({
            groupOpen: true,
            groupSort: false,
            gName: selectedGroup.name,
            gType: selectedGroup.type
        });
    }

    handleGroupOpen = flag => {
        if(!flag) {
		    this.resetGroupDialog();
	    }
        this.setState({ groupOpen: flag });
    }

    resetDialog = () => {
        this.setState({
            name: '',
            stoId: '',
            parentId: '',
            parentName: '',
            standardPerc: 90,
            standardCnt: this.state.selectedGroup.type == 0 ? 1 : 3,
            rows: [],
            target: [{
                text: '', status: '0'
            }],
            promptCode: '',
            promptMemo: '',
            scheduleMemo: '',
            programMemo: '',
        });
    }

    resetGroupDialog = () => {
        this.setState({
            gName: '',
            gType: 0
        });
    }

    handleGroupCollapse = groupId => {
        this.setState({
            groupMenuOpen: this.state.groupMenuOpen == groupId ? '' : groupId
        })
    }

    calcDecisions = () => {
        if(!confirm("모든 디시전을 재계산 하시겠습니까?\nCalculate All decisions again")) return;
        const { selectedSto } = this.state;
        const res = this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/calcDecisions', false, false, null, null);
        console.log(res);
    }

    updateSelectedStoItem = stoItem => {
        const { selectedSto } = this.state;
        this.setState({
            selectedSto: {
                ...selectedSto,
                total_reach: stoItem.total_reach,
                standard_perc: stoItem.standard_perc,
                standard_cnt: stoItem.standard_cnt
            }
        });
    }

    toggleSideExtend = () => {
        this.setState({
            extendSide: !this.state.extendSide
        });
    }

    // <FormControl variant="outlined" className={classes.formControl}>
    //     <InputLabel
    //         ref={ref => {
    //             if(ref) this.setLabelWidth(3, ref);
    //         }}
    //         htmlFor="parent-label"
    //     >
    //         부모 STO
    //     </InputLabel>
    //     <Select
    //         value={parentId ? parentId + "?=" + parentName : ""}
    //         onChange={this.hcandleChangeParent}
    //         input={
    //         <OutlinedInput
    //             labelWidth={parentLabelWidth}
    //             name="parentLabelWidth"
    //             id="parent-label"
    //         />
    //         }
    //     >
    //         <MenuItem value="">
    //             <em>선택</em>
    //         </MenuItem>
    //         {parentList.map((v,i) => {
    //             return (
    //                 <MenuItem key={"p_"+i} value={v.id + "?=" + v.name}>
    //                     {v.name}
    //                 </MenuItem>
    //             );
    //         })}
    //     </Select>
    // </FormControl>

    // <div className={classes.optionBox}>
    //     <Typography variant="h6" color="inherit" className={classes.titleRow}>
    //         {"추가 옵션 입력"}
    //     </Typography>
    //     <div>
    //         {rows ? rows.map((v,i) => {
    //             return (
    //                 <div key={i} autoComplete="off" className={classes.optonContent}>
    //                     <Typography variant="subtitle2" color="inherit" className={classes.optionOrder}>
    //                         {(i + 1) + "# option // "}
    //                     </Typography>
    //                     <TextField
    //                         label="LABEL"
    //                         className={classNames(classes.textField, classes.optionLabel)}
    //                         value={v.label}
    //                         onChange={this.handleChangeOption(v.order, 'label')}
    //                         margin="normal"
    //                         variant="outlined"
    //                     />
    //                     <TextField
    //                         label="CONTENT"
    //                         className={classNames(classes.textField, classes.optionText)}
    //                         value={v.text}
    //                         onChange={this.handleChangeOption(v.order, 'text')}
    //                         margin="normal"
    //                         variant="outlined"
    //                     />
    //                     <IconButton aria-label="remove" className={classes.optionRemove}
    //                         onClick={
    //                             () => this.removeOption(v.order)}>
    //                         <DeleteIcon  />
    //                     </IconButton>
    //                 </div>
    //             );
    //         }) : null}
    //     </div>
    // </div>
    // <div className={classes.listIconBox}>
    //     <Fab size="small" variant="extended" aria-label="prev" className={classes.fab}
    //         onClick={
    //             () => this.addRow()}>
    //         <PlaylistAdd  />
    //     </Fab>
    // </div>

  render() {
    const { mainStore, classes, open, program, student, order } = this.props;
    const { stoGroups, stoItems, stoChilds,
        view, selectedSto, selectedGroup, name, target, rows, points,
        standardPerc, standardCnt,
        promptCode, promptMemo, scheduleMemo, programMemo,
        percLabelWidth, cntLabelWidth, targetLabelWidth, promptLabelWidth, typeLabelWidth,
        graphOpen, graphStoItem, graphStoGroup,
        stoTargets, searchTarget,
        dialogOpen, dialogSort, pointFlag,
        groupOpen, groupSort, gName, gType, groupMenuOpen, extendSide } = this.state;

    const user = mainStore.user;
    const lang = this.props.mainStore.getLang(this.props.mainStore.country);
    const content = program && selectedGroup ? (
        <div className={classes.stoGroupRoot}>
            <div className={classes.stoGroupBox}>
                {(user.id == program.user_id || user.level > 2) && (
                    <div className={classes.topBtns}>
                        <div className={classes.topSettingBtns}>
                            <Fab size="small" aria-label="edit" className={classNames(classes.fab)}
                                onClick={
                                    () => this.openEditGroupDialog()}
                            >
                                <Edit />
                            </Fab>
                            <Fab size="small" aria-label="copy" className={classNames(classes.fab)}
                                onClick={
                                    () => this.duplicateStoGroup(selectedGroup)}
                            >
                                <FileCopy />
                            </Fab>
                        </div>
                        {selectedGroup && (user.id == program.user_id || user.level > 2) ? (
                            <ButtonGroup color="primary" aria-label="Status" className={classes.statusBtn}>
                                <Button
                                    className={selectedGroup.finish == 0 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleGroupFinish(0)}}
                                >{lang['stoitem15']}</Button>
                                <Button
                                    className={selectedGroup.finish == 1 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleGroupFinish(1)}}
                                >{lang['stoitem18']}</Button>
                                <Button
                                    className={selectedGroup.finish == 2 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleGroupFinish(2)}}
                                >{lang['stoitem17']}</Button>
                                <Button
                                    className={selectedGroup.finish == -1 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleGroupFinish(-1)}}
                                >{lang['stoitem19']}</Button>
                            </ButtonGroup>
                        ) : null}
                    </div>    
                )}
                <div className={classes.stoGrouptitlebox}>
                    <div className={classes.stoGrouptitle}>
                        <Typography variant="h5" color="inherit" className={classes.mainTitle}>
                            {"LTO PROGRAM Details"}
                        </Typography>
                        <Button
                            variant="outlined" className={classes.stoChartBtn}
                            onClick={(e)=>{
                                e.preventDefault();
                                this.openStoGroupGraph()
                            }}
                        ><MultilineChart /></Button>
                        {(user.level > 0) && ((user.level < 10 && user.id == program.user_id) || (user.level >= 10)) ? (<Button
                            variant="outlined"
                            color="default"
                            size="small"
                            className={classNames(classes.margin, classes.newStoBtn)}
                            startIcon={<AddIcon />}
                            onClick={()=>this.openNewDialog()}
                        >
                            NEW STO
                        </Button>) : null}
                    </div>
                    <div className={classes.contentText}>
                        {selectedGroup.name ? "\nLTO : " + selectedGroup.name : ''}
                        {"\n"+lang['stoitem20']+" : " + (selectedGroup.type == 0 ? lang['stoitem21'] : lang['stoitem22'])}
                    </div>
                </div>
            </div>
            <div className={classes.stoRootContainer}>
                <div className={classes.stoRootBox}>
                    <div className={classes.stoRootTitleBox}>
                        <div className={classes.titleBox}>
                            <Typography variant="h5" color="inherit" className={classes.mainTitle}>
                                {"STO Details"}
                            </Typography>
                            {selectedGroup.type == 0 && (
                                <Button
                                    variant="outlined" className={classes.stoChartBtn}
                                    onClick={(e)=>{
                                        e.preventDefault();
                                        this.openStoItemGraph()
                                    }}
                                ><ShowChart /></Button>
                            )}
                        </div>
                        
                        {selectedGroup.stoItems && selectedSto ? (
                            <div className={classes.contentText}>
                                {selectedSto.name ? "\n"+lang['name']+" : " + selectedSto.name : ''}
                                {selectedGroup.type == 0 ? "\n"+lang['stoitem1']+" : " + selectedSto.standard_perc : ""}
                                {"\n"+lang['stoitem2']+" : " + selectedSto.standard_cnt}
                                {"\n"+lang['stoitem3']+" : " + (selectedSto.total_reach > 0 ? "O" : "X")}
                                {"\n"+lang['stoitem4']+" : " + (selectedSto.target ? selectedSto.target.map((v,i) => v.text) : "")}
                            </div>
                        ) : (
                            <div className={classes.contentText}>
                                STO is empty
                            </div>
                        )}
                    </div>
                    <div className={classes.stoTopBtns}>
                        {(user.id == program.user_id || user.level > 2) && (
                            <div className={classes.topSettingBtns}>
                                <Fab size="small" aria-label="calc" className={classNames(classes.fab)}
                                    disabled={selectedSto ? false : true}
                                    onClick={() => this.calcDecisions()}
                                >
                                    <Replay />
                                </Fab>
                                <Fab size="small" aria-label="edit" className={classNames(classes.fab)}
                                    disabled={selectedSto ? false : true}
                                    onClick={() => this.openEditDialog()}
                                >
                                    <Edit />
                                </Fab>
                                <Fab size="small" aria-label="copy" className={classNames(classes.fab)}
                                    disabled={selectedSto ? false : true}
                                    onClick={() => this.duplicateSto(selectedSto)}
                                >
                                    <FileCopy />
                                </Fab>
                            </div>
                        )}
                        {selectedSto && (user.id == program.user_id || user.level > 2) ? (
                            <ButtonGroup color="primary" aria-label="Status" className={classes.statusBtn}>
                                <Button
                                    className={selectedSto.finish == 0 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleFinish(0)}}
                                >{lang['stoitem15']}</Button>
                                <Button
                                    className={selectedSto.finish == 1 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleFinish(1)}}
                                >{lang['stoitem18']}</Button>
                                <Button
                                    className={selectedSto.finish == 2 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleFinish(2)}}
                                >{lang['stoitem17']}</Button>
                                <Button
                                    className={selectedSto.finish == -1 ? classes.onFinishBtn : classes.unFinishBtn}
                                    onClick={()=>{this.handleFinish(-1)}}
                                >{lang['stoitem19']}</Button>
                            </ButtonGroup>
                        ) : null}
                    </div>
                </div>
            </div>
            {selectedGroup.stoItems && selectedSto ? (
                <div className={classes.pointRootBox}>
                    <div className={classes.titleBox}>
                        <Typography variant="h5" color="inherit" className={classes.mainPointTitle}>
                            {"POINTS List"}
                        </Typography>
                    </div>
                    <Points
                        stoItem={selectedSto}
                        updateSelectedStoItem={this.updateSelectedStoItem}
                        list={points}
                        flag={pointFlag}
                        standardForStack={selectedSto.standard_cnt}
                        auth={(user.id == program.user_id || user.level > 2)}
                    />
                </div>
            ) : null}
        </div>
    ) : (
        <div className={classes.stoGroupRoot}>
            LTO Program is empty
        </div>
    );

    const dialogButton = (!dialogSort && program) ? (
        <div className={classes.dialogBtn}>
                <Fab size="small" variant="round" aria-label="save" className={classNames(classes.fab)}
                    disabled={!(user.id == program.user_id || user.level > 2)}
                    onClick={() => this.editSto()}>
                    <Save />
                </Fab>
                <Fab size="small" variant="round" aria-label="remove" className={classNames(classes.fab)}
                    disabled={!(user.id == program.user_id || user.level > 2)}
                    onClick={
                        () => this.removeSto()}>
                    <Delete />
                </Fab>
            </div>
    ) : null;

    const dialogForm = (
        <div>
            <form className={classes.container} noValidate autoComplete="off">
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {lang['sto1']}
                </Typography>
                {selectedGroup.type == 0 ? (
                    <div className={classes.standardBox}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(1, ref);
                                }}
                                htmlFor="perc-label"
                            >
                                {lang['sto2']}
                            </InputLabel>
                            <Select
                                value={standardPerc}
                                onChange={this.handleChange('standardPerc')}
                                input={
                                <OutlinedInput
                                    labelWidth={percLabelWidth}
                                    name="percLabelWidth"
                                    id="perc-label"
                                />
                                }
                            >
                                <MenuItem value={"70"}>{"70%"}</MenuItem>
                                <MenuItem value={"75"}>{"75%"}</MenuItem>
                                <MenuItem value={"80"}>{"80%"}</MenuItem>
                                <MenuItem value={"85"}>{"85%"}</MenuItem>
                                <MenuItem value={"90"}>{"90%"}</MenuItem>
                                <MenuItem value={"95"}>{"95%"}</MenuItem>
                                <MenuItem value={"100"}>{"100%"}</MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(2, ref);
                                }}
                                htmlFor="count-label"
                            >
                                {lang['sto4']}
                            </InputLabel>
                            <Select
                                value={standardCnt}
                                onChange={this.handleChange('standardCnt')}
                                input={
                                    <OutlinedInput
                                        labelWidth={cntLabelWidth}
                                        name="cntLabelWidth"
                                        id="count-label"
                                    />
                                }
                            >
                                <MenuItem value={"1"}>{"1"}</MenuItem>
                                <MenuItem value={"2"}>{"2"}</MenuItem>
                                <MenuItem value={"3"}>{"3"}</MenuItem>
                                <MenuItem value={"4"}>{"4"}</MenuItem>
                                <MenuItem value={"5"}>{"5"}</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                ) : (
                    <div className={classes.standardBox}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(2, ref);
                                }}
                                htmlFor="count-label"
                            >
                                {lang['sto3']}
                            </InputLabel>
                            <Select
                                value={standardCnt}
                                onChange={this.handleChange('standardCnt')}
                                input={
                                    <OutlinedInput
                                        labelWidth={cntLabelWidth}
                                        name="cntLabelWidth"
                                        id="count-label"
                                />
                                }
                            >
                                <MenuItem value={"3"}>{"3"}</MenuItem>
                                <MenuItem value={"4"}>{"4"}</MenuItem>
                                <MenuItem value={"5"}>{"5"}</MenuItem>
                                <MenuItem value={"6"}>{"6"}</MenuItem>
                                <MenuItem value={"7"}>{"7"}</MenuItem>
                            </Select>
                        </FormControl>
                    </div>
                )}
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {"STO " + lang['sto5']}
                </Typography>
                <div className={classes.standardBox} id="target_box">
                    {target ? target.map((t,i) => {
                        return (
                            <TextField
                                key={"t_"+i}
                                label={"STO " + lang['sto6']}
                                className={classNames(classes.textField, classes.newText)}
                                value={t.text ? t.text : ""}
                                onChange={this.handleChangeTarget(i, 'text')}
                                margin="normal"
                                variant="outlined"
                                InputProps={{
                                    endAdornment: t.text ? (
                                        <InputAdornment position="end">
                                            <IconButton
                                                onClick={() => this.handleChangeTargetRemove(i)}
                                            >
                                                <HighlightOff />
                                            </IconButton>
                                        </InputAdornment>
                                    ) : null
                                }}
                            />
                        );
                    }) : null}
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {lang['sto7']}
                </Typography>
                <div className={classes.standardBox}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(5, ref);
                            }}
                            htmlFor="prompt-label"
                        >
                            {lang['sto8']}
                        </InputLabel>
                        <Select
                            value={promptCode}
                            onChange={this.handleChange('promptCode')}
                            input={
                            <OutlinedInput
                                labelWidth={promptLabelWidth}
                                name="promptLabelWidth"
                                id="prompt-label"
                            />
                            }
                        >
                            <MenuItem value="">선택</MenuItem>
                            {promptArr.map((p,i) => {
                                return (
                                    <MenuItem key={'p_'+i} value={p.id}>{p.name}</MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <TextField
                        label={lang['sto9']}
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={promptMemo ? promptMemo : ""}
                        onChange={this.handleChange('promptMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {lang['sto10']}
                </Typography>
                <div className={classes.standardBox}>
                    <TextField
                        label={lang['sto13']}
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={scheduleMemo ? scheduleMemo : ""}
                        onChange={this.handleChange('scheduleMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {lang['sto11']}
                </Typography>
                <div className={classes.standardBox}>
                    <TextField
                        label={lang['sto12']}
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={programMemo ? programMemo : ""}
                        onChange={this.handleChange('programMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                </div>
            </form>
        </div>
    );

    let newLtoForm = null;
    if(user.level > 0 && (open && program)) {
        if(user.level < 10) {
            if(user.id == program.user_id) {
                newLtoForm = (
                    <Button
                        variant="outlined"
                        color="default"
                        size="small"
                        className={classNames(classes.margin, classes.graphTopBtn)}
                        startIcon={<AddIcon />}
                        onClick={()=>this.openNewGroupDialog()}
                    >
                        NEW LTO PROGRAM
                    </Button>
                );
            }
        } else {
            newLtoForm = (
                <Button
                    variant="outlined"
                    color="default"
                    size="small"
                    className={classNames(classes.margin, classes.graphTopBtn)}
                    startIcon={<AddIcon />}
                    onClick={()=>this.openNewGroupDialog()}
                >
                    NEW LTO PROGRAM
                </Button>
            );
        }
    }

    const groupButton = (!groupSort && program) ? (
        <div className={classes.dialogBtn}>
            <Fab size="small" variant="round" aria-label="save" className={classNames(classes.fab)}
                disabled={!(user.id == program.user_id || user.level > 2)}
                onClick={() => this.editStoGroup()}>
                <Save />
            </Fab>
            <Fab size="small" variant="round" aria-label="remove" className={classNames(classes.fab)}
                disabled={!(user.id == program.user_id || user.level > 2)}
                onClick={
                    () => this.removeStoGroup()}>
                <Delete />
            </Fab>
        </div>
    ) : null;

    const groupForm = (
        <div>
            <form className={classes.groupContainer} noValidate autoComplete="off">
                <TextField
                    id="outlined-name"
                    label={"LTO " + lang['sto6']}
                    className={classes.groupName}
                    value={gName ? gName : ''}
                    onChange={this.handleChange('gName')}
                    variant="outlined"
                />
                <FormControl variant="outlined" className={classes.groupSelect}>
                    <InputLabel
                        ref={ref => {
                            if(ref) this.setLabelWidth(6, ref);
                        }}
                        htmlFor="type-label"
                    >{lang['stoitem20']}</InputLabel>
                    <Select
                        value={gType ? gType : "0"}
                        onChange={this.handleChange('gType')}
                        input={
                            <OutlinedInput
                                labelWidth={typeLabelWidth}
                                name="typeLabelWidth"
                                id="type-label"
                            />
                        }
                    >
                        <MenuItem value="0">
                            {lang['stoitem21']}(Percentage / Counting)
                        </MenuItem>
                        <MenuItem value="1">
                            {lang['stoitem22']}(Accumulation)
                        </MenuItem>
                    </Select>
                </FormControl>
            </form>
        </div>
    );

    return (
        <div>
            {(open && program) ? (
                <React.Fragment>
                    <Dialog
                        fullScreen
                        open={open}
                        onClose={this.handleClose}
                        TransitionComponent={Transition}
                        BackdropProps={{
                            classes: {
                                root: classes.dialgoBack
                            }
                        }}
                        classes={{
                                root: classes.dialogRoot,
                                container: classes.dialogContainer,
                        }}
                    >
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <div className={classes.slideTitleBox}>
                                {program && (
                                    <Typography variant="h6" color="inherit">
                                        {"#"+order+". "+(student.student_name ? student.student_name : "")}
                                        {" / DOMAIN : " + (program.lto_name ? program.lto_name : "")}
                                    </Typography>
                                )}
                            </div>
                            {newLtoForm}
                            <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <div className={classes.root}>
                        <Grid container spacing={0} className={classes.gridRoot}>
                            <Grid item xs={4} className={classes.stosBox}>
                                <List className={classNames(classes.listRoot, extendSide ? classes.extended : classes.folded)}>
                                    { stoGroups.map((v,i) => {
                                        const groupStatus = v.finish == 0 ? "진행중" : v.finish == 1 ? "완료" : v.finish == 2 ? "중지" : "취소";
                                        return (
                                            <React.Fragment key={i}>
                                                <ListItem id={v.id} className={classes.listBox}>
                                                    {selectedGroup.id == v.id && <div className={classes.selectedCheck}></div>}
                                                    <div className={classNames(classes.parentRoot, this.stoStatusClass(v.finish))}>
                                                        <Button
                                                            className={classes.listTextBox}
                                                            onClick={()=>{
                                                                this.handleGroupCollapse(v.id);
                                                                this.setState({
                                                                    selectedGroup: v,
                                                                    selectedSto: (v.stoItems ? v.stoItems[0] : '') 
                                                                });
                                                                this.handleView('detail');
                                                            }}
                                                        >{"["+groupStatus+"] " + v.name + " ["+(v.stoItems ? v.stoItems.length : 0)+"]"}</Button>
                                                        {(user.id == program.user_id || user.level > 2) && (
                                                            <div className={classes.listIconBox}>
                                                                <Fab size="small" variant="round" aria-label="prev"
                                                                    className={classNames(classes.fab, classes.arrowBtn)}
                                                                    onClick={
                                                                        () => this.handleGroupOrder('up', v.id)}
                                                                    disabled={i == 0 ? true : false}
                                                                    classes={{
                                                                        disabled: classes.arrowDisabled
                                                                    }}
                                                                >
                                                                    <ArrowDropUpIcon  />
                                                                </Fab>
                                                                <Fab size="small" variant="round" aria-label="next"
                                                                    className={classNames(classes.fab, classes.arrowBtn)}
                                                                    onClick={
                                                                        () => this.handleGroupOrder('down', v.id)}
                                                                    disabled={i == stoGroups.length - 1 ? true : false}
                                                                    classes={{
                                                                        disabled: classes.arrowDisabled
                                                                    }}
                                                                >
                                                                    <ArrowDropDownIcon />
                                                                </Fab>
                                                            </div>
                                                        )}
                                                    </div>
                                                </ListItem>
                                                <Collapse in={groupMenuOpen == v.id} timeout="auto" className={classes.collapseRoot} unmountOnExit>
                                                    <List component="div" disablePadding>
                                                        {v.stoItems ? v.stoItems.map((sto, idx) => {
                                                            const stoStatus = sto.finish == 0 ? "진행중" : sto.finish == 1 ? "완료" : sto.finish == 2 ? "중지" : "취소";
                                                            return (
                                                                <ListItem key={v.id+"_"+idx} className={classNames(classes.nested)}>
                                                                    {(selectedSto && selectedSto.id == sto.id) && <div className={classes.selectedCheck}></div>}
                                                                    <div className={classNames(classes.parentRoot)}>
                                                                        <Button
                                                                            className={classes.listTextBox}
                                                                            onClick={()=>{
                                                                                this.setState({
                                                                                    selectedSto: sto
                                                                                });
                                                                            }}
                                                                        >
                                                                            {sto.target ? sto.target.map((t, idx2) => {
                                                                                let tmp = idx2 > 0 ? " / " : "> ["+stoStatus+"] ";
                                                                                tmp += t.text;
                                                                                return tmp;
                                                                            }) : ""}
                                                                        </Button>
                                                                        {(user.id == program.user_id || user.level > 2) && (
                                                                            <div className={classes.listIconBox}>
                                                                                <Fab size="small" variant="round" aria-label="prev"
                                                                                    className={classNames(classes.fab, classes.arrowBtn, classes.itemArrow)}
                                                                                    onClick={
                                                                                        () => this.handleOrder('up', sto.id)}
                                                                                    disabled={idx == 0 ? true : false}
                                                                                    classes={{
                                                                                        disabled: classes.arrowDisabled
                                                                                    }}
                                                                                >
                                                                                    <ArrowDropUpIcon  />
                                                                                </Fab>
                                                                                <Fab size="small" variant="round" aria-label="next"
                                                                                    className={classNames(classes.fab, classes.arrowBtn, classes.itemArrow)}
                                                                                    onClick={
                                                                                        () => this.handleOrder('down', sto.id)}
                                                                                    disabled={idx == v.stoItems.length - 1 ? true : false}
                                                                                    classes={{
                                                                                        disabled: classes.arrowDisabled
                                                                                    }}
                                                                                >
                                                                                    <ArrowDropDownIcon />
                                                                                </Fab>
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                </ListItem>
                                                            )
                                                        }) : ''}
                                                    </List>
                                                </Collapse>
                                                <Divider className={classes.listDivider} />
                                            </React.Fragment>
                                        );
                                    })
                                    }
                                </List>
                                <div
                                    className={classes.extendBtn}
                                    onClick={()=>this.toggleSideExtend()}
                                >{extendSide ? (<ChevronLeft />) : (<ChevronRight />)}</div>
                            </Grid>
                            <Grid item style={{flex:1, backgroundColor:'#333', marginLeft: 40,}}>
                                {content}
                            </Grid>
                        </Grid>
                    </div>
                        <StoGraphSlide
                            open={graphOpen}
                            stoItem={graphStoItem}
                            stoGroup={graphStoGroup}
                            handleSlide={this.handleGraphSlide}
                        />
                        {dialogSort ? (
                            <Dialogs
                                kinds={"content"}
                                open={dialogOpen}
                                handleOpen={this.handleOpen}
                                title={"New STO"}
                                subButton={dialogButton}
                                content={dialogForm}
                                leftFunc={{
                                    label: lang['domain8'],
                                    func: ()=> {
                                        this.register();
                                    }
                                }}
                                rightFunc={{
                                    label: lang['domain9'],
                                    func: ()=> {
                                        this.handleOpen(false);
                                    }
                                }}
                            />
                        ) : (
                            <Dialogs
                                kinds={"content"}
                                open={dialogOpen}
                                handleOpen={this.handleOpen}
                                title={"STO Info"}
                                subButton={dialogButton}
                                content={dialogForm}
                                rightFunc={{
                                    label: lang['domain9'],
                                    func: ()=> {
                                        this.handleOpen(false);
                                    }
                                }}
                            />
                        )}

                        {groupSort ? (
                            <Dialogs
                                kinds={"content"}
                                open={groupOpen}
                                handleOpen={this.handleGroupOpen}
                                title={"New LTO"}
                                subButton={groupButton}
                                content={groupForm}
                                leftFunc={{
                                    label: "Confirm",
                                    func: ()=> {
                                        this.addNewGroup();
                                    }
                                }}
                                rightFunc={{
                                    label: lang['domain8'],
                                    func: ()=> {
                                        this.handleGroupOpen(false);
                                    }
                                }}
                            />
                        ) : (
                            <Dialogs
                                kinds={"content"}
                                open={groupOpen}
                                handleOpen={this.handleGroupOpen}
                                title={"LTO Info"}
                                subButton={groupButton}
                                content={groupForm}
                                rightFunc={{
                                    label: lang['domain8'],
                                    func: ()=> {
                                        this.handleGroupOpen(false);
                                    }
                                }}
                            />
                        )}
                    </Dialog>
                </React.Fragment>
            ) : null}
        </div>
    );
  }
}

StoSlide.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    dialogRoot: {
        top: '64px!important'
    },
    dialogContainer: {
        alignItems: 'flex-end'
    },
    dialgoBack: {
        display: 'none'
    },
    container: {
        flexWrap: 'wrap',
    },
    appBar: {
      position: 'relative',
      backgroundColor: '#f68a15'
    },
    flex: {
      flex: 1,
    },
    root: {
        flexGrow: 1,
        display: 'flex',
        overflowY: 'hidden'
    },
    listRoot: {
        paddingTop: 0,
        paddingBottom: 0,
        overflowY: 'auto',
        transition: 'all .08s ease-out',
    },
    standardBox: {
        width: '100%',
        display: 'flex',
        flex: 1,
        flexDirection: 'column'
    },
    listDivider: {
        backgroundColor: '#eee'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        flex: 1
    },
    targetForm: {
        minWidth: 300,
        flex: 1
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    addList: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#fff',
        backgroundColor: 'deepskyblue',
        '&:hover': {
            backgroundColor: 'deepskyblue',
        },
    },
    addIcon: {

    },
    topBtns: {
        position: 'absolute',
        //top: 80,
        right: 15,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    stoTopBtns: {
        position: 'absolute',
        top: 15,
        right: 15,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end'
    },
    topSettingBtns: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 8
    },
    copyBtn: {
        flex: 1,
    },
    statusBtn: {
        //flex: 1,
        height: 42,
        marginLeft: 8,
        marginRight: 8,
    },
    onFinishBtn: {
        borderColor: '#eee',
        backgroundColor: '#eee',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#eee',
            opacity: '.9',
            borderColor: '#eee'
        }
    },
    unFinishBtn: {
        borderColor: '#eee',
        backgroundColor: 'transparent',
        color: '#fff',
        '&:hover': {
            opacity: '.9',
            borderColor: '#eee'
        }
    },
    gridRoot: {
        //height: '100%',
        flex: 1,
        flexWrap: 'initial',
    },
    stosBox: {
        position: 'absolute',
        display: 'flex',
        flexDirection: 'row',
        height: 'calc(100% - 64px)',
        //maxWidth: 260,
        borderRight: '1px solid #eee',
        backgroundColor: '#333',
        overflow: 'hidden',
        zIndex: 9998,
    },
    extended: {
        width: 600,
        visibility: 'visible'
    },
    folded: {
        width: 0,
        visibility: 'hidden'
    },
    extendBtn: {
        cursor: 'pointer',
        padding: '15px 3px',
        backgroundColor: 'rgb(51, 51, 51)',
        borderLeft: '1px solid #fff',
        display: 'flex',
        alignItems: 'center',
        zIndex: 9999,
        '& svg': {
            fontSize: '2rem',
            color: '#fff',
        },
        '&:hover, &:active': {
            opacity: '.9'
        }
    },
    fab: {
        margin: theme.spacing(1),
    },
    arrowDisabled: {
        backgroundColor: 'rgba(255, 255, 255, 0.26)!important'    
    },
    parentRoot: {
        display: 'flex',
        flex: 1,
        width: '100%'
    },
    childRoot: {
        display: 'flex',
        flex: 1,
        width: '100%',
        paddingLeft: 10
    },
    childContent: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        '&:before': {
            content: "123123",
            fontSize: 18,
            color: '#000'
        }
    },
    stoRootBox:{
        position: 'relative',
        minHeight: 150,
        padding: 23,
        color: '#fff',
        backgroundColor: '#545454',
        borderBottom: '1px solid #eee'
    },
    pointRootBox: {
        backgroundColor: '#faedf3',
        color: '#232323',
        padding: 23,
        borderTop: '1px solid #eee'
    },
    stoDivider: {
        width: '40%',
        marginTop: 15,
        marginBottom: 10
    },
    selectedCheck: {
        position: 'absolute',
        backgroundColor: '#dedede',
        width: 5,
        height: '100%',
        left: 0
    },
    listBox: {
        flex: 1,
        flexDirection: 'column',
        padding: 0
    },
    listTextBox: {
        flex: 1,
        minHeight: 50,
        display: 'flex',
        color: '#fff',
        fontSize: 12,
        padding: 15,
        alignItems: 'center',
        justifyContent: 'flex-start',
        wordBreak: 'break-all',
        '& > span': {
            textAlign: 'left'
        }
    },
    listIconBox: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    optionBox: {
        width: '100%'
    },
    newText: {
        flex: 1,
    },
    optonContent: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    optionOrder: {
        flex: 'none',
        width: '100%'
    },
    optionLabel: {
        minWidth: 200,
        flex: 1
    },
    optionText: {
        flex: 3
    },
    optionRemove: {
        width: 50
    },
    contentText: {
        whiteSpace: 'pre-line',
        fontSize: 14,
        lineHeight: '1.2rem'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    canceledSto: {
        backgroundColor: '#ca5388'
    },
    finishedSto: {
        backgroundColor: '#485680'
    },
    stoppedSto: {
        backgroundColor: '#865995'
    },
    stoRootTitleBox: {
        width: 'calc(100% - 260px)',
    },
    titleBox: {
        display: 'flex',
        alignItems: 'center'
    },
    mainTitle: {
        fontFamily: 'JungBold',
        color: '#fff'
    },
    mainPointTitle: {
        flex: 'inherit',
        fontFamily: 'JungBold',
        color: '#444'
    },
    slideTitleBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-start'
    },
    graphTopBtn: {
        borderColor: '#fff',
        marginLeft: 15,
        marginRight: 15,
        '& > span': {
            color: '#fff'
        }
    },
    graphBtn: {
        marginLeft: 10
    },
    dialogBtn: {
        // position: 'absolute',
        // top: 10,
        // right: 10,
        // zIndex: 9999
    },
    editSaveBtn: {

    },
    arrowBtn: {
        width: 24,
        height: 24,
        minHeight: 24,
        margin: 0,
        border: '1px solid #444',
        borderRadius: 0
    },
    stoChartBtn: {
        borderColor: '#fff',
        marginLeft: 15,
        padding: '3px 15px',
        '& > span': {
            color: '#fff'
        }
    },
    titleRow: {
        width: '100%',
        marginTop: 15
    },
    groupContainer: {
        flex: 1,
        display: 'flex'
    },
    groupName: {
        flex: 1,
        marginLeft: 8,
        marginRight: 8
    },
    groupSelect: {
        width: 300,
        marginLeft: 8,
        marginRight: 8
    },
    stoGroupRoot: {
        height: '100%',
        overflowY: 'auto'
    },
    newStoBtn: {
        height: 'auto',
        marginLeft: 8,
        marginRight: 8,
        borderColor: '#fff',
        '& > span': {
            color: '#fff'
        }
    },
    stoGroupBox: {
        display: 'block',
        position: 'relative',
        minHeight: 150,
        padding: 23,
        color: '#fff',
        borderBottom: '1px solid #eee'
    },
    stoGrouptitlebox: {
        position: 'absolute'
    },
    stoGrouptitle: {
        display: 'flex'
    },
    stoRootContainer: {
        position: 'relative',
        borderBottom: '1px solid #eee'
    },
    nested: {
        borderTop: '1px dashed #dedede',
        padding: 0,
        backgroundColor: '#444'
    },
    itemArrow: {
        backgroundColor: '#aaa'
    }
});

export default withStyles(styles)(StoSlide);