import React from 'react';
import { observer, inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { ButtonGroup, Button, Fab, Dialog, ListItem, List, Divider, AppBar, Toolbar, IconButton, OutlinedInput,
    Typography, Slide, Grid, InputLabel, MenuItem, FormControl, Select, TextField, InputAdornment } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import PlaylistAdd from '@material-ui/icons/PlaylistAdd';
import DeleteIcon from '@material-ui/icons/Delete';
import { FileCopy, SubdirectoryArrowRight, InsertChartOutlined, HighlightOff, Edit, Delete, ZoomIn, Save, MultilineChart, ShowChart } from '@material-ui/icons';

import { Points, Dialogs } from '../../components/';
import { StoGraphSlide } from '../../pages';

let optionOrder = 0;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const promptArr = [
    {id:1, name: "독립(independent)"},
    {id:2, name: "전체 신체(full physical)"},
    {id:3, name: "부분 신체(partial physical)"},
    {id:4, name: "전체 음성(full aditory)"},
    {id:5, name: "부분 음성(partial aditory)"},
    {id:6, name: "몸짓(gestual)"},
    {id:7, name: "글자(textual)"},
    {id:8, name: "그림(picture)"},
];

@inject(stores => ({
    funcStore: stores.funcStore
}))
class StoSlide extends React.Component {
    state = {
        open: this.props.open,
        stoItems: [],
        view: 'intro',
        selectedSto: '',
        name: '',
        rows: [],
        target: [{
            text: '', status: '0'
        }],
        points: [],
        stoTargets: [],
        userTargets: [],
        searchTarget: '',
        parentId: '',
        standardPerc: 80,
        standardCnt: 3,
        promptCode: '',
        promptMemo: '',
        scheduleMemo: '',
        programMemo: '',
        percLabelWidth: 0,
        cntLabelWidth: 0,
        parentLabelWidth: 0,
        targetLabelWidth: 0,
        promptLabelWidth: 0,
        graphOpen: false,
        graphPoint: null,
        graphProgram: null,
        dialogOpen: false,
        dialogSort: true,
        pointFlag: false
    };
  
//   shouldComponentUpdate(nextProps, nextState) {
// 	if(this.props.open == nextProps.open) {
//         if(this.state.view ) {

//         }
// 		return false;
//     }
// 	return true;
//   }

//   static getDerivedStateFromProps(nextProps, prevState) {
//       if(prevState.open !== nextProps.open && nextProps.open) {
//         nextProps.funcStore.conn('get', '/stoItems/' + nextProps.program.id + '/get', false, true, null, (res)=>{
//             if(res.result == "true") {
//                 return {
//                     stoItems: res.stoItems
//                 }
//             } else {
//                 return [];
//             }
//         });
//       }
//       return null;
//   }
    static getDerivedStateFromProps(nextProps, prevState) {
        if(prevState.open == nextProps.open && !nextProps.open) {
            return {
                stoItems: [],
                stoChilds: [],
                view: 'new',
                selectedSto: '',
                name: '',
                rows: [],
                target: [{
                    text: '', status: '0'
                }],
                points: [],
                standardPerc: 80,
                standardCnt: 3,
                promptCode: '',
                promptMemo: '',
                scheduleMemo: '',
                programMemo: '',
            }
        }
        return true;
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if (prevProps.open !== this.props.open && this.props.open && this.props.program) {
            return {
                refresh: true
            }
        }
        return null;
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (snapshot && snapshot.refresh) {
            this.loadStoItems();
        }
    }

    loadStoItems = () => {
        const { funcStore } = this.props;
        funcStore.conn('get', '/stoItems/' + this.props.program.id + '/get', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoItems: res.stoItems,
                    stoChilds: res.stoChilds,
                    stoTargets: res.stos,
                    selectedSto: (res.stoItems.length ? res.stoItems[0] : ''),
                    view: (res.stoItems.length ? 'detail' : 'empty')
                });
                this.forceUpdate();
            } else {
                console.log("STO 로딩 실패");
            }
        });
    }

    handleView = view => {
        this.setState({
            view: view
        });
    }

    handleClose = () => {
        this.props.handleSlide(false);
    };

    handleOrder = (kind, id) => {
        const { stoItems } = this.state;
        const index = stoItems.findIndex(v => v.id == id);
        if (index === -1) return;
        let original = stoItems[index];
        let newPos = 0, target = '';
        if(kind == 'up') {
            if(original) original.order += 1;
            newPos = index - 1;
            if (newPos < 0) {
                newPos = 0;
                original.order = stoItems.length;
            }
            stoItems.splice(index, 1);
            if(stoItems[newPos]) {
                stoItems[newPos].order -= 1;
                target = stoItems[newPos].id;
            }
        } else if(kind == 'down') {
            if(original) original.order -= 1;
            newPos = index + 1;
            if (newPos >= stoItems.length) {
                newPos = stoItems.length;
                original.order = 0;
            }
            stoItems.splice(index, 1);
            if(stoItems[index]) {
                stoItems[index].order += 1;
                target = stoItems[index].id;
            }
        }
        stoItems.splice(newPos, 0, original);
        // switch both
        const param = {
            myId: original.id,
            targetId: target
        }
        
        this.props.funcStore.conn('post', '/stoItem/switchOrder', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoItems: stoItems
                });
                this.forceUpdate();
            } else {
                console.log("순서 교체 실패");
            }
        });
    }

    handleChildOrder = (kind, id, parentId) => {
        const { stoChilds } = this.state;
        const arr = stoChilds[parentId];
        const index = arr.findIndex(v => v.id == id);
        if (index === -1) return;
        let original = arr[index];
        let newPos = 0, target = '';
        if(kind == 'up') {
            if(original) original.order += 1;
            newPos = index - 1;
            if (newPos < 0) {
                newPos = 0;
                original.order = arr.length;
            }
            arr.splice(index, 1);
            if(arr[newPos]) {
                arr[newPos].order -= 1;
                target = arr[newPos].id;
            }
        } else if(kind == 'down') {
            if(original) original.order -= 1;
            newPos = index + 1;
            if (newPos >= arr.length) {
                newPos = arr.length;
                original.order = 0;
            }
            arr.splice(index, 1);
            if(arr[index]) {
                arr[index].order += 1;
                target = arr[index].id;
            }
        }
        arr.splice(newPos, 0, original);
        // switch both
        const param = {
            myId: original.id,
            targetId: target
        }
        
        this.props.funcStore.conn('post', '/stoItem/switchOrder', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoChilds: {
                        ...stoChilds,
                        [parentId] : arr
                    }
                });
                this.forceUpdate();
            } else {
                console.log("순서 교체 실패");
            }
        });
    }

    hcandleChangeParent = e => {
        const v = e.target.value;
        if(v) {
            const t = v.split("?=");
            this.setState({
                parentId: parseInt(t[0]),
                parentName: t[1]
            });
        } else {
            this.setState({
                parentId: '',
                parentName: ''
            });
        }
    }

    handleChange = name => e => {
        if(name == "searchTarget") {
            const { stoTargets } = this.state;
            const tId = e.target.value;
            const t = stoTargets.filter(v => v.id == tId);
            if(t.length && t[0].target) {
                this.setState({
                    target: t[0].target
                });
            }
        }
        this.setState({
            [name]: e.target.value
        });
    };

    handleChangeTarget = (idx, name) => e => {
        const { target } = this.state;
        if(idx == target.length - 1) {
            const t = target.map((t,i) => i == idx ?
            {
                ...t,
                [name]: e.target.value
            } : t);

            this.setState({
                target: t.concat({ text: '', status: 0 })
            });
        } else {
            if(!e.target.value) {
                this.handleChangeTargetRemove(idx);
            } else {
                this.setState({
                    target: target.map((t,i) => i == idx ?
                        {
                            ...t,
                            [name]: e.target.value
                        } : t
                    )
                });
            }
        }
    };

    handleChangeTargetRemove = idx => {
        const { target } = this.state;
        this.setState({
            target: target.filter((t,i) => i !== idx)
        });
    }

    handleChangeSelectedTarget = (idx, name) => e => {
        const { selectedSto, stoItems } = this.state;
        const target = selectedSto.target;
        if(idx == target.length - 1) {
            const tmp = target.map((t,i) => i == idx ? {
                    ...t,
                    [name]: e.target.value
                } : t);
            const t2 = tmp.concat({ text: '', status: 0 });
            this.setState({
                stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                    ...v, target: t2.concat({ text: '', status: 0 })
                } : v),
                selectedSto: {
                    ...selectedSto,
                    target: t2.concat({ text: '', status: 0 })
                }
            });
        } else {
            if(!e.target.value) {
                this.handleChangeSelectedTargetRemove(idx);
            } else {
                const tmp = target.map((t,i) => i == idx ? {
                    ...t,
                    [name]: e.target.value
                } : t);
                this.setState({
                    stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                        ...v, target: tmp
                    } : v),
                    selectedSto: {
                        ...selectedSto,
                        target: tmp
                    }
                });
            }
        }
    };

    handleChangeSelectedTargetRemove = idx => {
        const { selectedSto, stoItems } = this.state;
        const tmp = selectedSto.target.filter((t,i) => i !== idx);
        this.setState({
            stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                ...v, target: tmp
            } : v),
            selectedSto: {
                ...selectedSto,
                target: tmp
            }
        });
    }

    handleChangeOption = (order, name) => event => {
        const { rows } = this.state;
        this.setState({
            rows: rows.map(row => row.order == order ?
                {
                    ...row,
                    [name]: event.target.value
                } : row
            )
        });
    };

    removeOption = order => {
        const { rows } = this.state;
        this.setState({
            rows: rows.filter(row => row.order !== order)
        });
    };

    setLabelWidth = (kind, ref) => {
        const { percLabelWidth, cntLabelWidth, parentLabelWidth, targetLabelWidth, promptLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != percLabelWidth) {
            this.setState({ percLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != cntLabelWidth) {
            this.setState({ cntLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != parentLabelWidth) {
            this.setState({ parentLabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != targetLabelWidth) {
            this.setState({ targetLabelWidth: ref.offsetWidth });
        } else if(kind == 5 && ref.offsetWidth != promptLabelWidth) {
            this.setState({ promptLabelWidth: ref.offsetWidth });
        }
    }

    addRow = () => {
        const { rows } = this.state;
        this.setState({
            rows: rows.concat({
                order: ++optionOrder,
                label: '',
                text: ''
            })
        });
    }

    handleGraphSlide = flag => {
	    this.setState({
		    graphOpen: flag 
	    });
    }

    openStoGraph = () => {
        const { selectedSto } = this.state;
        this.setState({
            graphOpen: true,
            graphPoint: {
                sto_item_id: selectedSto.id
            },
            graphProgram: '',
        });
    }

    openStosGraph = () => {
        this.setState({
            graphOpen: true,
            graphPoint: '',
            graphProgram: this.props.program,
        });
    }

    handleFinish = kind => {
        const { selectedSto, stoItems, stoChilds } = this.state;
        if(selectedSto.finish == kind) return;
        if(!confirm("상태를 변경하시겠습니까?")) return;
        
        if(selectedSto.id) {
            const param = {
                status: kind
            }
            this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/status', false, true, param, (res)=>{
                if(res.result == "true") {
                    if(selectedSto.parent_id) {
                        this.setState({
                            stoChilds: {
                                ...stoChilds,
                                [selectedSto.parent_id]: stoChilds[selectedSto.parent_id].map(v => v.id == selectedSto.id ? {
                                    ...v, finish: kind
                                } : v)
                            },
                            selectedSto: {
                                ...selectedSto,
                                finish: kind
                            }
                        });
                    } else {
                        this.setState({
                            stoItems: stoItems.map(v => v.id == selectedSto.id ? {
                                ...v, finish: kind
                            } : v),
                            selectedSto: {
                                ...selectedSto,
                                finish: kind
                            }
                        });
                    }
                } else {
                    console.log("STO 상태 수정 오류");
                }
            });
        } else return;
    }

    validation = () => {
        return true;
    }

    register = () => {
        if(!this.validation()) return;
        const { stoItems, stoChilds, name, rows, target, standardPerc, standardCnt, parentId, parentName,
            promptCode, promptMemo, scheduleMemo, programMemo } = this.state;
        const param = {
            programId: this.props.program.id,
            name: name,
            stoId: '',
            parentId: parentId,
            parentName: parentName,
            standardPerc: standardPerc,
            standardCnt: standardCnt,
            rows: rows,
            target: target ? target.filter(v => v.text != "") : [],
            promptCode: promptCode ? promptCode : '',
            promptMemo: promptMemo,
            scheduleMemo: scheduleMemo,
            programMemo: programMemo
        }
        this.props.funcStore.conn('post', '/stoItem/post', false, true, param, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoItem];
                if(parentId) {
                    this.setState({
                        stoChilds: {
                            ...stoChilds,
                            [parentId]: temp.concat(stoChilds[parentId])
                        },
                        selectedSto: res.stoItem
                    });
                } else {
                    this.setState({
                        stoItems: temp.concat(stoItems),
                        selectedSto: res.stoItem
                    });
                }
                this.handleView('detail');
                this.handleOpen(false);
                //this.loadStoItems();
            } else {
                console.log("STO 추가 오류");
            }
        });
    }

    duplicateSto = stoItem => {
        const { stoItems } = this.state;
        this.props.funcStore.conn('post', '/stoItem/'+stoItem.id+'/duplicate', false, true, null, (res)=>{
            if(res.result == "true") {
                const temp = [res.stoItem];
                this.setState({
                    stoItems: temp.concat(stoItems),
                    selectedSto: res.stoItem
                });
                alert("복사 완료");
                this.handleView('detail');
                this.handleOpen(false);
            } else {
                console.log("STO 추가 오류");
            }
        });
    }

    editSto = () => {
        if(!this.validation()) return;
        const { selectedSto, stoItems, name, rows, target, standardPerc, standardCnt, pointFlag,
            promptCode, promptMemo, scheduleMemo, programMemo } = this.state;
        const param = {
            name: name,
            standardPerc: standardPerc,
            standardCnt: standardCnt,
            rows: rows,
            target: target ? target.filter(v => v.text != "") : [],
            promptCode: promptCode ? promptCode : '',
            promptMemo: promptMemo,
            scheduleMemo: scheduleMemo,
            programMemo: programMemo
        }
        this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/put', false, true, param, (res)=>{
            if(res.result == "true" && res.stoItem) {
                this.setState({
                    stoItems: stoItems.map(s => s.id == res.stoItem.id ? res.stoItem : s),
                    selectedSto: res.stoItem,
                    pointFlag: !pointFlag
                });
                alert("수정을 완료하였습니다");
                this.handleOpen(false);
                this.forceUpdate();
            } else {
                console.log("STO 수정 오류");
            }
        });
    }

    removeSto = () => {
        if(!confirm("해당 LTO를 삭제하시겠습니까?")) return;
        const { selectedSto, stoItems, pointFlag } = this.state;
        this.props.funcStore.conn('post', '/stoItem/'+selectedSto.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    stoItems: stoItems.filter(s => s.id != selectedSto.id),
                    selectedSto: '',
                    view: 'empty',
                    pointFlag: !pointFlag
                });
                this.handleOpen(false);
                this.forceUpdate();
            } else {
                console.log("STO 삭제 오류");
            }
        });
    }

    stoStatusClass = (status) => {
        const { classes } = this.props;
        let clname = "";
        if(status == -1) {
            clname = classes.canceledSto;
        } else if(status == 1) {
            clname = classes.finishedSto;
        } else if(status == 2) {
            clname = classes.stoppedSto;
        }
        return clname;
    }

    openNewDialog = () => {
        this.setState({
            dialogOpen: true,
            dialogSort: true
        });
    }

    openEditDialog = () => {
        const { selectedSto } = this.state;
        const tmp = {
            text: '', status: '0'
        };
        this.setState({
            dialogOpen: true,
            dialogSort: false,
            name: selectedSto.name,
            parentId: selectedSto.parent_id,
            parentName: selectedSto.parent_name,
            standardPerc: selectedSto.standard_perc,
            standardCnt: selectedSto.standard_cnt,
            rows: selectedSto.rows ? selectedSto.rows : [],
            target: selectedSto.target ? selectedSto.target.concat(tmp) : [tmp],
            promptCode: selectedSto.prompt_code ? selectedSto.prompt_code : '',
            promptMemo: selectedSto.prompt_memo,
            scheduleMemo: selectedSto.schedule_memo,
            programMemo: selectedSto.program_memo,
        });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    resetDialog = () => {
        this.setState({
            name: '',
            stoId: '',
            parentId: '',
            parentName: '',
            standardPerc: 80,
            standardCnt: 3,
            rows: [],
            target: [{
                text: '', status: '0'
            }],
            promptCode: '',
            promptMemo: '',
            scheduleMemo: '',
            programMemo: '',
        });
    }

    // <FormControl variant="outlined" className={classes.formControl}>
    //     <InputLabel
    //         ref={ref => {
    //             if(ref) this.setLabelWidth(3, ref);
    //         }}
    //         htmlFor="parent-label"
    //     >
    //         부모 STO
    //     </InputLabel>
    //     <Select
    //         value={parentId ? parentId + "?=" + parentName : ""}
    //         onChange={this.hcandleChangeParent}
    //         input={
    //         <OutlinedInput
    //             labelWidth={parentLabelWidth}
    //             name="parentLabelWidth"
    //             id="parent-label"
    //         />
    //         }
    //     >
    //         <MenuItem value="">
    //             <em>선택</em>
    //         </MenuItem>
    //         {parentList.map((v,i) => {
    //             return (
    //                 <MenuItem key={"p_"+i} value={v.id + "?=" + v.name}>
    //                     {v.name}
    //                 </MenuItem>
    //             );
    //         })}
    //     </Select>
    // </FormControl>

    // <div className={classes.optionBox}>
    //     <Typography variant="h6" color="inherit" className={classes.titleRow}>
    //         {"추가 옵션 입력"}
    //     </Typography>
    //     <div>
    //         {rows ? rows.map((v,i) => {
    //             return (
    //                 <div key={i} autoComplete="off" className={classes.optonContent}>
    //                     <Typography variant="subtitle2" color="inherit" className={classes.optionOrder}>
    //                         {(i + 1) + "# option // "}
    //                     </Typography>
    //                     <TextField
    //                         label="LABEL"
    //                         className={classNames(classes.textField, classes.optionLabel)}
    //                         value={v.label}
    //                         onChange={this.handleChangeOption(v.order, 'label')}
    //                         margin="normal"
    //                         variant="outlined"
    //                     />
    //                     <TextField
    //                         label="CONTENT"
    //                         className={classNames(classes.textField, classes.optionText)}
    //                         value={v.text}
    //                         onChange={this.handleChangeOption(v.order, 'text')}
    //                         margin="normal"
    //                         variant="outlined"
    //                     />
    //                     <IconButton aria-label="remove" className={classes.optionRemove}
    //                         onClick={
    //                             () => this.removeOption(v.order)}>
    //                         <DeleteIcon  />
    //                     </IconButton>
    //                 </div>
    //             );
    //         }) : null}
    //     </div>
    // </div>
    // <div className={classes.listIconBox}>
    //     <Fab size="small" variant="extended" aria-label="prev" className={classes.fab}
    //         onClick={
    //             () => this.addRow()}>
    //         <PlaylistAdd  />
    //     </Fab>
    // </div>

  render() {
    const { classes, open, program, order } = this.props;
    const { stoItems, stoChilds,
        view, selectedSto, name, target, rows, points,
        standardPerc, standardCnt,
        promptCode, promptMemo, scheduleMemo, programMemo,
        percLabelWidth, cntLabelWidth, targetLabelWidth, promptLabelWidth,
        graphOpen, graphPoint, graphProgram,
        stoTargets, searchTarget,
        dialogOpen, dialogSort, pointFlag } = this.state;
    const content = view == 'detail' ? (
        <div>
            <div className={classes.stoRootBox}>
                <div className={classes.titleBox}>
                    <Typography variant="h5" color="inherit" className={classes.mainTitle}>
                        {"PROGRAM Details"}
                    </Typography>
                </div>
                <div className={classes.topBtns}>
                    <div className={classes.topSettingBtns}>
                        <Fab size="small" aria-label="edit" className={classNames(classes.fab)}
                            onClick={
                                () => this.openEditDialog()}
                        >
                            <Edit />
                        </Fab>
                        <Fab size="small" aria-label="copy" className={classNames(classes.fab)}
                            onClick={
                                () => this.duplicateSto(selectedSto)}
                        >
                            <FileCopy />
                        </Fab>
                    </div>
                    <ButtonGroup color="primary" aria-label="Status" className={classes.statusBtn}>
                        <Button
                            className={selectedSto.finish == 0 ? classes.onFinishBtn : classes.unFinishBtn}
                            onClick={()=>{this.handleFinish(0)}}
                        >{"진행중"}</Button>
                        <Button
                            className={selectedSto.finish == 1 ? classes.onFinishBtn : classes.unFinishBtn}
                            onClick={()=>{this.handleFinish(1)}}
                        >{"완료"}</Button>
                        <Button
                            className={selectedSto.finish == 2 ? classes.onFinishBtn : classes.unFinishBtn}
                            onClick={()=>{this.handleFinish(2)}}
                        >{"중지"}</Button>
                        <Button
                            className={selectedSto.finish == -1 ? classes.onFinishBtn : classes.unFinishBtn}
                            onClick={()=>{this.handleFinish(-1)}}
                        >{"취소"}</Button>
                    </ButtonGroup>
                </div>
                <div className={classes.contentText}>
                    {selectedSto.name ? "\n이름 : " + selectedSto.name : ''}
                    {"\n준거도달 기준 : " + selectedSto.standard_perc}
                    {"\n도달 기준 : " + selectedSto.standard_cnt}
                    {"\n총 도달 횟수 : " + selectedSto.total_reach}
                    {"\n타겟 : " + (selectedSto.target ? selectedSto.target.map((v,i) => (i>0 ? " / " : "") + v.text) : "")}
                </div>
            </div>
            <Divider style={{backgroundColor: '#eee'}} />
            <div className={classes.pointRootBox}>
                <div className={classes.titleBox}>
                    <Typography variant="h5" color="inherit" className={classes.mainPointTitle}>
                        {"POINTS List"}
                    </Typography>
                    <Button
                        variant="outlined" className={classes.stoChartBtn2}
                        onClick={(e)=>{
                            e.preventDefault();
                            this.openStoGraph()
                        }}
                    ><ShowChart /></Button>
                </div>
                <Points
                    stoItem={selectedSto}
                    list={points}
                    flag={pointFlag}
                />
            </div>
        </div>
    ) : view == 'empty' ? (
        <div>
            <div className={classes.titleBox}>
                <Typography variant="h4" color="inherit" className={classes.mainTitle}>
                    {"- Empty -"}
                </Typography>
            </div>
        </div>
    ) : (
        <div></div>
    );

    const dialogForm = (
        <div>
            {!dialogSort && (
                <div className={classes.dialogBtn}>
                    <Fab size="small" variant="round" aria-label="save" className={classNames(classes.fab)}
                        onClick={() => this.editSto()}>
                        <Save />
                    </Fab>
                    <Fab size="small" variant="round" aria-label="remove" className={classNames(classes.fab)}
                        onClick={
                            () => this.removeSto()}>
                        <Delete />
                    </Fab>
                </div>
            )}
            <form className={classes.container} noValidate autoComplete="off">
                <TextField
                    id="outlined-name"
                    label="LTO Name"
                    className={classes.textField}
                    value={name ? name : ''}
                    onChange={this.handleChange('name')}
                    variant="outlined"
                />
                <FormControl variant="outlined" className={classes.targetForm}>
                    <InputLabel
                        ref={ref => {
                            if(ref) this.setLabelWidth(4, ref);
                        }}
                        htmlFor="sto-target"
                    >
                        STO sample
                    </InputLabel>
                    <Select
                        value={searchTarget ? searchTarget : ""}
                        onChange={this.handleChange('searchTarget')}
                        input={
                            <OutlinedInput
                                labelWidth={targetLabelWidth}
                                name="searchTarget"
                                id="sto-target"
                            />
                        }
                    >
                        <MenuItem value="">
                            <em>선택</em>
                        </MenuItem>
                        {stoTargets.length ? stoTargets.map((s,i) => {
                            let names = "";
                            if(s.target) {
                                names = s.target.map((row,idx) => {
                                    let temp = "";
                                    if(row.text) {
                                        if(idx > 0) temp += " / ";
                                        temp += row.text;
                                    }
                                    return temp;
                                });
                            }
                            
                            return (
                                <MenuItem key={"s_" + i} value={s.id}>
                                    <em>{names + (s.name ? " (" + s.name + ")" : "") }</em>
                                </MenuItem>
                            );
                        }) : null}
                    </Select>
                </FormControl>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {"준거도달 설정"}
                </Typography>
                <div className={classes.standardBox}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(1, ref);
                            }}
                            htmlFor="perc-label"
                        >
                            도달 기준
                        </InputLabel>
                        <Select
                            value={standardPerc}
                            onChange={this.handleChange('standardPerc')}
                            input={
                            <OutlinedInput
                                labelWidth={percLabelWidth}
                                name="percLabelWidth"
                                id="perc-label"
                            />
                            }
                        >
                            <MenuItem value={"70"}>{"70%"}</MenuItem>
                            <MenuItem value={"75"}>{"75%"}</MenuItem>
                            <MenuItem value={"80"}>{"80%"}</MenuItem>
                            <MenuItem value={"85"}>{"85%"}</MenuItem>
                            <MenuItem value={"90"}>{"90%"}</MenuItem>
                            <MenuItem value={"95"}>{"95%"}</MenuItem>
                            <MenuItem value={"100"}>{"100%"}</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(2, ref);
                            }}
                            htmlFor="count-label"
                        >
                            도달 기준
                        </InputLabel>
                        <Select
                            value={standardCnt}
                            onChange={this.handleChange('standardCnt')}
                            input={
                            <OutlinedInput
                                labelWidth={cntLabelWidth}
                                name="cntLabelWidth"
                                id="count-label"
                            />
                            }
                        >
                            <MenuItem value={"1"}>{"1"}</MenuItem>
                            <MenuItem value={"2"}>{"2"}</MenuItem>
                            <MenuItem value={"3"}>{"3"}</MenuItem>
                            <MenuItem value={"4"}>{"4"}</MenuItem>
                            <MenuItem value={"5"}>{"5"}</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {"STO 설정"}
                </Typography>
                <div className={classes.standardBox} id="target_box">
                    {target ? target.map((t,i) => {
                        return (
                            <TextField
                                key={"t_"+i}
                                label="STO 입력"
                                className={classNames(classes.textField, classes.newText)}
                                value={t.text ? t.text : ""}
                                onChange={this.handleChangeTarget(i, 'text')}
                                margin="normal"
                                variant="outlined"
                                InputProps={{
                                    endAdornment: t.text ? (
                                        <InputAdornment position="end">
                                            <IconButton
                                                onClick={() => this.handleChangeTargetRemove(i)}
                                            >
                                                <HighlightOff />
                                            </IconButton>
                                        </InputAdornment>
                                    ) : null
                                }}
                            />
                        );
                    }) : null}
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {"촉구"}
                </Typography>
                <div className={classes.standardBox}>
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(5, ref);
                            }}
                            htmlFor="prompt-label"
                        >
                            촉구 설정 선택
                        </InputLabel>
                        <Select
                            value={promptCode}
                            onChange={this.handleChange('promptCode')}
                            input={
                            <OutlinedInput
                                labelWidth={promptLabelWidth}
                                name="promptLabelWidth"
                                id="prompt-label"
                            />
                            }
                        >
                            <MenuItem value="">선택</MenuItem>
                            {promptArr.map((p,i) => {
                                return (
                                    <MenuItem key={'p_'+i} value={p.id}>{p.name}</MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <TextField
                        label="촉구 관련 메모"
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={promptMemo ? promptMemo : ""}
                        onChange={this.handleChange('promptMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                </div>
                <Typography variant="h6" color="inherit" className={classes.titleRow}>
                    {"기타"}
                </Typography>
                <div className={classes.standardBox}>
                    <TextField
                        label="강화 관련 메모"
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={scheduleMemo ? scheduleMemo : ""}
                        onChange={this.handleChange('scheduleMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                    <TextField
                        label="프로그램 관련 메모"
                        className={classNames(classes.textField, classes.newText)}
                        multiline
                        rows="2"
                        value={programMemo ? programMemo : ""}
                        onChange={this.handleChange('programMemo')}
                        margin="normal"
                        variant="outlined"
                    />
                </div>
            </form>
        </div>
    );

    return (
      <div>
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
          BackdropProps={{
              classes: {
                  root: classes.dialgoBack
              }
          }}
          classes={{
                root: classes.dialogRoot,
                container: classes.dialogContainer,
          }}
        >
          <AppBar className={classes.appBar}>
                <Toolbar>
                    <div className={classes.slideTitleBox}>
                        {program && (
                            <Typography variant="h6" color="inherit">
                                {"#"+order+". "+(program.student_name ? program.student_name : "")}
                            </Typography>
                        )}
                        <Button
                            variant="outlined" className={classes.stoChartBtn}
                            onClick={(e)=>{
                                e.preventDefault();
                                this.openStosGraph()
                            }}
                        ><MultilineChart /></Button>
                    </div>
                        <Button
                            variant="outlined"
                            color="default"
                            size="small"
                            className={classNames(classes.margin, classes.graphTopBtn)}
                            startIcon={<AddIcon />}
                            onClick={()=>this.openNewDialog()}
                        >
                            NEW PROGRAM
                        </Button>
                    <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                        <CloseIcon />
                    </IconButton>
                </Toolbar>
          </AppBar>
          <div className={classes.root}>
            <Grid container spacing={0} className={classes.gridRoot}>
                <Grid item xs={4} className={classes.stosBox}>
                    <List className={classes.listRoot}>
                        { stoItems.map((v,i) => {
                            let name = "";
                            if(v.name == "" || !v.name) {
                                if(v.target) {
                                    v.target.map((item,idx) => {
                                        if(item.text && item.text != '') {
                                            if(idx > 0) name += ", ";
                                            name += item.text;
                                        }
                                    });
                                } else {
                                    name = "";
                                }
                            } else {
                                name = v.name;
                            }
                            return (
                                <React.Fragment key={i}>
                                    <ListItem id={v.id} className={classes.listBox}>
                                        <div className={classNames(classes.parentRoot, this.stoStatusClass(v.finish))}>
                                            <Button
                                                className={classes.listTextBox}
                                                onClick={()=>{
                                                    this.setState({
                                                        selectedSto: v 
                                                    });
                                                    this.handleView('detail');
                                                }}
                                            >{name}</Button>
                                            <div className={classes.listIconBox}>
                                                <Fab size="small" variant="round" aria-label="prev"
                                                    className={classNames(classes.fab, classes.arrowBtn)}
                                                    onClick={
                                                        () => this.handleOrder('up', v.id)}
                                                    disabled={i == 0 ? true : false}
                                                    classes={{
                                                        disabled: classes.arrowDisabled
                                                    }}
                                                >
                                                    <ArrowDropUpIcon  />
                                                </Fab>
                                                <Fab size="small" variant="round" aria-label="next"
                                                    className={classNames(classes.fab, classes.arrowBtn)}
                                                    onClick={
                                                        () => this.handleOrder('down', v.id)}
                                                    disabled={i == stoItems.length - 1 ? true : false}
                                                    classes={{
                                                        disabled: classes.arrowDisabled
                                                    }}
                                                >
                                                    <ArrowDropDownIcon />
                                                </Fab>
                                            </div>
                                        </div>
                                        {stoChilds[v.id] ? stoChilds[v.id].map((child, idx) => {
                                            if(child.parent_id == v.id) {
                                                return (
                                                    <React.Fragment key={child.id + "_" + idx}>
                                                        <Divider variant="middle" className={classes.stoDivider} />
                                                        <div className={classNames(classes.childRoot, this.stoStatusClass(child.finish))}>
                                                            <div className={classes.childContent}>
                                                                <SubdirectoryArrowRight />
                                                                <Button
                                                                    className={classes.listTextBox}
                                                                    onClick={()=>{
                                                                        this.setState({
                                                                            selectedSto: child
                                                                        });
                                                                        this.handleView('detail');
                                                                    }}>{child.name}</Button>
                                                            </div>
                                                            <div className={classes.listIconBox}>
                                                                <Fab size="small" variant="extended" aria-label="prev"
                                                                    className={classNames(classes.fab)}
                                                                    onClick={
                                                                        () => this.handleChildOrder('up', child.id, child.parent_id)}
                                                                    disabled={idx == 0 ? true : false}
                                                                >
                                                                    <ArrowDropUpIcon  />
                                                                </Fab>
                                                                <Fab size="small" variant="extended" aria-label="next"
                                                                    className={classNames(classes.fab)}
                                                                    onClick={
                                                                        () => this.handleChildOrder('down', child.id, child.parent_id)}
                                                                    disabled={idx == stoChilds[v.id].length - 1 ? true : false}
                                                                >
                                                                    <ArrowDropDownIcon />
                                                                </Fab>
                                                            </div>
                                                        </div>
                                                    </React.Fragment>
                                                );
                                            }
                                        }) : null}
                                    </ListItem>
                                    <Divider className={classes.listDivider} />
                                </React.Fragment>
                            );
                        })
                        }
                    </List>
                </Grid>
                <Grid item style={{flex:1, backgroundColor:'#333'}}>
                    {content}
                </Grid>
            </Grid>
          </div>
            <StoGraphSlide
                open={graphOpen}
                point={graphPoint}
                program={graphProgram}
                sto={null}
                handleSlide={this.handleGraphSlide}
            />
            {dialogSort ? (
                <Dialogs
                    kinds={"content"}
                    open={dialogOpen}
                    handleOpen={this.handleOpen}
                    title={"New LTO"}
                    content={dialogForm}
                    leftFunc={{
                        label: "Confirm",
                        func: ()=> {
                            this.register();
                        }
                    }}
                    rightFunc={{
                        label: "cancel",
                        func: ()=> {
                            this.handleOpen(false);
                        }
                    }}
                />
            ) : (
                <Dialogs
                    kinds={"content"}
                    open={dialogOpen}
                    handleOpen={this.handleOpen}
                    title={"LTO Info"}
                    content={dialogForm}
                    rightFunc={{
                        label: "close",
                        func: ()=> {
                            this.handleOpen(false);
                        }
                    }}
                />
            )}
        </Dialog>
      </div>
    );
  }
}

StoSlide.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = theme => ({
    dialogRoot: {
        top: '64px!important'
    },
    dialogContainer: {
        alignItems: 'flex-end'
    },
    dialgoBack: {
        display: 'none'
    },
    container: {
        flexWrap: 'wrap',
    },
    appBar: {
      position: 'relative',
      backgroundColor: '#f68a15'
    },
    flex: {
      flex: 1,
    },
    root: {
        flexGrow: 1,
    },
    listRoot: {
        paddingTop: 0,
        paddingBottom: 0
    },
    standardBox: {
        width: '100%',
        display: 'flex',
        flex: 1,
        flexDirection: 'column'
    },
    listDivider: {
        backgroundColor: '#eee'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        flex: 1
    },
    targetForm: {
        minWidth: 300,
        flex: 1
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    addList: {
        alignItems: 'center',
        justifyContent: 'center',
        color: '#fff',
        backgroundColor: 'deepskyblue',
        '&:hover': {
            backgroundColor: 'deepskyblue',
        },
    },
    addIcon: {

    },
    topBtns: {
        position: 'absolute',
        top: 80,
        right: 15,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    topSettingBtns: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
        marginRight: 8
    },
    copyBtn: {
        flex: 1,
    },
    statusBtn: {
        //flex: 1,
        height: 42
    },
    onFinishBtn: {
        borderColor: '#eee',
        backgroundColor: '#eee',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#eee',
            opacity: '.9',
            borderColor: '#eee'
        }
    },
    unFinishBtn: {
        borderColor: '#eee',
        backgroundColor: 'transparent',
        color: '#fff',
        '&:hover': {
            opacity: '.9',
            borderColor: '#eee'
        }
    },
    gridRoot: {
        height: '100%',
        flexWrap: 'initial'
    },
    stosBox: {
        maxWidth: 260,
        borderRight: '1px solid #eee',
        backgroundColor: '#333'
    },
    fab: {
        margin: theme.spacing(1),
    },
    arrowDisabled: {
        backgroundColor: 'rgba(255, 255, 255, 0.26)!important'    
    },
    parentRoot: {
        display: 'flex',
        flex: 1,
        width: '100%'
    },
    childRoot: {
        display: 'flex',
        flex: 1,
        width: '100%',
        paddingLeft: 10
    },
    childContent: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        '&:before': {
            content: "123123",
            fontSize: 18,
            color: '#000'
        }
    },
    stoRootBox:{ 
        backgroundColor: '#333',
        color: '#fff',
        padding: 15
    },
    pointRootBox: {
        backgroundColor: '#faedf3',
        color: '#232323',
        padding: 30
    },
    stoDivider: {
        width: '40%',
        marginTop: 15,
        marginBottom: 10
    },
    listBox: {
        flex: 1,
        flexDirection: 'column',
        padding: 0
    },
    listTextBox: {
        flex: 1,
        minHeight: 50,
        display: 'flex',
        color: '#fff',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'flex-start',
        wordBreak: 'break-all',
        '& > span': {
            textAlign: 'left'
        }
    },
    listIconBox: {
        display: 'flex',
        alignItems: 'center'
    },
    optionBox: {
        width: '100%'
    },
    newText: {
        flex: 1,
    },
    optonContent: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    optionOrder: {
        flex: 'none',
        width: '100%'
    },
    optionLabel: {
        minWidth: 200,
        flex: 1
    },
    optionText: {
        flex: 3
    },
    optionRemove: {
        width: 50
    },
    contentText: {
        whiteSpace: 'pre-line',
        fontSize: 14,
        lineHeight: '1.2rem'
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
    },
    canceledSto: {
        backgroundColor: '#ca5388'
    },
    finishedSto: {
        backgroundColor: '#485680'
    },
    stoppedSto: {
        backgroundColor: '#865995'
    },
    titleBox: {
        display: 'flex',
        alignItems: 'center'
    },
    mainTitle: {
        flex: 'inherit',
        fontFamily: 'JungBold',
        color: '#fff'
    },
    mainPointTitle: {
        flex: 'inherit',
        fontFamily: 'JungBold',
        color: '#444'
    },
    slideTitleBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-start'
    },
    graphTopBtn: {
        borderColor: '#fff',
        marginLeft: 15,
        marginRight: 15,
        '& > span': {
            color: '#fff'
        }
    },
    graphBtn: {
        marginLeft: 10
    },
    dialogBtn: {
        position: 'absolute',
        top: 10,
        right: 10
    },
    editSaveBtn: {

    },
    arrowBtn: {
        width: 30,
        height: 30,
        minHeight: 30
    },
    stoChartBtn: {
        borderColor: '#fff',
        marginLeft: 15,
        padding: '0 15px',
        '& > span': {
            color: '#fff'
        }
    },
    stoChartBtn2: {
        borderColor: '#444',
        marginLeft: 15,
        padding: '3px 15px',
        '& > span': {
            color: '#333'
        }
    },
    titleRow: {
        width: '100%',
        marginTop: 15
    }
});

export default withStyles(styles)(StoSlide);