export { default as Intro } from './intro/Intro';
export { default as Main } from './intro/Main';
export { default as Login } from './intro/Login';
export { default as Join } from './intro/Join';

export { default as Chart } from './chart/Chart';
export { default as Daily } from './chart/Daily';
export { default as StoSlide } from './chart/StoSlide';
export { default as StoGraphSlide } from './chart/GraphSlide';
export { default as LtoReachSlide } from './manage/ReachSlide';

export { default as Analysis } from './analysis/Analysis';
export { default as Graph } from './analysis/Graph';
export { default as StoList } from './analysis/StoList';

export { default as Manage } from './manage/Manage';
export { default as Student } from './manage/Student';
export { default as Program } from './manage/Program';
export { default as Goal } from './manage/Goal';
export { default as Error } from './manage/Error';
export { default as Criteria } from './manage/Criteria';

export { default as Setting } from './setting/Setting';
export { default as Board } from './setting/Board';
export { default as Teacher } from './setting/Teacher';
export { default as Klass } from './setting/Class';