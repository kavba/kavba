import React, { Component, Fragment } from 'react';
import { observer, inject } from 'mobx-react';
import { withStyles } from '@material-ui/core/styles';
import { Fade } from '@material-ui/core';
import { Login, Join } from '../';

import { Button, Grid, Select, MenuItem } from '@material-ui/core';

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
@observer
class Intro extends Component {
    state = {
        view: 'login',
        first: true,
        drawing: false
    }

    componentDidMount() {
        if(this.state.first) {
            setTimeout(()=>{
                this.setState({
                    drawing: true,
                    first: false
                });
            }, 600);
        }
        this.setWindowHeight();
        this.props.mainStore.setCountry();
        //this.setData();
    }

    setData = () => {
        const param = {
            dataSet: ""
        }
        const _ms = this.props.mainStore;
        this.props.funcStore.conn('post', '/dataSet', false, true, param, (res)=>{
            _ms.setDataSet(res.dataSet);
        });
    }

    setWindowHeight = () => {
        document.getElementById('app_body').style.height = '100%';
    }

    changeView = () => {
        this.setState({
            view: this.state.view == 'login' ? 'join' : 'login'
        });
    }

    changeCountry = e => {
        this.props.mainStore.setCountry(e.target.value);
    }

    render() {
        const { view, drawing } = this.state;
        const { classes } = this.props;
        const lang = this.props.mainStore.getLang();

        return (
            <Grid container alignContent="center" alignItems="center" className={classes.container}>
                <div className={classes.topBar}>
                    <Select
                        value={this.props.mainStore.country}
                        onChange={this.changeCountry}
                        variant="outlined"
                        classes={{
                            root: classes.conSelect
                        }}
                    >
                        <MenuItem value="ko">
                            Korean
                        </MenuItem>
                        <MenuItem value="en">
                            English
                        </MenuItem>
                    </Select>
                    { view == 'login' ? (
                        <Button
                            className={classes.topBtn}
                            variant="contained"
                            color="primary"
                            onClick={() => this.changeView()} >
                                {lang['join']}
                        </Button>
                    ) : (
                        <Button
                            className={classes.topBtn}
                            variant="contained"
                            color="primary"
                            onClick={() => this.changeView()} >
                                {lang['login']}
                        </Button>
                    ) }
                </div>
                <div className={classes.contentContainer}>
                    <div className={classes.contentBox}>
                        <Fade
                            in={drawing}
                            style={{ transformOrigin: '0 0 0' }}
                            {...(drawing ? { timeout: 1000 } : {})}
                        >
                            <div className={classes.logoBox}>
                                <div className={classes.logoText}>
                                    <p className={classes.logoMain2Text}>{lang['labs']}</p>
                                    <p className={classes.logoMainText}>KAVBA ABA</p>
                                    <p className={classes.logoSubText}>Korean Advancement of Behavior Analysis</p>
                                </div>
                                {/* {<img className={classes.logoImg} src="/image/kavba_logo_150.png"/>} */}
                            </div>
                        </Fade>
                        <Fade in={true}>
                            <div className={classes.inputBox}>
                                { view == 'login' ? (
                                    <Fragment>
                                        <Login />
                                    </Fragment>
                                ) : (
                                    <Fragment>
                                        <Join changeView={this.changeView}/>
                                    </Fragment>
                                ) }
                            </div>
                        </Fade>
                    </div>
                </div>
                
            </Grid>
        );
    }
}

const styles = theme => ({
    container: {
        flex: 1,
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        //backgroundImage: 'linear-gradient(150deg, #f1a829, #f18629 31%, #f16529 65%, #f14429)',
        backgroundImage: 'linear-gradient(45deg, #865995 0%,#ca5388 36%,#f55f5c 67%,#f68a15 100%)',
        '& button': {
            outline: 'none!important',
            boxShadow: 'none!important'
        }
    },
    topBar: {
        position: 'absolute',
        top: '30px',
        right: '30px',
    },
    topBtn: {
        width: 88,
        marginLeft: 8,
        backgroundColor: '#232323',
        borderColor: '#232323',
        color: '#F68A15',
        boxShadow: 'none',
        '&:hover': {
            backgroundColor: '#444',
        },
        '& span': {
            fontFamily: 'JungBold!important',
        }
    },
    contentContainer: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    contentBox: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoBox: {
        minHeight: '300px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column',
        color: '#232323',
        '& *': {
           fontFamily: 'Ssangmun!important'
        },
        '& p': {
            marginBottom: 0
        }
    },
    inputBox: {
        flex: 1,
        maxWidth: 320,
        transition: 'all .3s ease'
    },
    logoText: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    logoMainText: {
        fontSize: '6.4rem',
        lineHeight: '6.4rem',
        marginBottom: -10
    },
    logoMain2Text: {
        alignSelf: 'flex-end',
        fontSize: '2.75rem',
        lineHeight: '3rem'
    },
    logoSubText: {
        alignSelf: 'flex-start',
        fontSize: '1.3rem',
        marginLeft: 4
    },
    conSelect: {
        paddingTop: 9,
        paddingBottom: 9,
        fontSize: 14,
    }
});

export default withStyles(styles)(Intro);