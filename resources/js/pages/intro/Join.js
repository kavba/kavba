import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { withStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#232323'
        },
    },
});

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
@observer
class Join extends Component {
    state = {
        email: '',
        name: '',
        password: '',
        passwordCheck: '',
        emailText: '',
        nameText: '',
        pwText: '',
        subpwText: ''
    }

    join() {
        const { funcStore, changeView } = this.props;
        const con = this.props.mainStore.country;
        const param = {
            email: this.state.email,
            name: this.state.name,
            password: this.state.password,
            passwordCheck: this.state.passwordCheck,
        }

        funcStore.conn('post', '/join', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert(con=='ko' ? "가입 완료" : "Sign up Complete");
                changeView();
            } else {
                console.log(res);
                this.setState({
                    emailText: res.email ? res.email : '',
                    nameText: res.name ? res.name : '',
                    pwText: res.password ? res.password : '',
                    subpwText: res.passwordCheck ? res.passwordCheck : '',
                });
            }
        });
    }

    handleChange = name => e => {
        e.preventDefault();
        this.setState({ [name]: e.target.value });
    }

    render() {
        const { classes } = this.props;
        const { emailText, nameText, pwText, subpwText } = this.state;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        return (
            <div className={classes.loginContainer}>
                <ThemeProvider theme={theme}>
                    <TextField
                        id="email"
                        label={lang['email']}
                        className={classes.textField}
                        value={this.state.email}
                        onChange={this.handleChange('email')}
                        autoComplete="off"
                        variant="outlined"
                        helperText={emailText}
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                    <TextField
                        id="text"
                        label={lang['name']}
                        className={classes.textField}
                        value={this.state.name}
                        onChange={this.handleChange('name')}
                        autoComplete="off"
                        variant="outlined"
                        helperText={nameText}
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                    <TextField
                        id="password"
                        label={lang['pw']}
                        className={classes.textField}
                        type="password"
                        onChange={this.handleChange('password')}
                        autoComplete="new-password"
                        variant="outlined"
                        helperText={pwText}
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                    <TextField
                        id="passwordCheck"
                        label={lang['pwck']}
                        className={classes.textField}
                        type="password"
                        onChange={this.handleChange('passwordCheck')}
                        autoComplete="new-password"
                        variant="outlined"
                        helperText={subpwText}
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                </ThemeProvider>
                <div className={classes.btnRoot}>
                    <Button
                        variant="outlined"
                        className={classes.confirmBtn}
                        onClick={() => this.join()} >
                        {lang['request']}
                    </Button>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    loginContainer: {
        width: '100%',
        minWidth: '320px',
        display: 'flex',
        flexDirection: 'column'
    },
    textField: {
        display: 'block',
        width: '100%',
        margin: '0 0 15px',
        '& > div': {
            width: '100%'
        },
        '& input:-webkit-autofill, & input:-webkit-autofill:hover, & input:-webkit-autofill:focus, & input:-webkit-autofill:active': {
            transition: 'background-color 5000s ease-in-out 0s, color 5000s ease-in-out 0s',
        },
    },
    confirmBtn: {
        fontFamily: 'JungBold!important',
        backgroundColor: '#232323',
        borderColor: '#232323',
        color: '#CA5388',
        boxShadow: 'none',
        '&:hover': {
            backgroundColor: '#444',
        },
        '& span': {
            fontFamily: 'JungBold!important',
        }
    },
    removeLabelBottomMargin: {
        marginBottom: 0
    },
    btnRoot: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    blackOutlined: {
        borderColor: '#000!important'
    },
    helperText: {
        color: 'peachpuff'
    }
});

export default withRouter(withStyles(styles)(Join));