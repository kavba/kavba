import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { withStyles, ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
    Button, TextField, FormControlLabel, Checkbox, Select, OutlinedInput, MenuItem
} from '@material-ui/core';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#232323'
        },
    },
});

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
@observer
class Login extends Component {
    state = {
        email: '',
        password: '',
        remember: true,
        loginText: '',
        pwText: '',
    }

    login() {
        const { funcStore } = this.props;
        const con = this.props.mainStore.country;
        funcStore.conn('post', '/login', false, true, this.state, (res)=>{console.log(res);
            if(res.result == 'true') {
                this.props.history.push('/web/main/');
            } else {
                let text = "";
                if(res.fail_code == "001") {
                    text = con == 'ko' ? "입력값 필수" : "Fill Inputs Required";
                    this.setState({ loginText:text, pwText:'' });
                } else if(res.fail_code == "003") {
                    text = con == 'ko' ? "등록되지 않은 이메일" : "Unregisterd Email";
                    this.setState({ loginText:text, pwText:'' });
                } else if(res.fail_code == "004") {
                    text = con == 'ko' ? "비밀번호 불일치" : "Confirm Password";
                    this.setState({ loginText:'', pwText:text });
                } else if(res.fail_code == "002") {
                    text = con == 'ko' ? "입력값 필수" : "Fill Inputs Required";
                    this.setState({ loginText:'', pwText:text });
                } else if(res.fail_code == "005") {
                    alert("비활성화 계정입니다.\n관리자에게 문의하세요.");
                } else {
                    this.props.history.push('/web/main/');
                }
            }
        });
    }

    handleChange = name => e => {
        e.preventDefault();
        this.setState({ [name]: e.target.value });
    }

    enterKey = () => e => {
        console.log(e.keyCode);
        if(e.keyCode == 13) this.login();
    }

    handleRemember = e => {
        this.setState({ remember: e.target.checked });
    };

    render() {
        const { classes } = this.props;
        const { loginText, pwText } = this.state;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        return (
            <div className={classes.loginContainer}>
                <ThemeProvider theme={theme}>
                    <TextField
                        id="email"
                        label={lang['email']}
                        variant="outlined"
                        className={classes.textField}
                        value={this.state.email}
                        onChange={this.handleChange('email')}
                        onKeyPress={e=>{
                            if (e.key === 'Enter') {
                                this.login();
                            }
                        }}
                        helperText={loginText}
                        autoComplete="off"
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                    <TextField
                        id="password"
                        label={lang['pw']}
                        variant="outlined"
                        className={classes.textField}
                        type="password"
                        value={this.state.password}
                        helperText={pwText}
                        autoComplete="off"
                        onChange={this.handleChange('password')}
                        onKeyPress={e=>{
                            if (e.key === 'Enter') {
                                this.login();
                            }
                        }}
                        FormHelperTextProps={{
                            classes: {
                                root: classes.helperText
                            }
                        }}
                    />
                </ThemeProvider>
                <div className={classes.btnRoot}>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.remember}
                                onChange={this.handleRemember}
                                value="remember"
                                classes={{
                                    root: classes.root,
                                    checked: classes.checked,
                                }}
                            />
                        }
                        classes={{
                            root: classes.removeLabelBottomMargin
                        }}
                        label={lang['stay']}
                    />
                    <Button
                        variant="outlined"
                        className={classes.confirmBtn}
                        onClick={() => this.login()} >
                        {lang['login']}
                    </Button>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    loginContainer: {
        width: '100%',
        minWidth: '320px',
        display: 'flex',
        flexDirection: 'column'
    },
    root: {
      color: '#232323',
      '&$checked': {
        color: '#333',
      },
    },
    checked: {},
    checkLabel: {
      '& span:last-child': {
        color: '#232323!important', marginLeft: '-7px'
      }
    },
    confirmBtn: {
        fontFamily: 'JungBold!important',
        backgroundColor: '#232323',
        borderColor: '#232323',
        color: '#CA5388',
        boxShadow: 'none',
        '&:hover': {
            backgroundColor: '#444',
        },
        '& span': {
            fontFamily: 'JungBold!important',
        }
    },
    textField: {
        display: 'block',
        width: '100%',
        margin: '0 0 15px',
        '& > div': {
            width: '100%'
        },
        '& input:-webkit-autofill, & input:-webkit-autofill:hover, & input:-webkit-autofill:focus, & input:-webkit-autofill:active': {
            transition: 'background-color 5000s ease-in-out 0s, color 5000s ease-in-out 0s',
        },
        // borderColor: 'white',
        // '& input': {
        //     color: 'white'
        // },
        // '& fieldset': {
        //     borderColor: 'white',
        //     '&:hover': {
        //         borderColor: 'white'
        //     }
        // },
        // '& label': {
        //     color: 'white'
        // }
    },
    removeLabelBottomMargin: {
        marginBottom: 0
    },
    btnRoot: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    helperText: {
        color: 'peachpuff'
    }
});

export default withRouter(withStyles(styles)(Login));