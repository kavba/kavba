import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { Nav } from '../../components/';
import { Chart, Analysis, Manage, Setting } from '../';
import { Typography } from '@material-ui/core/';

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
class Main extends Component {
    state = {
        viewName: 'chart',
        isTop: true,
        dataSet: "",
        dbYears: [],
    }

    componentDidMount() {
        const path = this.props.history.location.pathname;
        const view = path.replace("/web/main/", "");
        if(['analysis', 'management', 'setting'].indexOf(view) !== -1) {
            this.setState({ viewName: view });
        } else {
            this.setState({ viewName: 'chart' });
        }
        this.setWindowHeight();
        this.props.mainStore.setCountry();
        this.setData();
        this.setDbYears();
        //window.addEventListener('scroll', this.onScroll);
    }

    setData = _d => {
        const param = {
            dataSet: _d ? _d : ""
        }
        const _ms = this.props.mainStore;
        this.props.funcStore.conn('post', '/dataSet', false, true, param, (res)=>{
            _ms.setDataSet(res.dataSet);
            if(_d) location.reload();
            else this.setState({ dataSet: res.dataSet });
        });
    }

    setDbYears = () => {
        const _ms = this.props.mainStore;
        this.props.funcStore.conn('get', '/getDbYears', false, true, null, (res)=>{
            _ms.setDbYears(res ? res.arr : []);
            this.setState({ dbYears: res ? res.arr : [] });
        });
    }

    setWindowHeight = () => {
        document.getElementById('app_body').style.height = 'calc(100% - 117px)';
    }

    onScroll = e => {
        const scrollTop = ('scroll', e.srcElement.scrollingElement.scrollTop);
        this.setState({ isTop: scrollTop < 30 });
    }

    logout = () => {
        const { mainStore, funcStore } = this.props;
        funcStore.conn('post', '/logout', false, true, null, (res)=>{
            if(res.result == 'true') {
                mainStore.auth = false;
                this.props.history.push('/web/intro');
            }
        });
    }

    moveView = view => {
        if(['analysis', 'management', 'setting'].indexOf(view) !== -1) {
            this.setState({ viewName: view });
            this.props.history.push('/web/main/' + view);
        } else {
            this.setState({ viewName: 'chart' });
            this.props.history.push('/web/main/chart');
        }
        window.scrollTo(0,0);
        this.setState({
            isTop: true
        });
    }

    sessionCheck = () => {
        const { mainStore, funcStore } = this.props;
        funcStore.conn('get', '/knock', false, true, null, (res)=>{
            console.log(res);
        });
    }

    apiCheck = () => {
        const { mainStore, funcStore } = this.props;
        const param = {
            ltoId: 1,
            studentId: 5
        }
        funcStore.conn('get', '/getStos/byStudent', false, true, param, (res)=>{
            console.log(res);
        });
    }

    render() {
        const { classes, mainStore, funcStore } = this.props;
        const { viewName, isTop, dataSet, dbYears } = this.state;
        return (
            <React.Fragment>
                <div className={classes.mainRoot}>
                    <Nav
                        isTop={isTop}
                        view={viewName}
                        moveView={this.moveView}
                        logout={this.logout}
                        dataSet={dataSet}
                        dbYears={dbYears}
                        setData={this.setData}
                        mainStore={mainStore}
                        funcStore={funcStore}
                    />
                    <div className={classes.mainContents}>
                        <Switch>
                            <Route path="/web/main/:view/:sub"
                                render={(props) => {
                                    const view = props.match.params.view;
                                    const sub = props.match.params.sub;
                                    switch(view) {
                                        case "chart" :
                                            return <Chart />;
                                        case "analysis" :
                                            return <Analysis />;
                                        case "management" :
                                            return <Manage />;
                                        case "setting" :
                                            return <Setting />;
                                        default :
                                            return <Redirect to="/web/main/chart" />;
                                    }
                                }}
                            />
                            <Route path="/web/main/:view"
                                render={(props) => {
                                    const view = props.match.params.view;
                                    switch(view) {
                                        case "chart" :
                                            return <Chart />;
                                        case "analysis" :
                                            return <Analysis />;
                                        case "management" :
                                            return <Manage />;
                                        case "setting" :
                                            return <Setting />;
                                        default :
                                            return <Redirect to="/web/main/chart" />;
                                    }
                                }}
                            />
                            <Redirect from="/web/main" to="/web/main/chart" />
                        </Switch>
                    </div>

                </div>
                <div className={classes.footerRoot}>
                    <div className={classes.footerContents}>
                        <img src={'/image/footer_logo2.png'} />
                        <div className={classes.footerItem}>
                            <Typography variant="overline" display="block" className={classes.footerText}>
                                ⓒ 2020. KAVBA ABA All rights reserved.
                            </Typography>
                            <Typography variant="overline" display="block" className={classes.footerText}>
                                신관_ 서초동 1489-7 소강빌딩 2층 & 본관_ 서초동 1487-3 서초빌딩 2층 / 대표_ 박혜숙
                            </Typography>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    mainRoot: {
        //display: 'flex',
        minHeight: '100%',
        flex: 1,
        '& button:focus': {
            outline: 'none!important',
        }
    },
    mainContents: {
        flex: 1,
        margin: '63px auto 0 auto',
        maxWidth: 1200,
        minWidth: 960,
        backgroundColor: '#faedf3',
        boxShadow: '2px 3px 20px -1px rgba(0,0,0,0.2), 0px 30px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12)',
        //marginBottom: 80
    },
    footerRoot: {
        position: 'relative',
        bottom: 0,
        width: '100%',
        //backgroundColor: 'rgba(255,255,255,.96)',
        backgroundImage: 'linear-gradient(to bottom, rgba(255,255,255,0.84), rgba(255,255,255,0.98))',
        padding: '8px 24px',
        borderTop: '1px solid #cdcdcd',
    },
    footerContents: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        '& img': {
            width: '30%',
            maxWidth: 100,
            height: 'auto'
        },
    },
    footerItem: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-between',
        paddingTop: 4,
        paddingLeft: 16
    },
    footerText: {
        fontFamily:'JungNormal',
        fontSize: '.7rem',
        color: '#888'
    }
});

export default withRouter(withStyles(styles)(Main));
