import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, TextField, Button, Input, OutlinedInput, FilledInput,
    InputLabel, MenuItem, FormControl, Select, Divider, InputAdornment,
    List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, IconButton } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import AddIcon from '@material-ui/icons/Add';
import { Comment, Delete, Save, HighlightOff } from '@material-ui/icons/';
import { Dialogs } from '../../components/';

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Criteria extends Component {

    state = {
        dialogOpen: false
    }

    resetDialog = () => {
	    // this.setState({
	    // });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    openAddDialog = () => {
        this.setState({
            dialogOpen: true,
            dialogSort: true
        });
    }

    addCriteria = () => {

    }
    
    render() {
        const { dialogOpen, dialogSort } = this.state;
        const { classes } = this.props;
        const dialogForm = (
            <React.Fragment>
                aaa
            </React.Fragment>
        );
        return(
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputRightBox}>
                        <Fab variant="extended" aria-label="add" className={classes.newBtn}
                            onClick={
                                () => this.openAddDialog()}>
                            <AddIcon className={classes.extendedIcon} />
                            {"New Criteria"}
                        </Fab>
                    </div>
                </div>
                <div className={classes.tableContainer}>

                </div>
                <Dialogs
                    kinds={"content"}
                    open={dialogOpen}
                    handleOpen={this.handleOpen}
                    title={"New CRITERIA"}
                    content={dialogForm}
                    leftFunc={{
                        label: "confirm",
                        func: ()=> {
	                        this.addCriteria();
                        }
                    }}
                    rightFunc={{
                        label: "cancel",
                        func: ()=> {
	                        this.handleOpen(false);
                        }
                    }}
                />
            </div>
        );
    }
}

const styles = theme => ({
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'flex-end',
    },
    subInputRightBox: {

    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15
    }
});

export default withStyles(styles)(Criteria);