import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, TextField, Button, Input, OutlinedInput, FilledInput,
    InputLabel, MenuItem, FormControl, Select, Divider, InputAdornment,
    List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, IconButton } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import AddIcon from '@material-ui/icons/Add';
import { Comment, Delete, Save, HighlightOff } from '@material-ui/icons/';
import { Dialogs } from '../../components/';

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Goal extends Component {
    state = {
        dialogFlag: true,
        dialogSort: true,
        dialogOpen: false,
        dialogName: '',
        dialogDesc: '',
        dialogUsing: '0',
        dialogParent: '',
        dialogTarget: '',
        usingLabelWidth: 0,
        ltoLabelWidth: 0,
        alertOpen: false,
        ltos: [],
        stos: [],
        infoLto: null,
        infoSto: null,
        target: [{
            text: '', status: '0'
        }],
        searchParent: ''
    }

    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && (this.loadLTO());
        this._isMounted && (this.loadSTO());
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this._isMounted;
    }

    loadLTO = () => {
        this.props.funcStore.conn('get', '/ltos', false, true, null, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    ltos: res.list
                });
            } else {
                console.log("LTO 로딩 실패");
            }
        });
    }

    loadSTO = () => {
        const { searchParent } = this.state;
        const param = {
            ltoId: searchParent
        }
        this.props.funcStore.conn('get', '/stos/get', false, true, param, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    stos: res.list
                });
            } else {
                console.log("STO 로딩 실패");
            }
        });
    }

    setLabelWidth = (kind, ref) => {
        const { usingLabelWidth, ltoLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != usingLabelWidth) {
            this.setState({ usingLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != ltoLabelWidth) {
            this.setState({ ltoLabelWidth: ref.offsetWidth });
        }
    }

    resetDialog = () => {
	    this.setState({
            dialogOpen: false,
            dialogName: '',
            dialogDesc: '',
            dialogUsing: '1',
            dialogParent: '',
            dialogTarget: '',
            target: [{
                text: '', status: '0'
            }]
	    });
    }

    resetInfo = () => {
        this.setState({
            alertOpen: false,
            infoLto: null,
            infoSto: null,
	    });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    handleOpenLto = () => {
        this.setState({ dialogFlag: true });
        this.handleOpen(true);
    }

    handleOpenSto = () => {
        this.setState({ dialogFlag: false });
        this.handleOpen(true);
    }

    handleAlertOpen = flag => {
        this.setState({ alertOpen: flag });
    }

    openInfo = lto  => {
        this.setState({
            dialogFlag: true,
            infoLto: lto,
            alertOpen: true
        });
    }

    openInfoSto = sto  => {
        let temp = sto;
        if(!temp.target || !temp.target.length) {
            temp.target = [{
                text: '', status: '0'
            }]
        }
        this.setState({
            dialogFlag: false,
            infoSto: temp,
            alertOpen: true
        });
    }

    handleInfoVal = flag => e => {
        const { dialogFlag, infoLto, infoSto } = this.state;
        if(dialogFlag) {
            this.setState({
                infoLto: {
                    ...infoLto,
                    [flag]: e.target.value
                }
            });
        } else {
            this.setState({
                infoSto: {
                    ...infoSto,
                    [flag]: e.target.value
                }
            });
        }
    }

    handleDialogChangeSelect = e => {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    handleInfoChangeSelect = e => {
        const { infoSto } = this.state;
        this.setState({
            infoSto: {
                ...infoSto,
                lto_id: e.target.value
            }
        });
    }

    handleDialogVal = name => event => {
        this.setState({
            [name]: event.target.value
        });
    };

    handleParent = e => {
        const parentId = e.target.value;
        this.setState({
            searchParent: parentId
        });
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadSTO();
            }, 0);
        });
    }

    handleToggle = lto => (e, val) => {
        const { ltos } = this.state;
        const param = {
            ltoId: lto.id,
            using: val
        }
        const res = this.props.funcStore.conn('post', '/lto/toggle', false, false, param, res=>console.log(res));
        if(res.result == 'true') {
            this.setState({
                ltos: ltos.map(l => lto.id == l.id ? {
                    ...l, using: val
                } : l)
            });
        } else {
            console.log("LTO 상태변경 실패");
        }
    }

    handleRemoveLto = lto => () => {
        if(!confirm("LTO를 삭제하시겠습니까?")) return;
        const { ltos } = this.state;
        const param = {
            ltoId: lto.id
        }
        this.props.funcStore.conn('post', '/lto/delete', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("LTO 삭제 완료");
                this.setState({
                    ltos: ltos.filter(l => l.id !== lto.id)
                });
            } else {
                console.log("LTO 삭제 실패");
            }
        });
    }

    handleRemoveSto = sto => () => {
        if(!confirm("STO 샘플을 삭제하시겠습니까?")) return;
        const { stos } = this.state;
        const param = {
            stoId: sto.id
        }
        this.props.funcStore.conn('post', '/sto/delete', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("STO 삭제 완료");
                this.setState({
                    stos: stos.filter(s => s.id !== sto.id)
                });
            } else {
                console.log("STO 삭제 실패");
            }
        });
    }

    handleChangeTarget = (idx, name) => e => {
        const { target } = this.state;
        if(idx == target.length - 1) {
            const t = target.map((t,i) => i == idx ?
            {
                ...t,
                [name]: e.target.value
            } : t);

            this.setState({
                target: t.concat({ text: '', status: 0 })
            });
        } else {
            if(!e.target.value) {
                this.handleChangeTargetRemove(idx);
            } else {
                this.setState({
                    target: target.map((t,i) => i == idx ?
                        {
                            ...t,
                            [name]: e.target.value
                        } : t
                    )
                });
            }
        }
    };

    handleChangeInfoTarget = (idx, name) => e => {
        const { infoSto } = this.state;
        const target = infoSto.target;
        if(target.length) {
            if(idx == target.length - 1) {
                const t = target.map((t,i) => i == idx ?
                {
                    ...t,
                    [name]: e.target.value
                } : t);
    
                this.setState({
                    infoSto: {
                        ...infoSto,
                        target: t.concat({ text: '', status: 0 })
                    }
                });
            } else {
                if(!e.target.value) {
                    this.handleChangeTargetRemove(idx);
                } else {
                    this.setState({
                        infoSto: {
                            ...infoSto,
                            target: target.map((t,i) => i == idx ?
                                {
                                    ...t,
                                    [name]: e.target.value
                                } : t
                            )
                        }
                    });
                }
            }
        }
    };

    handleChangeTargetRemove = idx => {
        const { target } = this.state;
        this.setState({
            target: target.filter((t,i) => i !== idx)
        });
    }

    handleChangeInfoTargetRemove = idx => {
        const { infoSto } = this.state;
        this.setState({
            infoSto: {
                ...infoSto,
                target: infoSto.target.filter((t,i) => i !== idx)
            }
        });
    }

    addLTO = () => {
        const { dialogName, dialogDesc, dialogUsing } = this.state;
        const param = {
            name: dialogName,
            desc: dialogDesc,
            using: dialogUsing
        }
        this.props.funcStore.conn('post', '/lto/post', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("LTO 등록 완료");
                this.loadLTO();
                this.resetDialog();
                //this.forceUpdate();
            } else {
                console.log("LTO 등록 실패");
            }
        });
    }

    addSTO = () => {
        const { dialogName, dialogDesc, dialogParent, target } = this.state;
        const param = {
            name: dialogName,
            desc: dialogDesc,
            ltoId: dialogParent,
            target: target
        }
        this.props.funcStore.conn('post', '/sto/post', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("STO 등록 완료");
                this.loadSTO();
                this.resetDialog();
                //this.forceUpdate();
            } else {
                console.log("LTO 등록 실패");
            }
        });
    }

    saveInfo = () => {
        const { infoLto, infoSto, dialogFlag } = this.state;
        if(dialogFlag) {
            const param = {
                ltoId: infoLto.id,
                name: infoLto.name,
                desc: infoLto.desc
            }
            this.props.funcStore.conn('post', '/lto/put', false, true, param, (res)=>{
                if(res.result == 'true') {
                    alert("LTO 수정 완료");
                    this.loadLTO();
                    this.resetInfo();
                } else {
                    console.log("LTO 수정 실패");
                }
            });
        } else {
            const param = {
                stoId: infoSto.id,
                ltoId: infoSto.lto_id,
                name: infoSto.name,
                desc: infoSto.desc,
                target: infoSto.target
            }
            this.props.funcStore.conn('post', '/sto/put', false, true, param, (res)=>{
                if(res.result == 'true') {
                    alert("STO 수정 완료");
                    this.loadSTO();
                    this.resetInfo();
                } else {
                    console.log("STO 수정 실패");
                }
            });
        }
    }

    render() {
        const { dialogFlag, dialogOpen, dialogDesc, dialogName, dialogUsing, dialogParent,
                usingLabelWidth, ltoLabelWidth, ltos, stos, alertOpen, infoLto, infoSto, target, searchParent } = this.state;
        const { classes } = this.props;
        const addForm = (
            <div className={classes.contentsContainer}>
                <TextField
                    id="register-name"
                    label="LTO 명"
                    className={classNames(classes.textField, classes.dialogInput)}
                    value={dialogName}
                    onChange={this.handleDialogVal('dialogName')}
                    variant="outlined"
                />
                <TextField
                    id="register-desc"
                    label="설명"
                    className={classNames(classes.textField, classes.dialogInput)}
                    value={dialogDesc}
                    onChange={this.handleDialogVal('dialogDesc')}
                    variant="outlined"
                />
                <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel
                        ref={ref => {
                            if(ref) this.setLabelWidth(1, ref);
                        }}
                        htmlFor="using"
                    >
                        사용유무
                    </InputLabel>
                    <Select
                        value={dialogUsing}
                        onChange={this.handleDialogChangeSelect}
                        input={
                        <OutlinedInput
                            labelWidth={usingLabelWidth}
                            name="dialogUsing"
                            id="using"
                        />
                        }
                    >
                        <MenuItem value="1">
                            <em>사용</em>
                        </MenuItem>
                        <MenuItem value="0">
                            <em>미사용</em>
                        </MenuItem>
                    </Select>
                </FormControl>
            </div>
        );

        const infoButton = infoLto ? (
            <Fab size="small" variant="round" aria-label="save"
                onClick={() => this.saveInfo()}>
                <Save />
            </Fab>
        ) : null;
        const infoForm = infoLto ? (
            <div className={classes.infoContainer}>
                <TextField
                    id="info-name"
                    label="LTO 명"
                    className={classNames(classes.textField, classes.dialogInput)}
                    value={infoLto.name}
                    onChange={this.handleInfoVal('name')}
                    variant="outlined"
                />
                <TextField
                    id="info-desc"
                    label="메모"
                    multiline
                    rows="4"
                    onChange={this.handleInfoVal('desc')}
                    value={infoLto.desc}
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    shrink="true"
                />
            </div>
        ) : null;

        const addStoForm = (
            <div className={classes.containerRoot}>
                <div className={classes.contentsContainer}>
                    <TextField
                        id="register-name"
                        label="STO 명"
                        className={classNames(classes.textField, classes.dialogInput)}
                        value={dialogName}
                        onChange={this.handleDialogVal('dialogName')}
                        variant="outlined"
                    />
                    <TextField
                        id="register-desc"
                        label="설명"
                        className={classNames(classes.textField, classes.dialogInput)}
                        value={dialogDesc}
                        onChange={this.handleDialogVal('dialogDesc')}
                        variant="outlined"
                    />
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(2, ref);
                            }}
                            htmlFor="parent-lto"
                        >
                            해당 LTO
                        </InputLabel>
                        <Select
                            value={dialogParent ? dialogParent : ""}
                            onChange={this.handleDialogChangeSelect}
                            input={
                                <OutlinedInput
                                    labelWidth={ltoLabelWidth}
                                    name="dialogParent"
                                    id="parent-lto"
                                />
                            }
                        >
                            <MenuItem value="">
                                <em>선택</em>
                            </MenuItem>
                            {ltos.map((l,i) => (
                                <MenuItem key={"p_" + i} value={l.id}>
                                    <em>{l.name}</em>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
                <div className={classes.containerRoot}>
                    <Typography variant="h6" color="inherit" className={classes.flex} style={{width:'100%'}}>
                        {"타겟 등록"}
                    </Typography>
                    <div className={classes.targetBox} id="target_box">
                        {target.map((t,i) => {
                            return (
                                <TextField
                                    key={"t_"+i}
                                    label="TARGET"
                                    className={classNames(classes.textField, classes.newText)}
                                    value={t.text ? t.text : ""}
                                    onChange={this.handleChangeTarget(i, 'text')}
                                    margin="normal"
                                    variant="outlined"
                                    InputProps={{
                                        endAdornment: t.text ?(
                                            <InputAdornment position="end">
                                                <IconButton
                                                    onClick={() => this.handleChangeTargetRemove(i)}
                                                >
                                                    <HighlightOff />
                                                </IconButton>
                                            </InputAdornment>
                                        ) : null
                                    }}
                                />
                            );
                        })}
                    </div>
                </div>
            </div>
        );

        const infoStoButton = infoSto ? (
            <Fab size="small" variant="round" aria-label="save" className={classes.saveBtn}
                onClick={() => this.saveInfo()}>
                <Save />
            </Fab>
        ) : null;
        const infoFormSto = infoSto ? (
            <div className={classes.infoContainer}>
                <div className={classes.infoTopBox}>
                    <TextField
                        id="info-name"
                        label="STO 명"
                        className={classNames(classes.textField, classes.dialogInput)}
                        value={infoSto.name}
                        onChange={this.handleInfoVal('name')}
                        variant="outlined"
                    />
                    <FormControl variant="outlined" className={classes.formControl}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(2, ref);
                            }}
                            htmlFor="parent-lto"
                        >
                            해당 LTO
                        </InputLabel>
                        <Select
                            value={infoSto.lto_id ? infoSto.lto_id : ""}
                            onChange={this.handleInfoChangeSelect}
                            input={
                            <OutlinedInput
                                labelWidth={ltoLabelWidth}
                                name="lto_id"
                                id="parent-lto"
                            />
                            }
                        >
                            <MenuItem value="">
                                <em>선택</em>
                            </MenuItem>
                            {ltos.map((l,i) => (
                                <MenuItem key={"p_" + i} value={l.id}>
                                    <em>{l.name}</em>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
                <TextField
                    id="info-desc"
                    label="메모"
                    multiline
                    rows="4"
                    onChange={this.handleInfoVal('desc')}
                    value={infoSto.desc}
                    className={classes.textField}
                    margin="normal"
                    variant="outlined"
                    shrink="true"
                />
                <div className={classes.targetBox} id="target_box">
                    {infoSto.target ? infoSto.target.map((t,i) => {
                        return (
                            <TextField
                                key={"t_"+i}
                                label="TARGET"
                                className={classNames(classes.textField, classes.newText)}
                                value={t.text ? t.text : ""}
                                onChange={this.handleChangeInfoTarget(i, 'text')}
                                margin="normal"
                                variant="outlined"
                                InputProps={{
                                    endAdornment: t.text ? (
                                        <InputAdornment position="end">
                                            <IconButton
                                                onClick={() => this.handleChangeInfoTargetRemove(i)}
                                            >
                                                <HighlightOff />
                                            </IconButton>
                                        </InputAdornment>
                                    ) : null
                                }}
                            />
                        );
                    }) : null}
                </div>
            </div>
        ) : null;

        return (
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputLeftBox}></div>
                    <div className={classes.subInputRightBox}>
                        <Fab variant="extended" aria-label="add" className={classes.newBtn}
                            onClick={
                                () => this.handleOpenLto()}>
                            <AddIcon className={classes.extendedIcon} />
                            {"New DOMAIN"}
                        </Fab>
                    </div>
                </div>
                <div className={classes.tableContainer}>
                    <Typography variant="h5" className={classes.subInputTitle}>
                        DOMAIN List
                    </Typography>
                    <List className={classes.listRoot}>
                        {ltos.map((lto, i) => {
                            const labelId = `lto-list-label-${i}`;
                            const u = lto.using == 0 ? '0' : '1';
                            return (
                                <ListItem key={"l_"+i} role={undefined} button className={classes.listItemRoot}
                                    onClick={()=>this.openInfo(lto)}>
                                    <ListItemText id={labelId} primary={`#${i+1} ${lto.name}`} />
                                    <ListItemSecondaryAction>
                                        <ToggleButtonGroup size="small" value={u} className={classes.toggleGroup} exclusive onChange={this.handleToggle(lto)}>
                                            <ToggleButton value="0" classes={{
                                                root: classes.toggleBtn,
                                                selected: classes.toggleSelectedBtn
                                            }}>
                                                {"미사용"}
                                            </ToggleButton>,
                                            <ToggleButton value="1" classes={{
                                                root: classes.toggleBtn,
                                                selected: classes.toggleSelectedBtn
                                            }}>
                                                {"사용"}
                                            </ToggleButton>
                                        </ToggleButtonGroup>
                                        <IconButton edge="end" aria-label="comments" onClick={this.handleRemoveLto(lto)}>
                                            <Delete className={classes.delIcon} />
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            );
                        })}
                    </List>
                </div>
                <div className={classes.space60}></div>
                <div>
                    <div className={classes.subInputBox}>
                        <div className={classes.subInputLeftBox}>
                            <FormControl variant="outlined" className={classes.subInputSelect}>
                                <InputLabel
                                    ref={ref => {
                                        if(ref) this.setLabelWidth(2, ref);
                                    }}
                                    htmlFor="search-lto"
                                >
                                    상위 LTO
                                </InputLabel>
                                <Select
                                    value={searchParent ? searchParent : ""}
                                    onChange={this.handleParent}
                                    input={
                                        <OutlinedInput
                                            labelWidth={ltoLabelWidth}
                                            name="searchParent"
                                            id="search-lto"
                                        />
                                    }
                                >
                                    <MenuItem value="">
                                        <em>전체</em>
                                    </MenuItem>
                                    {ltos.map((l,i) => (
                                        <MenuItem key={"p_" + i} value={l.id}>
                                            <em>{l.name}</em>
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </div>
                        <div className={classes.subInputRightBox}>
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={
                                    () => this.handleOpenSto()}>
                                <AddIcon className={classes.extendedIcon} />
                                {"New Sample STO"}
                            </Fab>
                        </div>
                    </div>
                    <div className={classes.tableContainer}>
                        <Typography variant="h5" className={classes.subInputTitle}>
                            STO List
                        </Typography>
                        <List className={classes.listRoot}>
                            {stos.map((sto, i) => {
                                let targets = "";
                                if(sto.target) {
                                    sto.target.map((item,idx) => {
                                        if(item.text && item.text != '') {
                                            if(idx > 0) targets += " / ";
                                            targets += item.text;
                                        }
                                    });
                                }
                                return (
                                    <ListItem key={"s_"+i} role={undefined} button className={classes.listItemRoot}
                                        onClick={()=>this.openInfoSto(sto)}>
                                        <ListItemText primary={`#${i+1} ${sto.name}`} className={classes.listCol1}/>
                                        <ListItemText primary={targets ? `(targets : ${targets})` : ''} className={classes.listCol2}/>
                                        <ListItemText primary={sto.lto_id ? `LTO : ${sto.lto_id}` : ''} className={classes.listCol3}/>
                                        
                                        <ListItemSecondaryAction>
                                            <IconButton edge="end" aria-label="comments" onClick={this.handleRemoveSto(sto)}>
                                                <Delete className={classes.delIcon} />
                                            </IconButton>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </div>
                </div>
                <Dialogs
                    kinds={"content"}
                    open={dialogOpen}
                    handleOpen={this.handleOpen}
                    title={dialogFlag ? "New LTO" : "New STO"}
                    content={dialogFlag ? addForm : addStoForm}
                    leftFunc={{
                        label: "추가",
                        func: ()=> {
	                        dialogFlag ? this.addLTO() : this.addSTO();
                        }
                    }}
                    rightFunc={{
                        label: "닫기",
                        func: ()=> {
	                        this.handleOpen(false);
                        }
                    }}
                />
                <Dialogs
                    kinds={"alert"}
                    open={alertOpen}
                    handleOpen={this.handleAlertOpen}
                    title={dialogFlag ? ("LTO info" + (infoLto ? " // " + infoLto.name : "")) : ("STO info" + (infoSto ? " // " + infoSto.name : ""))}
                    subButton={dialogFlag ? infoButton : infoStoButton}
                    content={dialogFlag ? infoForm : infoFormSto}
                />
            </div>
        );
    }
}

const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    contentsContainer: {
        display: 'flex'
    },
    containerRoot: {
        display: 'flex',
        flexDirection: 'column'
    },
    infoContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    infoTopBox: {
        display: 'flex'
    },
    textField: {
        flex: 1,
        margin: 8
    },
    formControl: {
        minWidth: 120,
        margin: 8
    },
    listRoot: {
        width: '100%',
        marginTop: 5,
        '& > li:not(:first-child)': {
            borderTop: '1px solid #fff'
        }
    },
    listItemRoot: {
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
    },
    space60: {
        width: '100%',
        padding: '30px 0',
    },
    saveBtn: {
        position: 'absolute',
        top: 5,
        right: 5
    },
    contentDivider: {
        marginTop: 30,
        marginBottom: 30,
    },
    newText: {
        flex: 1,
    },
    targetBox: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column'
    },
    listCol1: {
        flex: 1
    },
    listCol2: {
        flex: 3
    },
    listCol3: {
        width: 120,
        flex: 'none'
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {
        flex: 1,
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
    },
    subInputRightBox: {
        display: 'flex',
        alignItems: 'center'
    },
    subInputTitle: {
        fontFamily: 'JungBold',
        marginRight: 15,
        color: '#fff',
        // '&::after': {
        //     content: `''`,
        //     width: '100%',
        //     maxWidth: 150,
        //     height: '50%',
        //     alignSelf: 'flex-start',
        //     borderBottom: '2px solid #fff'
        // }
    },
    subInputSelect: {
        width: 120,
        marginRight: 15
    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15,
        padding: 30,
        borderRadius: 5,
        backgroundColor: '#333',
        color: '#fff'
    },
    toggleGroup: {
        backgroundColor: 'transparent',
        marginRight: 15
    },
    toggleBtn: {
        color: '#fff',
        backgroundColor: 'transparent',
        borderColor: '#fff'
    },
    toggleSelectedBtn: {
        color: '#232323!important',
        backgroundColor: '#fff!important'
    },
    delIcon: {
        color: '#fff'
    }
});

export default withStyles(styles)(Goal);