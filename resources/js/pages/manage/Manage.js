import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject } from 'mobx-react';
import { Typography, Button } from '@material-ui/core/';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { Student, Program, Teacher, Error } from '../';
import { Bread } from '../../components/';

const subMenus = [
    {title: "DOMAIN", value: "domain"},
    {title: "STUDENT", value: "student"},
    {title: "TEACHER", value: "teacher"},
    //{title: "ERROR", value: "error"},
];

const subMenusForLv1 = [
    {title: "DOMAIN", value: "domain"},
    {title: "STUDENT", value: "student"},
];

@inject(stores => ({
    funcStore: stores.funcStore,
    mainStore: stores.mainStore,
}))
class Manage extends Component {
    state = {
        view: 'domain'
    }
    
    handleView = (e, page) => {
        if(!page || this.state.view == page) return;
        this.setState({ view: page });
    }

    render() {
        const { view } = this.state;
        const { classes, mainStore } = this.props;
        let page = null;
        if(view == 'student') {
            page = (<Student />);
        } else if(view == 'domain') {
            page = (<Program />);
        } else if(view == 'teacher') {
            page = (<Teacher />);
        }
        // else if(view == 'error') {
        //     page = (<Error />);
        // }
        
        return (
            <div>
                <Bread title={"MANAGEMENT"} subTitle={view} subMenus={mainStore.user.level > 0 ? subMenus : subMenusForLv1} handleSubMenu={this.handleView}/>
                {page}
            </div>
        );
    }
}

const styles = theme => ({
    onBtn: {
        
    },
    offBtn: {

    }
});

export default withStyles(styles)(Manage);