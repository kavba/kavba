import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, Button, IconButton, Input, OutlinedInput, FilledInput, TextField,
	InputLabel, MenuItem, FormControl, Select, FormControlLabel, Checkbox,
	Card, CardActionArea, CardActions, CardContent, ButtonGroup, InputAdornment,
    ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';
import ReactPaginate from 'react-paginate';
import AddIcon from '@material-ui/icons/Add';
import { Edit, Delete, Save, BarChart, Search } from '@material-ui/icons/';
import { Dialogs } from '../../components/';
import { StoSlide, LtoReachSlide } from '../../pages';

function sleep(delay = 0) {
    return new Promise(resolve => {
        setTimeout(resolve, delay);
    });
}

function Indicators(props) {
    const [open, setOpen] = React.useState(false);
    const loading = open && props.userList.length === 0;
    const studentId = props.student ? props.student.student_id : '';
    React.useEffect(() => {
        let active = true;
        if (!props.dialogOpen) {
            return undefined;
        }
        (async () => {
            // const url = window.location.origin + '/web/user/'+props.user.id+'/classUsers';
            const url = window.location.origin + '/web/user/'+studentId+'/classUsersByStudent';
            const doing = {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN' : document.getElementById('csrf').getAttribute('content'),
                    'ACCESS-FROM' : 'web'
                }
            }
            const response = await fetch(url, doing);
            await sleep(1e3);
            const data = await response.json();

            if (active) {
                props.setIndicators(data.users, studentId);
            }
        })();

        return () => {
            active = false;
        };
    }, [loading]);

    return (
        <Autocomplete
            id="asynchronous-demo"
            style={{ width: 180 }}
            open={open}
            onChange={(e, v)=> {
                props.handleIndicator(v);
            }}
            onOpen={() => {
                setOpen(true);
            }}
            onClose={() => {
                setOpen(false);
            }}
            getOptionLabel={option => option.name ? option.name : ""}
            options={props.userList}
            loading={loading}
            value={props.assignUser}
            renderInput={params => (
                <TextField
                {...params}
                label={props.lang ? props.lang['domain7'] : ""}
                fullWidth
                variant="outlined"
                InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                    <React.Fragment>
                        {loading ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                    </React.Fragment>
                    ),
                }}
                />
            )}
            classes={{
                root: props.classes.indicatorRoot
            }}
        />
    );
}

const CustomCheckbox = withStyles({
    root: {
      color: '#333',
      '&$checked': {
        color: '#333',
      },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

class ProgramList extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.dialogOpen == nextProps.dialogOpen;
    }

    handleChangePage = e => {
        this.props.handlePage(e.selected);
    };

    render() {
        const { classes, funcStore, mainStore, programList, expanded, total, page, perPage } = this.props;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        return (
            <React.Fragment>
                {programList.map((st, idx1) => {
                    if(st) {
                        const programs = st.programs;
                        const studentName = st.student_name ? st.student_name : "NULL";
                        const studentBirth = st.student_birth ? st.student_birth : "2002-01-01";
                        return (
                            <ExpansionPanel key={idx1} classes={{ root: classes.ltoRoot }}
                                expanded={expanded === ('panel_' + idx1)} onChange={this.props.handelExpand('panel_' + idx1)}
                            >
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls="panel1a-content"
                                    id={"panel-" + idx1 + "-header"}
                                    classes={{
                                        root: classes.ltoSummaryRoot,
                                        expandIcon: classes.summaryIcon
                                    }}
                                >
                                    <Typography className={classes.heading}>
                                        {"["+st.class_names+"] "+studentName +" / "+ studentBirth + " ["+funcStore.calcMonths(studentBirth)+" "+lang['domain3']+"]"}
                                    </Typography>
                                    <div className={classes.penelLtoCnt}>{"DOMAIN CELL : " + programs.length}</div>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails className={classes.panelDetail}>
                                    <div className={classes.detailContentTitle}>
                                        <Typography style={{fontSize:'.9rem'}}>
                                            {"- "+lang['domain1']+" : " + st.total + " / " + (st.total_reach ? st.total_reach : 0)}
                                        </Typography>
                                        {st.total_reach ? (
                                            <Button
                                                variant="outlined" className={classes.barChartBtn}
                                                onClick={(e)=>{
                                                    e.preventDefault();
                                                    this.props.openReachSlide(st);
                                                }}
                                            ><BarChart /></Button>
                                        ) : null}
                                    </div>
                                    {programs.map((pg, idx2) => {
                                        return (
                                            <Card
                                                key={pg.lto_name + idx2}
                                                className={classes.card}
                                                classes={{
                                                    root: mainStore.user.id == pg.user_id ? classes.cardRootMy : classes.cardRoot
                                                }}
                                            >
                                                <CardActionArea>
                                                    <CardContent className={classes.cardContent}
                                                        onClick={()=>{
                                                            this.props.selectProgram(true, pg, st, idx2);
                                                        }}
                                                    >
                                                        <div className={classes.penelProgramCnt}>
                                                            <div className={classes.penelCntItem}>{"LTO/"+lang['stoitem18']+" : " + pg.lto_count + "/" + pg.finish_lto}</div>
                                                        </div>
                                                        <Typography variant="body1" style={{fontSize:'.8rem'}}>
                                                        {lang['domain2']+" : " + pg.user_name}
                                                        </Typography>
                                                        <Typography variant="body2">
                                                            {"#" + (idx2+1) + ". DOMAIN : " + pg.lto_name}
                                                        </Typography>
                                                    </CardContent>
                                                </CardActionArea>
                                                <CardActions>
                                                    <div className={classes.penelProgramRightRoot}>
                                                        <div className={classes.penelProgramBtn}>
                                                            {(mainStore.user.level > 10) && (
                                                                <React.Fragment>
                                                                    <IconButton
                                                                        className={classes.button} aria-label="edit"
                                                                        onClick={() => this.props.openEditialog(pg, st)}
                                                                    >
                                                                        <Edit />
                                                                    </IconButton>
                                                                    <IconButton
                                                                        className={classes.button} aria-label="delete"
                                                                        onClick={() => this.props.removeProgram(pg)}
                                                                    >
                                                                        <Delete />
                                                                    </IconButton>
                                                                </React.Fragment>
                                                            )}
                                                        </div>
                                                    </div>
                                                </CardActions>
                                            </Card>
                                        );
                                    })}
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        );
                    }
                })}
                {total > perPage && (
                    <ReactPaginate
                        previousLabel={"<"}
                        nextLabel={">"}
                        breakLabel={<span className="gap">...</span>}
                        pageCount={parseInt((total - 1) / perPage) + 1}
                        pageRangeDisplayed={20}
                        marginPagesDisplayed={10}
                        onPageChange={this.handleChangePage}
                        containerClassName={"pagination"}
                        previousLinkClassName={"previous_page"}
                        nextLinkClassName={"next_page"}
                        disabledClassName={"disabled"}
                        activeClassName={"active"}
                    />
                )}
            </React.Fragment>
        );
    }
}

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
class Program extends Component {
    state = {
        page: 0,
        perPage: 10,
        total: 0,
        dialogOpen: false,
        dialogSort: true,
        slideOpen: false,
        slideOrder: '',
        domainGraphOpen: false,
        selectedProgram: null,
        selectedStudent: null,
        classList: [],
        studentList: [],
        programList: [],
        ltoList: [],
        classId: '',
        studentId: '',
        ltoId: '',
        rLtos: false,
        classLabelWidth: 0,
        studentLabelWidth: 0,
        ltoLabelWidth: 0,
        assignLabelWidth: 0,
        keywordLabelWidth: 0,
        expanded: "",
        userList: [],
        assignUser: '',
        keyword: "",
    }
    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && (this.loadProgramList());
        this._isMounted && (this.loadListsForDialog());
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(this._isMounted) {
            return true;
        }
        
        return false;
    }

	loadProgramList = () => {
		const { funcStore } = this.props;
        const { programList, page, perPage, keyword } = this.state;
        const param = {
            page: page,
            perPage: perPage,
            keyword: keyword,
        }
		// load program list
		funcStore.conn('get', '/programs', false, true, param, (res)=>{
            if(res.students) {
	            if(JSON.stringify(res.studnets) != JSON.stringify(programList)) {
		            this.setState({
                        total: res.total,
                        programList: res.students,
	                });
	            }
            } else {
                console.log("도메인 로딩 실패");
            }
        });
	}

    loadListsForDialog = () => {
        const { funcStore } = this.props;
        const { ltoList, classList } = this.state;
        // load lto list
        funcStore.conn('get', '/ltos', false, true, null, (res)=>{
            if(res.list) {
	            if(JSON.stringify(res.list) != JSON.stringify(ltoList)) {
		            this.setState({
	                    ltoList: res.list
	                });
	            }
            } else {
                console.log("DOMAIN 로딩 실패");
            }
        });
        // load class list
        funcStore.conn('get', '/user/getClasses', false, true, null, (res)=>{
            if(res) {
	            if(JSON.stringify(res) != JSON.stringify(classList)) {
		            this.setState({
                        classList: res
                        //classList: res.filter(v => v.id != 0)
	                });
	            }
            } else {
                console.log("CLASS 로딩 실패");
            }
        });
    }
    
    loadStudentList = classId => {
	    const { funcStore } = this.props;
	    const cId = classId ? classId : 0;
        // load student list
        funcStore.conn('get', '/students/' + cId + '/class', false, true, null, (res)=>{
            if(res.list) {
                this.setState({
                    studentList: res.list
                });
            } else {
                console.log("STUDENT 로딩 실패");
            }
        });
    }

    setLabelWidth = (kind, ref) => {
        const { classLabelWidth, studentLabelWidth, ltoLabelWidth, keywordLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != studentLabelWidth) {
            this.setState({ studentLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != ltoLabelWidth) {
            this.setState({ ltoLabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != assignLabelWidth) {
            this.setState({ assignLabelWidth: ref.offsetWidth });
        } else if(kind == 5 && ref.offsetWidth != keywordLabelWidth) {
            this.setState({ keywordLabelWidth: ref.offsetWidth });
        }
    }
    
    resetDialog = () => {
	    this.setState({
            studentList: [],
            classId: '',
            studentId: '',
            ltoId: '',
            assignUser: ''
	    });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    openNewDialog = () => {
        this.setState({
            dialogSort: true,
            dialogOpen: true,
        });
    }

    openEditialog = (program, student) => {
        this.setState({
            dialogSort: false,
            dialogOpen: true,
            classId: student.class_id,
            studentId: program.student_id,
            ltoId: program.lto_id,
            selectedProgram: program,
            selectedStudent: student, 
            assignUser: {
                id: program.user_id,
                name: program.user_name
            }
        });
    }

    openReachSlide = student => {
        this.setState({
            selectedStudent: student
        });
        this.handleReachSlide(true);
    }

    handleReachSlide = flag => {
        if(!flag) {
            this.setState({
                selectedStudent: '',
                domainGraphOpen: flag,
            });
        } else {
            this.setState({
                domainGraphOpen: flag,
            });
        }
    }

    selectProgram = (open, p, s, order) => {
        this.setState({
            selectedStudent: s,
            selectedProgram: p,
            slideOpen: open,
            slideOrder: order
        });
    }

    handleProgramDialogChange = e => {
	    if(e.target.name == 'classId') {
		    if(e.target.value == 0 || e.target.value != '') {
			    this.loadStudentList(e.target.value);
		    } else {
			    this.setState({
				    studentId: '', studentList: []
			    });
		    }
	    }
	    this.setState({
            [e.target.name]: e.target.value,
        });
    }
    
    handleDialogCheck = e => {
        this.setState({
            rLtos: e.target.checked,
        });
    }

    handlePage = page => {
        let t = page;
        if(!page) t = 0;
        this.setState({
            page: page
        });
        this.reloadProgramList();
    }

    reloadProgramList = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadProgramList();
            }, 0);
        });
    }
	
	validation = () => {
		const { ltoId, classId, studentId } = this.state;
		if(classId == '') {
			// **alert
			return false;
		}
		if(studentId == '') {
			// **alert
			return false;
		}
		if(ltoId == '') {
			// **alert
			return false;
		}
		return true;
	}
	
	addProgram = () => {
		if(!this.validation) return;
		
		const { funcStore } = this.props;
        const { studentList, studentId, ltoId, rLtos, classId } = this.state;
        const student = studentList.find(s => s.id === studentId);

		const param = {
            classId: classId,
			ltoId: ltoId,
            studentId: studentId,
            classRelationId: student.class_relation_id,
            buildAll: rLtos
        }
        funcStore.conn('post', '/program/post', false, true, param, (res)=>{
            if(res.result == "true") {
                alert("도메인 등록 완료");
                this.reloadProgramList();
            } else {
                console.log("도메인 입력 실패");
            }
            this.handleOpen(false);
        });
		// funcStore.conn('get', '/program/studentLto', false, true, param, (res1)=>{
        //     if(res1 == 0) {
	    //         // register
		//         funcStore.conn('post', '/program/post', false, true, param, (res2)=>{
		//             if(res2.result == "true") {
		// 	            // **alert
		// 	            // list 초기화
		// 	            tempFunc2();
		//                 console.log("도메인 등록 완료");
		//                 this.forceUpdate();
		//             } else {
		//                 console.log("도메인 입력 실패");
		//             }
		//             tempFunc(false);
		//         });
        //     } else {
	    //         if(confirm("동일 LTO 도메인이 존재합니다\n추가하시겠습니까?")) {
		//             // register
		// 	        funcStore.conn('post', '/program/post', false, true, param, (res2)=>{
		// 	            if(res2.result == "true") {
		// 		            // **alert
		// 		            // list 초기화
		// 		            tempFunc2();
		// 	                console.log("도메인 등록 완료");
		// 	                this.forceUpdate();
		// 	            } else {
		// 	                console.log("도메인 입력 실패");
		// 	            }
		// 	            tempFunc(false);
		// 	        });
	    //         }
        //     }
        // });
    }
    
    saveProgram = () => {
        const { selectedProgram, ltoId, assignUser } = this.state;
        const param = {
            ltoId: ltoId,
            assignId: assignUser ? assignUser.id : null
		}
        this.props.funcStore.conn('post', '/program/'+selectedProgram.id+'/put', false, true, param, (res)=>{
            if(res.result == "true") {
                alert("도메인 수정 완료");
                this.reloadProgramList();
            } else {
                if(res.error == 100) {
                    alert("운영자만 수정 가능합니다");
                } else console.log("도메인 수정 실패");
            }
            this.handleOpen(false);
        });
    }

    removeProgram = program => {
        if(!confirm("도메인에 해당하는 모든 STO와 체크 정보가 제거됩니다.\n계속하시겠습니까?")) return;
        this.props.funcStore.conn('post', '/program/'+program.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                alert("도메인 삭제 완료");
                this.reloadProgramList();
            } else {
                if(res.error == 100) {
                    alert("운영자만 삭제 가능합니다");
                } else console.log("도메인 삭제 실패");
            }
        });
    }
	
	handelExpand = panel => (event, isExpanded) => {
        this.setState({
            expanded: (isExpanded ? panel : false),
        });
    }
    
    handleSlide = flag => {
	    this.setState({
		    slideOpen: flag
	    });
    }

    setIndicators = (users, studentId) => {
        this.setState({
            userList: users,
            indicatorStudent: studentId
        });
        this.forceUpdate();
    }

    handleIndicator = user => {
        this.setState({
            assignUser: user
        });
    }

    handleChangeKeyword = e => {
        this.setState({ keyword: e.target.value });
    }

    handleKeyPress = e => {
        if(e.key === 'Enter') {
            this.setState({ page:0 });
            this.reloadProgramList();
        }
    }

    handleClickSearching = e => {
        if(!this.state.keyword) return;
        this.setState({ page:0 });
        this.reloadProgramList();
    }

    handleMouseDownSearching = e => {
        e.preventDefault();
    };
    
    render() {
        const { mainStore, classes } = this.props;
        const { dialogOpen, dialogSort, slideOpen, slideOrder, selectedProgram, selectedStudent, rLtos, userList, assignUser, domainGraphOpen,
            classLabelWidth, studentLabelWidth, ltoLabelWidth, assignLabelWidth, keywordLabelWidth,
            classList, classId, studentList, studentId, indicatorStudent, ltoList, ltoId, programList, expanded, keyword } = this.state;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);
        const dialogButton = !dialogSort ? (
            <Fab size="small" aria-label="save" className={classes.editSaveBtn}
                onClick={() => this.saveProgram()}>
                <Save />
            </Fab>
        ) : null;
        const dialogForm = (
            <div className={classes.contentsContainer}>
                <div className={classes.contentsContainerRow}>
                    {dialogSort ? (
                        <React.Fragment>
                            <form className={classes.formRoot} autoComplete="off">
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel
                                        ref={ref => {
                                            if(ref) this.setLabelWidth(1, ref);
                                        }}
                                        htmlFor="register-class"
                                    >
                                        {lang['domain4']}
                                    </InputLabel>
                                    <Select
                                        value={classId}
                                        onChange={this.handleProgramDialogChange}
                                        input={
                                        <OutlinedInput
                                            labelWidth={classLabelWidth}
                                            name="classId"
                                            id="register-class"
                                        />
                                        }
                                    >
                                        <MenuItem value="">
                                            <em>선택</em>
                                        </MenuItem>
                                        {classList.map((v,i) => {
                                            return (
                                                <MenuItem key={i} value={v.id}>
                                                    {v.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>
                            </form>
                            <form className={classes.formRoot} autoComplete="off">
                                <FormControl variant="outlined" className={classes.formControl}>
                                    <InputLabel
                                        ref={ref => {
                                            if(ref) this.setLabelWidth(2, ref);
                                        }}
                                        htmlFor="register-student"
                                    >
                                        {lang['domain5']}
                                    </InputLabel>
                                    <Select
                                        value={studentId}
                                        onChange={this.handleProgramDialogChange}
                                        input={
                                        <OutlinedInput
                                            labelWidth={studentLabelWidth}
                                            name="studentId"
                                            id="register-student"
                                        />
                                        }
                                    >
                                        <MenuItem value="">
                                            <em>{lang['etc1']}</em>
                                        </MenuItem>
                                        {studentList.map((v,i) => {
                                            return (
                                                <MenuItem key={i} value={v.id}>
                                                    {v.name}
                                                </MenuItem>
                                            );
                                        })}
                                    </Select>
                                </FormControl>
                            </form>
                        </React.Fragment>
                    ) : (
                        <React.Fragment>
                            <TextField
                                id="register-class"
                                label={lang['domain4']}
                                value={selectedStudent.class_name}
                                className={classes.editInputs}
                                variant="outlined"
                                disabled={true}
                            />
                            <TextField
                                id="register-student"
                                label={lang['domain5']}
                                value={selectedStudent.student_name}
                                className={classes.editInputs}
                                variant="outlined"
                                disabled={true}
                            />
                        </React.Fragment>
                    )}
                    
                    <form className={classNames(classes.formRoot, classes.ltoBox)} autoComplete="off">
                        <FormControl variant="outlined" className={classNames(classes.formControl, classes.ltoBoxInput)}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(3, ref);
                                }}
                                htmlFor="register-lto"
                            >
                                {lang['domain6']}
                            </InputLabel>
                            <Select
                                value={ltoId}
                                onChange={this.handleProgramDialogChange}
                                input={
                                    <OutlinedInput
                                        labelWidth={ltoLabelWidth}
                                        name="ltoId"
                                        id="register-lto"
                                    />
                                }
                                disabled={rLtos}
                            >
                                <MenuItem value="">
                                    <em>선택</em>
                                </MenuItem>
                                {ltoList.map((v,i) => {
                                    return (
                                        <MenuItem key={i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </form>
                </div>
                <div className={classes.contentsContainerRow}>
                    {!dialogSort && (
                        <Indicators
                            classes={classes}
                            dialogOpen={dialogOpen}
                            user={mainStore.user}
                            student={selectedStudent}
                            assignUser={assignUser}
                            userList={userList}
                            prevStudent={indicatorStudent}
                            setIndicators={this.setIndicators}
                            handleIndicator={this.handleIndicator}
                            lang={lang}
                        />
                    )}
                    <FormControlLabel
                        className={classes.checkboxRoot}
                        control={
                            <CustomCheckbox
                                checked={rLtos} onChange={this.handleDialogCheck} value="all"
                            />
                        }
                        label="Create All Domains"
                    />
                </div>
            </div>
        );

        return (
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputLeftBox}>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <InputLabel 
                                ref={ref => {
                                    if(ref) this.setLabelWidth(5, ref);
                                }}
                                htmlFor="searching"
                            >{lang['board3']}</InputLabel>
                            <OutlinedInput
                                id="searching"
                                type="text"
                                labelWidth={keywordLabelWidth}
                                value={keyword}
                                onChange={this.handleChangeKeyword}
                                onKeyPress={this.handleKeyPress}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="searching"
                                            onClick={this.handleClickSearching}
                                            onMouseDown={this.handleMouseDownSearching}
                                        >
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </div>
                    <div className={classes.subInputRightBox}>
                        { mainStore.user.level > 0 ? (
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={
                                    () => this.openNewDialog()}>
                                <AddIcon className={classes.extendedIcon} />
                                {"NEW DOMAIN CELL"}
                            </Fab>
                        ) : ""}
                        
                    </div>
                </div>
                <div className={classes.tableContainer}>
                    <ProgramList classes={classes} {...this.state}
                        funcStore={this.props.funcStore}
                        mainStore={this.props.mainStore}
                        handelExpand={this.handelExpand}
                        selectProgram={this.selectProgram}
                        openEditialog={this.openEditialog}
                        removeProgram={this.removeProgram}
                        handlePage={this.handlePage}
                        openReachSlide={this.openReachSlide}
                    />
                </div>
                <LtoReachSlide
                    open={domainGraphOpen}
                    student={selectedStudent}
                    handleSlide={this.handleReachSlide}
                />
                {dialogSort ? (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"New Domain Cell"}
                        content={dialogForm}
                        leftFunc={{
                            label: lang['domain8'],
                            func: ()=> {
                                this.addProgram();
                            }
                        }}
                        rightFunc={{
                            label: lang['domain9'],
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                ) : (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"Edit Domain Cell"}
                        subButton={dialogButton}
                        content={dialogForm}
                        rightFunc={{
                            label: lang['domain9'],
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                )}
                
                <StoSlide
                	open={slideOpen}
                	handleSlide={this.handleSlide}
                    program={selectedProgram}
                    student={selectedStudent}
                    order={slideOrder}
                />
            </div>
        );
    }
}

const styles = theme => ({
    masterRoot: {
        padding: 30,
        '& .pagination': {
            justifyContent: 'center',
            padding: '10px 0 15px',
            '& a' : {
                fontFamily: 'JungBold',
                fontSize: 16,
                cursor: 'pointer',
                padding: '0 5px',
                color: '#aaa',
                '&:hover': {
                    fontWeight: 'bold',
                    color: '#444'
                },
                '&:focus': {
                    outline: 'none!important',
                    boxShadow: 'none!important'
                },
            },
            '& > li.active a': {
                color: '#444'
            }
        }
    },
    ltoRoot: {
        backgroundColor: '#333',
        '&:before': {
            backgroundColor: '#eee'
        }
    },
    ltoSummaryRoot: {
        color: '#fff',
    },
    summaryIcon: {
        color: '#fff'
    },
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        width: '100%'
    },
    editInputs : {
        margin: theme.spacing(1),
        flex: 1,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    contentsContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    contentsContainerRow: {
        flex: 1,
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'space-between'
    },
    ltoBox: {
        flex: 3
    },
    ltoBoxInput: {
        flex: 1
    },
    panelDetail: {
	    justifyContent: 'space-between',
        flexWrap: 'wrap',
        padding: '0 24px 24px'
    },
    detailContentTitle: {
        width: '100%',
        color: '#fff',
        fontSize: '.9rem',
        marginBottom: 8,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    card: {
	    flex: '1 0 33%',
	    margin: 5,
        maxWidth: 'calc(50% - 10px)',
        color: '#fff'
    },
    cardContent: {
        display: 'flex',
        flexDirection: 'column',
        '& p': {
            fontSize: '1rem',
            wordBreak: 'keep-all'
        }
    },
    cardRoot: {
        display: 'flex',
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
    },
    cardRootMy: {
        display: 'flex',
        backgroundImage: 'linear-gradient(to right, #ca5388 0%,#f55f5c 65%,#f68a15 100%)',
    },
    cardBtnBox: {
        display: 'flex'
    },
    button: {
        color: '#fff'
    },
    editSaveBtn: {
        // position: 'absolute',
        // top: 5,
        // right: 5
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {

    },
    subInputRightBox: {

    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    tableContainer: {
        marginTop: 15
    },
    penelProgramRightRoot: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    penelProgramCnt: {
        display: 'flex',
        flexDirection: 'row',
    },
    penelCntItem: {
        display: 'flex',
        minWidth: 65,
        fontSize: '0.75rem',
        lineHeight: '0.9rem',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        color: '#232323',
        padding: '4px 10px 2px',
        borderRadius: 5,
        fontFamily: 'JungNormal',
        opacity: '0.84',
        marginBottom: 8,
        marginRight: 8
    },
    penelProgramBtn: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    penelLtoCnt: {
        display: 'flex',
        fontSize: '0.7rem',
        lineHeight: '0.7rem',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff',
        color: '#232323',
        padding: '2px 10px',
        marginLeft: 15,
        borderRadius: 5,
        fontFamily: 'JungNormal'
    },
    checkboxRoot: {
        margin: '8px 8px 8px 0px'
    },
    indicatorRoot: {
        margin: 8
    },
    barChartBtn: {
        borderColor: '#fff',
        marginLeft: 15,
        padding: '3px 15px',
        '& > span': {
            color: '#fff'
        },
        minWidth: 30,
        '& svg': {
            fontSize: '1.1rem'
        }
    },
});

export default withStyles(styles)(Program);