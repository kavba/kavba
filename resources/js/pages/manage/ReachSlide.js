import React from 'react';
import { inject } from 'mobx-react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
  } from 'recharts';
import { Button, Fab, Dialog, ListItem, List, Divider, AppBar, Toolbar, IconButton, OutlinedInput,
    Typography, Slide, Grid, InputLabel, MenuItem, FormControl, Select, TextField, Popover,
    FormControlLabel, Checkbox } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import CachedIcon from '@material-ui/icons/Cached';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="down" ref={ref} {...props} />;
});

class CustomizedAxisTick extends React.Component {
    render() {
        const { x, y, stroke, payload, index } = this.props;
        return (
            <g transform={`translate(${x},${y})`}>
                <text x={0} y={0} dy={16} textAnchor="end" fill="#666" fontSize="12" transform="rotate(-30)">{payload.value}</text>
            </g>
        );
    }
}

const CustomTooltip = ({ active, payload, label }) => {
    if (active && payload[0]) {
        const tmp = payload[0].payload;
        const ltos = tmp.ltos ? tmp.ltos.split(",").map((t,i) => {
            let text = "";
            if(i > 0) text += " / ";
            return text + t;
        }) : "-";
        return (
            <div style={{
                whiteSpace: 'pre-line',
                lineHeight: '0.8rem',
                padding: '15px 15px 0 15px',
                backgroundColor: '#fff',
                border: '1px solid #bbb'
            }}>
                <p>{tmp.name}</p>
                <p>{"완료 LTO : " + tmp.reach}</p>
                <p>{"LTOs : " + ltos}</p>
            </div>
        );
    }
    return null;
};

@inject(stores => ({
    funcStore: stores.funcStore
}))
class ReachSlide extends React.Component {
    state = {
        open: this.props.open,
        first: true,
        data: [],
        title: '',
        ticks: [],
        domain: [],
        loadLtoReachFunc: null,
    }

    componentDidMount() {
        const func = this.drawReachGraph;
        this.setState({
            loadLtoReachFunc: func
        });
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.open !== prevState.open) {
            if(nextProps.open) {
                if(nextProps.student) {
                    const s = nextProps.student;
                    const d = prevState.loadLtoReachFunc(s);
                    return {
                        open: nextProps.open,
                        first: nextProps.open,
                        data: d,
                        title: " _"+s.student_name + " / " + s.class_names,
                    }
                }
            } else {
                return {
                    open: nextProps.open,
                    first: nextProps.open,
                    data: [],
                    title: ''
                }
            }
        }
        return null;
    }

    drawReachGraph = student =>{
        let d = [];
        if(student && student.programs) {
            const p = student.programs;console.log(p);
            const CUSTOMCOUNT = 10;
            const max = parseInt(this.props.funcStore.arrMax(p, "finish_lto"));
            let ticks = [], domain = [];
            if(max < CUSTOMCOUNT) {
                ticks = [...Array(CUSTOMCOUNT+1)].map((v,i) => i );
                domain = [0, CUSTOMCOUNT];
            } else {
                const s = max+1;
                ticks = [...Array(s)].map((v,i) => i );
                domain = [0, s];
            }
            p.map((v,i) => {
                d.push({
                    name: v.lto_name,
                    reach: v.finish_lto,
                    ltos: v.finish_lto_names,
                    ticks: ticks,
                    domain: domain
                });
                // if(v.finish_lto > 0) {
                //     d.push({
                //         name: v.lto_name,
                //         reach: v.finish_lto,
                //         ltos: v.finish_lto_names,
                //         ticks: ticks,
                //         domain: domain
                //     });
                // }
            });
        }
        return d;
    }

    handleClose = () => {
        this.props.handleSlide(false);
    }

    render() {
        const { classes, open } = this.props;
        const { data, title, ticks, domain } = this.state;
        return (
            <Dialog
                fullScreen
                open={open}
                onClose={this.handleClose}
                TransitionComponent={Transition}
                classes={{
                    root: classes.dialogRoot,
				    paper: classes.dialogRoot
                }}
            >
                <AppBar className={classes.appBar}>
                    <Toolbar>
                    <Typography variant="h6" color="inherit" className={classes.flex}>
                        {"DOMAIN REACH GRAPH" + title}
                    </Typography>
                    <IconButton color="inherit" onClick={this.handleClose} aria-label="Close">
                        <CloseIcon />
                    </IconButton>
                    </Toolbar>
                </AppBar>
                <div className={classes.root}>
                    <div className={classes.infoRoot}>
                        {data.length ? (
                            <BarChart
                                className={classes.graphDraw}
                                width={data.length * 70 < 600 ? 600 : data.length * 70}
                                height={485}
                                data={data}
                                margin={{
                                    top: 5, right: 30, left: 20, bottom: 5,
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis
                                    dataKey="name"
                                    minTickGap={0}
                                    interval={0}
                                    tick={<CustomizedAxisTick />}
                                />
                                <YAxis
                                    tick={{
                                        fontSize: 10
                                    }}
                                    ticks={[0,1,2,3,4,5,6,7,8,9,10]}
                                    domain={domain}
                                />
                                <Tooltip content={<CustomTooltip />}/>
                                <Legend align="left" iconSize={14} fontSize={11} wrapperStyle={{paddingLeft:16, paddingTop:100, bottom: 'auto'}}
                                    formatter={(value, entry, index)=>{
                                        return "도메인 별 준거도달 수";
                                    }}
                                />
                                <Bar dataKey="reach" fill="#444" barSize={15} />
                            </BarChart>
                        ) : null}
                    </div>
                </div>
            </Dialog>
        );
    }
}

const styles = theme => ({
    container: {
        flexWrap: 'wrap',
    },
    appBar: {
      position: 'relative',
      backgroundColor: '#ca5388'
    },
    flex: {
      flex: 1,
    },
    root: {
        flexGrow: 1,
        display: 'flex',
        overflowX: 'auto',
        overflowY: 'hidden',
        padding: '0 30px',
        justifyContent: 'center'
    },
    dialogRoot: {
        height: 720
    },
    infoRoot: {
        marginTop: 24,
        overflow: 'visible'
        //overflowY: 'hidden'
    },
    graphDraw: {
        marginTop: 0,
        marginLeft: -40,
        '& svg.recharts-surface': {
            overflow: 'visible'
        }
    },
});

export default withStyles(styles)(ReachSlide);