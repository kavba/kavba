import React, { Component } from 'react';
import { inject } from 'mobx-react';
import clsx from 'clsx';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Chip, Select, TextField, Fab, Button, Input, OutlinedInput, FilledInput, InputLabel, IconButton, InputAdornment,
    FormControl, FormControlLabel, Checkbox,
    MenuItem, List, ListItem, Collapse, ListItemText, ListItemSecondaryAction, Divider
    } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup, Autocomplete } from '@material-ui/lab';
import { Add, Save, Delete, Search, Done, ExpandLess, ExpandMore } from '@material-ui/icons';

import { Dialogs, StudentTable, Svgtext } from '../../components/';

//const tempArr = Array.apply(null, new Array(18)).map(Object.prototype.valueOf, {val:"0" , updated_at: ''});

const CustomCheckbox = withStyles({
    root: {
      color: '#333',
      '&$checked': {
        color: '#333',
      },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

@inject(stores => ({
    funcStore: stores.funcStore,
    mainStore: stores.mainStore
}))
class Student extends Component {
    state = {
        overId: '',
        classId: '',
        my: false,
        keyword: "",
        page: 0,
        perPage: 10,
        total: 0,
        sort: false,
        students: [],
        type: 0,
        classList: [],
        overList: [],
        parentList: [],
        pyramidList: [],
        circleList: [],
        dialogOpen: false,
        dialogSort: true, // true: adding
        selectedStudent: null,
        selectedParents: [],
        alertOpen: false,
        alertFlag: false,
        pyramidStudent: [],
        newPyramid: [],
        pyramidType: "0",
        circleStudent: [],
        circleType: "0",
        circleMenuOpen: null,
        alertStudent: '',
        memoToPersonal: '',
        memoToAll: '',
        memoType: "0",
        rName: '',
        rBirth: this.props.funcStore.convertToymd(),
        rClassId: '',
        rLtos: false,
        rValid: '1',
        classLabelWidth: 0,
        class2LabelWidth: 0,
        searchLabelWidth: 0,
        textWidth: 160
    }
    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && (this.loadFilterList());
        this._isMounted && (this.loadStudents());
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this._isMounted;
    }

    loadFilterList = () => {
        this.props.funcStore.conn('get', '/common/getFilterList', false, true, null, (res)=>{
            this.setState({
                classList: res.classList,
                overList: res.overList,
                parentList: res.parentList,
                circleList: res.circleList
            });
        });
    }

    loadStudents = () => {
        const { overId, classId, my, sort, keyword, page, perPage } = this.state;
        const param = {
            qry: keyword,
            overId: overId,
            classId: classId,
            sort: sort,
            my: my,
            page: page,
            perPage: perPage,
        }
        this.props.funcStore.conn('get', '/students/getAll', false, true, param, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    students: res.students,
                    total: res.total,
                    type: res.type
                });
            } else {
                console.log("학생 로딩 실패");
            }
        });
    }

    handleChangeKeyword = e => {
        this.setState({ keyword: e.target.value });
    }

    handleChangeMemo = e => {
        const { memoType } = this.state;
        if(memoType == "0") {
            this.setState({ 
                memoToPersonal: e.target.value
            });
        } else {
            this.setState({ 
                memoToAll: e.target.value
            });
        }
        
    }

    handleViewToggle = e => {
        this.setState({ my: !this.state.my });
        this.reloadStudents();
    }

    handlePage = page => {
        let t = page;
        if(!page) t = 0;
        this.setState({
            page: page
        });
        this.reloadStudents();
    }

    handleFilter = name => e => {
        this.setState({
            [name]: e.target.value
        });
        this.reloadStudents();
    }

    reloadStudents = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadStudents();
                this.forceUpdate();
            }, 0);
        });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    openNewDialog = () => {
        this.setState({
            dialogSort: true,
            dialogOpen: true,
            rValid: '1'
        });
    }

    openEditDialog = student => {
        const { parentList } = this.state;
        const tmp1 = student.parent_id ? student.parent_id.split(",") : [];
        let tmp2 = [];
        tmp1.map(t => {
            const idx = parentList.findIndex(p => p.id == t);
            if(idx > -1) tmp2.push(parentList[idx]);
        });
        this.setState({
            dialogSort: false,
            dialogOpen: true,
            rName: (student.name ? student.name : ''),
            rBirth: (student.birth_date ? student.birth_date : ''),
            rClassId: (student.class_id ? student.class_id : ''),
            rValid: student.is_valid.toString(),
            selectedParents: tmp2,
            selectedStudent: student
        });
    }

    handleAlertOpen = flag => {
        this.setState({
            alertOpen: flag,
        });
    }

    // openPyramid = studentId => {
    //     // 학생 피라미드 정보 로딩
    //     this.setState({ alertOpen: true, alertFlag: true });
    //     new Promise(resolve => {
    //         setTimeout(() => {
    //             resolve();
    //             this.props.funcStore.conn('get', '/student/'+studentId+'/getPyramid', false, true, null, (res)=>{
    //                 if(res.result == 'true') {
    //                     const t = (res.rows && res.rows[0] ? res.rows : tempArr.slice());
    //                     this.setState({
    //                         pyramidStudent: t,
    //                         newPyramid: t,
    //                         alertStudentId: studentId,
    //                     });
    //                 } else {
    //                     console.log("학생 피라미드 조회 실패");
    //                 }
    //             });
    //         }, 0);
    //     });
    // }

    createPie = (id, cx, cy, r, cc) => {
        const { circleStudent } = this.state;
        const slices = cc.length;
        let fromAngle, toAngle, halfAngle,
            fromCoordX, fromCoordY, halfCoordY,
            toCoordX, toCoordY, halfCoordX,
            path, text, d, de;
        let temp = 0, stCircle = null;
        if(slices %2 != 0) temp = 90 - (360 / slices);
        else if(slices == 6) temp = 30;
        if(circleStudent.hasOwnProperty(id)) {
            stCircle = circleStudent[id];
        }
        cc.map((c,i)=>{
            //path = document.createElementNS("http://www.w3.org/2000/svg", "path");
            path = document.getElementById('path_'+c.id);
            fromAngle = i * 360 / slices - temp;
            toAngle = (i + 1) * 360 / slices - temp;
            halfAngle = (fromAngle + toAngle) / 2;
            fromCoordX = cx + (r * Math.cos(fromAngle * Math.PI / 180));
            fromCoordY = cy + (r * Math.sin(fromAngle * Math.PI / 180));
            toCoordX = cx + (r * Math.cos(toAngle * Math.PI / 180));
            toCoordY = cy + (r * Math.sin(toAngle * Math.PI / 180));
            halfCoordX = cx + (r * Math.cos(halfAngle * Math.PI / 180)*2/3);
            halfCoordY = cy + (r * Math.sin(halfAngle * Math.PI / 180)*2/3);
            d = 'M' + cx + ',' + cy + ' L' + fromCoordX + ',' + fromCoordY + ' A' + r + ',' + r + ' 0 0,1 ' + toCoordX + ',' + toCoordY + 'z';
            path.setAttributeNS(null, "d", d);
            const rt = slices*1.4;
            if(stCircle && stCircle.hasOwnProperty(c.id)) {
                if(stCircle[c.id] == "1") {
                    path.setAttribute("class", "on"); // 진행중
                } else if(stCircle[c.id] == "2") {
                    path.setAttribute("class", "finish"); // 완료
                }
            }
            $('#path_'+c.id).hover(function(){
                const d = $(this).attr("d");
                $(this).attr("d", d.replace(("A"+r+","+r), ("A"+(r-rt)+","+(r-rt))));
            }, function(){
                const d = $(this).attr("d");
                $(this).attr("d", d.replace(("A"+(r-rt)+","+(r-rt)), ("A"+r+","+r)));
            });

            text = document.getElementById("text_"+c.id);
            text.setAttribute("x", halfCoordX);
            text.setAttribute("y", halfCoordY);

            const tspans = text.getElementsByTagName('tspan');
            for (let tspan of tspans) {
                tspan.setAttribute("x", halfCoordX);
                tspan.setAttribute("y", halfCoordY);
            }
            text.setAttribute("fill", '#232323');
        });
    }

    drawPie = () => {
        const { circleList } = this.state;
        if(!circleList || circleList.length == 0) return;
        circleList.map((c,i)=>{
            const cc = c.circles ? c.circles : [];
            this.createPie(c.id, 55, 55, 50, cc);
        });
        
    }

    openCircle = studentRow => {
        const studentId = studentRow.id;
        this.setState({ alertOpen: true, alertFlag: true });
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                const param = {
                    special: studentRow.special
                }
                this.props.funcStore.conn('get', '/student/'+studentId+'/getCircle', false, true, param, (res)=>{
                    if(res.result == 'true') {
                        this.setState({
                            circleStudent: res.studentCircles ? res.studentCircles : [],
                            alertStudent: studentRow,
                        });
                        this.drawPie();
                    } else {
                        console.log("학생 피라미드 조회 실패");
                    }
                });
            }, 0);
        });
    }

    changeCircleStatus = (parentId, circleId) => {
        const { circleStudent } = this.state;
        let temp = $.extend({}, circleStudent);
        let tempObj = document.getElementById('path_'+circleId);
        if(temp[parentId]) {
            if(temp[parentId].hasOwnProperty(circleId)) {
                if(temp[parentId][circleId] == 1) {
                    temp[parentId][circleId] = "2";
                    tempObj.setAttribute("class", "finish");
                } else if(temp[parentId][circleId] == 2) {
                    delete temp[parentId][circleId];
                    tempObj.setAttribute("class", "");
                } else {
                    temp[parentId][circleId] = "1";
                    tempObj.setAttribute("class", "on");
                }
            } else {
                temp[parentId][circleId] = "1";
                tempObj.setAttribute("class", "on");
            }
        } else {
            temp[parentId] = {};
            temp[parentId][circleId] = "1";
            tempObj.setAttribute("class", "on");
        }
        
        this.setState({
            circleStudent:temp
        });
    }

    openMemo = studentRow => {
        this.setState({ alertOpen: true, alertFlag: false });
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                const param = {
                    special: studentRow.special
                }
                this.props.funcStore.conn('get', '/student/'+studentRow.id+'/getMemo', false, true, param, (res)=>{
                    if(res.result == 'true') {
                        this.setState({
                            memoToPersonal: (res.personal ? res.personal.memo : ""),
                            memoToAll: (res.all ? res.all.memo : ""),
                            alertStudent: studentRow,
                        });
                    } else {
                        console.log("학생 메모 조회 실패");
                    }
                });
            }, 0);
        });
    }

    resetDialog = () => {
	    this.setState({
            rName: '',
            rBirth: this.props.funcStore.convertToymd(),
            rParents: [],
            rClassId: '',
            circleStudent: [],
            selectedParents: []
	    });
    }

    handleDateChange = date => {
        // const d = this.props.funcStore.convertToymd(date);console.log(date.target.value);
        const d = date.target.value;
        this.setState({
            rBirth: d
        });
    }

    handleDialogChange = e => {
	    // if(e.target.name == 'classId') {
		//     if(e.target.value != '') {
		// 	    this.loadStudentList(e.target.value);
		//     } else {
		// 	    this.setState({
		// 		    studentId: '', studentList: []
		// 	    });
		//     }
	    // }
	    this.setState({
            [e.target.name]: e.target.value,
        });
    }

    handleDialogVal = name => e => {
        this.setState({
            [name]: (name == "rLtos" ? e.target.checked : e.target.value)
        });
    }

    handleToggle = () => (e, val) => {
        this.setState({
            rValid: val
        });
    }

    setLabelWidth = (kind, ref) => {
        const { classLabelWidth, class2LabelWidth, searchLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != class2LabelWidth) {
            this.setState({ class2LabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != searchLabelWidth) {
            this.setState({ searchLabelWidth: ref.offsetWidth });
        }
    }
    
    addStudent = () => {
        const { rName, rBirth, rClassId, rLtos, rValid, selectedParents } = this.state;
        let tmp = [];
        if(selectedParents.length) {
            tmp = selectedParents.map(v => v.id);
        }
        const param = {
            name: rName,
            birthDate: rBirth,
            parentIds: tmp,
            phone: '',
            classId: rClassId,
            buildAll: rLtos,
            isValid: rValid,
        }
        this.props.funcStore.conn('post', '/student/post', false, true, param, (res)=>{
            if(res.result == 'true') {
                // alert("등록 완료");
                this.handleOpen(false);
                this.reloadStudents();
            } else {
                if(res.result_code == '001') { 
                    alert("이미 등록된 학생입니다");
                } else {
                    console.log("학생 등록 실패");
                }
            }
        });
    }

    saveStudent = () => {
        const { rName, rBirth, rClassId, rValid, selectedParents, selectedStudent } = this.state;
        let tmp = [];
        if(selectedParents.length) {
            tmp = selectedParents.map(v => v.id);
        }
        const param = {
            name: rName,
            birthDate: rBirth,
            parentIds: tmp,
            phone: '',
            classId: rClassId,
            isValid: rValid,
        }
        this.props.funcStore.conn('post', '/student/'+selectedStudent.id+'/put', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("수정 완료");
                this.handleOpen(false);
                this.reloadStudents();
            } else {
                console.log("학생 정보 수정 실패");
            }
        });
    }

    removeStudent = () => {
        const { selectedStudent } = this.state;
        this.props.funcStore.conn('post', '/student/'+selectedStudent.id+'/delete', false, true, null, (res)=>{
            if(res.result == 'true') {
                alert("삭제 완료");
                this.handleOpen(false);
                this.reloadStudents();
            } else {
                console.log("학생 삭제 실패");
            }
        });
    }

    handleSort = () => {
        this.setState({ sort: !this.state.sort });
        this.reloadStudents();
    }

    handleChange = index => (e, val) => {
        const { newPyramid, pyramidStudent } = this.state;
        this.setState({
            pyramidStudent: pyramidStudent.map((v,i) => i == index ? {
                ...v,
                val: val
            } : v),
            newPyramid: newPyramid.map((v,i) => i == index ? {
                val: val, updated_at: this.props.funcStore.convertToymd()
            } : v)
        });
    }

    handleMemoType = (e, val) => {
        this.setState({
            memoType: val
        });
    }

    handlePyramidType = (e, val) => {
        this.setState({
            pyramidType: val
        });
    }

    handleCorcleCollapse = circleId => {
        this.setState({
            circleMenuOpen: circleId
        })
    }

    savePyramid = () => {
        const { newPyramid, alertStudent } = this.state;
        const param = {
            studentId: alertStudent.id,
            pyramid: newPyramid
        }
        this.props.funcStore.conn('post', '/student/setPyramid', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("피라미드 저장 완료");
            } else {
                console.log("학생 등록 실패");
            }
        });
    }

    saveCircle = () => {
        const { circleStudent, alertStudent } = this.state;
        const param = {
            studentId: alertStudent.id,
            circle: circleStudent,
            special: alertStudent.special
        }
        this.props.funcStore.conn('post', '/student/setCircle', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("서클 저장 완료");
            } else {
                console.log("학생 서클 수정 실패");
            }
        });
    }

    saveMemo = () => {
        const { memoToPersonal, memoToAll, memoType, alertStudent } = this.state;
        const param = {
            studentId: alertStudent.id,
            type: memoType,
            memo: (memoType == "0" ? memoToPersonal : memoToAll),
            special: alertStudent.special
        }
        this.props.funcStore.conn('post', '/student/setMemo', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("메모 저장 완료");
            } else {
                console.log("메모 등록 실패");
            }
        });
    }

    handleKeyPress = e => {
        if(e.key === 'Enter') {
            this.setState({ page:0 });
            this.reloadStudents();
        }
    }

    handleMouseDownSearching = e => {
        e.preventDefault();
    };

    handleClickSearching = e => {
        if(!this.state.keyword) return;
        this.setState({ pare:0 });
        this.reloadStudents();
    }

    // <FormControlLabel
    //     className={classes.checkboxRoot}
    //     control={
    //         <CustomCheckbox
    //             checked={rLtos} onChange={this.handleDialogVal('rLtos')} value="all"
    //         />
    //     }
    //     label="Build LTOs"
    // />

    render() {
        const { classes, mainStore } = this.props;
        const { classId, overId, classList, overList, parentList, pyramidList, circleList, students, page, perPage, my, total, sort, type, keyword,
            dialogOpen, dialogSort, alertOpen, alertFlag,
            pyramidStudent, pyramidType, 
            circleStudent, circleType, circleMenuOpen,
            memoToPersonal, memoToAll, memoType,
            rName, rBirth, rClassId, rLtos, rValid,
            classLabelWidth, class2LabelWidth, searchLabelWidth, selectedParents, selectedStudent, textWidth } = this.state;

        const dialogButton = !dialogSort ? (
            <div className={classes.dialogTopBtn}>
                <Fab size="small" aria-label="save" className={classes.dialogSave}
                    onClick={() => this.saveStudent()}>
                    <Save />
                </Fab>
                <Fab size="small" aria-label="remove" className={classes.dialogRemove}
                    onClick={() => this.removeStudent()}>
                    <Delete />
                </Fab>
            </div>
        ) : null;
        const dialogForm = (
            <div className={classes.contentsContainer}>
                <form className={classes.formRoot} autoComplete="off">
                    <TextField
                        id="register-name"
                        label="이름"
                        className={classNames(classes.textField, classes.dialogInput)}
                        value={rName}
                        onChange={this.handleDialogVal('rName')}
                        variant="outlined"
                    />
                    <TextField
                        id="date-picker"
                        label="생일"
                        type="date"
                        defaultValue={rBirth}
                        className={classNames(classes.textField, classes.dialogInput)}
                        onChange={this.handleDateChange}
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true
                        }}
                    />
                    <FormControl variant="outlined" className={classes.dialogInput}>
                        <InputLabel
                            ref={ref => {
                                if(ref) this.setLabelWidth(1, ref);
                            }}
                            htmlFor="register-class"
                        >
                            반
                        </InputLabel>
                        <Select
                            value={rClassId}
                            onChange={this.handleDialogVal('rClassId')}
                            input={
                                <OutlinedInput
                                    labelWidth={classLabelWidth}
                                    name="classId"
                                    id="register-class"
                                />
                            }
                        >
                            <MenuItem value="">
                                <em>선택</em>
                            </MenuItem>
                            {classList.map((v,i) => {
                                return (
                                    <MenuItem key={i} value={v.id}>
                                        {v.name}
                                    </MenuItem>
                                );
                            })}
                        </Select>
                    </FormControl>
                    <ToggleButtonGroup size="small" value={rValid} className={classes.toggleGroup} exclusive onChange={this.handleToggle()}>
                        <ToggleButton value="0" classes={{
                            root: classes.toggleBtn,
                            selected: classes.toggleSelectedBtn
                        }}>
                            {"비활성"}
                        </ToggleButton>,
                        <ToggleButton value="1" classes={{
                            root: classes.toggleBtn,
                            selected: classes.toggleSelectedBtn
                        }}>
                            {"활성"}
                        </ToggleButton>
                    </ToggleButtonGroup>
                </form>
            </div>
        );

        // <Autocomplete
        //     multiple
        //     options={parentList}
        //     getOptionLabel={option => option.name}
        //     defaultValue={selectedParents}
        //     filterSelectedOptions
        //     loading={parentList.length ? false : true}
        //     onChange={(e, v)=> {
        //         this.setState({
        //             selectedParents: v,
        //         });
        //     }}
        //     classes={{
        //         root: classes.dialogParent,
        //         popup: classes.parentPopup
        //     }}
        //     renderOption={(opt, state) => {
        //         return opt.name + (opt.phone ? " | " + opt.phone : "");
        //     }}
        //     renderInput={params => {
        //         return (
        //             <TextField
        //                 {...params}
        //                 id="register-parent"
        //                 variant="outlined"
        //                 label="부모 선택"
        //                 fullWidth
        //             />
        //         );
        //     }}
        // />

        const subButton = alertFlag ? (
            <Fab size="small" variant="round" aria-label="save"
                onClick={() => this.saveCircle()}>
                <Save />
            </Fab>
        ) : (
            <Fab size="small" aria-label="save"
                onClick={() => this.saveMemo()}>
                <Save />
            </Fab>
        );
        const circleForm = (
            <div style={{ flex: 1, display: 'flex' }}>
                {circleList.length && (
                    <div style={{ flex: 1, display: 'flex' }}>
                        <div className={classes.circleRoot}>
                            <div className={classes.circleLeft}>
                                <List
                                    component="nav"
                                    aria-labelledby="circle-parents"
                                    className={classes.circleListRoot}
                                >
                                    {circleList.map((cp,i) => {
                                        let stCircle = null;
                                        let v1 = 0, v2 = 0;
                                        if(circleStudent.hasOwnProperty(cp.id)) {
                                            stCircle = circleStudent[cp.id];
                                            Object.values(stCircle).map(v => {
                                                if(v == 1) v1++;
                                                else if(v == 2) v2++;
                                            });
                                        }
                                        return (
                                            <React.Fragment key={"cp_"+i}>
                                                <ListItem id={cp.id} className={classes.listBox}>
                                                    <div className={classes.parentRoot}>
                                                        <div className={classes.listTextArea}
                                                            onClick={()=>{
                                                                document.getElementById('circle_'+cp.id).scrollIntoView({ behavior: 'smooth', block: 'center' });
                                                            }}>
                                                            <span className={classes.listTextBox}>{cp.name ? cp.name.toUpperCase() : ""}</span>
                                                            {v1 > 0 ? <span className={classes.onRect}>{v1}</span> : ""}
                                                            {v2 > 0 ? <span className={classes.finishRect}>{v2}</span> : ""}
                                                        </div>
                                                        <div className={classNames(classes.listIconBox)}>
                                                            {circleMenuOpen == cp.id ? <ExpandLess onClick={()=>this.handleCorcleCollapse(null)} />
                                                            : <ExpandMore onClick={()=>this.handleCorcleCollapse(cp.id)} />}
                                                        </div>
                                                    </div>
                                                </ListItem>
                                                <Collapse in={circleMenuOpen == cp.id} timeout="auto" className={classes.collapseRoot} unmountOnExit>
                                                    <List component="div" disablePadding>
                                                        {cp.circles.map((c, idx) => {
                                                            let cn = "";
                                                            if(stCircle && stCircle.hasOwnProperty(c.id)) {
                                                                if(stCircle[c.id] == "1") { // 진행중
                                                                    cn = classes.onListItem;
                                                                } else if(stCircle[c.id] == "2") { // 완료
                                                                    cn = classes.finishListItem;
                                                                }
                                                            }
                                                            return (
                                                                <ListItem key={"c_"+idx} button className={classNames(classes.nested, cn)}
                                                                    onClick={()=>{
                                                                        document.getElementById('circle_'+cp.id).scrollIntoView({ behavior: 'smooth', block: 'center' });
                                                                        this.changeCircleStatus(cp.id, c.id)
                                                                    }}>
                                                                    {c.name}
                                                                </ListItem>
                                                        )})}
                                                    </List>
                                                </Collapse>
                                            </React.Fragment>
                                        );
                                    })}
                                </List>
                            </div>
                            <div id="circle_right" className={classes.circleRight}>
                                {circleList && circleList.map((cp,i)=>{
                                    return (
                                        <div key={"circle_"+cp.id} id={"circle_"+cp.id} className={classes.circleBox}>
                                            <Typography variant="h6" display="block" style={{ fontFamily:'JungBold' }} gutterBottom>
                                                {cp.name ? cp.name.toUpperCase() : ""}
                                            </Typography>
                                            <svg viewBox="0 0 110 110" id={"pie_"+cp.id} fill="transparent">
                                                {cp.circles.map(c => {
                                                    return (
                                                        <React.Fragment key={c.id}>
                                                            <path id={"path_"+c.id}
                                                                onClick={()=>this.changeCircleStatus(cp.id, c.id)}></path>
                                                            <Svgtext
                                                                id={"text_"+c.id}
                                                                width={textWidth}
                                                                textAnchor="middle">{c.name}</Svgtext>
                                                        </React.Fragment>
                                                    );
                                                })}
                                            </svg>
                                        </div>
                                    
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );

        // const pyramidForm = (
        //     <div>
        //         {pyramidList.length && pyramidStudent.length ? (
        //             <div>
        //                 <ToggleButtonGroup size="small" value={pyramidType} exclusive onChange={this.handlePyramidType}>
        //                     <ToggleButton value="0">
        //                         {"전체"}
        //                     </ToggleButton>,
        //                     <ToggleButton value="1">
        //                         {"완성"}
        //                     </ToggleButton>
        //                     <ToggleButton value="2">
        //                         {"과정중"}
        //                     </ToggleButton>
        //                     <ToggleButton value="3">
        //                         {"부재"}
        //                     </ToggleButton>
        //                 </ToggleButtonGroup>
        //                 <Fab variant="extended" aria-label="save" className={classes.fab}
        //                     onClick={() => this.savePyramid()}>
        //                     <Save />
        //                 </Fab>
        //                 <List className={classes.root}>
        //                     {pyramidList.map((v,i) => {
        //                         if(pyramidType == "1" && pyramidStudent[i].val != "2") return null;
        //                         else if(pyramidType == "2" && pyramidStudent[i].val != "1") return null;
        //                         else if(pyramidType == "3" && pyramidStudent[i].val != "0") return null;

        //                         const labelId = `checkbox-list-label-${v}`;
        //                         return (
        //                             <ListItem
        //                                 key={'c_'+i}
        //                                 role={undefined}
        //                                 className={classes.listItemRoot}
        //                             >
        //                                 <div style={{
        //                                     position: 'absolute',
        //                                     width: '100%',
        //                                     height: '100%',
        //                                     left: 0,
        //                                     top: 0,
        //                                     overflow: 'hidden',
        //                                     zIndex: '1'
        //                                 }}>
        //                                     <div style={{
        //                                         position: 'absolute',
        //                                         top: 0,
        //                                         left: '-30px',
        //                                         width: 300 + (i * 20),
        //                                         height: '100%',
        //                                         background: (pyramidStudent[i].val=="2"?"#7ef2a9":pyramidStudent[i].val=="1"?"#fcf06a":"whitesmoke"),
        //                                         borderRightWidth: '1px',
        //                                         borderRightStyle: 'solid',
        //                                         borderRightColor: (pyramidStudent[i].val=="2"?"#7ef2a9":pyramidStudent[i].val=="1"?"#fcf06a":"#eee"),
        //                                         transform: 'skew(15deg)',
        //                                         transition: "background .3s ease"
        //                                     }}></div>
        //                                 </div>
        //                                 <div className={classes.listTextRoot}>
        //                                     <ListItemText id={labelId} primary={v.name} />
        //                                     <Typography variant="caption" display="block" gutterBottom>
        //                                         {pyramidStudent[i].updated_at ? "Updated : " + pyramidStudent[i].updated_at : ''}
        //                                     </Typography>
        //                                 </div>
        //                                 <ToggleButtonGroup size="small" className={classes.listBtnRoot} value={(pyramidStudent[i].val)} exclusive onChange={this.handleChange(i)}>
        //                                     <ToggleButton value="2">
        //                                         {"완성"}
        //                                     </ToggleButton>,
        //                                     <ToggleButton value="1">
        //                                         {"과정중"}
        //                                     </ToggleButton>,
        //                                     <ToggleButton value="0">
        //                                         {"부재"}
        //                                     </ToggleButton>
        //                                 </ToggleButtonGroup>
        //                             </ListItem>
        //                         );
        //                     })}
        //                 </List>
        //             </div>
        //         ) : null}
        //     </div>
        // );

        const memoForm = (
            <div>
                <ToggleButtonGroup size="small" value={memoType} exclusive onChange={this.handleMemoType}>
                    <ToggleButton value="0">
                        {"개인"}
                    </ToggleButton>,
                    <ToggleButton value="1">
                        {"전체"}
                    </ToggleButton>
                </ToggleButtonGroup>
                <Divider variant="middle" />
                {memoType == "0" ? (
                    <TextField
                        id="memo-textarea1"
                        label="메모 입력"
                        multiline
                        rows="8"
                        onChange={this.handleChangeMemo}
                        value={memoToPersonal}
                        className={classes.memoTextarea}
                        margin="normal"
                        variant="outlined"
                        shrink="true"
                    />
                ) : (
                    <TextField
                        id="memo-textarea2"
                        label="메모 입력"
                        multiline
                        rows="8"
                        onChange={this.handleChangeMemo}
                        value={memoToAll}
                        className={classes.memoTextarea}
                        margin="normal"
                        variant="outlined"
                        shrink="true"
                    />
                )}
            </div>
        );

        return (
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputLeftBox}>
                        <Chip
                            label="내 학생 보기"
                            clickable
                            className={my ? classes.onChip : classes.offChip}
                            onClick={this.handleViewToggle}
                            onDelete={this.handleViewToggle}
                            deleteIcon={<Done />}
                        />
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(2, ref);
                                }}
                                htmlFor="class-change"
                            >
                                반
                            </InputLabel>
                            <Select
                                value={classId}
                                onChange={this.handleFilter('classId')}
                                input={
                                <OutlinedInput
                                    labelWidth={class2LabelWidth}
                                    name="classId"
                                    id="class-change"
                                />
                                }
                            >
                                <MenuItem value="">
                                    <em>전체</em>
                                </MenuItem>
                                {classList.map((v,i) => {
                                    return (
                                        <MenuItem key={"cl_"+i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <InputLabel 
                                ref={ref => {
                                    if(ref) this.setLabelWidth(3, ref);
                                }}
                                htmlFor="searching"
                            >검색어 입력</InputLabel>
                            <OutlinedInput
                                id="searching"
                                type="text"
                                labelWidth={searchLabelWidth}
                                value={keyword}
                                onChange={this.handleChangeKeyword}
                                onKeyPress={this.handleKeyPress}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="searching"
                                            onClick={this.handleClickSearching}
                                            onMouseDown={this.handleMouseDownSearching}
                                        >
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </div>
                    <div className={classes.subInputRightBox}>
                        {mainStore.user.level >= 10 ? (
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={
                                    () => this.openNewDialog()}>
                                <Add className={classes.extendedIcon} />
                                New Student
                            </Fab>
                        ) : null}
                    </div>
                </div>
                <div className={classes.tableContainer}>
                    <StudentTable
                        func={this.props.funcStore}
                        keyword={keyword}
                        data={students}
                        page={page}
                        perPage={perPage}
                        total={total}
                        sort={sort}
                        type={type}
                        my={my}
                        classId={classId}
                        classList={classList}
                        overId={overId}
                        overList={overList}
                        handleViewToggle={this.handleViewToggle}
                        reloadStudents={this.reloadStudents}
                        handlePage={this.handlePage}
                        handleSort={this.handleSort}
                        openEditDialog={this.openEditDialog}
                        //openPyramid={this.openPyramid}
                        openCircle={this.openCircle}
                        openMemo={this.openMemo}
                    />
                </div>
                {dialogSort ? (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"New Student"}
                        content={dialogForm}
                        leftFunc={{
                            label: "추가",
                            func: ()=> {
                                this.addStudent();
                            }
                        }}
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                ) : (
                    <Dialogs
                        kinds={"alert"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"Edit Student"}
                        subButton={mainStore.user.level > 2 ? dialogButton : ''}
                        content={dialogForm}
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                )}
                
                <Dialogs
                    kinds={"alert"}
                    open={alertOpen}
                    handleOpen={this.handleAlertOpen}
                    title={alertFlag ? "Checking Circle" : "Memo"}
                    //content={alertFlag ? pyramidForm : memoForm}
                    subButton={mainStore.user.level > 1 ? subButton : ''}
                    content={alertFlag ? circleForm : memoForm}
                    classes={alertFlag ? {
                        paper: classes.dialogRoot,
                    } : null}
                />
            </div>
        );
    }
}

const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    dialogInput: {
        minWidth: 120,
        margin: 8,
    },
    dialogParent: {
        flex: 1,
        margin: 8
    },
    parentPopup: {
        zIndex: 9999
    },
    listTextRoot: {
        flex: 1,
        zIndex: '2'
    },
    listItemRoot: {
        '&:not(:last-child)': {
            borderBottom: '1px solid #dedede'
        },
        minHeight: 72.19,
    },
    listBtnRoot: {
        zIndex: '3'
    },
    memoTextarea: {
        width: '100%'
    },
    dialogRoot: {
        height: 'calc(100% - 96px)',
        maxWidth: 1100,
        '& .dialog-content-custom': {
            padding: 0,
            display: 'flex'
        }
    },
    dialogTopBtn: {
        // position: 'absolute',
        // top: '15px',
        // right: '15px'
    },
    dialogSave: {
        margin: '0 5px'
    },
    dialogRemove: {
        margin: '0 5px'
    },
    onChip: {
        backgroundColor: '#333',
        border: '1px solid #333',
        color: '#fff',
        '& svg': {
            color: '#fff',
        },
        boxShadow: 'none',
        '&:hover': {
            backgroundColor: '#333',
            border: '1px solid #333',
            color: '#fff',
            '& svg': {
                color: '#fff',
            },
        },
        '&:active': {
            backgroundColor: '#333',
            border: '1px solid #333',
            color: '#fff',
            '& svg': {
                color: '#fff',
            }
        },
        '&:focus': {
            backgroundColor: '#333',
            border: '1px solid #333',
            color: '#fff',
            '& svg': {
                color: '#fff',
            }
        },
    },
    offChip: {
        backgroundColor: 'transparent',
        border: '1px solid #888',
        color: '#232323',
        '& svg': {
            color: '#232323',
        },
        boxShadow: 'none',
        '&:hover': {
            backgroundColor: 'transparent',
            border: '1px solid #888',
            color: '#232323',
            '& svg': {
                color: '#232323',
            }
        },
        '&:active': {
            backgroundColor: 'transparent',
            border: '1px solid #888',
            color: '#232323',
            '& svg': {
                color: '#232323',
            }
        },
        '&:focus': {
            backgroundColor: 'transparent',
            border: '1px solid #888',
            color: '#232323',
            '& svg': {
                color: '#232323',
            }
        },
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {
        display: 'flex',
        alignItems: 'center'
    },
    subInputRightBox: {
        display: 'flex',
        alignItems: 'center'
    },
    formControl: {
        width: 120,
        marginLeft: 15,
        marginRight: 15
    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15
    },
    circleRoot: {
        width: '100%',
        display: 'flex',
        borderTop: '1px solid #444',
        borderBottom: '1px solid #444',
    },
    nested: {
        paddingLeft: theme.spacing(4),
        borderBottom: '1px solid #444',
        backgroundColor: '#fff',
        '&:active': {
            backgroundColor: '#93dbaf'
        },
        '&:hover': {
            backgroundColor: '#fff'
        }
    },
    circleListRoot: {
        width: '100%',
        paddingTop: 0,
        paddingBottom: 0
    },
    circleLeft: {
        flex: 1,
        overflowY: 'auto',
        maxWidth: 360,
        borderRight: '1px solid #444',
        backgroundColor: '#dedede'
    },
    circleRight: {
        flex: 1,
        textAlign: 'center',
        overflowY: 'auto',
        padding: 15,
        '& svg[id*="pie_"]': {
            height: '600px',
            width: '600px'
        },
        '& svg[id*="pie_"] path': {
            cursor: 'pointer',
            fill: 'transparent',
            stroke: '#444',
            strokeWidth: '0.3',
            transition: 'all .2s ease',
            '&:active': {
                fill: '#9c98eb',
            },
            '&.on': {
                fill: '#8884d8',
                '&:active': {
                    fill: '#93dbaf'
                }
            },
            '&.finish': {
                fill: '#82ca9d',
                '&:active': {
                    fill: '#bbb'
                }
            }
        },
        '& svg[id*="pie_"] text': {
            fontSize: '3px',
            textAnchor: 'middle',
            pointerEvents: 'none',
            '& tspan': {
                fontFamily: 'JungBold'
            }
        },
    },
    listBox: {
        flex: 1,
        flexDirection: 'column',
        padding: 0,
        borderBottom: '1px solid #444',
        backgroundColor: '#fff',
        '& span': {
            fontFamily: "JungNormal"
        }
    },
    listTextBox: {
        display: 'flex',
        justifyContent: 'flex-start',
    },
    listTextArea: {
        cursor: 'pointer',
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        minHeight: 50,
        fontSize: 14,
        display: 'flex',
        color: '#232323',
        padding: 15,
        alignItems: 'center',
        justifyContent: 'flex-start',
        wordBreak: 'keep-all',
    },
    listIconBox: {
        display: 'flex',
        alignItems: 'center',
        padding: 8,
        cursor: 'pointer'
    },
    parentRoot: {
        display: 'flex',
        flex: 1,
        width: '100%',
        alignItems: 'center'
    },
    childRoot: {
        display: 'flex',
        flex: 1,
        width: '100%',
        paddingLeft: 10
    },
    childContent: {
        display: 'flex',
        flex: 1,
        alignItems: 'center',
        '&:before': {
            content: "123123",
            fontSize: 18,
            color: '#000'
        }
    },
    circleBox: {
        margin: '15px auto',
        paddingBottom: 30,
        '& h4': {
            fontFamily: "JungBold"
        }
    },
    onListItem: {
        backgroundColor: '#8884d8',
        '&:active': {
            backgroundColor: '#9c98eb',
        },
        '&:hover': {
            backgroundColor: '#8884d8',
        }
    },
    finishListItem: {
        backgroundColor: '#82ca9d',
        '&:active': {
            backgroundColor: '#bbb',
        },
        '&:hover': {
            backgroundColor: '#82ca9d',
        }
    },
    onRect: {
        fontFamily: 'JungBold',
        backgroundColor: '#8884d8',
        color: '#fff',
        justifyContent: 'center',
        marginLeft: 10,
        padding: '0 7px',
        borderRadius: 4
    },
    finishRect: {
        fontFamily: 'JungBold',
        backgroundColor: '#82ca9d',
        color: '#fff',
        justifyContent: 'center',
        marginLeft: 10,
        padding: '0 7px',
        borderRadius: 4
    },
    collapseRoot: {
        //paddingBottom: 15,
        //borderBottom: '1px solid #444'
    },
    checkboxRoot: {
        margin: '8px 8px 8px 0px'
    },
    toggleGroup: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginRight: 8,
        marginLeft: 8
    },
    toggleBtn: {
        color: '#444',
        backgroundColor: '#fff',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        flex: 1,
        height: '56px'
    },
    toggleSelectedBtn: {
        color: '#fff!important',
        backgroundColor: '#444!important'
    },
});

export default withStyles(styles)(Student);