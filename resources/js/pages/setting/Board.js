import React, { Component } from 'react';
import { inject } from 'mobx-react';
import clsx from 'clsx';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, Button, TextField, FormControl, InputLabel, OutlinedInput, 
    InputAdornment, IconButton, Select, MenuItem,  } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { Add, Save, Delete, Search, Done, SubdirectoryArrowRight } from '@material-ui/icons';

import { Dialogs, BoardTable } from '../../components/';

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
class Board extends Component {
    state = {
        posts: [],
        comments: [],
        keyword: "",
        page: 0,
        perPage: 10,
        total: 0,
        sort: true,
        type: '',
        classList: [],
        classId: '',
        dialogOpen: false,
        dialogSort: true, // true: add
        dTitle: '',
        dType: '0',
        dClassId: '',
        dContent: '',
        dRgstDate: '',
        dWriter: '',
        dComments: [],
        dComment: '',
        dAuth: '0',
        dId: '',
        classLabelWidth: 0,
        typeLabelWidth: 0,
        class2LabelWidth: 0,
        type2LabelWidth: 0,
        searchLabelWidth: 0
    }

    componentDidMount() {
        this.loadClasses();
        this.loadPosts();
    }

    loadClasses = () => {
        this.props.funcStore.conn('get', '/user/getAllClasses', false, true, null, (res)=>{
            this.setState({
                classList: res.list,
            });
        });
    }

    loadPosts = () => {
        const { posts, keyword, page, perPage, type, sort, classId } = this.state;
        const param = {
            qry: keyword,
            classId: classId,
            sort: sort,
            page: page,
            perPage: perPage,
            type: type
        }
        this.props.funcStore.conn('get', '/posts/getAll', false, true, param, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    posts: res.posts,
                    total: res.total,
                });
            } else {
                console.log("게시글 로딩 실패");
            }
        });
    }

    reloadPosts = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadPosts();
            }, 0);
        });
    }

    handleFilter = name => e => {
        this.setState({
            [name]: e.target.value
        });
        this.reloadPosts();
    }

    handleChangeKeyword = e => {
        this.setState({ keyword: e.target.value });
    }

    handlePage = page => {
        let t = page;
        if(!page) t = 0;
        this.setState({
            page: page
        });
        this.reloadPosts();
    }

    openRow = post => {
        this.props.funcStore.conn('get', '/common/'+post.class_id+'/checkClassAuth', false, true, null, (res)=>{console.log(res);
            if(res.result == 'false') {
                alert("글 열람 권한이 없습니다. 반을 배정받으세요");
            } else if(res.result == 'true') {
                this.setState({
                    dialogSort: false,
                    dialogOpen: true,
                    dTitle: post.title,
                    dType: post.type,
                    dClassId: post.class_id,
                    dContent: post.content,
                    dRgstDate: post.rgst_date,
                    dWriter: post.writer,
                    dComments: post.comments,
                    dAuth: post.auth,
                    dId: post.id
                });
            } else {
                console.log("게시글 로딩 실패");
            }
        });
    }

    handleSort = () => {
        this.setState({ sort: !this.state.sort });
        this.reloadPosts();
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    setLabelWidth = (kind, ref) => {
        const { classLabelWidth, typeLabelWidth, class2LabelWidth, type2LabelWidth, searchLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != typeLabelWidth) {
            this.setState({ typeLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != class2LabelWidth) {
            this.setState({ class2LabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != type2LabelWidth) {
            this.setState({ type2LabelWidth: ref.offsetWidth });
        } else if(kind == 5 && ref.offsetWidth != searchLabelWidth) {
            this.setState({ searchLabelWidth: ref.offsetWidth });
        }
    }

    openDialog = flag => {
        this.setState({
            dialogSort: flag,
            dialogOpen: true
        });
    }

    resetDialog = () => {
	    this.setState({
            dTitle: '',
            dType: '0',
            dClassId: '',
            dContent: '',
            dRgstDate: '',
            dWriter: '',
            dComments: [],
            dComment: '',
            dAuth: '0',
            dId: ''
	    });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    handleDialogVal = name => event => {
        if(name == "dTitle" && event.target.value.length > 255) return;
        this.setState({
            [name]: event.target.value
        });
    };

    handleDialogComment = (postId, comment, e) => {
        const { dComments } = this.state;
        this.setState({
            dComments: dComments.map(c => c.id === comment.id ? {
                ...c,
                text: e.target.value
            } : c)
        });
    }

    addPost = () => {
        const { dTitle, dContent, dType, dClassId } = this.state;
        const param = {
            title: dTitle,
            content: dContent,
            type: dType,
            classId: dClassId,
        }
        this.props.funcStore.conn('post', '/board/post', false, true, param, (res)=>{
            if(res.result == 'true') {
                // alert("등록 완료");
                this.handleOpen(false);
                this.reloadPosts();
            } else {
                console.log("글 등록 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    savePost = () => {
        const { dTitle, dContent, dType, dClassId, dId } = this.state;
        const param = {
            title: dTitle,
            content: dContent,
            type: dType,
            classId: dClassId,
        }
        this.props.funcStore.conn('post', '/board/' +dId+ '/put', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("수정 완료");
                this.handleOpen(false);
                this.reloadPosts();
            } else {
                console.log("글 수정 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    removePost = () => {
        if(!confirm("게시글을 삭제하시겠습니까?")) return;
        const { dId } = this.state;
        this.props.funcStore.conn('post', '/board/' +dId+ '/delete', false, true, null, (res)=>{
            if(res.result == 'true') {
                alert("삭제 완료");
                this.handleOpen(false);
                this.reloadPosts();
            } else {
                console.log("글 삭제 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    saveComment = comment => {
        const { dComments } = this.state;
        const t = dComments.filter(c => c.id === comment.id);
        if(t.length) {
            const param = {
                text: t[0].text
            }
            this.props.funcStore.conn('post', '/comment/' +comment.id+ '/put', false, true, param, (res)=>{
                if(res.result == 'true') {
                    alert("수정 완료");
                } else {
                    console.log("글 수정 실패");
                    if(res.error) alert(res.error);
                }
            });
        }
    }

    removeComment = comment => {
        const { dComments } = this.state;
        this.props.funcStore.conn('post', '/comment/' +comment.id+ '/delete', false, true, null, (res)=>{
            if(res.result == 'true') {
                alert("댓글 삭제 완료");
                this.setState({
                    dComments: dComments.filter(c => c.id !== comment.id)
                });
            } else {
                console.log("글 수정 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    addComment = () => {
        const { dComments, dComment, dId } = this.state;
        const param = {
            postId: dId,
            text: dComment
        }
        this.props.funcStore.conn('post', '/comment/post', false, true, param, (res)=>{
            if(res.result == 'true' && res.comment) {
                this.setState({
                    dComments: dComments.concat(res.comment),
                    dComment: ''
                });
                this.reloadPosts();
            } else {
                console.log("댓글 등록 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    handleMouseDownSearching = e => {
        e.preventDefault();
    };

    handleClickSearching = e => {
        if(!this.state.keyword) return;
        this.reloadPosts();
    }

    handleKeyPress = e => {
        if(e.key === 'Enter') {
            this.reloadPosts();
        }
    }

    render() {
        const { posts, keyword, page, perPage, total, sort, type, classList, classId,
            dialogSort, dialogOpen, dTitle, dType, dClassId, dContent, dRgstDate, dWriter, dComments, dComment, dId, dAuth, 
            classLabelWidth, typeLabelWidth, class2LabelWidth, type2LabelWidth, searchLabelWidth } = this.state;
        const { classes } = this.props;
        const lang = this.props.mainStore.getLang(this.props.mainStore.country);

        const dialogButton = (!dialogSort && dAuth) ? (
            <div>
                <Fab size="small" variant="round" aria-label="save"
                    onClick={() => this.savePost()}>
                    <Save />
                </Fab>
                <Fab size="small" variant="round" aria-label="remove"
                    onClick={() => this.removePost()}>
                    <Delete />
                </Fab>
            </div>
        ) : null;
        const dialogForm = (
            <div className={classes.contentsContainer}>
                <form className={classes.formRoot} autoComplete="off">
                    <div className={classes.dialogBox}>
                        <TextField
                            id="dialog-title"
                            label={lang['board5']}
                            className={classNames(classes.dialogInput, classes.dTitle)}
                            value={dTitle}
                            onChange={this.handleDialogVal('dTitle')}
                            variant="outlined"
                            disabled={(!dialogSort && dAuth == '0') ? true : false}
                        />
                        <FormControl variant="outlined" className={classNames(classes.dialogInput, classes.dType)}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(2, ref);
                                }}
                                htmlFor="dialog-type"
                            >
                                {lang['board1']}
                            </InputLabel>
                            <Select
                                value={dType}
                                onChange={this.handleDialogVal('dType')}
                                input={
                                    <OutlinedInput
                                        labelWidth={typeLabelWidth}
                                        name="dType"
                                        id="dialog-type"
                                        disabled={(!dialogSort && dAuth == '0') ? true : false}
                                        classes={{
                                            disabled: classes.disabledColor
                                        }}
                                    />
                                }
                            >
                                <MenuItem value="0">
                                    {lang['board9']}
                                </MenuItem>
                                <MenuItem value="1">
                                    {lang['board10']}
                                </MenuItem>
                                <MenuItem value="2">
                                    {lang['board11']}
                                </MenuItem>
                                <MenuItem value="3">
                                    {lang['board12']}
                                </MenuItem>
                                <MenuItem value="4">
                                    {lang['board13']}
                                </MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" className={classNames(classes.dialogInput, classes.dClass)}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(1, ref);
                                }}
                                htmlFor="dialog-class"
                            >
                                {lang['board2']}
                            </InputLabel>
                            <Select
                                value={dClassId}
                                onChange={this.handleDialogVal('dClassId')}
                                input={
                                    <OutlinedInput
                                        labelWidth={classLabelWidth}
                                        name="dClassId"
                                        id="dialog-class"
                                        disabled={(!dialogSort && dAuth == '0') ? true : false}
                                        classes={{
                                            disabled: classes.disabledColor
                                        }}
                                    />
                                }
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                {classList.map((v,i) => {
                                    return (
                                        <MenuItem key={"c_"+i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    </div>
                    {!dialogSort && (
                        <div className={classNames(classes.dialogBox, classes.subText)}>
                            <Typography variant="caption" gutterBottom>
                                {lang['board6']+":" + (dWriter ? dWriter : "-") + " | "+lang['board8']+":" + dRgstDate}
                            </Typography>
                        </div>
                    )}
                    <div className={classes.dialogBox} style={{flex:1}}>
                        <TextField
                            id="dialog-content"
                            label={lang['board14']}
                            multiline
                            rows="8"
                            onChange={this.handleDialogVal('dContent')}
                            value={dContent}
                            className={classNames(classes.dialogInput, classes.dContent, classes.textArea)}
                            variant="outlined"
                            shrink="true"
                            disabled={(!dialogSort && dAuth == '0') ? true : false}
                        />
                    </div>
                    {dialogSort && (<div className={classes.commentSpace}></div>)}
                    {(!dialogSort && dComments.length) ? (
                        <div className={classNames(classes.dialogBox, classes.commentRoot)}>
                            {dComments.map((c,i) => {
                                if(c.auth == 0) {
                                    return (
                                        <div className={classes.commentBox} key={"c_"+i}>
                                            <SubdirectoryArrowRight />
                                            <div className={classes.commentSub}>
                                                <Typography variant="caption" className={classes.commentDates}>{c.rgst_date + " | " +(c.writer ? c.writer : "-")}</Typography>
                                                <div className={classes.commentText}>
                                                    <Typography variant="body1" >{c.text}</Typography>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                } else {
                                    return (
                                        <div className={classes.commentBoxEditable} key={"c_"+i} >
                                            <SubdirectoryArrowRight />
                                            <div className={classes.commentSub}>
                                                <Typography variant="caption" className={classes.commentDates}>{c.rgst_date + " | " +(c.writer ? c.writer : "-")}</Typography>
                                                <div className={classes.commentMy}>
                                                    <TextField
                                                        label=""
                                                        multiline
                                                        rowsMax="4"
                                                        onChange={(e)=>this.handleDialogComment(dId, c, e)}
                                                        value={c.text ? c.text : ""}
                                                        className={classNames(classes.commentTextField, classes.dContent)}
                                                        variant="outlined"
                                                    />
                                                    <div className={classes.commentBtns}>
                                                        <Fab size="small" color="primary" aria-label="save" className={classes.margin}
                                                            onClick={()=>this.saveComment(c)}>
                                                            <Save />
                                                        </Fab>
                                                        <Fab size="small" color="primary" aria-label="remove" className={classes.margin}
                                                            onClick={()=>this.removeComment(c)}>
                                                            <Delete />
                                                        </Fab>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                }
                            })}
                        </div>
                    ) : null}
                </form>
                <div className={classes.commentInsertRoot}>
                    <TextField
                        label=""
                        multiline
                        rowsMax="4"
                        onChange={this.handleDialogVal('dComment')}
                        value={dComment}
                        className={classNames(classes.commentTextField, classes.dContent)}
                        variant="outlined"
                    />
                    <Button variant="contained" className={classes.commentInsertBtn} onClick={()=>this.addComment()}>
                        Enter
                    </Button>
                </div>
            </div>
        );

        return (
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputLeftBox}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(3, ref);
                                }}
                                htmlFor="dialog-type"
                            >
                                {lang['board1']}
                            </InputLabel>
                            <Select
                                value={type}
                                onChange={this.handleFilter('type')}
                                input={
                                    <OutlinedInput
                                        labelWidth={type2LabelWidth}
                                        name="type"
                                        id="type-change"
                                    />
                                }
                            >
                                <MenuItem value="">
                                    <em>All</em>
                                </MenuItem>
                                <MenuItem value="0">
                                    {lang['board9']}
                                </MenuItem>
                                <MenuItem value="1">
                                    {lang['board10']}
                                </MenuItem>
                                <MenuItem value="2">
                                    {lang['board11']}
                                </MenuItem>
                                <MenuItem value="3">
                                    {lang['board12']}
                                </MenuItem>
                                <MenuItem value="4">
                                    {lang['board13']}
                                </MenuItem>
                            </Select>
                        </FormControl>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(4, ref);
                                }}
                                htmlFor="class-change"
                            >
                                {lang['board2']}
                            </InputLabel>
                            <Select
                                value={classId}
                                onChange={this.handleFilter('classId')}
                                input={
                                <OutlinedInput
                                    labelWidth={class2LabelWidth}
                                    name="classId"
                                    id="class-change"
                                />
                                }
                            >
                                <MenuItem value="">
                                    <em>All</em>
                                </MenuItem>
                                {classList.map((v,i) => {
                                    return (
                                        <MenuItem key={"cl_"+i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <InputLabel 
                                ref={ref => {
                                    if(ref) this.setLabelWidth(5, ref);
                                }}
                                htmlFor="searching"
                            >{lang['board3']}</InputLabel>
                            <OutlinedInput
                                id="searching"
                                type="text"
                                labelWidth={searchLabelWidth}
                                value={keyword}
                                onChange={this.handleChangeKeyword}
                                onKeyPress={this.handleKeyPress}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="searching"
                                            onClick={this.handleClickSearching}
                                            onMouseDown={this.handleMouseDownSearching}
                                        >
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </div>
                    <div className={classes.subInputRightBox}>
                        <Fab variant="extended" aria-label="add" className={classes.newBtn}
                            onClick={
                                () => this.openDialog(true)}>
                            <Add className={classes.extendedIcon} />
                            New Post
                        </Fab>
                    </div>
                </div>
                <div className={classes.tableContainer}>
                    <BoardTable
                        keyword={keyword}
                        data={posts}
                        page={page}
                        perPage={perPage}
                        total={total}
                        sort={sort}
                        type={type}
                        classId={classId}
                        classList={classList}
                        handleFilter={this.handleFilter}
                        handleChangeKeyword={this.handleChangeKeyword}
                        reloadPosts={this.reloadPosts}
                        handlePage={this.handlePage}
                        handleSort={this.handleSort}
                        openRow={this.openRow}
                        lang={lang}
                    />
                </div>
                {dialogSort ? (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"New Post"}
                        content={dialogForm}
                        leftFunc={{
                            label: lang['domain8'],
                            func: ()=> {
                                this.addPost();
                            }
                        }}
                        rightFunc={{
                            label: lang['domain9'],
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                ) : (
                    <Dialogs
                        kinds={"alert"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"Post details"}
                        subButton={dialogButton}
                        content={dialogForm}
                        classes={{
                            paper: classes.dialogPaper
                        }}
                    />
                )}
            </div>
        );
    }
}

const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    contentsContainer: {
        width: '100%',
        height: '100%',
    },
    dialogPaper: {
        height: 'calc(100% - 96px)'
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        //flex: 1,
        flexDirection: 'column',
        height: 'calc(100% - 90px)'
    },
    dialogBox: {
        display: 'flex',
        flexWrap: 'wrap',
        //flex: 1
    },
    inputRoot: {
        '&$disabled': {
            color: '#000'
        }
    },
    dialogInput: {
        margin: 8,
        '& .Mui-disabled': {
            color: '#000'
        }
    },
    dTitle: { flex: 1 },
    dType: { width: 140 },
    dClass: { width: 140 },
    dContent: { flex: 1 },
    textArea: {
        '& > div': {
            flex: 1, padding: 0
        },
        '& textarea': {
            height: '100%',
            padding: '18px 14px',
            boxSizing: 'border-box'
        }
    },
    subText: {
        justifyContent: 'flex-end',
        paddingRight: 10
    },
    dialogTopBtn: {
        // position: 'absolute',
        // top: '15px',
        // right: '15px'
    },
    dialogSave: {
        margin: '0 5px'
    },
    dialogRemove: {
        margin: '0 5px'
    },
    commentRoot: {
        flexDirection: 'column'
    },
    commentBox: {
        display: 'flex',
        alignItems: 'center'
    },
    commentSub: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    commentText: {
        display: 'flex',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,.08)',
        border: '1px solid rgba(0,0,0,.23)',
        borderRadius: '4px',
        margin: '0 8px 8px',
        padding: 15,
        alignItems: 'center',
        '& p': {
            whiteSpace: 'pre-line',
            lineHeight: '1rem'
        }
    },
    commentTextField: {
        flex: 1
    },
    commentDates: {
        paddingLeft: 15,
        color: '#888'
    },
    commentBoxEditable: {
        display: 'flex',
        alignItems: 'center'
    },
    commentMy: {
        display: 'flex',
        flex: 1,
        margin: '0 8px 8px',
    },
    commentBtns: {
        display: 'flex',
        alignItems: 'flex-end',
        margin: 8,
        '& button': {
            margin: '0 3px'
        }
    },
    commentInsertRoot: {
        borderTop: '1px solid #bbb',
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: '52px',
        left: 0,
        width: '100%',
        height: 'auto',
        display: 'flex',
        padding: '15px 30px',
    },
    commentInsertBtn: {
        marginLeft: 15,
        width: 94
    },
    commentSpace: {
        height: 90
    },
    disabledColor: {
        color: '#000'
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {
        display: 'flex',
        alignItems: 'center'
    },
    subInputRightBox: {
        display: 'flex',
        alignItems: 'center'
    },
    formControl: {
        width: 120,
        marginRight: 15
    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15
    }
});

export default withStyles(styles)(Board);