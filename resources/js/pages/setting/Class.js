import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, TextField, Button, Input, OutlinedInput, FilledInput,
    InputLabel, MenuItem, FormControl, Select, Divider, InputAdornment,
    List, ListItem, ListItemIcon, ListItemSecondaryAction, ListItemText, IconButton } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { Add, Comment, Delete, Save, HighlightOff } from '@material-ui/icons/';
import { Dialogs } from '../../components/';

class KlassDialogForm extends Component {
    render() {
        const { classes, klass, center,
            dialogOpen, dialogSort, dialogFlag, dialogName, centerLabelWidth,
            dName, dCenter, } = this.props;
        return (
            <div className={classes.contentsContainer}>
                <form className={classes.formRoot} autoComplete="off">
                    <TextField
                        id="dialog-name"
                        label={dialogSort==0 ? "클래스명" : "센터명"}
                        value={dName}
                        className={classes.textField}
                        onChange={this.props.handleDialogVal('dName')}
                        variant="outlined"
                    />
                    {dialogSort==0 && (
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.props.setLabelWidth(1, ref);
                                }}
                                htmlFor="register-class"
                            >
                                센터 선택
                            </InputLabel>
                            <Select
                                value={dCenter}
                                onChange={this.props.handleDialogVal('dCenter')}
                                input={
                                <OutlinedInput
                                    labelWidth={centerLabelWidth}
                                    name="dCenter"
                                    id="input-center"
                                />
                                }
                            >
                                <MenuItem value="">
                                    <em>선택</em>
                                </MenuItem>
                                {center.map((v,i) => {
                                    return (
                                        <MenuItem key={i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                    )}
                </form>
            </div>
        );
    }
}

class KlassList extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.dialogOpen == nextProps.dialogOpen;
    }
    render() {
        const { classes, klass } = this.props;
        return (
            <List className={classes.listRoot}>
                {klass.map((k, i) => {
                    const labelId = `class-list-label-${i}`;
                    const title = "#"+(i+1)+". " +k.name+ (k.over_name ? " / "+k.over_name : "");
                    const vis = k.visible == 0 ? '0' : '1';
                    return (
                        <ListItem key={"k_"+i} role={undefined} button className={classes.listItemRoot}
                            onClick={()=>this.props.openClass(k, false)}>
                            <ListItemText id={labelId} primary={title} />
                            <ListItemSecondaryAction>
                                <ToggleButtonGroup size="small" value={vis} className={classes.toggleGroup} exclusive onChange={this.props.handleToggle(k)}>
                                    <ToggleButton value="0" classes={{
                                        root: classes.toggleBtn,
                                        selected: classes.toggleSelectedBtn
                                    }}>
                                        {"비활성"}
                                    </ToggleButton>,
                                    <ToggleButton value="1" classes={{
                                        root: classes.toggleBtn,
                                        selected: classes.toggleSelectedBtn
                                    }}>
                                        {"활성"}
                                    </ToggleButton>
                                </ToggleButtonGroup>
                                <IconButton edge="end" aria-label="remove" onClick={()=>this.props.removeKlass()}>
                                    <Delete className={classes.delIcon} />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    );
                })}
            </List>
        );
    }
}

@inject(stores => ({
    funcStore: stores.funcStore
}))
class Klass extends Component {
    state = {
        dialogOpen: false,
        dialogSort: '', // class or center
        dialogFlag: true, // add or edit
        dialogName: '',
        dName: '',
        dCenter: '',
        centerLabelWidth: 0,
        klass: [],
        center: [],
        infoKlass: null,
        infoCenter: null,
    }

    constructor(props) {
        super(props);
        this._isMounted = false;
    }

    componentDidMount() {
        this._isMounted = true;
        this._isMounted && (this.loadOverAndKlass());
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this._isMounted;
    }

    loadOverAndKlass = () => {
        this.props.funcStore.conn('get', '/klass/getOverAndKlass', false, true, null, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    klass: res.klass,
                    center: res.over
                });
            } else {
                console.log("반 로딩 실패");
            }
        });
    }

    setLabelWidth = (kind, ref) => {
        const { centerLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != centerLabelWidth) {
            this.setState({ centerLabelWidth: ref.offsetWidth });
        }
    }

    resetDialog = () => {
	    this.setState({
            dialogOpen: false,
            dialogSort: '', // class or center
            dialogFlag: true, // add or edit
            dialogName: '',
            dName: '',
            dCenter: '',
            infoKlass: null,
            infoCenter: null,
	    });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    handleDialogVal = name => event => {
        this.setState({
            [name]: event.target.value
        });
    };

    openClass = (klass, sort) => {
        const a = (!sort && klass != '') ? klass.name : '';
        this.setState({
            infoKlass: klass,
            dialogSort: 0,
            dialogFlag: sort,
            dialogName: sort ? "ADD CLASS" : "EDIT CLASS",
            dName: (!sort && klass != '') ? klass.name : '',
            dCenter: (!sort && klass != '') ? klass.over_id ? klass.over_id : '' : '',
        });
        this.handleOpen(true);
    }

    openCenter = (center, sort) => {
        this.setState({
            infoCenter: center,
            dialogSort: 1,
            dialogFlag: sort,
            dialogName: sort ? "ADD CENTER" : "EDIT CENTER",
            dName: (!sort && center != '') ? center.name : ''
        });
        this.handleOpen(true);
    }

    saveKlass = () => {
        const { klass, dName, dCenter, infoKlass, dialogFlag } = this.state;
        const param = {
            name: dName,
            center: dCenter
        }
        if(dialogFlag) { // add
            this.props.funcStore.conn('post', '/klass/post', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        klass: [res.inserted].concat(klass)
                    });
                } else {
                    console.log("클래스 입력 실패");
                }
                this.handleOpen(false);
            });
        } else { // edit
            this.props.funcStore.conn('post', '/klass/'+infoKlass.id+'/put', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        klass: klass.map(k => k.id == infoKlass.id ? res.edited : k)
                    });
                } else {
                    console.log("클래스 수정 실패");
                }
                this.handleOpen(false);
            });
        }
    }

    saveCenter = () => {
        const { center, dName, infoCenter, dialogFlag } = this.state;
        const param = {
            name: dName,
        }
        if(dialogFlag) { // add
            this.props.funcStore.conn('post', '/over/post', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        center: [res.inserted].concat(center)
                    });
                } else {
                    console.log("센터 입력 실패");
                }
                this.handleOpen(false);
            });
        } else { // edit
            this.props.funcStore.conn('post', '/over/'+infoCenter.id+'/put', false, true, param, (res)=>{
                if(res.result == "true") {
                    this.setState({
                        center: center.map(c => c.id == infoCenter.id ? res.edited : c)
                    });
                } else {
                    console.log("센터 수정 실패");
                }
                this.handleOpen(false);
            });
        }
    }

    removeKlass = () => {
        if(!confirm("클래스를 삭제하시겠습니까?")) return;
        const { infoKlass, klass } = this.state;
        this.props.funcStore.conn('post', '/klass/'+infoKlass.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    klass: klass.filter(k => k.id != infoKlass.id)
                });
            } else {
                console.log("클래스 삭제 실패");
            }
            this.handleOpen(false);
        });
    }

    removeCenter = () => {
        if(!confirm("센터를 삭제하시겠습니까?")) return;
        const { infoCenter, center } = this.state;
        this.props.funcStore.conn('post', '/over/'+infoCenter.id+'/delete', false, true, null, (res)=>{
            if(res.result == "true") {
                this.setState({
                    center: center.filter(c => c.id != infoCenter.id)
                });
            } else {
                console.log("센터 삭제 실패");
            }
            this.handleOpen(false);
        });
    }

    handleToggle = selectedKlass => (e, val) => {
        const { klass } = this.state;
        const param = {
            visible: val
        }
        this.props.funcStore.conn('post', '/klass/'+selectedKlass.id+'/setVisible', false, true, param, (res)=>{
            if(res.result == "true") {
                this.setState({
                    klass: klass.map(k => k.id == selectedKlass.id ? {
                        ...k, visible: val
                    } : k)
                });
            } else {
                console.log("클래스 활성상태 변경 실패");
            }
            this.handleOpen(false);
        });
    }

    render() {
        const { klass, center, dialogName, dialogFlag, dialogOpen, dialogSort,
            dName, dCenter, infoKlass, infoCenter } = this.state;
        const { classes } = this.props;

        const subButton = (
            <div>
                <Fab size="small" aria-label="save"
                    onClick={() => dialogSort == 0 ? this.saveKlass() : this.saveCenter() }>
                    <Save />
                </Fab>
                <Fab size="small" aria-label="remove"
                    onClick={() => dialogSort == 0 ? this.removeKlass() : this.removeCenter()}>
                    <Delete />
                </Fab>
            </div>
        );
        return (
            <div className={classes.masterRoot}>
                <div>
                    <div className={classes.subInputBox}>
                        <div className={classes.subInputLeftBox}></div>
                        <div className={classes.subInputRightBox}>
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={()=>this.openClass(null, true)}
                            >
                                <Add className={classes.extendedIcon} />
                                {"New Class"}
                            </Fab>
                        </div>
                    </div>
                    <div className={classes.tableContainer}>
                        <Typography variant="h5" className={classes.subInputTitle}>
                            CLASS List
                        </Typography>
                        <KlassList classes={classes} {...this.state}
                            openClass={this.openClass}
                            removeKlass={this.removeKlass}
                            handleToggle={this.handleToggle}
                        />
                    </div>
                </div>
                <div className={classes.space60}></div>
                <div>
                    <div className={classes.subInputBox}>
                        <div className={classes.subInputLeftBox}></div>
                        <div className={classes.subInputRightBox}>
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={()=>this.openCenter(null, true)}
                            >
                                <Add className={classes.extendedIcon} />
                                {"New Over Class"}
                            </Fab>
                        </div>
                    </div>
                    <div className={classes.tableContainer}>
                        <Typography variant="h5" className={classes.subInputTitle}>
                            CENTER List
                        </Typography>
                        <List className={classes.listRoot}>
                            {center.map((o, i) => {
                                const labelId = `over-list-label-${i}`;
                                const title = "#" + (i+1) + ". " + o.name;
                                return (
                                    <ListItem key={"o_"+i} role={undefined} button className={classes.listItemRoot}
                                        onClick={()=>this.openCenter(o, false)}>
                                        <ListItemText id={labelId} primary={title} />
                                        <ListItemSecondaryAction>
                                        </ListItemSecondaryAction>
                                    </ListItem>
                                );
                            })}
                        </List>
                    </div>
                </div>
                {dialogFlag ? (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={dialogName}
                        content={
                            <KlassDialogForm
                                {...this.state} classes={classes}
                                handleDialogVal={this.handleDialogVal}
                                openClass={this.openClass}
                                openCenter={this.openCenter}
                                saveKlass={this.saveKlass}
                                saveCenter={this.saveCenter}
                                removeKlass={this.removeKlass}
                                removeCenter={this.removeCenter}
                                setLabelWidth={this.setLabelWidth}
                            />
                        }
                        leftFunc={{
                            label: "추가",
                            func: ()=> {
                                dialogSort == 0 ? this.saveKlass() : this.saveCenter();
                            }
                        }}
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                ) : (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={dialogName}
                        subButton={subButton}
                        content={
                            <KlassDialogForm
                                {...this.state} classes={classes}
                                handleDialogVal={this.handleDialogVal}
                                openClass={this.openClass}
                                openCenter={this.openCenter}
                                saveKlass={this.saveKlass}
                                saveCenter={this.saveCenter}
                                removeKlass={this.removeKlass}
                                removeCenter={this.removeCenter}
                                setLabelWidth={this.setLabelWidth}
                            />
                        }
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                )}
                
            </div>
        );
    }
}

const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    contentsContainer: {
        display: 'flex'
    },
    containerRoot: {
        display: 'flex',
        flexDirection: 'column'
    },
    infoContainer: {
        display: 'flex',
        flexDirection: 'column'
    },
    infoTopBox: {
        display: 'flex'
    },
    textField: {
        flex: 1,
        margin: 8
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    formControl: {
        width: 180,
        margin: 8
    },
    listRoot: {
        width: '100%',
        marginTop: 5,
        '& > li:not(:first-child)': {
            borderTop: '1px solid #fff'
        }
    },
    listItemRoot: {
        backgroundImage: 'linear-gradient(to right, #485680 0%,#865995 65%,#ca5388 100%)',
    },
    saveBtn: {
        position: 'absolute',
        top: 5,
        right: 5
    },
    contentDivider: {
        marginTop: 30,
        marginBottom: 30,
    },
    newText: {
        flex: 1,
    },
    listCol1: {
        flex: 1
    },
    listCol2: {
        flex: 3
    },
    listCol3: {
        width: 120,
        flex: 'none'
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {
        flex: 1,
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
    },
    subInputRightBox: {
        display: 'flex',
        alignItems: 'center'
    },
    subInputTitle: {
        fontFamily: 'JungBold',
        marginRight: 15,
        color: '#fff',
    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15,
        padding: 30,
        borderRadius: 5,
        backgroundColor: '#333',
        color: '#fff'
    },
    delIcon: {
        color: '#fff'
    },
    space60: {
        width: '100%',
        padding: '30px 0',
    },
    dialogTopBtn: {
        position: 'absolute',
        top: '15px',
        right: '15px'
    },
    dialogSave: {
        margin: '0 5px'
    },
    dialogRemove: {
        margin: '0 5px'
    },
    toggleGroup: {
        backgroundColor: 'transparent',
        marginRight: 15
    },
    toggleBtn: {
        color: '#fff',
        backgroundColor: 'transparent',
        borderColor: '#fff'
    },
    toggleSelectedBtn: {
        color: '#232323!important',
        backgroundColor: '#fff!important'
    },
    delIcon: {
        color: '#fff'
    }
});

export default withStyles(styles)(Klass);