import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject } from 'mobx-react';
import { Typography, Button } from '@material-ui/core/';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import { Board, Goal, Klass } from '../';
import { Bread } from '../../components/';

@inject(stores => ({
    mainStore: stores.mainStore
}))
class Setting extends Component {
    state = {
        view: 'board',
        subMenus: this.props.mainStore.user.level > 10 ? [
            {title: "BOARD", value: "board"},
            //{title: "MY INFO", value: "my info"},
            {title: "GOAL", value: "goal"},
            {title: "CLASS", value: "class"},
        ] : [
            {title: "BOARD", value: "board"},
            //{title: "MY INFO", value: "my info"},
        ]
    }
    
    handleView = (e, page) => {
        if(!page || this.state.view == page) return;
        this.setState({ view: page });
    }

    render() {
        const { view, subMenus } = this.state;
        const { mainStore } = this.props;
        let page = null;
        if(view == 'board') {
            page = (<Board />);
        } else if(view == 'goal') {
            page = (<Goal />);
        } else if(view == 'class') {
            page = (<Klass />);
        } else if(view == 'my info') {
            page = null;
        }
        
        return (
            <div>
                <Bread title={"SETTING"} subTitle={view} subMenus={subMenus} handleSubMenu={this.handleView}/>
                {page}
            </div>
        );
    }
}

const styles = theme => ({
    onBtn: {
        
    },
    offBtn: {

    }
});

export default withStyles(styles)(Setting);