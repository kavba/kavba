import React, { Component } from 'react';
import { inject } from 'mobx-react';
import clsx from 'clsx';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Typography, Fab, Button, TextField, FormControl, InputLabel, OutlinedInput, IconButton,
    InputAdornment, Select, MenuItem, Divider } from '@material-ui/core';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Add, Save, Delete, Search, Done } from '@material-ui/icons';

import { Dialogs, UserTable, Errors } from '../../components/';

let selectedClasses = [];
let selectedStudents = [];

@inject(stores => ({
    funcStore: stores.funcStore,
    mainStore: stores.mainStore,
}))
class Teacher extends Component {
    state = {
        users: [],
        keyword: "",
        page: 0,
        perPage: 10,
        total: 0,
        sort: false,
        classList: [],
        classId: '',
        studentList: [],
        onlyClassList: [],
        dialogOpen: false,
        dialogSort: 0, // 0: add
        dName: '',
        dMail: '',
        dPhone: '',
        dPw: '',
        dPwck: '',
        dLevel: "0",
        dClasses: [],
        dId: '',
        dCreated: '',
        dValid: '1',
        classLabelWidth: 0,
        levelLabelWidth: 0,
        class2LabelWidth: 0,
        searchLabelWidth: 0,
        popupFocusing: false,
        popupFocusing0: false,
        errorOpen: false,
        errorUser: null
    }

    componentDidMount() {
        this.loadClasses();
        this.loadUsers();
    }

    loadClasses = () => {
        this.props.funcStore.conn('get', '/common/getUserFilterList', false, true, null, (res)=>{
            this.setState({
                classList: res.classList,
                studentList: res.studentList,
                onlyClassList: res.list
            });
        });
    }

    loadUsers = () => {
        const { keyword, page, perPage, sort, classId } = this.state;
        const param = {
            qry: keyword,
            classId: classId,
            sort: sort,
            page: page,
            perPage: perPage,
        }
        this.props.funcStore.conn('get', '/users/getAll', false, true, param, (res)=>{
            if(res.result == 'true') {
                this.setState({
                    users: res.users,
                    total: res.total,
                });
            } else {
                console.log("사용자 로딩 실패");
            }
        });
    }

    reloadUsers = () => {
        new Promise(resolve => {
            setTimeout(() => {
                resolve();
                this.loadUsers();
            }, 0);
        });
    }

    handleFilter = name => e => {
        this.setState({
            [name]: e.target.value
        });
        this.reloadUsers();
    }

    handleChangeKeyword = e => {
        this.setState({ keyword: e.target.value });
    }

    handlePage = page => {
        let t = page;
        if(!page) t = 0;
        this.setState({
            page: page
        });
        this.reloadUsers();
    }

    openRow = user => {
        this.setState({
            dialogSort: 1,
            dialogOpen: true,
            dName: user.name,
            dMail: user.email,
            dPhone: user.phone,
            dLevel: user.level,
            dId: user.id,
            dCreated: user.created_at,
            dValid: user.is_valid.toString(),
        });
    }

    openChild = user => {
        selectedClasses = user.selectedClasses;
        selectedStudents = user.selectedStudents;
        this.setState({
            dialogSort: 2,
            dialogOpen: true,
            dId: user.id,
            popupFocusing: selectedClasses.length ? true : false,
            popupFocusing0: selectedStudents.length ? true : false,
        });
    }

    openError = user => {
        this.setState({
            errorOpen: true,
            errorUser: user
        });
    }

    handleSort = () => {
        this.setState({ sort: !this.state.sort });
        this.reloadUsers();
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    handleErrorOpen = flag => {
        if(!flag) this.setState({ errorOpen: false, errorUser: null });
        else this.setState({ errorOpen: true });
    }

    setLabelWidth = (kind, ref) => {
        const { classLabelWidth, levelLabelWidth, class2LabelWidth, searchLabelWidth } = this.state;
        if(kind == 1 && ref.offsetWidth != classLabelWidth) {
            this.setState({ classLabelWidth: ref.offsetWidth });
        } else if(kind == 2 && ref.offsetWidth != levelLabelWidth) {
            this.setState({ levelLabelWidth: ref.offsetWidth });
        } else if(kind == 3 && ref.offsetWidth != class2LabelWidth) {
            this.setState({ class2LabelWidth: ref.offsetWidth });
        } else if(kind == 4 && ref.offsetWidth != searchLabelWidth) {
            this.setState({ searchLabelWidth: ref.offsetWidth });
        }
    }

    openDialog = flag => {
        this.setState({
            dialogSort: flag,
            dialogOpen: true,
        });
    }

    resetDialog = () => {
        selectedClasses = [];
        selectedStudents = [];
	    this.setState({
            dName: '',
            dMail: '',
            dPhone: '',
            dPw: '',
            dPwck: '',
            dLevel: "0",
            dClasses: [],
            dId: '',
            dCreated: '',
	    });
    }

    handleOpen = flag => {
	    if(!flag) {
		    this.resetDialog();
	    }
        this.setState({ dialogOpen: flag });
    }

    handleDialogVal = name => event => {
        if(name == "dName" && event.target.value.length > 255) return;
        this.setState({
            [name]: event.target.value
        });
    };

    handleToggle = () => (e, val) => {
        this.setState({
            dValid: val
        });
    }

    addUser = () => {
        const { dName, dMail, dPhone, dLevel, dPw, dValid } = this.state;
        const param = {
            name: dName,
            email: dMail,
            phone: dPhone,
            level: dLevel,
            password: dPw,
            is_valid: dValid,
        }
        this.props.funcStore.conn('post', '/users/post', false, true, param, (res)=>{
            if(res.result == 'true') {
                // alert("등록 완료");
                this.handleOpen(false);
                this.reloadUsers();
            } else {
                console.log("사용자 등록 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    saveUser = () => {
        const { dName, dMail, dPhone, dLevel, dPw, dId, dValid } = this.state;
        const param = {
            name: dName,
            email: dMail,
            phone: dPhone,
            level: dLevel,
            password: dPw,
            is_valid: dValid,
        }
        this.props.funcStore.conn('post', '/user/' +dId+ '/put', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("수정 완료");
                this.handleOpen(false);
                this.reloadUsers();
            } else {
                console.log("사용자 정보 수정 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    removeUser = () => {
        if(!confirm("사용자를 삭제하시겠습니까?")) return;
        const { dId } = this.state;
        this.props.funcStore.conn('post', '/user/' +dId+ '/delete', false, true, null, (res)=>{
            if(res.result == 'true') {
                alert("삭제 완료");
                this.handleOpen(false);
                this.reloadUsers();
            } else {
                console.log("사용자 삭제 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    saveClasses = () => {
        const { dId } = this.state;
        const param = {
            selectedClasses: selectedClasses.map(v => {
                return v.id;
            }),
            selectedStudents: selectedStudents.map(v => {
                return v.id;
            })
        }
        this.props.funcStore.conn('post', '/user/' +dId+ '/setClasses', false, true, param, (res)=>{
            if(res.result == 'true') {
                alert("할당 완료");
                this.reloadUsers();
            } else {
                console.log("할당 실패");
                if(res.error) alert(res.error);
            }
        });
    }

    handleMouseDownSearching = e => {
        e.preventDefault();
    };

    handleClickSearching = e => {
        if(!this.state.keyword) return;
        this.setState({ page:0 });
        this.reloadUsers();
    }

    handleKeyPress = e => {
        if(e.key === 'Enter') {
            this.setState({ page:0 });
            this.reloadUsers();
        }
    }

    render() {
        const { users, keyword, page, perPage, total, sort, classList, classId, studentList, onlyClassList,
            dialogOpen,dialogSort,dName,dMail,dPhone,dPw,dPwck,dLevel,dClasses,dId, dCreated, dValid,
            classLabelWidth, levelLabelWidth, class2LabelWidth, searchLabelWidth, popupFocusing, popupFocusing0,
            errorOpen, errorUser } = this.state;
        const { mainStore, classes } = this.props;

        const dialogButton = dialogSort != 0 ? (
            <div>
                <Fab size="small" variant="round" aria-label="save"
                    onClick={() => dialogSort == 1 ? this.saveUser() : this.saveClasses()}>
                    <Save />
                </Fab>
                {dialogSort == 1 ? (
                    <Fab size="small" variant="round" aria-label="remove"
                        onClick={() => this.removeUser()}>
                        <Delete />
                    </Fab>
                ) : null}
            </div>
        ) : null;
        const dialogForm = (
            <div className={classes.contentsContainer}>
                {dialogSort != 2 ? (
                    <form className={classes.formRoot} autoComplete="off">
                        <div className={classes.dialogBox}>
                            <TextField
                                id="dialog-name"
                                label="이름"
                                className={classNames(classes.dialogInput, classes.dName)}
                                value={dName}
                                onChange={this.handleDialogVal('dName')}
                                variant="outlined"
                                inputProps={{
                                    autoComplete: "off"
                                }}
                            />
                            <TextField
                                id="dialog-e"
                                label="메일"
                                className={classNames(classes.dialogInput, classes.dMail)}
                                value={dMail}
                                onChange={this.handleDialogVal('dMail')}
                                variant="outlined"
                                autoComplete="off"
                                inputProps={{
                                    autoComplete: "off"
                                }}
                                disabled={dialogSort==1 ? true : false}
                            />
                        </div>
                        <div className={classes.dialogBox}>
                            <ToggleButtonGroup size="small" value={dValid} className={classes.toggleGroup} exclusive onChange={this.handleToggle()}>
                                <ToggleButton value="0" classes={{
                                    root: classes.toggleBtn,
                                    selected: classes.toggleSelectedBtn
                                }}>
                                    {"비활성"}
                                </ToggleButton>,
                                <ToggleButton value="1" classes={{
                                    root: classes.toggleBtn,
                                    selected: classes.toggleSelectedBtn
                                }}>
                                    {"활성"}
                                </ToggleButton>
                            </ToggleButtonGroup>
                            <TextField
                                id="dialog-name"
                                label="등록일"
                                className={classNames(classes.dialogInput, classes.dName)}
                                value={dCreated}
                                variant="outlined"
                                inputProps={{
                                    autoComplete: "off"
                                }}
                                disabled={true}
                            />
                        </div>
                        <div className={classes.dialogBox}>
                            <TextField
                                id="dialog-phone"
                                label="휴대폰"
                                type="number"
                                className={classNames(classes.dialogInput, classes.dPhone)}
                                value={dPhone ? dPhone : ""}
                                onChange={this.handleDialogVal('dPhone')}
                                variant="outlined"
                            />
                            <FormControl variant="outlined" className={classNames(classes.dialogInput, classes.dLevel)}>
                                <InputLabel
                                    ref={ref => {
                                        if(ref) this.setLabelWidth(2, ref);
                                    }}
                                    htmlFor="dialog-type"
                                >
                                    권한
                                </InputLabel>
                                <Select
                                    value={dLevel}
                                    onChange={this.handleDialogVal('dLevel')}
                                    input={
                                        <OutlinedInput
                                            labelWidth={levelLabelWidth}
                                            name="dLevel"
                                            id="dialog-level"
                                        />
                                    }
                                >
                                    <MenuItem value="0">
                                        Level 1
                                    </MenuItem>
                                    <MenuItem value="1">
                                        Level 2
                                    </MenuItem>
                                    <MenuItem value="2">
                                        Level 3
                                    </MenuItem>
                                    <MenuItem value="10">
                                        Level 4
                                    </MenuItem>
                                    <MenuItem value="100">
                                        Level 5
                                    </MenuItem>
                                </Select>
                            </FormControl>
                        </div>
                        <Divider />
                        <div className={classes.dialogBox}>
                            <TextField
                                id="dialog-pw"
                                label="비밀번호"
                                type="password"
                                className={classNames(classes.dialogInput, classes.dPw)}
                                value={dPw ? dPw : ""}
                                onChange={this.handleDialogVal('dPw')}
                                variant="outlined"
                                autoComplete="off"
                                inputProps={{
                                    autoComplete: "off"
                                }}
                            />
                            <TextField
                                id="dialog-pwck"
                                label="비밀번호 확인"
                                type="password"
                                className={classNames(classes.dialogInput, classes.dPw)}
                                value={dPwck ? dPwck : ""}
                                onChange={this.handleDialogVal('dPwck')}
                                variant="outlined"
                                autoComplete="off"
                                inputProps={{
                                    autoComplete: "off"
                                }}
                            />
                        </div>
                    </form>
                ) : null}
                {dialogSort == 2 ? (
                    <form className={classes.formRoot} autoComplete="off">
                        <div className={classes.dialogBox}>
                            <Autocomplete
                                multiple
                                options={onlyClassList}
                                getOptionLabel={option => option.name}
                                defaultValue={selectedClasses.map(v => {
                                    const t = onlyClassList.filter(c => c.id === v.id);
                                    return t.length ? t[0] : null;
                                })}
                                filterSelectedOptions
                                loading={onlyClassList.length ? false : true}
                                onChange={(e, v)=> {
                                    selectedClasses = v;
                                    if(!selectedClasses.length) {
                                        this.setState({
                                            popupFocusing: false
                                        });
                                    }
                                }}
                                onOpen={()=>{
                                    this.setState({
                                        popupFocusing: true
                                    });
                                }}
                                onClose={()=>{
                                    this.setState({
                                        popupFocusing: (selectedClasses.length ? true : false)
                                    });
                                }}
                                classes={{
                                    root: classes.autoRoot,
                                    popup: classes.autoPopup
                                }}
                                renderInput={params => {
                                    return (
                                        <TextField
                                            {...params}
                                            id="register-parent"
                                            variant="outlined"
                                            label="반 선택"
                                            InputLabelProps={{ shrink: popupFocusing }}
                                            fullWidth
                                        />
                                    );
                                }}
                                autoSelect={true}
                            />
                        </div>
                        <div className={classes.dialogBox}>
                            <Autocomplete
                                multiple
                                options={studentList}
                                getOptionLabel={option => option.name}
                                defaultValue={selectedStudents.map(v => {
                                    const t = studentList.filter(c => c.id === v.id);
                                    return t.length ? t[0] : null;
                                })}
                                filterSelectedOptions
                                loading={studentList.length ? false : true}
                                onChange={(e, v)=> {
                                    selectedStudents = v;
                                    if(!selectedStudents.length) {
                                        this.setState({
                                            popupFocusing0: false
                                        });
                                    }
                                }}
                                onOpen={()=>{
                                    this.setState({
                                        popupFocusing0: true
                                    });
                                }}
                                onClose={()=>{
                                    this.setState({
                                        popupFocusing0: (selectedStudents.length ? true : false)
                                    });
                                }}
                                classes={{
                                    root: classes.autoRoot,
                                    popup: classes.autoPopup
                                }}
                                renderInput={params => {
                                    return (
                                        <TextField
                                            {...params}
                                            id="register-s"
                                            variant="outlined"
                                            label="개별 학생 선택"
                                            InputLabelProps={{ shrink: popupFocusing0 }}
                                            fullWidth
                                        />
                                    );
                                }}
                            />
                        </div>
                    </form>
                ) : null}
            </div>
        );

        return (
            <div className={classes.masterRoot}>
                <div className={classes.subInputBox}>
                    <div className={classes.subInputLeftBox}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    if(ref) this.setLabelWidth(3, ref);
                                }}
                                htmlFor="class-change"
                            >
                                반
                            </InputLabel>
                            <Select
                                value={classId}
                                onChange={this.handleFilter('classId')}
                                input={
                                <OutlinedInput
                                    labelWidth={class2LabelWidth}
                                    name="classId"
                                    id="class-change"
                                />
                                }
                            >
                                <MenuItem value="">
                                    <em>전체</em>
                                </MenuItem>
                                {classList.map((v,i) => {
                                    return (
                                        <MenuItem key={"cl_"+i} value={v.id}>
                                            {v.name}
                                        </MenuItem>
                                    );
                                })}
                            </Select>
                        </FormControl>
                        <FormControl className={clsx(classes.margin, classes.textField)} variant="outlined">
                            <InputLabel 
                                ref={ref => {
                                    if(ref) this.setLabelWidth(4, ref);
                                }}
                                htmlFor="searching"
                            >검색어 입력</InputLabel>
                            <OutlinedInput
                                id="searching"
                                type="text"
                                labelWidth={searchLabelWidth}
                                value={keyword}
                                onChange={this.handleChangeKeyword}
                                onKeyPress={this.handleKeyPress}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="searching"
                                            onClick={this.handleClickSearching}
                                            onMouseDown={this.handleMouseDownSearching}
                                        >
                                            <Search />
                                        </IconButton>
                                    </InputAdornment>
                                }
                            />
                        </FormControl>
                    </div>
                    <div className={classes.subInputRightBox}>
                        {mainStore.user.level >= 10 ? (
                            <Fab variant="extended" aria-label="add" className={classes.newBtn}
                                onClick={
                                    () => this.openDialog(0)}>
                                <Add className={classes.extendedIcon} />
                                New Teacher
                            </Fab>
                        ) : ''}
                    </div>
                </div>
                <div className={classes.tableContainer}>
                    <UserTable
                        keyword={keyword}
                        user={mainStore.user}
                        data={users}
                        page={page}
                        perPage={perPage}
                        total={total}
                        sort={sort}
                        classId={classId}
                        classList={classList}
                        handleFilter={this.handleFilter}
                        handleChangeKeyword={this.handleChangeKeyword}
                        reloadUsers={this.reloadUsers}
                        handlePage={this.handlePage}
                        handleSort={this.handleSort}
                        openRow={this.openRow}
                        openChild={this.openChild}
                        openError={this.openError}
                    />
                </div>
                {dialogSort == 0 ? (
                    <Dialogs
                        kinds={"content"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={"New User"}
                        content={dialogForm}
                        fullWidth={false}
                        classes={{
                            paper: classes.dialogRoot
                        }}
                        leftFunc={{
                            label: "추가",
                            func: ()=> {
                                this.addUser();
                            }
                        }}
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                ) : (
                    <Dialogs
                        kinds={"alert"}
                        open={dialogOpen}
                        handleOpen={this.handleOpen}
                        title={dialogSort == 1 ? "User details" : "Assign Class or Student"}
                        subButton={mainStore.user.level > 2 ? dialogButton : ''}
                        content={dialogForm}
                        fullWidth={false}
                        classes={{
                            paper: classes.dialogRoot
                        }}
                        rightFunc={{
                            label: "닫기",
                            func: ()=> {
                                this.handleOpen(false);
                            }
                        }}
                    />
                )}
                <Dialogs
                    kinds={"alert"}
                    open={errorOpen}
                    handleOpen={this.handleErrorOpen}
                    title={"ERROR DATA"}
                    content={<Errors user={errorUser} openError={this.openError} />}
                    fullWidth={false}
                    classes={{
                        paper: classes.dialogRoot
                    }}
                    rightFunc={{
                        label: "close",
                        func: ()=> {
                            this.handleErrorOpen(false);
                        }
                    }}
                />
            </div>
        );
    }
}

const styles = theme => ({
    fab: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
    contentsContainer: {
        width: '100%',
    },
    dialogPaper: {
        height: 'calc(100% - 96px)'
    },
    formRoot: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1,
        flexDirection: 'column'
    },
    dialogBox: {
        display: 'flex',
        flexWrap: 'wrap',
        flex: 1
    },
    inputRoot: {
        '&$disabled': {
            color: '#000'
        }
    },
    dialogInput: {
        flex: 1,
        margin: 8,
        '& .Mui-disabled': {
            color: '#000'
        }
    },
    dTitle: { flex: 1 },
    dType: { width: 140 },
    dClass: { width: 140 },
    dContent: { flex: 1 },
    subText: {
        justifyContent: 'flex-end',
        paddingRight: 10
    },
    dialogRoot: {
        minWidth: 600
    },
    dialogTopBtn: {
        position: 'absolute',
        top: '15px',
        right: '15px'
    },
    dialogSave: {
        margin: '0 5px'
    },
    dialogRemove: {
        margin: '0 5px'
    },
    commentRoot: {
        flexDirection: 'column'
    },
    commentBox: {
        display: 'flex',
        alignItems: 'center'
    },
    commentSub: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center'
    },
    commentText: {
        display: 'flex',
        flex: 1,
        backgroundColor: 'rgba(0,0,0,.08)',
        border: '1px solid rgba(0,0,0,.23)',
        borderRadius: '4px',
        margin: '0 8px 8px',
        padding: 15,
        alignItems: 'center',
        '& p': {
            whiteSpace: 'pre-line',
            lineHeight: '1rem'
        }
    },
    commentTextField: {
        flex: 1
    },
    commentDates: {
        paddingLeft: 15,
        color: '#888'
    },
    commentBoxEditable: {
        display: 'flex',
        alignItems: 'center'
    },
    commentMy: {
        display: 'flex',
        flex: 1,
        margin: '0 8px 8px',
    },
    commentBtns: {
        display: 'flex',
        alignItems: 'flex-end',
        margin: 8,
        '& button': {
            margin: '0 3px'
        }
    },
    commentInsertRoot: {
        borderTop: '1px solid #bbb',
        backgroundColor: '#fff',
        position: 'absolute',
        bottom: '52px',
        left: 0,
        width: '100%',
        height: 'auto',
        display: 'flex',
        padding: '15px 30px',
    },
    commentInsertBtn: {
        marginLeft: 15,
        width: 94
    },
    commentSpace: {
        height: 150
    },
    disabledColor: {
        color: '#000'
    },
    autoRoot: {
        flex: 1,
        margin: 8
    },
    autoPopup: {
        zIndex: 9999
    },
    subInputBox: {
        display: 'flex',
        flex: 1,
        justifyContent: 'space-between',
    },
    subInputLeftBox: {
        display: 'flex',
        alignItems: 'center'
    },
    subInputRightBox: {
        display: 'flex',
        alignItems: 'center'
    },
    formControl: {
        width: 120,
        marginRight: 15
    },
    newBtn: {
        backgroundColor: 'transparent',
        color: '#232323',
        '&:hover': {
            backgroundColor: '#444',
            color: '#fff'
        },
        '& span': {
            fontFamily: "JungNormal",
        }
    },
    masterRoot: {
        padding: 30
    },
    tableContainer: {
        marginTop: 15
    },
    toggleGroup: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        marginRight: 8,
        marginLeft: 8
    },
    toggleBtn: {
        color: '#444',
        backgroundColor: '#fff',
        borderColor: 'rgba(0, 0, 0, 0.23)',
        flex: 1,
        height: '56px'
    },
    toggleSelectedBtn: {
        color: '#fff!important',
        backgroundColor: '#444!important'
    },
});

export default withStyles(styles)(Teacher);