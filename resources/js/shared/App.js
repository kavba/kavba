import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';
import { Intro, Main } from '../pages';
// import Main from '../intro/Main';

@inject(stores => ({
    mainStore: stores.mainStore,
    funcStore: stores.funcStore
}))
@observer
class App extends Component {
    checkSession = () => {
        const { mainStore, funcStore } = this.props;
        funcStore.conn('get', '/knock', false, false, null, (res)=>{
            if((res.check && res.user) && res.user.is_valid == '1') {
                mainStore.auth = true;
                mainStore.user = res.user;
            } else {
                mainStore.auth = false;
                mainStore.user = null;
            }
        });
    }
    // <Router>
    //     <div>
    //         <h2>로그인 세션 선택</h2>
    //         <nav className="navbar navbar-expand-lg navbar-light bg-light">
    //         <ul className="navbar-nav mr-auto">
    //             <li><Link to={'/web/intro'} className="nav-link">Intro</Link></li>
    //             <li><Link to={'/web/main'} className="nav-link">Main</Link></li>
    //         </ul>
    //         </nav>
    //         <hr />
    //         <Switch>
    //             <Route path='/web/intro' component={Intro} />
    //             <Route path='/web/main' component={Main} />
    //             <Route path='/web/:pp' render={(props) => {
    //                 if(props.match.params.pp == "intro") {
    //                     return (
    //                         <Intro />
    //                     );
    //                 } else if(props.match.params.pp == "main") {
    //                     return (
    //                         <Main />
    //                     );
    //                 }
    //             }} />
    //         </Switch>
    //     </div>
    // </Router>
    render() {
        const { mainStore } = this.props;
        // 로그인 세션 판별
        // > false : intro
        // > true : main
        return (
            <Fragment>
                <Router>
                    <Switch>
                        <Route path='/web/:pp' render={(props) => {
                            this.checkSession();
                            const end = props.match.params.pp;
                            switch(end) {
                                case "intro" :
                                    if(mainStore.auth) {
                                        return ( <Redirect to="/web/main" /> );
                                    }
                                    return (
                                        <Intro />
                                    );
                                case "main" :
                                    if(!mainStore.auth) {
                                        return (<Redirect to="/web/intro" />);
                                    }
                                    return (
                                        <Main />
                                    );
                                default :
                                    if(mainStore.auth) {
                                        return ( <Redirect to="/web/main" /> );
                                    } else {
                                        return ( <Redirect to="/web/intro" /> );
                                    }
                            }
                        }} />
                        <Route path='/web' render={(props) => {
                            if(mainStore.auth) {
                                return ( <Redirect to="/web/main" /> );
                            } else {
                                return ( <Redirect to="/web/intro" /> );
                            }
                        }} />
                    </Switch>
                </Router>
            </Fragment>
            
        );
    }
}

export default App;