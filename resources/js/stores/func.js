import { observable, action } from 'mobx';

const general_url = window.location.origin;
const api_url = general_url + '/web';
export default class FuncStore {

    @action conn = (type, url, url_sub, async, param, callback) => {
        let t='', u='', set = {};
        if(type.toLowerCase() == 'post') {
            t = 'POST';
            set.headers = {
                'X-CSRF-TOKEN' : document.getElementById('csrf').getAttribute('content'),
                'ACCESS-FROM' : 'web'
            };
        } else {
            t = 'GET';
            set.headers = {
                'ACCESS-FROM' : 'web'
            };
        }
        if(url_sub) {
            u = general_url;
        } else {
            u = api_url;
        }
        set.url = u + url;
        set.type = t;
        set.contentType = "application/x-www-form-urlencoded; charset=UTF-8";
        set.async = async;
        set.data = param;
        set.dataType = 'json';
        set.success = (res) => {
            if(res == "auth_error") { alert("인증 오류"); return; }
            if(callback) callback(res);
        };
        set.error = (err) => {
            console.log('통신 오류');
            console.log(err);
        }
        if(async) $.ajax(set);
        else {
            const res = $.ajax(set);
            if(res.status == 200) {
                return res.responseJSON;
            } else {
                return ;
            }
        };
    }

    // @action conn = (_method, _url, _part, _async, _param, _callback) => {
    //     const GENERAL_URI = _part ? general_url : api_url;
    //     let doing = {
    //         method: _method,
    //         headers: {
    //             'Accept': 'application/json',
    //             'Content-Type': 'application/json',
    //             'X-CSRF-TOKEN' : document.getElementById('csrf').getAttribute('content'),
    //             'ACCESS-FROM' : 'web'
    //         }
    //     }
    //     if(!_param) _param = {};
    //     if(_method.toLowerCase() == 'post') {
    //         doing.body = JSON.stringify(_param);
    //     } else {
    //         let urlParameters = Object.entries(_param).map(e => e.join('=')).join('&');
    //         _url += '?' + urlParameters;
    //     }

    //     if(_async) {
    //         // 비동기 통신
    //         fetch(GENERAL_URI + _url, doing)
    //         .then( res => res.json())
    //         .then( json => {
    //             if(_callback) _callback(json);
    //         })
    //         .catch( err => {
    //             console.log("응답 에러 : ", err);
    //             if(_callback) _callback(err);
    //         });
    //     } else {
    //         // 동기
    //         try {
    //             (async() => {
    //                 const res = await fetch(GENERAL_URI + _url, doing).then( res => res.json());
    //                 if(res) {
    //                     if(_callback) _callback(res);
    //                 }
    //             })();
    //         } catch(err) {
    //             console.log(err);
    //         }
    //     }
    // }

    @action convertToymd = date => {
        let d = null;
        if(date) {
            d = new Date(date);
        } else {
            d = new Date();
        }
        if(!d.getFullYear()) return;
        const yyyy = d.getFullYear().toString();
        const mm = (d.getMonth() + 1).toString();
        const dd = d.getDate().toString();

        return yyyy + '-' + (mm[1] ? mm : '0'+mm[0]) + '-' + (dd[1] ? dd : '0'+dd[0]);
    }

    @action formatDate = d => {
        let date = null;
        if(d) {
            date = new Date(d);
        } else {
            date = new Date();
        }
        date.setMonth(date.getMonth() - 3);
        var prevYear = date.getFullYear();
        var prevMonth = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
        var prevDay = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var prev_date = prevYear + "-" + prevMonth + "-" + prevDay; 
        return prev_date; 
    }

    @action setThisDate = gap => {
        let cd = new Date();
        if(!cd.getFullYear()) return;
        const yyyy = cd.getFullYear().toString();
        const mm = (cd.getMonth() + ((gap ? gap : 0) + 1)).toString();
        return yyyy + '-' + (mm[1] ? mm : '0'+mm[0]) + '-01';
    }

    @action apiCheck = () => {
        const param = {
            ltoId: 1,
            studentId: 5
        }
        this.conn('get', '/getStos/byStudent', false, true, param, (res)=>{
            console.log(res);
        });
    }

    @action desc = (a, b, orderBy) => {
        if (b[orderBy] < a[orderBy]) {
            return -1;
        }
        if (b[orderBy] > a[orderBy]) {
            return 1;
        }
        return 0;
    }
    
    @action stableSort = (array, cmp) => {
        const stabilizedThis = array.map((el, index) => [el, index]);
        stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
        });
        return stabilizedThis.map(el => el[0]);
    }
    
    @action getSorting = (order, orderBy) => {
        return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
    }

    @action calcMonths = birth => {
        if(!birth) return "";
        const tempDate = new Date(birth);
        const end = new Date();
        if(end < tempDate) return "0";
        var monthCount = 0;
        while((tempDate.getMonth()+''+tempDate.getFullYear()) != (end.getMonth()+''+end.getFullYear())) {
            monthCount++;
            tempDate.setMonth(tempDate.getMonth()+1);
        }
        return monthCount+1;
    }

    @action arrMax = (arr, key) => {
        const max = arr.reduce(function(prev, current) {
            return (parseInt(prev[key]) > parseInt(current[key])) ? prev : current
        });
        return max[key];
    }

    @action arrMin = (arr, key) => {
        const min = arr.reduce(function(prev, current) {
            return (prev[key] < current[key]) ? current : prev
        });
        return min[key];
    }

    @action printSvg = (title, svgObject) => {
        if(!title || !svgObject) return null;
        const popUpAndPrint = function()
        {
            const obj = svgObject.outerHTML;
            //const width = parseFloat(svgObject.getAttribute("width"))
            //const height = parseFloat(svgObject.getAttribute("height"))
            let printWindow = window.open('', 'PrintMap',
            'width=800,height=740');
            printWindow.document.write('<html><head><style>svg {height:auto;max-height:740;} text.recharts-label {text-anchor: middle;} .page-title {position:absolute;width:100%;top:0;text-align:center;} .page-title h2 {font-size:1.5rem;}</style></head><body onload="window.print()" style="display:flex;align-items:center;justify-content:flex-start;margin:0;" >'
                +'<div class="page-title"><h2>'+title+'</h2></div>'+ obj +
                '</html>');
            printWindow.document.close();
            printWindow.print();
            printWindow.close();
        };
        setTimeout(popUpAndPrint, 500);
    }

    @action dateToHangul = (date) => {
        const week = ['일', '월', '화', '수', '목', '금', '토'];
        const dd = date ? new Date(date) : new Date();
        const y = dd.getFullYear().toString();
        const m = (dd.getMonth() + 1).toString();
        const d = dd.getDate().toString();
        const t = week[dd.getDay()];
        return y + "년 " + m + "월 " + d + "일 " + t + "요일";
    }

    @action cutString = (original, len) => {
        if(!original) return;
        let str = original;
        let s = 0;
        for (let i=0; i<str.length; i++) {
            s += (str.charCodeAt(i) > 128) ? 2 : 1;
            if (s > len) return str.substring(0,i) + "..";
        } 
        return str;
    }
}
