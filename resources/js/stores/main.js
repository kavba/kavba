import { observable, action } from 'mobx';

export default class MainStore {
    @observable auth = false;
    @observable user = null;
    @observable country = 'ko';
    @observable dataSet = '22';
    @observable dbYears = [];

    @observable lang = {
        ko : {
            login: "로그인",
            join: "회원가입",
            labs: "카바 연구소",
            stay: "로그인상태 유지",
            email: "이메일",
            pw: "비밀번호",
            pwck: "비밀번호 확인",
            name: "이름",
            request: "가입요청",

            daily1: "LTO",
            daily2: "담당자",
            daily3: "등록일",
            daily4: "상태",
            daily5: "촉구 방법",
            daily6: "촉구 메모",
            daily7: "강화스케쥴",
            daily8: "프로그램 메모",
            daily9: "준거도달 방식",
            daily10: "도달기준 횟수",
            daily11: "타겟",
            daily12: "최초지시일자",
            daily13: "강화 계획",
            daily14: "메모",

            domain1: "총 완료 LTO",
            domain2: "등록",
            domain3: "개월",
            domain4: "반",
            domain5: "학생",
            domain6: "도메인 선택",
            domain7: "담당자 변경",
            domain8: "추가",
            domain9: "닫기",

            etc1: "선택",

            stoitem1: "준거도달 기준",
            stoitem2: "도달 기준",
            stoitem3: "도달 여부",
            stoitem4: "타겟",
            stoitem5: "날짜",
            stoitem6: "회기 당 시도수",
            stoitem7: "지시자",
            stoitem8: "최초 날짜",
            stoitem9: "현재 연장날짜",
            stoitem10: "현재 지시자",
            stoitem11: "카운트(연장 카운트)",
            stoitem12: "상태",
            stoitem13: "도달",
            stoitem14: "변경",
            stoitem15: "진행중",
            stoitem16: "종료",
            stoitem17: "중지",
            stoitem18: "완료",
            stoitem19: "취소",
            stoitem20: "준거도달 타입",
            stoitem21: "일반",
            stoitem22: "누적",

            sto1: '준거도달 설정',
            sto2: '도달 기준 정반응 비율',
            sto3: '도달 누적 기준',
            sto4: '도달 기준 총 횟수',
            sto5: '설정',
            sto6: '입력',
            sto7: '촉구',
            sto8: '촉구 설정 선택',
            sto9: '촉구 관련 메모',
            sto10: '강화 스케쥴 설정',
            sto11: '기타',
            sto12: '프로그램 관련 메모',
            sto13: '강화 관련 메모',

            point1: '시도 날짜',
            point2: '시도한 지시자',
            point3: '시도',
            point4: '삭제',
            point5: '카운트 상세 구분',
            point6: '시도 지시자 변경',

            graph1: '정반응',
            graph2: '촉구',
            graph3: '기준',
            graph4: '그래프 종류',
            graph5: '준거도달 완료목록',
            graph6: '기준',
            graph8: '지시자 선택',
            graph9: '부터',
            graph10: '까지',
            graph11: '시도수',
            graph12: '디시전',
            graph13: '학생 선택',
            graph14: '날짜 선택 필수',

            reach1: '반',
            reach2: '학생',
            reach3: '도메인',
            reach4: '순번',
            reach5: '상태',
            reach6: '구분',
            reach7: '담당자',
            reach8: '도달일',
            reach9: '센터',

            board1: '타입',
            board2: '반',
            board3: '검색어 입력',
            board4: '순번',
            board5: '제목',
            board6: '작성자',
            board7: '관련 반',
            board8: '작성일',
            board9: '일반',
            board10: '공지',
            board11: '학생',
            board12: '시스템',
            board13: '기타',
            board14: '내용',
        },
        en : {
            login: "LOG IN",
            join: "JOIN",
            labs: "LABORATORY",
            stay: "STAY SIGNED IN",
            email: "EMAIL",
            pw: "PASSWORD",
            pwck: "CONFIRM PW",
            name: "NAME",
            request: "REQUEST TO JOIN",

            daily1: "Long term objective",
            daily2: "Main instructor",
            daily3: "Date of registered",
            daily4: "Status",
            daily5: "Prompting strategy",
            daily6: "Prompting notes",
            daily7: "Schedule of reinforcement",
            daily8: "Program notes",
            daily9: "Type of graph(count, percentage, accumulation)",
            daily10: "Consecutive number of correct response",
            daily11: "Target",
            daily12: "First date of instruction",
            daily13: "Reinforcement plan",
            daily14: "Note",

            domain1: "Total number of completed LTO",
            domain2: "Teacher in charge(program)",
            domain3: "Month",
            domain4: "Class",
            domain5: "Student",
            domain6: "Choose domain",
            domain7: "Change Teacher in charge(program)",
            domain8: "Add",
            domain9: "Close",

            etc1: "Select",

            stoitem1: "Standard of Criterion for Mastery",
            stoitem2: "Reaching criteria",
            stoitem3: "Reaching status(Reached or not)",
            stoitem4: "Target",
            stoitem5: "Date",
            stoitem6: "attempts per trial",
            stoitem7: "Instructor",
            stoitem8: "First date",
            stoitem9: "Current date of extension",
            stoitem10: "Current instructor",
            stoitem11: "Count(extension count)",
            stoitem12: "Status",
            stoitem13: "Completed",
            stoitem14: "Change",
            stoitem15: "In Progress",
            stoitem16: "Finished",
            stoitem17: "Pause",
            stoitem18: "Criteria met",
            stoitem19: "Cancel",
            stoitem20: "Type of Reaching criteria",
            stoitem21: "Normal",
            stoitem22: "Stack",

            sto1: 'Setting for Criterion met',
            sto2: 'Percentage of Correct Response',
            sto3: 'Consecutive Number of correct response',
            sto4: 'Total set needed to reach criterion for mastery',
            sto5: 'Setting',
            sto6: 'Input',
            sto7: 'Prompting',
            sto8: 'Choose prompting strategy',
            sto9: 'Prompting notes',
            sto10: 'Choose schedule of reinforcement',
            sto11: 'etc',
            sto12: 'Program notes',
            sto13: 'Reinforcement notes',

            point1: 'Date of trial',
            point2: 'Instructor ',
            point3: 'Trial',
            point4: 'Delete',
            point5: 'Detail',
            point6: 'Change instructor',

            graph1: 'Correct response',
            graph2: 'Prompting',
            graph3: 'Type of Y-axis',
            graph4: 'types of graph',
            graph5: 'List of completed objectives',
            graph6: 'Choose',
            graph8: 'Choose instructor',
            graph9: 'From',
            graph10: 'To',
            graph11: 'Attempts',
            graph12: 'Decisions',
            graph13: 'Choose student',
            graph14: 'Selecting Date is Required',

            reach1: 'Class',
            reach2: 'Student',
            reach3: 'Domain',
            reach4: 'Order',
            reach5: 'Status',
            reach6: 'Type of Graph',
            reach7: 'Main instructor',
            reach8: 'Date of Completion',
            reach9: 'Center',

            board1: 'Type',
            board2: 'Class',
            board3: 'Search',
            board4: 'Order',
            board5: 'Title',
            board6: 'Author',
            board7: 'Related Class',
            board8: 'Reporting date',
            board9: 'Normal',
            board10: 'Notice',
            board11: 'Student',
            board12: 'System',
            board13: 'etc',
            board14: 'Content',
        },
    };

    @action getLang = con => {
        const lang = con ? this.lang[con] : this.lang[this.country];
        if(lang) return lang;
        else return this.lang[this.country];
    }

    @action setCountry = con => {
        if(con) {
            this.country = con;
        } else {
            const type = navigator.appName;
            let lang = 'en';
            if (type == 'Netscape') {
                lang = navigator.language;
            } else {
                lang = navigator.userLanguage;
            }
            const country_code = lang.substr(0, 2);
            this.country = country_code;
        }
    }

    @action setDataSet = _d => {
        this.dataSet = _d;
        return _d;
    }

    @action setDbYears = _d => {
        this.dbYears = _d;
    }
}

