<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" id="csrf" content="{{ csrf_token() }}">
        
        <title>시도이력 - 학생</title>

        <style type='text/css'>
            table { border-collapse:collapse; }
            table th, table td { border:1px solid #666; padding:4px; text-align:center;}
            table th { background:#dedede; padding:8px 4px;}
            .title { font-size:24px;margin:15px; }
        </style>
    </head>
    <body>
        <div class='title'>{{ $name }} {{ $date }} : 지시자별 시도 기록</div>
        <table>
            <thead>
                <tr>
                    <th style='width:110px;'>포인트 최초날짜</th>
                    <th style='width:110px;'>포인트 연장날짜</th>
                    <th style='width:80px;'>시도 지시자</th>
                    <th>경로</th>
                    <th style='width:50px;'>시도수</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($sticks as $s)
                <tr>
                    <td>{{ $s->rgst_date }}</td>
                    <td>{{ $s->postponed }}</td>
                    <td>{{ $s->stick_user_name }}</td>
                    <td style='text-align:left;'>{{ $s->domain }} >> {{ $s->lto }}</br> >> {{ $s->target }}</td>
                    <td>{{ $s->cnt }}</td>
                </tr>
            @endforeach
                <tr>
                    <td colspan='4'>총 시도 합</td>
                    <td>{{ $total }}</td>
                </tr>
            </tbody>
        <tbody>
    </body>
</html>