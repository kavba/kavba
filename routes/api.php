<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test',
    'A1AuthController@test'
);

Route::post('/test2',
    'W1TeacherController@test2'
);

// 웹용 회원가입
Route::post('/join', 'W1AuthController@join');
// 웹용 로그인
Route::post('/login', 'W1AuthController@login');
// 웹용 로그아웃
Route::post('/logout', 'W1AuthController@logout');
// 웹 세션 체크
Route::get('/knock', 'W1AuthController@knock');

// lto-sto
Route::get('/ltos', 'GoalController@getLtos');
Route::get('/stos', 'GoalController@getStos');