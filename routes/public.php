<?php

//Auth::routes();

Route::get('/', function () {
    return redirect('/web');
});

// 웹용 회원가입
Route::post('/web_join', 'W1AuthController@join');
// 웹용 로그인
Route::post('/web_login', 'W1AuthController@login');
// 웹용 로그아웃
Route::post('/web_logout', 'W1AuthController@logout');
// 웹 세션 체크
Route::get('/web_knock', 'W1AuthController@knock');

Route::get('/search/StudentSticks/{date}/{userId}', 'PublicController@getSticksForUserCommon');
Route::get('/search/userSticks/{date}/{studentId}', 'PublicController@getSticksForStudentCommon');