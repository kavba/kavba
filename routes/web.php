<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');

// 웹용 회원가입
Route::post('/join', 'W1AuthController@join');
// 웹용 로그인
Route::post('/login', 'W1AuthController@login');
// 웹용 로그아웃
Route::post('/logout', 'W1AuthController@logout');
// 웹 세션 체크
Route::get('/knock', 'W1AuthController@knock');
// 사용자 데이터셋 조회
Route::post('/dataSet', 'W1AuthController@setDataSet');

Route::get('/getDbYears', 'W1AuthController@getDbYears');

// LTO
Route::get('/ltos', 'GoalController@getLtos');
Route::get('/lto/{ltoId?}', 'GoalController@getLto');
Route::post('/lto/post', 'GoalController@createLto');
Route::post('/lto/put', 'GoalController@editLto');
Route::post('/lto/delete', 'GoalController@removeLto');
// STO
Route::get('/stos/get', 'GoalController@getStos');
Route::get('/getStos/byStudent', 'GoalController@getStosByStudent');
Route::get('/sto/{stoId?}', 'GoalController@getSto');
Route::post('/sto/post', 'GoalController@createSto');
Route::post('/sto/put', 'GoalController@editSto');
Route::post('/sto/delete', 'GoalController@removeSto');
// toggling LTO/STO
Route::post('/lto/toggle', 'GoalController@toggleLto');
Route::post('/sto/toggle', 'GoalController@toggleSto');

// Klass
Route::get('/klass/getAll', 'KlassController@getKlass');
Route::post('/klass/post', 'KlassController@createKlass');
Route::post('/klass/{id?}/put', 'KlassController@editKlass');
Route::post('/klass/{id?}/delete', 'KlassController@removeKlass');
Route::post('/klass/{id?}/setVisible', 'KlassController@toggleVisible');

// Over Classes
Route::get('/over/getAll', 'KlassController@getOverClass');
Route::post('/over/post', 'KlassController@createOverClass');
Route::post('/over/{id?}/put', 'KlassController@editOverClass');
Route::post('/over/{id?}/delete', 'KlassController@removeOverClass');
Route::get('/klass/getOverAndKlass', 'KlassController@getClasses');

// Students
// 해당 학생 조회
Route::get('/students/{userId?}/get', 'StudentController@getStudents');
Route::get('/students/{classId?}/class', 'StudentController@getStudentsByClass');
Route::get('/students/getAll', 'StudentController@getAllByAuth');
Route::get('/student/{id?}', 'StudentController@getInfo');
Route::post('/student/post', 'StudentController@createStudent');
Route::post('/student/{id?}/put', 'StudentController@editStudent');
Route::post('/student/{id?}/delete', 'StudentController@removeStudent');
Route::get('/student/{id?}/getPyramid', 'StudentController@getPyramid');
Route::post('/student/setPyramid', 'StudentController@setPyramid');
Route::get('/student/{id?}/getCircle', 'StudentController@getCircle');
Route::post('/student/setCircle', 'StudentController@setCircle');
Route::get('/student/{id?}/getMemo', 'StudentController@getMemo');
Route::post('/student/setMemo', 'StudentController@setMemo');
Route::get('/student/{id?}/getProgramsReach', 'StudentController@getAllDomainReaches');

// User Classes
Route::post('/user/setClass', 'UserController@setClass');
Route::post('/student/setClass', 'StudentController@setClass');
Route::get('/user/getClasses', 'UserController@getClasses');
Route::get('/user/getAllClasses', 'UserController@getAllClasses');

// Activation
Route::post('/user/{studentId?}/active', 'UserController@activeStudent');
Route::post('/user/{studentId?}/inactive', 'UserController@inactiveStudent');

// common
Route::get('/common/getFilterList', 'CommonController@getFilters');
Route::get('/common/getUserFilterList', 'CommonController@getUserFilters');
Route::get('/common/getClassesUsers', 'CommonController@getClassesUsers');
Route::get('/common/getGraphSubList', 'CommonController@getStudentsUsers');
Route::get('/common/{classId?}/checkClassAuth', 'CommonController@getAuthOnClass');

/*
// 1003 - 기존 용어 및 개념 바껴서 백업
// Programs
Route::get('/programs/{date?}', 'ProgramController@getPrograms');
Route::post('/program/post', 'ProgramController@createProgram');
Route::get('/checkLtoPrograms', 'ProgramController@checkLtoPrograms');
Route::post('/program/put', 'ProgramController@editProgram');
Route::post('/program/delete', 'ProgramController@removeProgram');

Route::get('/program/{id}/count', 'ProgramController@getProgramCount');
// 완료 상태 전환
Route::post('/program/{id}/finish', 'ProgramController@setFinish');
Route::post('/program/{id}/unfinish', 'ProgramController@setUnfinish');
*/

// Programs
Route::post('/program/post', 'ProgramController@createProgram');
Route::post('/program/{id?}/put', 'ProgramController@editProgram');
Route::post('/program/{id?}/delete', 'ProgramController@removeProgram');
Route::get('/programs', 'ProgramController@getPrograms');
Route::get('/program/{id?}/get', 'ProgramController@getProgram'); // 0, 1, 2 conditions
Route::get('/program/studentLto', 'ProgramController@ducheckProgramLto');
Route::get('/program/test123', 'ProgramController@sample');

// STO_items
Route::post('/stoItem/post', 'GoalController@createStoItem');
Route::post('/stoItem/{id?}/put', 'GoalController@editStoItem');
Route::post('/stoItem/{id?}/duplicate', 'GoalController@copyStoItem');
Route::post('/stoItem/{id?}/delete', 'GoalController@removeStoItem');
Route::get('/stoItem/{id?}/get', 'GoalController@getStoItem');
Route::get('/stoItems/{programId?}/get', 'GoalController@getStoItems');
Route::post('/stoItem/{id?}/status', 'GoalController@setStatus');
Route::post('/stoItem/switchOrder', 'GoalController@switchOrder');
Route::get('/stoItem/{id?}/getDecision', 'GoalController@getDecision');
Route::get('/stoItem/getReached', 'GoalController@getReached');
Route::get('/stoItem/{id?}/completed', 'GoalController@getStoStatus');
Route::post('/stoItem/{id?}/calcDecisions', 'GoalController@setDecisionsAll');
Route::get('/stoItem/{id?}/getLastDecision', 'GoalController@checkLastDecision');

// STO_groups
Route::get('/stoGroup/{programId?}/get', 'GoalController@getStoGroupsByProgram');
Route::post('/stoGroup/{id?}/status', 'GoalController@setStoGroupStatus');
Route::post('/stoGroup/post', 'GoalController@createStoGroup');
Route::post('/stoGroup/{id?}/put', 'GoalController@editStoGroup');
Route::post('/stoGroup/{id?}/delete', 'GoalController@removeStoGroup');
Route::post('/stoGroup/{id?}/duplicate', 'GoalController@copyStoGroup');
Route::post('/stoGroup/switchOrder', 'GoalController@switchGroupOrder');

// Points
Route::post('/point/post', 'PointController@createPoint');
Route::post('/point/{id?}/put', 'PointController@editPoint');
Route::post('/point/{id?}/delete', 'PointController@removePoint');
Route::get('/point/{id?}/get', 'PointController@getPoint');
Route::get('/point/{id?}/getAll', 'PointController@getPointAndSticks');
Route::get('/points/{stoItemId?}/get', 'PointController@getPoints');
Route::get('/points/{stoItemId?}/getAll', 'PointController@getPointsAndSticks');
Route::get('/points/{stoItemId?}/getAllPoints', 'PointController@getPointsAndSticksList');
//Route::get('/points/{stoItemId?}/getAllChild', 'PointController@getPointsAndSticksTree');
Route::get('/points/{stoItemId?}/getAllBySto', 'PointController@getPointsAndSticksSto');
Route::get('/points/{groupId?}/getAllByStoGroup', 'PointController@getPointsAndSticksGroup');
Route::get('/points/{programId?}/getAllByProgram', 'PointController@getPointsAndSticksProgram');
Route::get('/points/getAllByTarget', 'PointController@getPointsByTarget');
Route::get('/points/getStudentLtoPoints', 'PointController@getStudentLtoPoints');
Route::post('/point/{id?}/status', 'PointController@setStatus');
Route::post('/point/{id?}/reach', 'PointController@setReach');
Route::post('/point/{id?}/postpone', 'PointController@resetDate');
Route::post('/point/{id?}/restart', 'PointController@duplicate');


// Sticks
Route::get('/sticks/{programId?}', 'StickController@getSticks');
Route::post('/stick/{pointId?}/check', 'StickController@checkStick');
Route::post('/stick/{id?}/put', 'StickController@editStick');
Route::post('/stick/{id?}/insert', 'StickController@insertStick');
Route::post('/stick/{id?}/delete', 'StickController@deleteStick');
// reset all stick for especial program
Route::post('/stick/{pointId?}/reset', 'StickController@resetAllSticks');
// remove last stick for program
Route::post('/stick/{pointId?}/deleteLast', 'StickController@removeLastStick');
Route::post('/stick/deleteHistory', 'StickController@removeHistoryStick');
Route::post('/stick/resetHistoryUser', 'StickController@setHistoryStickUser');

// Sticks for Graph
Route::get('/sticks/date/getRelatedUsersOnSticks', 'StickController@getAllUsersOnDate');
Route::get('/sticks/user/{userId?}', 'StickController@getSticksForUser');
Route::get('/sticks/student/{studentId?}', 'StickController@getSticksForStudent');

// Errors
Route::get('/errors/{userId?}', 'ErrorController@getErrors');
Route::post('/error/{userId?}/post', 'ErrorController@createError');
Route::post('/error/{id?}/put', 'ErrorController@editError');
Route::post('/error/{id?}/delete', 'ErrorController@removeError');

// Board
Route::get('/posts/getAll', 'BoardController@getAll');
Route::post('/board/post', 'BoardController@createPost');
Route::post('/board/{postId?}/put', 'BoardController@editPost');
Route::post('/board/{postId?}/delete', 'BoardController@deletePost');
Route::post('/comment/post', 'BoardController@createComment');
Route::post('/comment/{commentId?}/put', 'BoardController@editComment');
Route::post('/comment/{commentId?}/delete', 'BoardController@deleteComment');

// Teacher(User)
Route::get('/users/getAll', 'UserController@getAll');
Route::get('/users/{stoItemId?}/getStoUsers', 'UserController@getByStoItem');
Route::get('/user/{userId?}/getInfo', 'UserController@getInfo');
Route::post('/user/post', 'UserController@createUser');
Route::post('/user/{userId?}/put', 'UserController@editUser');
Route::post('/user/{userId?}/delete', 'UserController@deleteUser');
Route::post('/user/{userId?}/setClasses', 'UserController@setClassesAndStudents');
Route::get('/user/{userId?}/classUsers', 'UserController@getMyClassTeachers');
Route::get('/user/{studentId?}/classUsersByStudent', 'UserController@getStudentClassTeachers');
Route::get('/user/{userId?}/getUsersClasses', 'UserController@getUsersClasses');

// Analysis Data
Route::get('/analysis/getGraphList', 'GraphController@getGraphList');
Route::get('/analysis/getData', 'GraphController@getData');

// 웹 페이지 라우팅
Route::get('/{arg?}/{trg?}', function () {
    return view('index');
});
Route::get('/{arg?}', function () {
    return view('index');
});
